(ns cayenne.works.query-filter-test
  (:require [cayenne.api-fixture :refer [api-fixture setup-api setup-test-works
                                         setup-test-journals setup-test-members api-get]]
            [clojure.set :refer [subset?]]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(deftest ^:integration querying-works

  (testing "works endpoint returns result for query"
    (doseq [q-filter ["query=psychology"
                      "query=behavioral+psychology+adults"
                      "query=215573+2019+Mark+Jenkinson"
                      "query=2019+Steven+Schmit"
                      "query.bibliographic=psychology"
                      "query.bibliographic=behavioral+psychology+adults"
                      "query.bibliographic=215573"
                      "query.bibliographic=2019"
                      "query.bibliographic=Mark+Jenkinson"
                      "query.bibliographic=Wellcome+Trust"
                      "query.bibliographic=Integrative+imaging"
                      "query.bibliographic=subtitletag"
                      "query.bibliographic=Neisseria"
                      "query.bibliographic=Ca+mml:mmultiscripts"
                      "query.title=stress"
                      "query.title=the+stress"
                      "query.title=subtitletag"
                      "query.title=Neisseria"
                      "query.title=Ca+mml:mmultiscripts"
                      "query.funder-name=wellcome"
                      "query.funder-name=Another+funder"
                      "query.container-title=stress"
                      "query.container-title=the+stress"
                      "query.author=richard"
                      "query.author=richard+li"
                      "query.editor=kimberely+fletcher"
                      "query.editor=kimberely"
                      "query.chair=john"
                      "query.chair=john+berns"
                      "query.translator=john"
                      "query.translator=john+savage"
                      "query.contributor=richard"
                      "query.contributor=richard+li"
                      "query.contributor=Mark+Jenkinson"
                      "query.contributor=Christian+Beckmann"
                      "query.affiliation=university"
                      "query.affiliation=oxford"
                      "query.affiliation=Radboud+Universiteit+Nijmegen"
                      "query.affiliation=university+of"
                      "query.description=biological+interactions"
                      "query.description=structure+mapping"]]
      (let [items (->> q-filter (str "/v1/works?") api-get :items)]
        (is (seq items))))))

(deftest ^:integration querying-works-of-member
  (testing "works related endpoints agree on work counts"
    (doseq [[wq mq] [["" ""]
                     [",type:journal-article" "?filter=type:journal-article"]
                     ["&query=memory" "?query=memory"]
                     [",type:journal-article&query=memory" "?filter=type:journal-article&query=memory"]]]
      (let [work-count (->> wq (str "/v1/works?filter=member:78") api-get :total-results)
            member-work-count (->> mq (str "/v1/members/78/works") api-get :total-results)]
        (is (= work-count member-work-count))))))

(deftest ^:integration querying-works-of-journal
  (testing "works related endpoints agree on work counts"
    (doseq [[wq mq] [["" ""]
                     [",type:journal-article" "?filter=type:journal-article"]
                     ["&query=memory" "?query=memory"]
                     [",type:journal-article&query=memory" "?filter=type:journal-article&query=memory"]]]
      (let [work-count (->> wq (str "/v1/works?filter=issn:0306-4530") api-get :total-results)
            member-work-count (->> mq (str "/v1/journals/0306-4530/works") api-get :total-results)]
        (is (= work-count member-work-count))))))

(deftest ^:integration querying-works-of-prefix
  (testing "works related endpoints agree on work counts"
    (doseq [[wq mq] [["" ""]
                     [",type:journal-article" "?filter=type:journal-article"]
                     ["&query=memory" "?query=memory"]
                     [",type:journal-article&query=memory" "?filter=type:journal-article&query=memory"]]]
      (let [work-count (->> wq (str "/v1/works?filter=prefix:10.1016") api-get :total-results)
            member-work-count (->> mq (str "/v1/prefixes/10.1016/works") api-get :total-results)]
        (is (= work-count member-work-count))))))

(deftest ^:integration querying-works-of-type
  (testing "works related endpoints agree on work counts"
    (doseq [[wq mq] [["" ""]
                     [",has-references:1" "?filter=has-references:1"]
                     ["&query=memory" "?query=memory"]
                     [",has-references:1&query=memory" "?filter=has-references:1&query=memory"]]]
      (let [work-count (->> wq (str "/v1/works?filter=type:journal-article") api-get :total-results)
            member-work-count (->> mq (str "/v1/types/journal-article/works") api-get :total-results)]
        (is (= work-count member-work-count))))))

(deftest ^:integration filter-and-query
  (testing "queries and filters are AND-ed"
    (let [doi-set (fn [query] (->> query (str "/v1/works?rows=500&") api-get :items (map :DOI) set))]
      (doseq [[wide-q narrow-q] [["filter=member:78" "filter=member:78,type:journal-article"]
                                 ["query=the" "query=the&filter=member:78"]
                                 ["filter=member:78" "query=the&filter=member:78"]
                                 ["query.author=li" "query.author=li&filter=member:78"]
                                 ["filter=member:78" "query.author=li&filter=member:78"]
                                 ["query=the" "query=the&query.author=richard"]
                                 ["query.author=richard" "query=the&query.author=richard"]
                                 ["query.bibliographic=the" "query.author=richard&query.bibliographic=the"]]]
        (is (subset? (doi-set narrow-q) (doi-set wide-q)))))))

(use-fixtures
  :once
  (api-fixture setup-api setup-test-journals setup-test-members setup-test-works))
