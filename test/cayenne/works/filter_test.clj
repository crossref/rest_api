(ns cayenne.works.filter-test
  (:require [cayenne.api-fixture :refer [api-fixture setup-api setup-test-works setup-test-members
                                         setup-test-journals api-get]]
            [clojure.set :refer [subset?]]
            [clojure.string :as str]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(defn filter-work-data [filter-name filter-value & {:keys [field] :or {field identity}}]
  (->> (str "/v1/works?rows=500&filter=" filter-name ":" filter-value)
       api-get
       :items
       (map field)))

(defn filter-grant-data [filter-name filter-value & {:keys [field] :or {field identity}}]
  (->> (str "/v1/types/grant/works?rows=500&filter=" filter-name ":" filter-value)
       api-get
       :items
       (map field)))

(defn item-contributors [item]
  (concat
    (:author item)
    (:editor item)
    (:chair item)
    (:translator item)
    (:contributor item)
    (->> item :project (mapcat :investigator))
    (->> item :project (mapcat :lead-investigator))
    (->> item :project (mapcat :co-lead-investigator))))

(defn item-funders [item]
  (concat
    (:funder item)
    (->> item :project (mapcat :funding) (map :funder))))

(defn item-awards [item]
  (if (:award item)
    [(:award item)]
    (->> item :funder (mapcat :award))))

(defn item-award-amount [item]
  (->> item
       :project
       (#(concat % (mapcat :funding %)))
       (map :award-amount)
       (map :amount)))

(defn funder-doi [funder]
  (if (:DOI funder)
    (:DOI funder)
    (->> funder :id (filter #(= "DOI" (:id-type %))) first :id)))

(defn funder-doi-asserted-by [funder]
  (if (:doi-asserted-by funder)
    (:doi-asserted-by funder)
    (->> funder :id (filter #(= "DOI" (:id-type %))) first :asserted-by)))

(defn get-funder-ror-ids
  "Extract ROR ids from an item's funder, if there is one.
   Checks if :id-type == 'ROR'.
   Returns list of strings or ()."
  [item]
  (->>
   item
   :funder
   (mapcat :id)
   (filter #(= "ROR" (:id-type %)))
   (map :id)))

(defn get-grant-ror-ids
  "Extract ROR ids from an item's project:funding entries, if there are any.
   Checks if :id-type == 'ROR'.
   Returns list of strings or ()."
  [item]
  (->>
   item
   :project
   (mapcat :funding)
   (map :funder)
   (mapcat :id)
   (filter #(= "ROR" (:id-type %)))
   (map :id)))

(defn item-ror-ids [item] 
  (->> (item-contributors item) 
       (mapcat :affiliation) 
       (concat (:institution item)) 
       (mapcat :id) 
       (filter #(= "ROR" (:id-type %))) 
       (map :id) 
       (concat (get-funder-ror-ids item))
       (concat (get-grant-ror-ids item))))

(deftest ^:integration filtering-works

  (testing "date filters correctly filter works"
    (doseq [[field f-str min-date max-date] [[:issued "issued-date:2016-11-13" 20161113 20161113]
                                             [:issued "issued-date:2017-02" 20170201 20170228]
                                             [:issued "issued-date:2016" 20160101 20161231]
                                             [:published "pub-date:2016-10-20" 20161020 20161020]
                                             [:published "pub-date:2016-10" 20161001 20161031]
                                             [:published "pub-date:2016" 20160101 20161231]
                                             [:published-online "online-pub-date:2016-10-20" 20161020 20161020]
                                             [:published-online "online-pub-date:2016-10" 20161001 20161031]
                                             [:published-online "online-pub-date:2016" 20160101 20161231]
                                             [:published-print "print-pub-date:2016-10-20" 20161020 20161020]
                                             [:published-print "print-pub-date:2016-10" 20161001 20161031]
                                             [:published-print "print-pub-date:2016" 20160101 20161231]
                                             [:created "created-date:2016-10-07" 20161007 20161007]
                                             [:created "created-date:2016-11" 20161101 20161130]
                                             [:created "created-date:2015" 20150101 20151231]
                                             [:deposited "deposit-date:2016-10-16" 20161016 20161016]
                                             [:deposited "deposit-date:2016-09" 20160901 20160930]
                                             [:deposited "deposit-date:2015" 20150101 20151231]
                                             [:deposited "update-date:2016-10-16" 20161016 20161016]
                                             [:deposited "update-date:2016-09" 20160901 20160930]
                                             [:deposited "update-date:2015" 20150101 20151231]
                                             [:indexed "index-date:2018" 20180101 20181231]
                                             [:indexed "index-date:2018-12" 20181201 20181231]
                                             [:indexed "index-date:2018-12-01" 20181201 20181201]
                                             [:posted "posted-date:2015" 20150101 20151231]
                                             [:posted "posted-date:2015-09" 20150901 20150930]
                                             [:posted "posted-date:2015-09-16" 20150916 20150916]
                                             [:accepted "accepted-date:2016" 20160101 20161231]
                                             [:accepted "accepted-date:2016-09" 20160901 20160930]
                                             [:accepted "accepted-date:2016-09-16" 20160916 20160916]
                                             [:award-start "awarded-date:2018" 20180101 20181231]
                                             [:award-start "awarded-date:2018-12" 20181201 20181231]
                                             [:award-start "awarded-date:2018-12-01" 20181201 20181201]]]
      (let [to-int (fn [date-parts]
                     (let [[y m d] (concat date-parts [1 1])]
                       (+ (* 10000 y) (* 100 m) d)))
            filter-by-date (fn [direction]
                             (->> (str "/v1/works?filter=" direction "-" f-str)
                                  api-get
                                  :items
                                  (mapcat (comp :date-parts field))
                                  (map to-int)))]
        (is (every? (partial <= min-date) (filter-by-date "from"))
          (str "all dates should be on or after " min-date))
        (is (every? (partial >= max-date) (filter-by-date "until"))
          (str "all dates should be on or before " max-date)))))

  (testing "boolean filters correctly filter works"
    (doseq [[filter-name field] [["has-license" :license]
                                 ["has-full-text" :link]
                                 ["has-archive" :archive]
                                 ["is-update" :update-to]
                                 ["has-update-policy" :update-policy]
                                 ["has-assertion" :assertion]
                                 ["has-abstract" :abstract]
                                 ["has-clinical-trial-number" :clinical-trial-number]
                                 ["has-content-domain" :content-domain]
                                 ["has-relation" :relation]]]
      (let [response (filter-work-data filter-name "1" :field field)]
        (is (seq response))
        (is (every? identity response)))))

  (testing "misc filters correctly filter works"
    (doseq [[filter-name filter-value field] [["prefix" "10.1016" :prefix]
                                              ["member" "78" :member]
                                              ["article-number" "113" :article-number]]]
      (let [response (filter-work-data filter-name filter-value :field field)]
        (is (seq response))
        (is (every? #{filter-value} response)))))

  (testing "assertion filters correctly filter works"
    (let [value "publisher"
          assertion-names (->> (filter-work-data "assertion" value :field :assertion)
                               (map (partial map :name)))]
      (is (seq assertion-names))
      (is (every? (partial some #{value}) assertion-names)))
    
    (let [value "multiple_resolution"
          assertion-groups (->> (filter-work-data "assertion-group" value :field :assertion)
                                (map (partial map (comp :name :group))))]
      (is (seq assertion-groups))
      (is (every? (partial some #{value}) assertion-groups))))

  (testing "ORCID filters correctly filter works"
    (doseq [prefix ["" "orcid.org/" "https://orcid.org/"]
            orcid-filter ["0000-0001-7424-8744" "0000-0001-6043-0166"]]
      (let [orcid (str "https://orcid.org/" orcid-filter)
            orcids (->> (filter-work-data "orcid" (str prefix orcid-filter))
                        (map item-contributors)
                        (map (partial map :ORCID)))]
        (is (seq orcids))
        (is (every? (partial some #{orcid}) orcids))))
    
    (doseq [[filter-str field] [["has-orcid" :ORCID]
                                ["has-authenticated-orcid" :authenticated-orcid]]]
      (let [contribs (->> (filter-work-data filter-str "1")
                          (map item-contributors))]
        (is (seq contribs))
        (is (every? (partial some field) contribs)))))
  
  (testing "category-name filter correctly filters works"
    (let [items (filter-work-data "category-name" "Biological+Psychiatry" :field :subject)]
      (is (empty? items))))

  (testing "alternative-id filter correctly filters works"
    (let [alt-id "S0306453016302268"
          filtered (filter-work-data "alternative-id" alt-id :field :alternative-id)]
      (is (seq filtered))
      (is (every? (partial some #{alt-id}) filtered))))

  (testing "type filters correctly filter works"
    (doseq [[filter-name filter-value] [["type" "journal-article"]
                                        ["type-name" "Journal+Article"]]]
      (is (= #{"journal-article"} (->> (filter-work-data filter-name filter-value :field :type)
                                       set)))))

  (testing "funder-related filters correctly filter works"
    
    (doseq [[filter-name field-fn] [["has-funder" item-funders]
                                    ["has-award" item-awards]]]
      (let [items (filter-work-data filter-name "1" :field field-fn)]
        (is (seq items))
        (is (every? seq items))))
    
    (let [funder-dois (->> (filter-work-data "has-funder-doi" "1")
                           (map item-funders)
                           (map (partial map funder-doi)))]
      (is (seq funder-dois))
      (is (every? (partial some identity) funder-dois)))
    
    (let [funder-asserts (->> (filter-work-data "funder-doi-asserted-by" "publisher")
                              (map item-funders)
                              (map (partial map funder-doi-asserted-by)))]
      (is (seq funder-asserts))
      (is (every? (partial some #{"publisher"}) funder-asserts)))
    
    (doseq [prefix ["" "10.13039/" "https://doi.org/10.13039/"]
            funder-id ["100000002" "100004440"]
            filter-type ["funder" "award.funder"]]
      (let [full-funder-id (str "10.13039/" funder-id)
            funder-dois (->> (filter-work-data filter-type (str prefix funder-id))
                             (map item-funders)
                             (map (partial map funder-doi)))]
        (is (seq funder-dois))
        (is (every? (partial some #{full-funder-id}) funder-dois))))
    
    (doseq [[award-number expected] [["CA184090" "CA184090"]
                                     ["ca184090" "CA184090"]
                                     ["215573" "215573"]
                                     ["SFI/12/RC/2273" "SFI/12/RC/2273"]
                                     ["KyzZ15_0334" "KYZZ15_0334"]
                                     ["18-ddc-436" "18-DDC-436"]]]
      (let [awards (->> (filter-work-data "award.number" award-number)
                        (map item-awards))]
        (is (seq awards))
        (is (every? (partial some #{expected}) awards)))))

  (testing "DOI filters correctly filter works"
    (doseq [prefix ["" "doi.org/" "http://doi.org/"]]
      (let [doi "10.1038/tp.2016.235"
            dois (filter-work-data "doi" (str prefix doi) :field :DOI)]
        (is (seq dois))
        (is (every? #{doi} dois)))))

  (testing "updates filters correctly filter works"
    (doseq [prefix ["" "doi.org/" "http://doi.org/"]]
      (let [doi "10.1016/j.psyneuen.2015.09.028"
            updates (->> (filter-work-data "updates" (str prefix doi) :field :update-to)
                         (map (partial map :DOI)))]
        (is (seq updates))
        (is (every? (partial some #{doi}) updates))))
    
    (let [type "erratum"
          updates (->> type
                       (str "/v1/works?filter=update-type:")
                       api-get
                       :items
                       (map :update-to)
                       (map (partial map :type)))]
      (is (seq updates))
      (is (every? (partial some #{type}) updates))))

  (testing "full-text filters correctly filter works"
    (let [type "text/xml"
          full-text-types (->> (filter-work-data "full-text.type" type :field :link)
                               (map (partial map :content-type)))]
      (is (seq full-text-types))
      (is (every? (partial some #{type}) full-text-types)))
    
    (let [version "vor"
          full-text-versions (->> (filter-work-data "full-text.version" version :field :link)
                                  (map (partial map :content-version)))]
      (is (seq full-text-versions))
      (is (every? (partial some #{version}) full-text-versions)))
    
    (let [application "text-mining"
          full-text-applications (->> (filter-work-data "full-text.application" application :field :link)
                                      (map (partial map :intended-application)))]
      (is (seq full-text-applications))
      (is (every? (partial some #{application}) full-text-applications))))

  (testing "issn filter correctly filters works"
    (doseq [issn ["03064530"
                  "0306-4530"
                  "http://id.crossref.org/issn/03064530"
                  "http://id.crossref.org/issn/0306-4530"]]
      (let [issns (filter-work-data "issn" issn :field :ISSN)]
        (is (seq issns))
        (is (every? (partial some #{"0306-4530"}) issns)))))

  (testing "isbn filter correctly filters works"
    (doseq [isbn ["9781466600683"
                  "https://id.crossref.org/isbn/9781466600683"]]
      (let [isbns (filter-work-data "isbn" isbn :field :ISBN)]
        (is (seq isbns))
        (is (every? (partial some #{"9781466600683"}) isbns)))))

  (testing "license filters correctly filter works"
    (let [license "http://doi.wiley.com/10.1002/tdm_license_1"
          licenses (->> (filter-work-data "license.url" license :field :license)
                        (map (partial map :URL)))]
      (is (seq licenses))
      (is (every? (partial some #{license}) licenses)))
    
    (let [version "vor"
          licenses (->> (filter-work-data "license.version" version :field :license)
                        (map (partial map :content-version)))]
      (is (seq licenses))
      (is (every? (partial some #{version}) licenses)))
    
    (let [delay 210
          doi "10.1084/jem.20151673"
          response (api-get (str "/v1/works?filter=license.delay:" delay ",doi:" doi))
          delays (->> (:items response) (map :license) (apply concat) (map :delay-in-days))]
      (is (some (partial >= delay) delays)))
    
    (let [delay [210 5]
          delay-filter (str/join "," (map (partial str "license.delay:") delay))
          response (api-get (str "/v1/works?filter=",delay-filter))
          result (:total-results response)
          delays (->> (:items response) (map :license) (apply concat) (map :delay-in-days))
          chk-delays (map #(some (partial >= %) delays) delay)]
      (is (> result 1))
      (is (some true? chk-delays))))

  (testing "archive filter correctly filter works"
    (let [archive "Portico"
          archives (filter-work-data "archive" archive :field :archive)]

      (is (seq archives))
      (is (every? (partial some #{archive}) archives))))

  (testing "container filter correctly filter works"
    (let [titles (filter-work-data "container-title" "PLoS+ONE" :field :container-title)]
      (is (seq titles))
      (is (every? (partial some #{"PLoS ONE"}) titles))))

  (testing "affiliation filters correctly filter works"
    (let [contributors (filter-work-data "has-affiliation" "1" :field item-contributors)]
      (is (seq contributors))
      (is (every? (partial some :affiliation) contributors))))

  (testing "content-domain filter correctly filter works"
    (let [domain "clinicalkey.es"
          domains (filter-work-data "content-domain" domain :field (comp :domain :content-domain))]
      (is (seq domains))
      (is (every? (partial some #{domain}) domains)))
    
    (let [domains (filter-work-data "has-domain-restriction" "1" :field :content-domain)]
      (is (seq domains))
      (is (every? :crossmark-restriction domains))))

  (testing "relation filters correctly filter works"
    (let [relations (filter-work-data "relation.type" "is-review-of" :field :relation)]
      (is (seq relations))
      (is (every? :is-review-of relations)))
    
    (let [doi "10.1093/gigascience/gix134"
          relations (filter-work-data "relation.object" doi :field :relation)]
      (is (seq relations))
      (is (every?
            #(some #{doi} (->> % vals flatten (map :id)))
            relations)))
    
    (let [type "doi"
          relations (filter-work-data "relation.object-type" type :field :relation)]
      (is (seq relations))
      (is (every?
            #(some #{type} (->> % vals flatten (map :id-type)))
            relations))))
  
  (testing "award amount filters correctly filter works"
    
    (doseq [value [1 5 100 2000000 4106203]]
      (let [amounts (filter-work-data "lte-award-amount" value :field item-award-amount)]
        (is (every? (partial some (partial >= value)) amounts)))
      (let [amounts (filter-work-data "gte-award-amount" value :field item-award-amount)]
        (is (every? (partial some (partial <= value)) amounts)))))

  (testing "ror id filters correctly filter works"
    (let [items (filter-work-data "has-ror-id" "1" :field item-ror-ids)]
      (is (seq items))
      (is (every? seq items)) 
      (testing "and a document containing just a funder ROR id is included"
        (is (some? (some (partial some #{"https://ror.org/03zttf063"}) items)))))
    
    (doseq [value ["05bp8ka05" "https://ror.org/05bp8ka05"]]
      (let [items (filter-work-data "ror-id" value :field item-ror-ids)]
        (is (seq items))
        (is (every? (partial some #{"https://ror.org/05bp8ka05"}) items)))))

  (testing "licenses version filters correctly filter works"
    (let [items (filter-work-data "license.version" "stm-asf" :field #(->> %
                                                                      :license
                                                                      (map :content-version)
                                                                      set))]
      (is (every? #(some #{"stm-asf"} %)
                          items)))))

(deftest ^:integration grants-filtering
  (testing "has-ror-id correctly filters grants"
    (let [items (filter-grant-data "has-ror-id" "1" :field item-ror-ids)]
      (is (seq items))
      (is (every? seq items))
      
      (testing "and a document containing just a funder ROR id is included"
        (is (some? (some (partial some #{"https://ror.org/006wxqw41"}) items)))))))

(deftest ^:integration multiple-valued-filters
  (testing "filters of same type are OR-ed"
    (let [doi-set (fn [f] (->> f (str "/v1/works?rows=500&filter=") api-get :items (map :DOI) set))]
      (doseq [[f-name val-1 val-2] [["type" "journal-article" "book-chapter"]
                                    ["from-pub-date" "2017" "2016"]
                                    ["until-pub-date" "2017" "2016"]
                                    ["has-abstract" "1" "0"]
                                    ["has-license" "1" "0"]
                                    ["has-domain-restriction" "1" "0"]
                                    ["orcid" "0000-0002-3368-4133" "0000-0003-3981-8654"]
                                    ["has-references" "1" "0"]
                                    ["has-authenticated-orcid" "1" "0"]
                                    ["license.url" "http://www.springer.com/tdm" "http://www.elsevier.com/tdm/userlicense/1.0/"]
                                    ["assertion" "publisher" "copyright"]
                                    ["has-award" "1" "0"]
                                    ["award.number" "Szzx201503" "23591667"]
                                    ["award.funder" "10.13039/100009633" "10.13039/100000002"]]]
        (let [dois-1 (doi-set (str f-name ":" val-1))
              dois-2 (doi-set (str f-name ":" val-2))
              dois-or (doi-set (str f-name ":" val-1 "," f-name ":" val-2))]
          (is (subset? dois-1 dois-or))
          (is (subset? dois-2 dois-or)))))))

(use-fixtures
  :once
  (api-fixture setup-api setup-test-journals setup-test-members setup-test-works))
