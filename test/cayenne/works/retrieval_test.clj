(ns cayenne.works.retrieval-test
  (:require [cayenne.api-fixture :refer [api-get api-get-raw api-get-status setup-api api-root]]
            [clojure.string :as str :refer [starts-with?]]
            [clj-http.client :as http]
            [org.httpkit.client :as httpkit-client]
            [clojure.test :refer [deftest testing is]]
            [user :refer [flush-elastic index-work-files]])
  (:import [java.net URLEncoder]))

(defn index-doc [fname]
  (setup-api)
  (index-work-files [(str "dev-resources/metadata-cases/" fname)])
  (flush-elastic))

(deftest ^:integration ingesting-retrieving-types

  (testing "types are correctly ingested and retrieved"
    (doseq [[fname value]
            [["type-book.body" "book"]
             ["link-2.body" "book-chapter"]
             ["type-book-part.body" "book-part"]
             ["type-book-section.body" "book-section"]
             ["type-book-series.body" "book-series"]
             ["part-number.body" "book-set"]
             ["license-issued.body" "book-track"]
             ["type-component.body" "component"]
             ["type-3.body" "database"]
             ["institution-nil.body" "dataset"]
             ["editor-2.body" "dissertation"]
             ["type-2.body" "edited-book"]
             ["grant.body" "grant"]
             ["short-title.body" "journal"]
             ["type.body" "journal-article"]
             ["type-journal-issue.body" "journal-issue"]
             ["type-journal-volume.body" "journal-volume"]
             ["editor.body" "monograph"]
             ["type-other.body" "other"]
             ["relation.body" "peer-review"]
             ["subtype.body" "posted-content"]
             ["event-empty-location.body" "proceedings"]
             ["issn-conference-series.body" "proceedings-article"]
             ["type-proceeding-series.body" "proceedings-series"]
             ["published-print-null.body" "reference-book"]
             ["publisher.body" "reference-entry"]
             ["institution-multiple.body" "report"]
             ["type-report.body" "report"]
             ["container-title-multiple.body" "report-component"]
             ["type-report-series.body" "report-series"]
             ["standards-body.body" "standard"]]]
      (index-doc fname)
      (is (= value (-> "/v1/works/" api-get :items first :type))))))

(deftest ^:integration ingesting-retrieving-metadata

  (testing "simple metadata is correctly ingested and retrieved"
    (doseq [[fname field value] [["alternative-id.body" :alternative-id ["S2213158219304334"]]
                                 ["alternative-id-2.body" :alternative-id ["10.4135/9781452276335"]]
                                 ["archive.body" :archive ["Portico"]]
                                 ["archive-2.body" :archive ["CLOCKSS" "LOCKSS" "Portico"]]
                                 ["article-number.body" :article-number "102086"]
                                 ["grant.body" :award "215573"]
                                 ["container-title.body" :container-title ["Plastic and Reconstructive Surgery"]]
                                 ["container-title-multiple.body" :container-title ["Professional Paper"]]
                                 ["container-title-xml.body" :container-title ["Volume <volume>10</volume>: Heat Transfer, Fluid Flows, and Thermal Systems, Parts A, B, and C"]]
                                 ["degree.body" :degree ["Master of Fine Arts"]]
                                 ["doi.body" :DOI "10.1002/nur.21773"]
                                 ["doi.body" :prefix "10.1002"]
                                 ["doi.body" :URL "https://doi.org/10.1002/nur.21773"]
                                 ["edition-number.body" :edition-number "2"]
                                 ["edition-number-2.body" :edition-number "1"]
                                 ["group-title.body" :group-title "Bioinformatics"]
                                 ["is-referenced-by-count.body" :is-referenced-by-count 11]
                                 ["issue.body" :issue "1"]
                                 ["language.body" :language "en"]
                                 ["member.body" :member "311"]
                                 ["original-title.body" :original-title ["Moor, Rev. Edward, (1880–11 Feb. 1953), Canon Residentiary, Winchester Cathedral, 1933–50; Canon Emeritus since 1950"]]
                                 ["original-title-xml.body" :original-title ["Moor, Rev. Edward, (1880–11 Feb. 1953), Canon Residentiary, <place>Winchester Cathedral</place>, 1933–50; Canon Emeritus since 1950"]]
                                 ["page.body" :page "51-62"]
                                 ["part-number.body" :part-number "33-6"]
                                 ["publisher.body" :publisher "John Wiley & Sons, Inc."]
                                 ["publisher-location.body" :publisher-location "Hoboken, NJ, USA"]
                                 ["references-count.body" :reference-count 55]
                                 ["references-count.body" :references-count 55]
                                 ["short-container-title.body" :short-container-title ["Res Nurs Health"]]
                                 ["short-title.body" :short-title ["CES Med"]]
                                 ["source.body" :source "Crossref"]
                                 ["subtitle.body" :subtitle ["Megavolume Autologous Fat Transfer"]]
                                 ["subtitle-xml.body" :subtitle ["Megavolume Autologous <b>Fat</b> Transfer"]]
                                 ["subtype.body" :subtype "preprint"]
                                 ["title.body" :title ["Assessment of cerebrovascular dysfunction after traumatic brain injury with fMRI and fNIRS"]]
                                 ["title-xml.body" :title ["NeisseriaBase: <subtitletag>a specialised<i>Neisseria</i>genomic <another>resource</another> and analysis platform</subtitletag>"]]
                                 ["type.body" :type "journal-article"]
                                 ["type-2.body" :type "edited-book"]
                                 ["type-3.body" :type "database"]
                                 ["update-policy.body" :update-policy "https://doi.org/10.1530/crossmarkpolicy"]
                                 ["volume.body" :volume "232"]]]
      (index-doc fname)
      (is (= value (-> "/v1/works/" api-get :items first field)))))

  (testing "date metadata is correctly ingested and retrieved"
    (doseq [[fname field date] [["accepted.body" :accepted [2016 9 16]]
                                ["approved.body" :approved [2015]]
                                ["approved2.body" :approved [2010 12 8]]
                                ["grant.body" (comp :award-planned-end first :project) [2024 4 30]]
                                ["grant.body" (comp :award-planned-start first :project) [2019 5 1]]
                                ["grant.body" (comp :award-start first :project) [2019 6 1]]
                                ["grant.body" (comp :award-end first :project) [2024 5 31]]
                                ["grant.body" :award-start [2019 11 25]]
                                ["content-created.body" :content-created [2015 9 13]]
                                ["content-created2.body" :content-created [2000]]
                                ["created.body" :created [2018 8 10]]
                                ["deposited.body" :deposited [2018 8 31]]
                                ["issued.body" :issued [2016 12 9]]
                                ["grant.body" :issued [2019 5 1]]
                                ["issued-null.body" :issued [nil]]
                                ["posted.body" :posted [2015 9 16]]
                                ["published.body" :published [2016 12 9]]
                                ["published.body" :published-print [2017 2]]
                                ["published.body" :published-online [2016 12 9]]
                                ["content-updated-2.body" :content-updated [2020 5 30]]]]
      (index-doc fname)
      (is (= date (-> "/v1/works/" api-get :items first field :date-parts first)))))

  (testing "funder is correctly ingested and retrieved"
    (index-doc "funder-award.body")
    (is (= [{:DOI "10.13039/100000002"
             :name "NIH"
             :doi-asserted-by "publisher"
             :award ["PL-1 DA02486" "PL-1 DA024860 (NIDA)" "MH30929 (NIMH)"],
             :id [
                           {
                            :id "10.13039/100000002",
                            :id-type "DOI",
                            :asserted-by "publisher"
                            }
                           ]
             }]
           (-> "/v1/works/" api-get :items first :funder)))

    (index-doc "funder-doi.body")
    (is (= [{:DOI "10.13039/100000870" :name "MacArthur Foundation" :doi-asserted-by "publisher", :id [{:id "10.13039/100000870", :id-type "DOI", :asserted-by "publisher"}]}
            {:DOI "10.13039/100015147" :name "IS" :doi-asserted-by "publisher", :id [{:id "10.13039/100015147", :id-type "DOI", :asserted-by "publisher"}]}]
           (-> "/v1/works/" api-get :items first :funder))))

  (testing "assertion is correctly ingested and retrieved"
    (index-doc "assertion.body")
    (is (= [{:value "This document is Similarity Check deposited"
             :name "similarity_check"
             :explanation {:URL "https://www.crossref.org/services/similarity-check/"}
             :group {:name "similarity_check" :label "SIMILARITY CHECK"}}
            {:value "Supplementary Information"
             :URL "https://doi.org/10.1039/D0TC00148A"
             :name "related_data"
             :group {:name "related_data", :label "RELATED DATA"}}
            {:value "Single-blind"
             :order 2
             :name "peer_review_method"
             :group {:name "peer_review_method", :label "PEER REVIEW METHOD"}}
            {:value "Received 10 January 2020; Accepted 13 March 2020;  Accepted Manuscript published 16 March 2020"
             :order 1
             :name "history"
             :group {:name "publication_history", :label "PUBLICATION HISTORY"}}]
           (-> "/v1/works/" api-get :items first :assertion)))

    (index-doc "assertion-group-label-null.body")
    (is (= [{:value "2020-10-14"
             :name "received"
             :label "Received"
             :group {:name "publication_history"}}
            {:value "2020-11-12"
             :name "revised"
             :label "Accepted"
             :group {:name "publication_history"}}]
           (-> "/v1/works/" api-get :items first :assertion))))

  (testing "update-to is correctly ingested and retrieved"
    (index-doc "update-to-label-vocab.body")
    (is (= [{:updated {:date-parts [[2017 4 1]]
                       :date-time "2017-04-01T00:00:00Z"
                       :timestamp 1491004800000}
             :DOI "10.1016/j.psyneuen.2015.09.028"
             :source "publisher"
             :type "erratum"
             :label "Erratum"}]
           (-> "/v1/works/" api-get :items first :update-to)))

    (index-doc "update-to-label-deposit.body")
    (is (= [{:updated {:date-parts [[2017 4 1]]
                       :date-time "2017-04-01T00:00:00Z"
                       :timestamp 1491004800000}
             :DOI "10.1016/j.psyneuen.2015.09.028"
             :source "publisher"
             :type "custom"
             :label "clabel"}]
           (-> "/v1/works/" api-get :items first :update-to)))

    (index-doc "update-to-label-unknown.body")
    (is (= [{:updated {:date-parts [[2017 4 1]]
                       :date-time "2017-04-01T00:00:00Z"
                       :timestamp 1491004800000}
             :DOI "10.1016/j.psyneuen.2015.09.028"
             :source "publisher"
             :type "custom"
             :label "custom"}]
           (-> "/v1/works/" api-get :items first :update-to)))

    (index-doc "crossmark-five-digit-year.body")
    (is (= [{:updated {:date-parts [[20201 4 26]]
                       :date-time "20201-04-26T00:00:00Z"
                       :timestamp 575324726400000}
             :DOI "10.3365/kjmm.2017.55.8.537"
             :source "publisher"
             :type "correction"
             :label "Correction"}]
           (-> "/v1/works/" api-get :items first :update-to))))

  (testing "references are correctly ingested and retrieved"
    (index-doc "references.body")
    (is (= {:key "cit0001" :doi-asserted-by "publisher" :DOI "10.1037/0021-9010.68.4.709"}
           (-> "/v1/works/" api-get :items first :reference first)))
    (is (= {:key "cit0002" :doi-asserted-by "crossref" :DOI "10.1016/j.cub.2007.09.060"}
           (-> "/v1/works/" api-get :items first :reference second)))
    (is (= {:key "cit0004" :volume "7" :author "Burke S. M." :year "2012" :journal-title "Plos One"}
           (-> "/v1/works/" api-get :items first :reference (nth 3))))
    (is (= {:key "cit0005" :issn-type "print" :ISSN "https://id.crossref.org/issn/1093-2129"}
           (-> "/v1/works/" api-get :items first :reference (nth 4))))
    (is (= {:key "cit0006" :issn-type "online" :ISSN "https://id.crossref.org/issn/0687-0459"}
           (-> "/v1/works/" api-get :items first :reference (nth 5))))
    (is (= {:key "cit0007" :isbn-type "print" :ISBN "https://id.crossref.org/isbn/11093029"}
           (-> "/v1/works/" api-get :items first :reference (nth 6))))
    (is (= {:key "cit0008" :isbn-type "online" :ISBN "https://id.crossref.org/isbn/9900147X"}
           (-> "/v1/works/" api-get :items first :reference (nth 7))))

    (index-doc "references-doi-format.body")
    (is (= [{:key "2019071913294605000_349019v7.1" :doi-asserted-by "publisher" :DOI "10.1038/ng.2804"}
            {:key "2019071913294605000_349019v7.12"}
            {:key "2019071820091949000_272690v2.26"}
            {:key "2020052312592349000_2020.05.20.106989v1.13"}]
           (-> "/v1/works/" api-get :items first :reference))))

  (testing "relation is correctly ingested and retrieved"
    (index-doc "relation.body")
    (is (= {:is-review-of [{:id-type "doi" :id "10.7717/peerj.1078" :asserted-by "subject"}]}
           (-> "/v1/works/" api-get :items first :relation)))
    
    (index-doc "relation-finances.body")
    (is (= {:finances [{:id-type "doi" :id "10.32013/4859104" :asserted-by "subject"}
                       {:id-type "doi" :id "10.5555/uzzXUo4" :asserted-by "object"}]
            :is-financed-by [{:id-type "doi" :id "10.32013/ewv" :asserted-by "subject"}
                             {:id-type "doi" :id "10.32013/501100002241" :asserted-by "object"}]}
           (-> "/v1/works/" api-get :items first :relation))))

  (testing "content-domain is correctly ingested and retrieved"
    (index-doc "content-domain.body")
    (is (= {:domain ["elsevier.com" "sciencedirect.com"] :crossmark-restriction true}
           (-> "/v1/works/" api-get :items first :content-domain))))

  (testing "abstract is correctly ingested and retrieved"
    (index-doc "abstract.body")
    (is (starts-with?
          (-> "/v1/works/" api-get :items first :abstract)
          "<p>Polymer composites with high permittivity are promising applications in advanced electronics."))

    (index-doc "abstract-large.body")
    (is (-> "/v1/works/" api-get :items first :abstract)))

  (testing "title is correctly ingested and retrieved"
    (index-doc "title-large.body")
    (is (-> "/v1/works/" api-get :items first :title)))

  (testing "authors are correctly ingested and retrieved"
    (index-doc "author.body")
    (is (= [{:ORCID "https://orcid.org/0000-0003-0569-9490"
             :authenticated-orcid false
             :given "Tobias"
             :family "Stenlund"
             :sequence "first"
             :affiliation []}
            {:given "André"
             :family "Nyberg"
             :sequence "additional"
             :affiliation []}
            {:given "Sara"
             :family "Lundell"
             :sequence "additional"
             :affiliation []}
            {:given "Karin"
             :family "Wadell"
             :sequence "additional"
             :affiliation []}]
           (-> "/v1/works/" api-get :items first :author)))

    (index-doc "author-2.body")
    (is (= [{:ORCID "https://orcid.org/0000-0002-7436-3176"
             :authenticated-orcid false
             :suffix "Jr"
             :given "Charles Thomas"
             :family "Parker"
             :sequence "first"
             :affiliation [{:name "NamesforLife, LLC"}]}
            {:ORCID "https://orcid.org/0000-0002-4465-7034"
             :authenticated-orcid false
             :given "George M"
             :family "Garrity"
             :sequence "additional"
             :affiliation [{:name "NamesforLife, LLC"}]}]
           (-> "/v1/works/" api-get :items first :author)))

    (index-doc "author-3.body")
    (is (= [{:ORCID "https://orcid.org/0000-0002-4011-3590"
             :authenticated-orcid true
             :given "Minerva"
             :family "Housecat"
             :sequence "first"
             :affiliation [{:id [{:id "https://ror.org/02twcfp32" :id-type "ROR" :asserted-by "publisher"}]
                            :department ["Feline Outreach"]}]}]
           (-> "/v1/works/" api-get :items first :author))))

  (testing "editors are correctly ingested and retrieved"
    (index-doc "editor.body")
    (is (= [{:given "Lesia"
             :family "Lennex"
             :sequence "first"
             :affiliation [{:name "Morehead State University, USA"}]}
            {:given "Kimberely Fletcher"
             :family "Nettleton"
             :sequence "additional"
             :affiliation [{:name "Morehead State University, USA"}]}]
           (-> "/v1/works/" api-get :items first :editor)))

    (index-doc "editor-2.body")
    (is (= [{:suffix "IIIIVX"
             :given "Moxie-Crimefighter!"
             :family "Chewbacca"
             :sequence "first"
             :affiliation [{:id [{:id "https://ror.org/01bj3aw27" :id-type "ROR" :asserted-by "publisher"}
                                 {:id "https://www.isni.org/0000000123423717" :id-type "ISNI" :asserted-by "publisher"}]}
                           {:id [{:id "https://ror.org/05gq02987" :id-type "ROR" :asserted-by "publisher"}]}]}]
           (-> "/v1/works/" api-get :items first :editor))))

  (testing "translators are correctly ingested and retrieved"
    (index-doc "translator.body")
    (is (= [{:given "John" :family "Savage" :sequence "additional" :affiliation []}]
           (-> "/v1/works/" api-get :items first :translator))))

  (testing "chairs are correctly ingested and retrieved"
    (index-doc "chair.body")
    (is (= [{:given "John" :family "Berns" :sequence "first" :affiliation [{:name "U. of Missouri"}]}
            {:given "John" :family "Berns" :sequence "additional" :affiliation [{:name "U. of Missouri"}]}]
           (-> "/v1/works/" api-get :items first :chair))))

  (testing "standards-body is correctly ingested and retrieved"
    (index-doc "standards-body.body")
    (is (= {:name "SAE International" :acronym "SAE"}
           (-> "/v1/works/" api-get :items first :standards-body))))

  (testing "events are indexed and retrieved in ES"
    (index-doc "event.body")
    (is (= {:name "ASME 2008 International Mechanical Engineering Congress and Exposition"
            :location "Boston, Massachusetts, USA"
            :acronym "IMECE2008"
            :sponsor ["ASME"]
            :start {:date-parts [[2008 10 31]]}
            :end {:date-parts [[2008 11 6]]}}
           (-> "/v1/works/" api-get :items first :event)))

    (index-doc "event-2.body")
    (is (= {:name "2016 IEEE Canadian Conference on Electrical and Computer Engineering (CCECE)"
            :location "Vancouver, BC, Canada"
            :start {:date-parts [[2016 5 15]]}
            :end {:date-parts [[2016 5 18]]}}
           (-> "/v1/works/" api-get :items first :event)))

    (index-doc "event-empty-location.body")
    (is (= {:name "Congresso Brasileiro de Engenharia Química em Iniciação Científica"
            :acronym "COBEQ IC 2015"
            :number "0"
            :start {:date-parts [[2016 10 26]]}
            :end {:date-parts [[2016 10 28]]}}
           (-> "/v1/works/" api-get :items first :event))))

  (testing "licenses are correctly ingested and retrieved"
    (index-doc "license.body")
    (is (= [{:start {:date-parts [[2016 11 1]]
                     :date-time "2016-11-01T00:00:00Z"
                     :timestamp 1477958400000}
             :content-version "tdm"
             :delay-in-days 0
             :URL "http://www.elsevier.com/tdm/userlicense/1.0/"}]
           (-> "/v1/works/" api-get :items first :license)))

    (index-doc "license-issued.body")
    (is (= [{:URL "http://creativecommons.org/licenses/by/4.0"
             :start {:date-parts [[2019 5 1]]
                     :date-time "2019-05-01T00:00:00Z"
                     :timestamp 1556668800000}
             :delay-in-days 0
             :content-version "unspecified"}
            {:URL "http://creativecommons.org/licenses/by/4.0"
             :start {:date-parts [[2019 5 1]]
                     :date-time "2019-05-01T00:00:00Z"
                     :timestamp 1556668800000}
             :delay-in-days 0
             :content-version "unspecified"}]
           (-> "/v1/works/" api-get :items first :license)))

    (index-doc "license-no-dates.body")
    (is (= [{:URL "https://www.karger.com/Services/SiteLicenses"
             :delay-in-days 0
             :content-version "vor"}
            {:URL "https://www.karger.com/Services/SiteLicenses"
             :delay-in-days 0
             :content-version "tdm"}]
           (-> "/v1/works/" api-get :items first :license)))

    (index-doc "license-five-digit-year.body")
    (is (= [{:start {:date-parts [[29021 4 2]]
                     :date-time "29021-04-02T00:00:00Z"
                     :timestamp 853654982400000}
             :content-version "tdm"
             :delay-in-days 9861514
             :URL "http://creativecommons.org/licenses/by-nc/3.0/"}]
           (-> "/v1/works/" api-get :items first :license)))

    (index-doc "multilicense.body")
    (let [[l1 l2 l3 l4] (-> "/v1/works/" api-get :items first :license)]
      (is (= {:start
              {:date-parts [[2021 11 18]],
               :date-time "2021-11-18T00:00:00Z",
               :timestamp 1637193600000},
              :content-version "tdm",
              :delay-in-days 1843,
              :URL "http://www.elsevier.com/tdm/userlicense/1.0/"}
             l1))
      (is  (= {:start
               {:date-parts [[2021 11 17]],
                :date-time "2021-11-17T00:00:00Z",
                :timestamp 1637107200000},
               :content-version "am",
               :delay-in-days 1842,
               :URL "http://rsc.li/journals-terms-of-use"}
              l2))
              
      (is  (= {:start
               {:date-parts [[2021 11 16]],
                :date-time "2021-11-16T00:00:00Z",
                :timestamp 1637020800000},
               :content-version "vor",
               :delay-in-days 1841,
               :URL "http://onlinelibrary.wiley.com/termsAndConditions"}
              l3))
              
      (is   (=  {:start
                 {:date-parts [[2021 11 15]],
                  :date-time "2021-11-15T00:00:00Z",
                  :timestamp 1636934400000},
                 :content-version "stm-asf",
                 :delay-in-days 1840,
                 :URL "https://doi.org/10.15223/policy-029"}
                l4))))

  (testing "journal-issue is correctly ingested and retrieved"
    (index-doc "journal-issue.body")
    (is (= {:issue "1/2"}
           (-> "/v1/works/" api-get :items first :journal-issue))))

  (testing "institution is correctly ingested and retrieved"
    (index-doc "institution.body")
    (is (= [{:name "American Psychological Association" :acronym ["APA"]}]
           (-> "/v1/works/" api-get :items first :institution)))

    (index-doc "institution-2.body")
    (is (= [{:name "Crossref"
             :acronym ["CR"]
             :place ["Lynnfield, MA"]
             :department ["Feline Outreach"]
             :id [{:id "https://ror.org/02twcfp32" :id-type "ROR" :asserted-by "publisher"}
                  {:id "https://www.isni.org/0000000405062673" :id-type "ISNI" :asserted-by "publisher"}
                  {:id "https://www.wikidata.org/entity/Q5188229" :id-type "wikidata" :asserted-by "publisher"}]}
            {:id [{:id "https://ror.org/05bp8ka05" :id-type "ROR" :asserted-by "publisher"}]}
            {:name "Society of Metadata Idealists"}]
           (-> "/v1/works/" api-get :items first :institution)))

    (index-doc "institution-multiple.body")
    (is (= [{:name "US Dept of the Air Force"
             :acronym ["TR-2000-012" "Another acronym"]
             :department ["Funding"]}
            {:name "MICHIGAN TECHNOLOGICAL UNIV  HOUGHTON DEPT OF PHYSICS"
             :place ["Michigan"]
             :department ["Performing"]}]
           (-> "/v1/works/" api-get :items first :institution))))

  (testing "ISSNs are correctly ingested and retrieved"
    (index-doc "issn.body")
    (is (= ["2213-1582"]
           (-> "/v1/works/" api-get :items first :ISSN)))
    (is (= [{:value "2213-1582" :type "print"}]
           (-> "/v1/works/" api-get :items first :issn-type)))

    (index-doc "issn-conference-series.body")
    (is (= ["2029-7149"]
           (-> "/v1/works/" api-get :items first :ISSN)))
    (is (= [{:value "2029-7149" :type "electronic"}]
           (-> "/v1/works/" api-get :items first :issn-type))))

  (testing "ISBNs are correctly ingested and retrieved"
    (index-doc "isbn.body")
    (is (= ["0571089895" "0064410145"]
           (-> "/v1/works/" api-get :items first :ISBN)))
    (is (= [{:value "0571089895" :type "electronic"}
            {:value "0064410145" :type "print"}]
           (-> "/v1/works/" api-get :items first :isbn-type)))

    (index-doc "isbn-2.body")
    (is (= ["9783131940919"]
           (-> "/v1/works/" api-get :items first :ISBN)))
    (is (= [{:value "9783131940919" :type "print"}]
           (-> "/v1/works/" api-get :items first :isbn-type)))

    (index-doc "isbn-book.body")
    (is (= ["9789401168021" "9789401168007"]
           (-> "/v1/works/" api-get :items first :ISBN)))
    (is (= [{:value "9789401168021" :type "print"}
            {:value "9789401168007" :type "electronic"}]
           (-> "/v1/works/" api-get :items first :isbn-type))))

  (testing "clinical trial numbers are correctly ingested and retrieved"
    (index-doc "clinical-trial-number.body")
    (is (= [{:clinical-trial-number "nct01000701"
             :registry "10.18810/clinical-trials-gov"}]
           (-> "/v1/works/" api-get :items first :clinical-trial-number))))

  (testing "links are correctly ingested and retrieved"
    (index-doc "link.body")
    (is (= [{:URL "http://journal.frontiersin.org/article/10.3389/fphys.2016.00471/full"
             :content-type "unspecified"
             :content-version "vor"
             :intended-application "similarity-checking"}]
           (-> "/v1/works/" api-get :items first :link)))

    (index-doc "link-2.body")
    (is (= [{:URL "http://link.springer.com/content/pdf/10.1007/978-981-13-2802-2_4"
             :content-type "unspecified"
             :content-version "vor"
             :intended-application "similarity-checking"}]
           (-> "/v1/works/" api-get :items first :link))))

  (testing "published-print is not present when not present in XML"
    (index-doc "published-print-null.body")
    (is (not (contains? (-> "/v1/works/" api-get :items first) :published-print))))

  (testing "project/award-amount in grants is correctly ingested and retrieved"
    (index-doc "grant.body")
    (is (= [{:amount 4106203.0 :currency "GBP"}
            {:amount 9.0 :currency "GBP"}]
           (->> "/v1/works/" api-get :items first :project (map :award-amount)))))

  (testing "project/project-title in grants is correctly ingested and retrieved"
    (index-doc "grant.body")
    (is (= [[{:title "Integrative imaging of brain structure and function in populations and individuals"
              :language "en"}
             {:title "Another title"}]
            [{:title "Second project"
              :language "en"}]]
           (->> "/v1/works/" api-get :items first :project (map :project-title))))

    (index-doc "grant-title-xml.body")
    (is (= [[{:title "<disease>ALS</disease> Drug Rescue using a Novel <technique>Machine-Learning</technique> Based Subgroup Analysis Tool"
              :language "en"}]]
           (->> "/v1/works/" api-get :items first :project (map :project-title)))))
  
  (testing "project/project-description in grants is correctly ingested and retrieved"
    (index-doc "grant.body")
    (is (= [2 0]
           (->> "/v1/works/" api-get :items first :project (map :project-description) (map count))))
    (is (= {:description "Neuroscientists and doctors use neuroimaging to study the brain without cutting into it."
            :language "en"}
           (->> "/v1/works/" api-get :items first :project first :project-description second)))

    (index-doc "grant-empty-description.body")
    (is (nil? (->> "/v1/works/" api-get :items first :project first :project-description))))

  (testing "investigators in grants are correctly ingested and retrieved"
    (index-doc "grant-no-investigators.body")
    (let [project (->> "/v1/works/" api-get :items first :project first)]
      (is (nil? (:lead-investigator project)))
      (is (nil? (:co-lead-investigator project)))
      (is (nil? (:investigator project))))

    (index-doc "grant.body")
    (is (= [1 0]
           (->> "/v1/works/" api-get :items first :project (map :lead-investigator) (map count))))
    (is (= {:ORCID "https://orcid.org/0000-0001-8166-069X"
            :authenticated-orcid false
            :given "Stephen"
            :family "Smith"
            :affiliation [{:name "University of Oxford"
                           :country "GB"}]}
           (->> "/v1/works/" api-get :items first :project first :lead-investigator first)))
    (is (= [1 0]
           (->> "/v1/works/" api-get :items first :project (map :co-lead-investigator) (map count))))
    (is (= {:given "Steven"
            :family "Schmit"
            :affiliation []}
           (->> "/v1/works/" api-get :items first :project first :co-lead-investigator first)))
    (is (= [2 1]
           (->> "/v1/works/" api-get :items first :project (map :investigator) (map count))))
    (is (= {:ORCID "https://orcid.org/0000-0001-6043-0166"
            :authenticated-orcid false
            :given "Mark"
            :family "Jenkinson"
            :alternate-name ["Mark Jenkinson" "M. Jenkinson"]
            :affiliation [{:name "University of Oxford"
                           :country "GB"
                           :id [{:id "https://ror.org/052gg0110"
                                 :id-type "ROR"
                                 :asserted-by "publisher"}]}
                          {:name "Some Other University"}]
            :role-start {:date-parts [[2017 5 29]]}
            :role-end {:date-parts [[2017 8 29]]}}
           (->> "/v1/works/" api-get :items first :project first :investigator first))))
  
  (testing "funding in grants are correctly ingested and retrieved"
    (index-doc "grant.body")
    (is (= [2 1]
           (->> "/v1/works/" api-get :items first :project (map :funding) (map count))))
    (is (= {:type "grant"
            :scheme "Cognitive Neuroscience and Mental Health"
            :award-amount {:amount 4106202.0
                           :currency "GBP"
                           :percentage 99}
            :funder {:name "Wellcome Trust"
                     :id [{:id "10.13039/100004440"
                           :id-type "DOI"
                           :asserted-by "publisher"}]}}
           (->> "/v1/works/" api-get :items first :project first :funding first))))
  
  (testing "description is correctly ingested and retrieved"
    (index-doc "description-empty.body")
    (is (nil? (->> "/v1/works/" api-get :items first :description))))
  
  (testing "resource URLs are correctly ingested and retrieved"
    (index-doc "resource-urls.body")
    (is (= {:primary {:URL "http://multi-science.atypon.com/doi/10.1260/1475473021502739"}
            :secondary [{:URL "http://triggered.edina.clockss.org/ServeContent?url=http%3A%2F%2Fmulti-science.atypon.com%2Fdoi%2Fpdf%2F10.1260%2F1475473021502739"
                         :label "CLOCKSS_EDINA"}
                        {:URL "http://triggered.stanford.clockss.org/ServeContent?url=http%3A%2F%2Fmulti-science.atypon.com%2Fdoi%2Fpdf%2F10.1260%2F1475473021502739"
                         :label "CLOCKSS_SU"}]}
           (->> "/v1/works/" api-get :items first :resource)))))

(deftest ^:integration retrieving-dois
  (testing "input DOI case (upper/lower) does not affect retrieving items"
    (setup-api)
    (index-work-files ["dev-resources/doi-case/doi.body"])
    (flush-elastic)
    (let [doi-lower "10.1016/j.explore.2016.10.009"
          doi-upper "10.1016/j.EXPLORE.2016.10.009"]
      (is (= doi-lower (->> doi-lower (str "/v1/works/") api-get :DOI)))
      (is (= doi-lower (->> doi-upper (str "/v1/works/") api-get :DOI)))
      (is (= doi-lower (->> doi-lower (str "/v1/works?filter=doi:") api-get :items first :DOI)))
      (is (= doi-lower (->> doi-upper (str "/v1/works?filter=doi:") api-get :items first :DOI)))))
  
  (testing "retrieving a non-existent DOI returns 404"
    (setup-api)
    (index-work-files ["dev-resources/doi-case/doi.body"])
    (flush-elastic)
    (is (= 404 (api-get-status "/v1/works/nonexistent")))
    (is (= 404 (api-get-status "/v1/works/10.1111/nonexistent"))))

  (testing "special characters do not affect retrieving items"
    (setup-api)
    (index-work-files ["dev-resources/doi-case/doi-with-plus.body"])
    (flush-elastic)
    (is (= "10.1021/jo070268+" (->> "/v1/works/10.1021/jo070268+" api-get :DOI)))
    (is (= "10.1021/jo070268+" (->> (URLEncoder/encode "10.1021/jo070268+" "UTF-8")
                                    (str "/v1/works?filter=doi:")
                                    api-get
                                    :items
                                    first
                                    :DOI)))
    
    (setup-api)
    (index-work-files ["dev-resources/doi-case/doi-with-unicode.body"])
    (flush-elastic)
    (is (= "10.2741/ortéga" (->> (URLEncoder/encode "10.2741/Ortéga" "UTF-8")
                                 (str "/v1/works/")
                                 api-get
                                 :DOI)))
    (is (= "10.2741/ortéga" (->> (URLEncoder/encode "10.2741/Ortéga" "UTF-8")
                                 (str "/v1/works?filter=doi:")
                                 api-get
                                 :items
                                 first
                                 :DOI)))))

(deftest ^:integration status-transforming-dois
  
  (let [status-h (fn [doi content-type] (api-get-status
                                          (str "/v1/works/" doi "/transform")
                                          {:headers {"accept" content-type}}))
        status-q (fn [doi content-type] (api-get-status
                                          (str "/v1/works/" doi "/transform/" content-type)))]
    (testing "transforming a missing DOI returns 404"
      (setup-api)
      (index-work-files ["dev-resources/metadata-cases/doi.body"])
      (flush-elastic)
      (doseq [doi ["nonexistent" "10.1111/nonexistent"]]
        (is (= 404 (status-h doi "application/x-bibtex")))
        (is (= 404 (status-q doi "application/x-bibtex")))))

    (testing "transforming a non-grant DOI returns 200 for all content types"
      (setup-api)
      (index-work-files ["dev-resources/metadata-cases/doi.body"])
      (flush-elastic)
      (doseq [content-type ["application/rdf+xml"
                            "text/turtle"
                            "text/n-triples"
                            "text/n3"
                            "application/vnd.citationstyles.csl+json"
                            "application/citeproc+json"
                            "application/json"
                            "text/x-bibliography"
                            "text/bibliography"
                            "text/plain"
                            "application/x-research-info-systems"
                            "application/x-bibtex"
                            "application/vnd.crossref.unixref+xml"
                            "application/unixref+xml"
                            "application/vnd.crossref.unixsd+xml"]]
        (is (= 200 (status-h "10.1002/nur.21773" content-type)))
        (is (= 200 (status-q "10.1002/nur.21773" content-type)))))

    (testing "transforming a grant DOI into JSON or Crossref XML returns 200"
      (setup-api)
      (index-work-files ["dev-resources/metadata-cases/grant.body"])
      (flush-elastic)
      (doseq [content-type ["application/vnd.citationstyles.csl+json"
                            "application/citeproc+json"
                            "application/json"
                            "application/vnd.crossref.unixref+xml"
                            "application/unixref+xml"
                            "application/vnd.crossref.unixsd+xml"]]
        (is (= 200 (status-h "10.35802/215573" content-type)))
        (is (= 200 (status-q "10.35802/215573" content-type)))))

    (testing "transforming a grant DOI into all content types except JSON and Crossref XML returns 406"
      (setup-api)
      (index-work-files ["dev-resources/metadata-cases/grant.body"])
      (flush-elastic)
      (doseq [content-type ["application/rdf+xml"
                            "text/turtle"
                            "text/n-triples"
                            "text/n3"
                            "text/x-bibliography"
                            "text/bibliography"
                            "text/plain"
                            "application/x-research-info-systems"
                            "application/x-bibtex"]]
        (is (= 406 (status-h "10.35802/215573" content-type)))
        (is (= 406 (status-q "10.35802/215573" content-type)))))))
    
(deftest ^:integration transforming-dois
 (testing "bibtex response contains DOI's metadata"
   (setup-api)
   (index-work-files ["dev-resources/metadata-cases/container-title.body"
                      "dev-resources/metadata-cases/event.body"
                      "dev-resources/metadata-cases/link-2.body"])
   (flush-elastic)
   (doseq [[key doi container] [["Khouri_2014" "10.1097/prs.0000000000000561" "Plastic and Reconstructive Surgery"]
                                ["Martel_2008" "10.1115/imece2008-69048" "Volume 10: Heat Transfer, Fluid Flows, and Thermal Systems, Parts A, B, and C"]
                                ["Bj_rkdahl_2018" "10.1007/978-981-13-2802-2_4" "Pandemics, Publics, and Politics"]]
           response [(api-get-raw
                        (str "/v1/works/" doi "/transform/application/x-bibtex"))
                     (api-get-raw
                       (str "/v1/works/" doi "/transform")
                       {:headers {"accept" "application/x-bibtex"}})]]
     (is (str/includes? response key))
     (is (str/includes? response doi))
     (is (str/includes? response container)))))

(deftest ^:integration check-abstract
  (testing "empty abstracts are not indexed"
    (setup-api)
    (index-work-files ["dev-resources/metadata-cases/abstract.body"
                       "dev-resources/metadata-cases/abstract2.body"])
    (flush-elastic)
    (doseq [[doi expected-number] [["10.1055/a-0720-5309" 0]["10.1039/d0tc00148a" 1]]]
      (let [query (str "/v1/works?filter=doi:" doi ",has-abstract:1")
            response (:total-results (api-get query))]
        (is (= expected-number response))))))

(deftest ^:integration retrieving-xml

  (with-redefs [httpkit-client/get (fn [_ _] (atom {:body "<xml></xml>"}))]
    (testing "input DOI case (upper/lower) does not affect retrieving items"
      (setup-api)
      (index-work-files ["dev-resources/doi-case/doi.body"
                         "dev-resources/doi-case/doi-with-plus.body"])
      (flush-elastic)
      (doseq [doi ["10.1016/j.explore.2016.10.009"
                   "10.1016/j.EXPLORE.2016.10.009"
                   "10.1021/jo070268+"
                   "10.1021/JO070268+"]]
        (let [response (http/get
                         (str api-root "/v1/works/" doi "/transform")
                         {:accept "application/vnd.crossref.unixsd+xml"
                          :throw-exceptions false})]
          (is (= 200 (:status response))))))))

