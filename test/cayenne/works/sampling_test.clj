(ns cayenne.works.sampling-test
  (:require [cayenne.api-fixture :refer [api-fixture setup-api api-get]]
            [cayenne.data.work :as work]
            [cayenne.elastic.util :as elastic-util]
            [cayenne.conf :as conf]
            [clojure.test :refer [use-fixtures deftest testing is]]
            [qbits.spandex :as elastic]
            [user :refer [flush-elastic]]))

(defn index-sample-data []
  (let [ids (concat (range 1 10) (range 11 25) (range 29 68) (range 70 100))
        work-data (mapcat
                    #(vector
                       {:index {:_id (str "10.5555/test" %)}}
                       {:doi (str "10.5555/test" %) :citation-id %})
                    ids)
        work-request {:method :post
                      :url (str (elastic-util/index-url-prefix :work) "_bulk")
                      :body (elastic-util/raw-jsons work-data)}]
    (elastic/request (conf/get-service :elastic) work-request)
    (flush-elastic)))

(deftest ^:integration sampling-works
  (index-sample-data)

  (testing "correct max citation id is retrieved from ES"
    (is (= 99 (work/max-citation-id))))

  (testing "citation-id sample of correct size is drawn"
    (is (= 10 (count (work/citation-id-sample 10))))
    (is (= 99 (count (work/citation-id-sample 150)))))
  
  (testing "a sample is retrieved if chosen citation ids contain enough valid values"
    (with-redefs [work/citation-id-sample (constantly [10 25 3 70 68])]
      (is (= 2 (->> "/v1/works?sample=2" api-get :items count))))
    
    (with-redefs [work/citation-id-sample (constantly [1 25 3 70 65])]
      (is (= 2(->> "/v1/works?sample=2" api-get :items count)))))
    
  (testing "a sample is retrieved in the second attempt"
    (with-redefs [work/citation-id-sample #(if (= % 5)
                                             [10 25 28 70 68]
                                             [10 20 30 40 50 60 70 80 90 1])]
      (is (= 2(->> "/v1/works?sample=2" api-get :items count)))))
    
  (testing "a sample is retrieved by the original process if all attempts fail"
    (with-redefs [work/citation-id-sample (constantly [10 25 28 70 68])]
      (is (= 4 (->> "/v1/works?sample=4" api-get :items count))))))

(use-fixtures
  :once
  (api-fixture setup-api))
