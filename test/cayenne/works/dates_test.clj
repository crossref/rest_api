(ns cayenne.works.dates-test
  (:require [cayenne.api-fixture :refer [api-get api-fixture setup-api]]
            [cayenne.conf :as conf]
            [cayenne.elastic.util :as elastic-util]
            [clojure.test :refer [use-fixtures deftest testing is]]
            [qbits.spandex :as elastic]
            [user :refer [flush-elastic]]))

(defn index-test-doc
  ([doc]
   (setup-api)
   (elastic/request
     (conf/get-service :elastic)
     {:method :post
      :url (str (elastic-util/index-url-prefix :work) "_doc")
      :body (assoc doc :doi "10.5555/test")})
   (flush-elastic))
  ([field date]
   (index-test-doc {field date})))

(deftest ^:integration displaying-dates

  (doseq [[date-parts date-str] [[[2018] "2018"]
                                 [[2018 4] "2018-04"]
                                 [[2018 11 3] "2018-11-03"]
                                 [[2018 4 19] "2018-04-19"]
                                 [[2018 12 31] "2018-12-31"]]]

    (testing "basic dates are properly displayed"
      (doseq [field [:issued :published :published-print :published-online :published-other
                     :posted :accepted :content-created :content-updated :approved]]
        (index-test-doc field date-str)

        (let [item (-> "/v1/works" api-get :items first)]
          (is (= {:date-parts [date-parts]} (field item))))))

    (testing "event dates are properly displayed"
      (index-test-doc {:event {:start date-str :end date-str}})

      (let [item (-> "/v1/works" api-get :items first)]
        (is (= {:start {:date-parts [date-parts]} :end {:date-parts [date-parts]}}
               (:event item)))))

    (testing "journal issue dates are properly displayed"
      (index-test-doc {:journal-issue {:issue 5 :published-print date-str :published-online date-str}})

      (let [item (-> "/v1/works" api-get :items first)]
        (is (= {:issue 5 :published-print {:date-parts [date-parts]} :published-online {:date-parts [date-parts]}}
               (:journal-issue item)))))))

(use-fixtures
  :once
  (api-fixture setup-api))
