(ns cayenne.works.facet-test
  (:require [cayenne.api-fixture :refer [api-fixture setup-api setup-test-works
                                         setup-test-journals api-get]]
            [clojure.string :as str]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(deftest ^:integration facet-counts

  (testing "faceting returns buckets"
    (doseq [[facet value] [[:affiliation "10"]
                           [:affiliation "*"]
                           [:funder-name "10"]
                           [:funder-name "*"]
                           [:container-title "10"]
                           [:assertion "10"]
                           [:assertion "*"]
                           [:archive "10"]
                           [:archive "*"]
                           [:update-type "10"]
                           [:update-type "*"]
                           [:published "10"]
                           [:published "*"]
                           [:license "10"]
                           [:license "*"]
                           [:relation-type "10"]
                           [:relation-type "*"]
                           [:assertion-group "10"]
                           [:assertion-group "*"]
                           [:publisher-name "10"]
                           [:publisher-name "*"]
                           [:ror-id "10"]
                           [:ror-id "*"]]]
      (is (seq (-> (str "/v1/works?facet=" (name facet) ":" value)
                   api-get
                   :facets
                   facet
                   :values))))

    (doseq [[facet value] [[:category-name "10"]
                           [:category-name "*"]]]
      (is (not (seq (-> (str "/v1/works?facet=" (name facet) ":" value)
                   api-get
                   :facets
                   facet
                   :values))))))

  (testing "facet values are of right format"
    (doseq [[facet prefix] [[:orcid "https://orcid.org/"]
                            [:issn "https://id.crossref.org/issn/"]
                            [:funder-doi "https://doi.org/"]]]
      (let [facets (->> (str "/v1/works?facet=" (name facet) ":10")
                        api-get
                        :facets
                        facet
                        :values
                        keys
                        (map str))]
        (is (seq facets))
        (is (every? #(str/starts-with? % (str ":" prefix)) facets)))))

  (testing "type-name facet values are correct"
    (let [type-names (->> "/v1/works?facet=type-name:*"
                          api-get
                          :facets
                          :type-name
                          :values
                          keys
                          (map name))]
      (is (seq type-names))
      (is (some #{"Journal Article"} type-names))
      (is (some #{"Chapter"} type-names))))

  (testing "source facet values are correct"
    (let [source-names (->> "/v1/works?facet=source:*"
                            api-get
                            :facets
                            :source
                            :values
                            keys
                            (map name))]
      (is (seq source-names))
      (is (some #{"Crossref"} source-names))))

  (testing "faceting on grants returns buckets"
    (doseq [[facet value] [[:affiliation "*"]
                           [:funder-name "*"]
                           [:funder-doi "*"]
                           [:orcid "*"]]]
      (let [buckets (-> (str "/v1/works?filter=type:grant&facet=" (name facet) ":" value)
                        api-get
                        :facets
                        facet
                        :values)]
        (is (seq buckets))))))

(use-fixtures
  :once
  (api-fixture setup-api setup-test-journals setup-test-works))
