(ns cayenne.dates-test
  (:require [cayenne.dates :refer [parse-month parse-date]]
            [clojure.test :refer [deftest testing is]]))

(def date-cases
  "Examples of input dates ranging from ok to missing parts, to having erroneous values"
  [[2023 01 01 {:date-parts [2023 1 1], :date-time "2023-01-01T00:00:00.000Z", :timestamp 1672531200000, :source "date-parts"}]
   [2023 35 01 {:date-parts [2023], :date-time "2023-01-01T00:00:00.000Z", :timestamp 1672531200000, :source "date-parts"}]
   [2023 02 31 {:date-parts [2023 2 31], :date-time "2023-02-01T00:00:00.000Z", :timestamp 1675209600000, :source "date-parts"}]
   [0000 01 01 nil]
   [2023 01 nil {:date-parts [2023 1], :date-time "2023-01-01T00:00:00.000Z", :timestamp 1672531200000, :source "date-parts"}]
   [2023 nil nil {:date-parts [2023], :date-time "2023-01-01T00:00:00.000Z", :timestamp 1672531200000, :source "date-parts"}]
   [nil nil nil nil]
   [2023 nil 12 {:date-parts [2023], :date-time "2023-01-01T00:00:00.000Z", :timestamp 1672531200000, :source "date-parts"}]
   [nil 01 12 nil]
   [nil nil 12 nil]])

((deftest ^:unit parse-month-test
   (testing "parse-month should return integer value when 1 <= month <= 34 or nil otherwise"
     (is (= nil (parse-month -1)))
     (is (= nil (parse-month 0)))
     (is (= 1 (parse-month 1)))
     (is (= 34 (parse-month 34)))
     (is (= nil (parse-month 35))))))

((deftest ^:unit parse-date-test
   (testing "Verify expected behavior for various date issues."
     (dorun
      (map #(let [[y m d expected] %]
              (is (= expected (parse-date y m d))))
           date-cases)))))