(ns cayenne.state-test
  (:require [cayenne.conf :as conf]
            [cayenne.state :as st]
            [cayenne.ingest.s3-storage :as s3-storage]
            [clojure.test :refer [testing is deftest]]))

(conf/set-param! [:s3 :cayenne-state :bucket] "md-bucket")
(conf/set-param! [:s3 :cayenne-state :key] "config.edn")
(conf/set-param! [:s3 :metadata-storage-service]
                 (s3-storage/->S3 "http://localstack-integration:4566" {:path-style-access-enabled true}))

(deftest ^:integration state-manipulation
  (let [storage-service (conf/get-param [:s3 :metadata-storage-service])]

    (testing "Setting cayenne state :var = 1"
      (st/set-cayenne-state [:var] 1)
      (is (= (st/get-cayenne-state [:var]) 1)))

    (testing "random var should return nil"
      (is (= (st/get-cayenne-state [:random]) nil)))))
