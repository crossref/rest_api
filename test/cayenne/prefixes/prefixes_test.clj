(ns cayenne.prefixes.prefixes-test
  (:require [cayenne.api-fixture :refer [api-get api-get-status api-fixture setup-api
                                         setup-test-members setup-test-works]]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(deftest ^:integration retrieving-prefixes

  (testing "prefixes endpoint returns expected result"
    (is (= {:member "https://id.crossref.org/member/1965"
            :name "Frontiers Media SA"
            :prefix "https://id.crossref.org/prefix/10.3389"}
           (api-get "/v1/prefixes/10.3389")))
    (is (= {:member "https://id.crossref.org/member/3820"
            :name "Stellenbosch University"
            :prefix "https://id.crossref.org/prefix/10.14804"}
           (api-get "/v1/prefixes/10.14804"))))

  (testing "retrieving a non-existent prefix returns 404"
    (println (api-get "/v1/prefixes/10.14804/works"))
    (is (= 404 (api-get-status "/v1/prefixes/nonexistent")))
    (is (= 404 (api-get-status "/v1/prefixes/10.9999")))
    (is (= 404 (api-get-status "/v1/prefixes/nonexistent/works")))
    (is (= 404 (api-get-status "/v1/prefixes/10.9999/works")))))

(deftest ^:intgration filtering-works
  (testing "the same number of works is returned through /prefixes/<id>/works route and the filter"
    (let [filter-total (-> "/v1/works?filter=prefix:10.1016" api-get :total-results)
          route-total (-> "/v1/prefixes/10.1016/works" api-get :total-results)
          route-total-0-rows (-> "/v1/prefixes/10.1016/works?rows=0" api-get :total-results)]
      (is (> filter-total 0))
      (is (= filter-total route-total))
      (is (= filter-total route-total-0-rows)))))

(use-fixtures
  :once
  (api-fixture setup-api setup-test-members setup-test-works))
