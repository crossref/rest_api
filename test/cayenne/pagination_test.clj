(ns cayenne.pagination-test
  (:require [cayenne.api-fixture :refer [api-get api-fixture setup-api]]
            [cayenne.conf :as conf]
            [cayenne.elastic.util :as elastic-util]
            [clojure.test :refer [use-fixtures deftest testing is]]
            [clojure.string :as str]
            [qbits.spandex :as elastic]
            [user :refer [flush-elastic]]))

(defn get-item-range [query start size]
  (-> (str query "rows=" size "&offset=" start)
      api-get
      :items))

(defn iterate-offset [query start step times]
  (let [end (+ start (* step times))
        offsets (range start end step)]
    (mapcat #(get-item-range query % step) offsets)))

(defn iterate-cursor [query step]
  (loop [cursor "*"
         acc-items []]
    (let [{:keys [items next-cursor]} (api-get (str query "rows=" step "&cursor=" cursor))
          nums (map #(-> % :DOI (str/split #"/") last Integer/parseInt) items)
          ext-items (into acc-items nums)]
      (if (empty? items)
        ext-items
        (recur next-cursor ext-items)))))

(deftest ^:integration pagination-works
  (let [chunk-size 200
        total 1000000
        offsets (range 0 total chunk-size)
        index-element (fn [id] (let [doi (str "https://doi.org/10.5555/" id)]
                                 [{:index {:_id doi}} {:doi doi}]))
        bulk-index-query (fn [i]
                           (let [ids (range i (+ i chunk-size) 1)
                                 body (mapcat index-element ids)]
                             {:method :post
                              :url (str (elastic-util/index-url-prefix :work) "_bulk")
                              :body (elastic-util/raw-jsons body)}))]
    ; index 1M small documents in ES
    (doseq [offset offsets]
      (elastic/request (conf/get-service :elastic) (bulk-index-query offset)))
    (flush-elastic)
    
    (testing "iteration over works using offset retrieves full item set"
      (let [items (iterate-offset "/v1/works?" 0 200 50)]
        (is (= 10000 (count items)))
        (is (= 10000 (count (set items))) "items should be distinct")))
        
    (testing "iteration over works using cursors retrieves full item set"
      (let [items (iterate-cursor "/v1/works?" 500)]
        (is (= total (count items)))
        (is (= total (count (set items))) "items should be distinct")))))

(use-fixtures
  :once
  (api-fixture setup-api))
