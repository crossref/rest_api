(ns cayenne.ids.ror-id-test
  (:require [clojure.test :refer [deftest testing is]]
            [cayenne.ids.ror-id :refer [normalize-ror-id to-ror-id-uri]]))

(deftest ^:unit normalize-ror-id-test
  (testing "normalize-ror-id returns nil for nil or malformed ror id"
    (is (nil? (normalize-ror-id nil)))
    (is (nil? (normalize-ror-id "nil"))))
  
  (testing "normalize-ror-id extracts short id from uri"
    (is (= "04a1a1e81" (normalize-ror-id "https://ror.org/04a1a1e81")))
    (is (= "04a1a1e81" (normalize-ror-id "http://ror.org/04a1a1e81")))
    (is (= "04a1a1e81" (normalize-ror-id "ror.org/04a1a1e81")))
    (is (= "04a1a1e81" (normalize-ror-id "/04a1a1e81")))
    (is (= "04a1a1e81" (normalize-ror-id "04a1a1e81")))))

(deftest ^:unit to-ror-id-uri-test
  (testing "to-ror-id-uri returns nil for nil or malformed ror id"
    (is (nil? (to-ror-id-uri nil)))
    (is (nil? (to-ror-id-uri "nil"))))
  
  (testing "to-ror-id-uri transforms to uri"
    (is (= "https://ror.org/04a1a1e81" (to-ror-id-uri "https://ror.org/04a1a1e81")))
    (is (= "https://ror.org/04a1a1e81" (to-ror-id-uri "http://ror.org/04a1a1e81")))
    (is (= "https://ror.org/04a1a1e81" (to-ror-id-uri "ror.org/04a1a1e81")))
    (is (= "https://ror.org/04a1a1e81" (to-ror-id-uri "/04a1a1e81")))
    (is (= "https://ror.org/04a1a1e81" (to-ror-id-uri "04a1a1e81")))))
