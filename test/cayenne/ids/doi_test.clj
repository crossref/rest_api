(ns cayenne.ids.doi-test
  (:require [clojure.test :refer [deftest testing is]]
            [cayenne.ids.doi :refer [to-doi-uri]]))

(deftest ^:unit to-doi-uri-test
  (testing "to-doi-uri returns nil for nil or malformed DOI"
    (is (nil? (to-doi-uri nil)))
    (is (nil? (to-doi-uri "nil"))))

  (testing "to-doi-uri transforms to uri"
    (let [uri "https://doi.org/10.1007/s11302-016-9551-2"]
      (is (= uri (to-doi-uri "https://dx.doi.org/10.1007/s11302-016-9551-2")))
      (is (= uri (to-doi-uri "http://dx.doi.org/10.1007/s11302-016-9551-2")))
      (is (= uri (to-doi-uri "https://doi.org/10.1007/s11302-016-9551-2")))
      (is (= uri (to-doi-uri "http://doi.org/10.1007/s11302-016-9551-2")))
      (is (= uri (to-doi-uri "dx.doi.org/10.1007/s11302-016-9551-2")))
      (is (= uri (to-doi-uri "doi.org/10.1007/s11302-016-9551-2")))
      (is (= uri (to-doi-uri "10.1007/s11302-016-9551-2")))))
  
  (testing "to-doi-uri does not remove characters"
    (let [uri "https://doi.org/10.2741/ortéga"]
      (is (= uri (to-doi-uri "https://dx.doi.org/10.2741/Ortéga")))
      (is (= uri (to-doi-uri "10.2741/Ortéga"))))))
