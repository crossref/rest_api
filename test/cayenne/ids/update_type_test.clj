(ns cayenne.ids.update-type-test
  (:require [clojure.test :refer [deftest testing is]]
            [cayenne.ids.update-type :refer :all]))

(deftest ^:unit label-dictionary-test
  (testing "Types and labels maps in both directions"
    (let [k (-> update-type-dictionary keys set)
          v (vals update-type-dictionary)]

      (is (= true (every? #(-> label-dictionary (get %) k) v)))


      (is (= "erratum" (label->type "Erratum")))
      (is (= "expression_of_concern" (label->type "Expression of concern"))))))

