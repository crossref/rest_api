(ns cayenne.coverage-test
  (:require [cayenne.api-fixture :refer [api-fixture setup-api api-get frozen-time]]
            [cayenne.conf :as conf]
            [cayenne.elastic.util :as elastic-util]
            [cayenne.tasks.coverage :refer [check-members check-journals]]
            [clojure.test :refer [use-fixtures deftest testing is]]
            [qbits.spandex :as elastic]
            [clj-time.core :as clj-time]
            [user :refer [flush-elastic index-work-files]]))

(defn index-request [index data]
  {:method :post
   :url (str (elastic-util/index-url-prefix index) "_bulk")
   :body (elastic-util/raw-jsons data)})

(defn coverage-docs-ids []
  (->> {:method :get
        :url (str (elastic-util/index-url-prefix :coverage) "_search")
        :body {:query {:match_all {}}}}
       (elastic/request (conf/get-service :elastic))
       (#(get-in % [:body :hits :hits]))
       (map :_id)))

(deftest ^:integration calculating-coverage
  (let [member-data [{:index {:_id "1"}}
                     {:id "1"
                      :primary-name "My Name"
                      :location "Loc"
                      :prefix [{:member-id "1" :name "" :value "10.5555"}
                               {:member-id "1" :name "" :value "10.5556"}]}]
        journal-data [{:index {:_id "1"}}
                      {:id "1"
                       :name "My Journal"
                       :issn [{:type "print" :value "0101-0101"}]}]
        work-data [{:index {:_id "10.5555/test1"}}
                   {:doi "10.5555/test1" :member-id "1" :journal-id "1" :owner-prefix "10.5555"}]]
    (elastic/request (conf/get-service :elastic) (index-request :member member-data))
    (elastic/request (conf/get-service :elastic) (index-request :journal journal-data))
    (elastic/request (conf/get-service :elastic) (index-request :work work-data))
    (flush-elastic)
    (testing "before calculating coverage index is empty"
      (is (empty? (coverage-docs-ids))))

    (check-journals)
    (check-members)
    (flush-elastic)
    (testing "coverage docs for all journals and members are indexed"
      (is (= 2 (count (coverage-docs-ids))))
      (is (= #{"journal-1" "member-1"} (set (coverage-docs-ids)))))

    (check-journals)
    (check-members)
    (flush-elastic)
    (testing "after additional coverage generation, there is still only one coverage doc for every journal and member"
      (is (= 2 (count (coverage-docs-ids))))
      (is (= #{"journal-1" "member-1"} (set (coverage-docs-ids)))))))

(deftest ^:integration calculating-counts-type
  (testing ":all contains all DOIs, both with null and non-null issued date"
    (index-work-files ["dev-resources/coverage/issued-null.body"
                       "dev-resources/coverage/issued-not-null.body"])
    (elastic/request
      (conf/get-service :elastic)
      (index-request :member [{:index {:_id "5626"}}
                              {:id "5626"
                               :primary-name "My Name"
                               :location "Loc"
                               :prefix []}]))
    (flush-elastic)
    (clj-time/do-at
      frozen-time
      (check-members))
    (flush-elastic)
    
    (is (= {:all {:report-component 1 :report 1} :current {:report 1} :backfile {}}
           (-> "/members" api-get :items first :counts-type)))))

(deftest ^:integration calculating-coverage-fractions
  (testing ":resource-links coverage does not count in similarity checking links"
    (index-work-files ["dev-resources/coverage/full-text-text-mining.body"
                       "dev-resources/coverage/full-text-similarity-checking.body"
                       "dev-resources/coverage/full-text-unspecified.body"])
    (elastic/request
      (conf/get-service :elastic)
      (index-request :member [{:index {:_id "138"}}
                              {:id "138"
                               :primary-name "My Name"
                               :location "Loc"
                               :prefix []}]))
    (flush-elastic)
    (clj-time/do-at
      frozen-time
      (check-members))
    (flush-elastic)
    (let [res-links (-> "/members" api-get :items first :coverage :resource-links-current)]
      (is (< (Math/abs (- res-links 0.66666667)) 0.00001)))))

(deftest ^:integration calculating-journal-coverage
  (testing "all DOIs with print or electronic ISSNs are included in journal coverage"
    (index-work-files ["dev-resources/coverage/journal-3074-issns.body"
                       "dev-resources/coverage/journal-3074-pissn.body"
                       "dev-resources/coverage/journal-3074-eissn.body"])
    (elastic/request
      (conf/get-service :elastic)
      (index-request :journal [{:index {:_id "3074"}}
                               {:id "3074"
                                :name "My Journal"
                                :issn [{:type "print" :value "0007-4888"}
                                       {:type "electronic" :value "1573-8221"}]}]))
    (flush-elastic)
    (clj-time/do-at
      frozen-time
      (check-journals))
    (flush-elastic)
    (is (= {:current-dois 0 :backfile-dois 3 :total-dois 3}
           (-> "/journals/0007-4888" api-get :counts)))
    (is (= {:current-dois 0 :backfile-dois 3 :total-dois 3}
           (-> "/journals/1573-8221" api-get :counts))))
  
  (testing "DOIs with journal's additional ISSNs are not included in journal coverage"
    (index-work-files ["dev-resources/coverage/journal-27878-issn.body"
                       "dev-resources/coverage/journal-27878-additional-issn.body"])
    (elastic/request
      (conf/get-service :elastic)
      (index-request :journal [{:index {:_id "27878"}}
                               {:id "27878"
                                :name "My Journal"
                                :issn [{:type "print" :value "1762-584X"}]}]))
    (flush-elastic)
    (clj-time/do-at
      frozen-time
      (check-journals))
    (flush-elastic)
    (is (= {:current-dois 0 :backfile-dois 1 :total-dois 1}
           (-> "/journals/1762-584X" api-get :counts)))))

(deftest ^:integration calculating-coverage-type
  (testing ":all contains all DOIs, both with null and non-null issued date"
    (index-work-files ["dev-resources/coverage/issued-null.body"
                       "dev-resources/coverage/issued-not-null.body"])
    (elastic/request
      (conf/get-service :elastic)
      (index-request :member [{:index {:_id "5626"}}
                              {:id "5626"
                               :primary-name "My Name"
                               :location "Loc"
                               :prefix [{:member-id "5626" :name "" :value "10.14509"}]}]))
    (flush-elastic)
    (clj-time/do-at
      frozen-time
      (check-members))
    (flush-elastic)
    
    (let [coverage-type-all (-> "/members" api-get :items first :coverage-type :all)]
      (is (= 0.0 (-> coverage-type-all :report-component :descriptions)))
      (is (= 0.0 (-> coverage-type-all :report :descriptions))))))

(deftest ^:integration grants-coverage
  (testing "coverage is correctly calculated for grants"
    (index-work-files ["dev-resources/coverage/grant.body"
                       "dev-resources/coverage/grant-2.body"])
    (elastic/request
      (conf/get-service :elastic)
      (index-request :member [{:index {:_id "13928"}}
                              {:id "13928"
                               :primary-name "Wellcome"
                               :location "Loc"
                               :prefix [{:member-id "13928" :name "" :value "10.35802"}]}]))

    (flush-elastic)
    (clj-time/do-at
      frozen-time
      (check-members))
    (flush-elastic)
    
    (let [member (-> "/members" api-get :items first)
          coverage (:coverage member)
          cov-grant-all (-> member :coverage-type :all :grant)
          cov-grant-current (-> member :coverage-type :current :grant)
          cov-grant-backfile (-> member :coverage-type :backfile :grant)]
      
      (is (= {:total-dois 2 :current-dois 1 :backfile-dois 1} (:counts member)))
      (is (= {:all {:grant 2} :current {:grant 1} :backfile {:grant 1}} (:counts-type member)))
      (is (= #{[2019 1] [2015 1]} (set (:dois-by-issued-year (:breakdowns member)))))
      
      (is (= 1.0 (:funders-current coverage)))
      (is (= 1.0 (:funders-backfile coverage)))
      (is (= 1.0 (:funders cov-grant-all)))
      (is (= 1.0 (:funders cov-grant-current)))
      (is (= 1.0 (:funders cov-grant-backfile)))
      
      (is (= 1.0 (:award-numbers-current coverage)))
      (is (= 1.0 (:award-numbers-backfile coverage)))
      (is (= 1.0 (:award-numbers cov-grant-all)))
      (is (= 1.0 (:award-numbers cov-grant-current)))
      (is (= 1.0 (:award-numbers cov-grant-backfile)))
    
      (is (= 1.0 (:affiliations-current coverage)))
      (is (= 0.0 (:affiliations-backfile coverage)))
      (is (= 0.5 (:affiliations cov-grant-all)))
      (is (= 1.0 (:affiliations cov-grant-current)))
      (is (= 0.0 (:affiliations cov-grant-backfile)))
    
      (is (= 1.0 (:orcids-current coverage)))
      (is (= 0.0 (:orcids-backfile coverage)))
      (is (= 0.5 (:orcids cov-grant-all)))
      (is (= 1.0 (:orcids cov-grant-current)))
      (is (= 0.0 (:orcids cov-grant-backfile)))
      
      (is (= 1.0 (:ror-ids-current coverage)))
      (is (= 0.0 (:ror-ids-backfile coverage)))
      (is (= 0.5 (:ror-ids cov-grant-all)))
      (is (= 1.0 (:ror-ids cov-grant-current)))
      (is (= 0.0 (:ror-ids cov-grant-backfile)))
    
      (is (= 1.0 (:descriptions-current coverage)))
      (is (= 0.0 (:descriptions-backfile coverage)))
      (is (= 0.5 (:descriptions cov-grant-all)))
      (is (= 1.0 (:descriptions cov-grant-current)))
      (is (= 0.0 (:descriptions cov-grant-backfile))))))

(deftest ^:integration empty-description
  (testing "empty descriptions are not included in coverage statistics"
    (index-work-files ["dev-resources/coverage/grant-empty-description.body"
                       "dev-resources/coverage/description-empty.body"])
    (elastic/request
      (conf/get-service :elastic)
      (index-request :member [{:index {:_id "30016"}}
                              {:id "30016"
                               :primary-name "The ALS Association"
                               :location "Loc"
                               :prefix [{:member-id "30016" :name "" :value "10.52546"}]}
                              {:index {:_id "2434"}}
                              {:id "2434"
                               :primary-name "SciVee, Inc"
                               :location "Loc"
                               :prefix [{:member-id "2434" :name "" :value "10.4016"}]}]))

    (flush-elastic)
    (clj-time/do-at
      frozen-time
      (check-members))
    (flush-elastic)

    (doseq [member (-> "/members" api-get :items)]
      (let [coverage (:coverage member)]
        (is (= 0.0 (:descriptions-current coverage)))
        (is (= 0.0 (:descriptions-backfile coverage)))))))

(use-fixtures
  :each
  (api-fixture setup-api))
