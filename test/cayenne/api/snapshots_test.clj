(ns cayenne.api.snapshots-test
  (:require [cayenne.api-fixture :refer [api-get-raw api-fixture setup-api]]
            [cayenne.conf :as conf]
            [cayenne.ingest.local-storage :as storage]
            [clojure.string :as str]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(deftest ^:integration test-snapshots-endpoint
  (conf/set-param! [:s3 :snapshot-storage-service] (storage/->LocalS3))
  (conf/set-param! [:s3 :snapshot-bucket] "dev-resources/snapshots")
  (conf/set-param! [:s3 :snapshot-bucket-ttl-mins] 0)
  (conf/set-param! [:service :api-key-auth] false)

  (testing "root endpoint returns expected response"
    (is (str/includes? (api-get-raw "/snapshots") "<a href='/snapshots/monthly'>monthly</a>")))

  (testing "monthly endpoint returns expected response"
    (let [response (api-get-raw "/snapshots/monthly")]
      (is (str/includes? response "<a href='/snapshots/monthly/latest'>latest</a>"))
      (is (str/includes? response "<a href='/snapshots/monthly/2019'>2019</a>"))
      (is (str/includes? response "<a href='/snapshots/monthly/2020'>2020</a>"))))

  (testing "monthly 2020 endpoint returns expected response"
    (let [response (api-get-raw "/snapshots/monthly/2020")]
      (is (str/includes? response "<a href='/snapshots/monthly/2020/01'>01</a>"))
      (is (str/includes? response "<a href='/snapshots/monthly/2020/02'>02</a>"))))

  (testing "monthly latest endpoint returns expected response"
    (is (str/includes?
          (api-get-raw "/snapshots/monthly/latest")
          "<a href='/snapshots/monthly/2020/02/all.json.tar.gz'>all.json.tar.gz</a>")))

  (testing "monthly 2020/01 endpoint returns expected response"
    (is (str/includes?
          (api-get-raw "/snapshots/monthly/2020/01")
          "<a href='/snapshots/monthly/2020/01/all.json.tar.gz'>all.json.tar.gz</a>"))
    (is (str/includes?
          (api-get-raw "/snapshots/monthly/2020/01")
          "<a href='/snapshots/monthly/2020/01/all.xml.tar.gz'>all.xml.tar.gz</a>"))))

(use-fixtures :once (api-fixture setup-api))
