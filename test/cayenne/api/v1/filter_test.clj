(ns cayenne.api.v1.filter-test
  (:require [cayenne.api.v1.filter :refer [obj-date]]
            [clj-time.core :as dt]
            [clojure.test :refer [deftest testing is]]))

(deftest ^:unit obj-date-creation
  (testing "date ranges are correctly constructed"
    
    (is (= (dt/date-time 2013 4 17 0 0 0 0)
           (obj-date "2013-04-17" :direction :from)))
    (is (= (dt/date-time 2013 4 1 0 0 0 0)
           (obj-date "2013-04" :direction :from)))
    (is (= (dt/date-time 2013 1 1 0 0 0 0)
           (obj-date "2013" :direction :from)))
    
    (is (= (dt/date-time 2013 4 17 23 59 59 999)
           (obj-date "2013-04-17" :direction :until)))
    (is (= (dt/date-time 2013 4 30 23 59 59 999)
           (obj-date "2013-04" :direction :until)))
    (is (= (dt/date-time 2013 12 31 23 59 59 999)
           (obj-date "2013" :direction :until)))))

