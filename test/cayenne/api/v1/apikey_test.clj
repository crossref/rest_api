(ns cayenne.api.v1.apikey-test
  (:require [cayenne.api-fixture :refer [api-get api-get-status api-fixture setup-api]]
            [cayenne.conf :as conf]
            [cayenne.ingest.local-storage :as storage]
            [clojure.test :refer [use-fixtures deftest testing is]]
            [cayenne.api.auth.apikey :as apikey]))

(deftest ^:integration apikey-test

  (conf/set-param! [:service :api-key-auth] true)

  (conf/set-param! [:service :keycloak :url] "http://keycloak:8888/auth")
  (conf/set-param! [:service :keycloak :client-secret] "**********")
  (conf/set-param! [:service :keycloak :api-key-salt] "")
  (conf/set-param! [:service :keycloak :realm] "crossref")
  (conf/set-param! [:service :keycloak :client] "crossref-resources")

  (testing "api returns expected response with no api-key"
    (is (= (->> (str "/v1/works/")
                api-get)
           "Access Denied"))
    (is (= (->> (str "/v1/works/")
                api-get-status)
           401)))

  (testing "api returns expected response with invalid api-key"
    (is (= (-> (str "/v1/works/")
               (api-get {:headers {"authorization" "Bearer invalidToken1"}}))
           "Access Denied"))
    (is (= (-> (str "/v1/works/")
               (api-get-status {:headers {"authorization" "Bearer invalidToken1"}}))
           401)))

  (testing "api returns expected response with valid api-key"
    (with-redefs [apikey/check-keycloak-permission #(if (= %2 "VALIDTOKEN") true false)
                  apikey/auth-interface true]
      (is (= (api-get-status "/v1/works/" {:headers {"authorization" "Bearer VALIDTOKEN"}})
             200))
      (is (= (api-get-status "/v1/works/" {:headers {"crossref-Plus-API-token" "Bearer VALIDTOKEN"}})
             200))
      (is (= (api-get-status "/v1/works/" {:headers {"crossref-Api-kEy" "VALIDTOKEN"}})
             200))
      (is (= (api-get-status "/v1/works/" {:headers {"krossref-Api-kEy" "VALIDTOKEN"}})
             401))
      (is (= (api-get-status "/v1/works/" {:headers {"crossref-Api-kEy" "INVALIDTOKEN"}})
             401))
      ))

  (testing "api returns 401 for bad route and valid api-key"
    (is (= (api-get-status "/v1/aaaaa/" {:headers {"authorization" "Bearer testToken1"}})
           401)))

  (testing "api returns 401 for bad route and no api-key"
    (is (= (api-get-status "/v1/aaaaa/")
           401))))

(use-fixtures :once (api-fixture setup-api))
(use-fixtures
  :each
  (fn [f]
    (conf/set-param! [:service :api-key-auth] true)
    (f)
    (conf/set-param! [:service :api-key-auth] false)))
