(ns cayenne.api.v1.output-regression-test
  "Regression tests for the cayenne.elastic.convert conversion from es JSON
   to citeproc JSON."
  (:require [clojure.test :refer [deftest is testing]]
            [cayenne.elastic.convert :as convert]
            [clojure.java.io :as io]
            [clojure.data.json :as json]
            [clj-time.core :as clj-time]
            [cheshire.core :refer [generate-stream]])
  (:import [java.io PushbackReader]))

(def input-dir (io/file "dev-resources/convert-regression/es-json"))

(def output-dir "dev-resources/convert-regression/citeproc-json")

; Results are time-dependent, so freeze the time when generating and comparing result files.
(def now (clj-time/date-time 2019 1 1))

(defn es-json->citeproc-filename
  "Produce an citeproc JSON filename from an es JSON filename."
  [file]
  (io/file output-dir (.replaceAll (.getName file) "\\.es.json" ".citeproc.json")))

(defn input-result-files
  "Given a directory of es JSON files, return a sequence of
   [es json input file, citeproc json output file]."
  []
  (keep
   (fn [es-json-file]
     (let [json-file (es-json->citeproc-filename es-json-file)]
       (when (-> es-json-file .getName (.endsWith ".es.json"))
         [es-json-file json-file])))
   (->> (file-seq input-dir)
        (filter #(.isFile %)))))

(defn missing-result-files
  "Sequence of [es json file, citeproc json files] when the output file doesn't exist."
  []
  (->> (input-result-files)
       (remove #(-> % second .exists))))

(deftest ^:unit  all-inputs-should-have-outputs
  ; Make sure no data is left behind to become irrelevant.
  (testing "Every input es JSON file should have a corresponding citeproc JSON output file."
             (is (empty? (missing-result-files))
                 (str "Found orphaned es JSON files without citeproc JSON files. Consider running generate-result-files."))))

(defn es-doc->citeproc [es-doc] (convert/es-doc->citeproc {:_source es-doc}))

(defn generate-result-files
  "Manual function used when writing tests.
   Scan inputs and generate result. These are then checked in and
   used for future regression tests.
   You should review the resulting files for correctness!"
  []
  (clj-time/do-at now
   (let [missing-files (missing-result-files)]
     (println "Found" (count missing-files) "missing files to generate.")
     (doseq [[in-file out-file] missing-files] 
       (let [input (slurp in-file)
             result (map  es-doc->citeproc (json/read-str input :key-fn keyword))]

         (when result
             ; Only create the file if there was an output.
             ; This will avoid creating empty files which will cause errors later.
             ; Better to have the absence of the file caught by all-inputs-should-have-outputs.
           (with-open [wrtr (io/writer out-file)]
               ; Using Cheshire JSON for pretty printing, allowing easier verification of results.
             (generate-stream result wrtr {:pretty true}))))))
   (println "IMPORTANT! Manually verify that output files are correct!")))

(deftest ^:unit inputs-should-parse-expected
  (testing "Every input file should convert to a known result"
           (clj-time/do-at now
           (doseq [[in-file expected-result-file] (input-result-files)] 
             (with-open [input (-> in-file io/reader)]
               (let [result (map (fn [es-doc] 
                                   (-> (es-doc->citeproc es-doc)
                                       (json/write-str )
                                       (json/read-str :key-fn keyword)))
                                 (json/read input :key-fn keyword))]
                 (is (.exists expected-result-file)
                     "Expected result file should exist.")

                 (with-open [expected-rdr (-> expected-result-file io/reader)]
                   (let [expected (json/read expected-rdr :key-fn keyword)]
                        ; Echo on failure for easy manual diff.
                     (when (not= result expected)
                       (print "\nFailed document: ")
                       (prn in-file))
                     (is (= result expected))))))))))
