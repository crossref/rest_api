(ns cayenne.tasks.retraction-watch-test
  (:require [cayenne.conf :as conf]
            [clojure.test :refer [testing is deftest]]
            [cayenne.tasks.retraction-watch :as rw]
            [clojure.string :as string]
            [clojure.java.io :as io]))

(def commits-sample
  "[{\"id\": \"324c244fd3018f775dedf1a7e2dafb9a07a68086\",},
    {\"id\": \"a148e3ce9a58958d415e067962059f40dc86beba\",},
    {\"id\": \"61d4f6f4c568ce75b94ce8995b24524b205767bd\",}]")

(def commit-map
  (update-vals
      {"c1"
       ["1,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z"
        "2,a,b,a,b,a,b,a,b,a,b,a,b,a,b,a,b,a,b,a"
        "3,s,q,w,e,r,t,y,u,i,h,g,v,c,x,z,t,y,u,i"]

       "c2"
       ["1,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z,z"
        "2,a,b,a,b,a,b,a,b,aa,b,a,b,a,b,a,b,a,b,a"
        "4,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s"]}
      (fn [v]
        (->> v
             (string/join "\n")))))

(conf/set-param! [:update :schedule :retraction-watch-url]
                   (System/getenv "RETRACTION_WATCH_GIT_URL"))

(deftest ^:component utility-functions
  (testing "itesting reformat-date"
    (is (= "1980-01-22T22:00:00.000Z" (#'rw/reformat-date "01/22/1980 22:00")))
    (is (= "1980-01-22T01:00:00.000Z" (#'rw/reformat-date "1/22/1980 1:00")))
    (is (= "1980-01-02T22:00:00.000Z" (#'rw/reformat-date "1/2/1980 22:00"))))

  (testing "Last commit works"
    (is (not (nil? (re-matches #"[a-zA-Z0-9]{40}" (#'rw/get-last-commit))))))

  (testing "Download and comparing commits"
    (with-redefs [io/reader (fn [_] (java.io.BufferedReader. (java.io.StringReader. (get commit-map "c1"))))]
      (#'rw/download-commit "c1"))
    (with-redefs [io/reader (fn [_] (java.io.BufferedReader. (java.io.StringReader. (get commit-map "c2"))))]
      (#'rw/download-commit "c2"))

    (let [{:keys [deleted updated]} (#'rw/compare-commits "c1" "c2" )
          deletedid (-> deleted first first)
          updatedids (map first updated)]

      (is (= "3" deletedid))
      (is (= ["2" "4"] updatedids)))))

(deftest ^:integration retraction-watch-commit-fetching
  (testing "Get last commit works"
    (with-redefs [slurp (constantly commits-sample)]
      (is (= (#'rw/get-last-commit) "324c244fd3018f775dedf1a7e2dafb9a07a68086"))))

  (testing "Last commit works"
    (is (not (nil? (re-matches #"[a-zA-Z0-9]{40}" (#'rw/get-last-commit)))))))
