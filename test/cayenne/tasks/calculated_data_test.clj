(ns cayenne.tasks.calculated-data-test
  (:require [cayenne.conf :as conf]
            [clojure.test :refer [testing is deftest]]
            [cayenne.tasks.calculated-data :as calc]
            [cayenne.tasks.retraction-watch :as rw]
            [qbits.spandex :as elastic]
            [clojure.string :as string]))

(def rw-records
  (->> [[["RECORD ID" "1234"]
        ["Title" "title1"]
        ["Subject" "subject1"]
        ["Institution" "institution1"]
        ["Journal" "PLoS One"]
        ["Publisher" "PLoS"]
        ["Country" "UK"]
        ["Author" "Author1"]
        ["URLS" "url1"]
        ["ArticleType" "Research Article;"]
        ["RetractionDate" "7/20/2015 0:00"]
        ["RetractionDOI" "10.5555/retraction.doi"]
        ["RetractionPubMedID" "1234345"]
        ["OriginalPaperDate" "7/26/2013 0:00"]
        ["OriginalPaperDOI" "10.5555/original.paper.doi"]
        ["OriginalPaperPubMedID" "12343456"]
        ["RetractionNature" "Correction"]
        ["Reason" "+Concerns/Issues About Image;"]
        ["Paywalled" "No"]
        ["Notes" ""]]

       [["RECORD ID" "1235"]
        ["Title" "title2"]
        ["Subject" "subject2"]
        ["Institution" "institution2"]
        ["Journal" "PLoS One"]
        ["Publisher" "PLoS"]
        ["Country" "UK"]
        ["Author" "Author2"]
        ["URLS" "url2"]
        ["ArticleType" "Research Article;"]
        ["RetractionDate" "7/21/2015 0:00"]
        ["RetractionDOI" "10.5555/retraction.doi2"]
        ["RetractionPubMedID" "1234345"]
        ["OriginalPaperDate" "7/26/2013 0:00"]
        ["OriginalPaperDOI" "10.5555/original.paper.doi2"]
        ["OriginalPaperPubMedID" "12343456"]
        ["RetractionNature" "Correction"]
        ["Reason" "+Concerns/Issues About Image;"]
        ["Paywalled" "No"]
        ["Notes" ""]]]
       (map #(map second %))))

(def es-rw-records
  (let [es-data (fn [[_ a _ b]] [a b])]
    {:body {:hits {:hits
                   (->> rw-records
                        (map (comp es-data #'rw/process-row))
                        flatten
                        (map #(hash-map :_source %)))}}}))

(deftest ^:unit calculated-rw-data
  (let [group-updates
        (->> es-rw-records
             (#'calc/group-updates)
             (map (fn [[[a b] c]] [[a (name b)] c])))]

    (testing "generation calculated data for rw"
      (with-redefs [elastic/request (constantly {:body {:docs []}})]
        (is (= (->> group-updates
                    (#'calc/update-group->es-commands)
                    rest
                    (partition 1 2)
                    flatten
                    (map (comp :calculated :doc)))

               '({:rw-update-to
                  ({:updated
                    {:date-parts [[2015 7 20]],
                     :date-time "2015-07-20T00:00:00Z",
                     :timestamp 1437350400000},
                    :type "correction",
                    :source "retraction-watch",
                    :DOI "10.5555/original.paper.doi",
                    :label "Correction",
                    :record-id "1234"})}
                 {:rw-updated-by
                  ({:updated
                    {:date-parts [[2015 7 20]],
                     :date-time "2015-07-20T00:00:00Z",
                     :timestamp 1437350400000},
                    :type "correction",
                    :source "retraction-watch",
                    :DOI "10.5555/retraction.doi",
                    :label "Correction",
                    :record-id "1234"})}
                 {:rw-update-to
                  ({:updated
                    {:date-parts [[2015 7 21]],
                     :date-time "2015-07-21T00:00:00Z",
                     :timestamp 1437436800000},
                    :type "correction",
                    :source "retraction-watch",
                    :DOI "10.5555/original.paper.doi2",
                    :label "Correction",
                    :record-id "1235"})}
                 {:rw-updated-by
                  ({:updated
                    {:date-parts [[2015 7 21]],
                     :date-time "2015-07-21T00:00:00Z",
                     :timestamp 1437436800000},
                    :type "correction",
                    :source "retraction-watch",
                    :DOI "10.5555/retraction.doi2",
                    :label "Correction",
                    :record-id "1235"})})))))

    (testing "generation calculated data for rw with existing calculated data"
      (with-redefs [elastic/request
                    (constantly {:body
                                 {:docs
                                  [{:_id "10.5555/retraction.doi"
                                    :_source
                                    {:calculated
                                     {:rw-update-to [{:testdata "willappear"}]}}}
                                   {:_id "10.5555/retraction.doi2"
                                    :_source
                                    {:calculated
                                     {:rw-update-to
                                      [{:record-id "1235" :test "wontappear"}
                                       {:record-id "1111" :test "willappear2"}]}}}
                                   {:_id "10.5555/original.paper.doi2"
                                    :_source
                                    {:calculated
                                     {:rw-updated-by
                                      [{:record-id "4444" :test "willappear3"}]}}}
                                   {:_id "10.5555/original.paper.doi"
                                    :_source
                                    {:calculated
                                     {:rw-updated-by
                                      [{:record-id "1234" :test "wontappear2"}]}}}]}})]
        (is (=
             (->> group-updates
                  (#'calc/update-group->es-commands)
                  rest
                  (partition 1 2)
                  flatten
                  (map (comp :calculated :doc)))

             '({:rw-update-to
                ({:updated
                  {:date-parts [[2015 7 20]],
                   :date-time "2015-07-20T00:00:00Z",
                   :timestamp 1437350400000},
                  :type "correction",
                  :source "retraction-watch",
                  :DOI "10.5555/original.paper.doi",
                  :label "Correction",
                  :record-id "1234"}
                 {:testdata "willappear"})}
               {:rw-update-to
                ({:updated
                  {:date-parts [[2015 7 21]],
                   :date-time "2015-07-21T00:00:00Z",
                   :timestamp 1437436800000},
                  :type "correction",
                  :source "retraction-watch",
                  :DOI "10.5555/original.paper.doi2",
                  :label "Correction",
                  :record-id "1235"}
                 {:record-id "1111", :test "willappear2"})}
               {:rw-updated-by
                ({:updated
                  {:date-parts [[2015 7 21]],
                   :date-time "2015-07-21T00:00:00Z",
                   :timestamp 1437436800000},
                  :type "correction",
                  :source "retraction-watch",
                  :DOI "10.5555/retraction.doi2",
                  :label "Correction",
                  :record-id "1235"}
                 {:record-id "4444", :test "willappear3"})}
               {:rw-updated-by
                ({:updated
                  {:date-parts [[2015 7 20]],
                   :date-time "2015-07-20T00:00:00Z",
                   :timestamp 1437350400000},
                  :type "correction",
                  :source "retraction-watch",
                  :DOI "10.5555/retraction.doi",
                  :label "Correction",
                  :record-id "1234"})})))))))
