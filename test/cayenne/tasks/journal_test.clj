(ns cayenne.tasks.journal-test
  (:require [cayenne.tasks.journal :as journal]
            [clojure.java.io :as io]
            [cayenne.util :as util]
            [clojure.test :refer [deftest is testing]]))

(deftest ^:unit column-ordering
  (testing "A csv file can be loaded regardless of column ordering."
           (with-open [rdr-1 (io/reader "dev-resources/cayenne/tasks/journal_test/10-titles.csv")
                       rdr-2 (io/reader "dev-resources/cayenne/tasks/journal_test/10-titles-out-of-order.csv")]
             (let [titles-1 (journal/fetch-titles rdr-1)
                   titles-2 (journal/fetch-titles rdr-2)]
               (is (= titles-1 titles-2)
                   "The same data with titles out of order is parsed to the same output")))))

(deftest ^:unit update-command-fields
  (let [test-row
          {:JournalID "296095"
           :pissn "1607419X"
           :eissn "24118524"
           :doi "10.5555/12345678"
           :JournalTitle "Journal of Inverse Reactance"
           :Publisher "Books Books Books"}]
    (testing "update-command should return tuple of elastic-command, elastic-body."
             (let [[command body] (journal/update-command test-row)]
               (is (= command {:update {:_id 296095}}) "The Elastic Search document ID is correct.")
               (is (= (:id (:doc body)) 296095) "The id field in the document is the same.")))

    (testing "fields should be represented correctly"
               (is (= (-> test-row
                          journal/update-command
                          second
                          :doc
                          :issn
                          set)
                      #{{:value "1607-419X" :type "print"}
                        {:value "2411-8524" :type "electronic"}})
                   "ISSNs should be included along with type (order irrelevant)."))

    (is (= (-> test-row
               journal/update-command
               second
               :doc
               :doi)
           "10.5555/12345678")
        "DOI should be indexed when present.")

                 ; DOI in a different format.
    (is (= (-> (assoc test-row :doi "https://doi.org/10.5555/12345678")
               journal/update-command
               second
               :doc
               :doi)
           "10.5555/12345678")
        "DOI should be normalised when present.")

    (is (= (-> test-row
               journal/update-command
               second
               :doc
               :title)
           "Journal of Inverse Reactance")
        "Title should be indexed.")

    (is (= (-> test-row
               journal/update-command
               second
               :doc
               :publisher)
           "Books Books Books")
        "Publisher name should be indexed.")

    (with-redefs [util/tokenize-name
                         ; Mock out the tokenizing function to expects the given title.
                  {"Journal of Inverse Reactance"
                   ["journal" "of" "inverse" "reactance"]}]

                 (is (= (-> test-row
                            journal/update-command
                            second
                            :doc
                            :token)
                        ["journal" "of" "inverse" "reactance"])
                     "token should be the result of tokenizing the title field"))))

(deftest ^:unit additional-issns
  (testing "Test all possible input scenarios for additional-issns"
    (is (nil? (journal/additional-issns {:additionalIssns nil})))
    (is (= '() (journal/additional-issns {:additionalIssns ""})))
    (is (= '() (journal/additional-issns {:additionalIssns ";"}))) 
    (is (= '("a") (journal/additional-issns {:additionalIssns " a; "})))
    (is (= '("a" "b") (journal/additional-issns {:additionalIssns "a; b"})))
    (is (= '("a" "b" "c") (journal/additional-issns {:additionalIssns "a; b;c"})))))