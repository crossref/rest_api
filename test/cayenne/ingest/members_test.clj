(ns cayenne.ingest.members-test
  (:require [cayenne.api-fixture :refer [api-get setup-api]]
            [cayenne.tasks.member :as member]
            [clojure.data.json :as json]
            [clojure.java.io :refer [resource reader]]
            [clojure.string :as str]
            [clojure.test :refer [deftest testing is]]
            [clj-http.client :as http]
            [user :refer [flush-elastic]]))

(defn get-test-members []
  (let [rdr (-> "ingest/members/all.json" resource reader)]
    (json/read rdr :key-fn keyword)))

(defn get-test-prefix [prefix]
  (-> (str "ingest/members/" prefix ".xml")
      resource
      slurp))

(defn mock-http-get-prefix [url & _]
  (let [prefix (last (str/split url #"="))
        content (.getBytes (get-test-prefix prefix))]
    {:status 200 :body content}))

(deftest ^:unit ingesting-prefix-info
  (testing "prefix information is corretly ingested from XML"
    (let [prefix "10.34197"
          member-id 267
          prefix-info (with-redefs [http/get mock-http-get-prefix]
                        (member/get-prefix-info member-id prefix))]
      
      (is (= "10.34197" (:value prefix-info)))
      (is (= 267 (:member-id prefix-info)))
      (is (= "ATS Scholar" (:name prefix-info)))
      (is (= " 25 Broadway 18th Floor 18th Floor New York NY 10004-1012 United States "
             (:location prefix-info)))
	  (is (not (contains? prefix-info :reference-visibility))))))
     
(deftest ^:unit ingesting-member-info
  (testing "member information is corretly transformed into ES command"
    (let [test-member {:prefixes ["10.26818" "10.18061"]
                       :name "The Ohio State University Libraries"
                       :technical_email "0@osu"
                       :memberId 7412}
          mock-get-prefix-info (fn [m p] {:member-id m
                                          :name (str "name-" p)
                                          :location (str "loc-" p)})
          command (with-redefs [member/get-prefix-info mock-get-prefix-info]
                    (member/index-command test-member))
          action (first command)
          body (second command)]
      
      (is (= 2 (count command)) "ES index command should have two elements")
      (is (= {:index {:_id 7412}} action))
      (is (= 7412 (:id body)))
      (is (= "The Ohio State University Libraries" (:primary-name body)))
      (is (= "loc-10.26818" (:location body)))
      (is (= #{"the" "ohio" "state" "university" "libraries"} (set (:token body))))
      (is (= 2 (count (:prefix body))))
      (is (= {:member-id 7412
              :name "name-10.26818"
              :location "loc-10.26818"} (first (:prefix body))))
      (is (= {:member-id 7412
              :name "name-10.18061"
              :location "loc-10.18061"} (second (:prefix body)))))))

(deftest ^:integration ingesting-members
  (testing "members are indexed during the ingestion"

    (setup-api)
    (is (empty? (-> "/v1/members" api-get :items)) "Index should be empty before test")
    
    (with-redefs
      [member/get-member-list get-test-members
       http/get mock-http-get-prefix]
      (member/index-members))
    (flush-elastic)
    
    (let [expected-ids '(3 19 7412)
          expected-prefix-counts '(1 2 4)
          items (:items (api-get "/v1/members"))]
      (is (= 3 (count items)) "Items should be indexed")
      (is (= expected-ids (sort (map :id items))) "Expected member ids should be in the index")
      (is (= expected-prefix-counts (sort (map (comp count :prefix) items)))))))
