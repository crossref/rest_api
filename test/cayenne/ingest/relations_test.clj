(ns cayenne.ingest.relations-test
  (:require [cayenne.api-fixture :refer [api-get setup-api api-request]]
            [cayenne.tasks.calculated-data :refer [process-calculated-data es-results->failed-dois]]
            [cayenne.data.relations :refer [find-relation-updates]]
            [clojure.java.io :as io]
            [clojure.test :refer [deftest testing is]]
            [user :refer [flush-elastic index-work-files]]))

(defn fetch-works
  ([]
   (fetch-works ""))
  ([filters]
   (-> (str "/v1/works" filters) api-get :items)))


(defn fetch-work [doi]
  (-> (str "/v1/works/" doi) api-get))

(defn index-all-and-flush [file-names content-type]
  (let [paths (map (partial str "dev-resources/ingest/works/data/") file-names)]
    (try
      (index-work-files paths content-type)
      (catch Exception _)))
  (flush-elastic))

(def es-doc
  {:doi "10.5555/doi"
   :update-to '({:doi "http://dx.doi.org/10.1177/0267659114547942",
                :type "retraction",
                :label "Retraction",
                :date-extended "2014-10-28T00:00:00.000Z"}
               {:doi "http://dx.doi.org/10.1177/0267659114522088",
                :type "retraction",
                :label "Retraction",
                :date-extended "2014-10-28T00:00:00.000Z"})})

(def es-response
  {:body
   {:took 0,
    :errors true,
    :items
    [{:update
      {:_index "work",
       :_type "work",
       :_id "10.1007/s12206-017-1037-9",
       :status 404,
       :error
       {:type "document_missing_exception",
        :reason "[work][10.1007/s12206-017-1037-9]: document missing",
        :index_uuid "mkpAjqAITLiEPQLa0aPqog",
        :shard "3",
        :index "work"}
       }}
     {:update
      {:_index "work",
       :_type "work",
       :_id "10.1007/s12206-017-1037-10",
       :status 404,
       :error
       {:type "another_exception",
        :reason "[work][10.1007/s12206-017-1037-9]: document missing",
        :index_uuid "mkpAjqAITLiEPQLa0aPqog",
        :shard "3",
        :index "work"}
       }}]},
   :status 200,
   :headers {"content-type" "application/json; charset=UTF-8",
             "content-length" "299"},
   :hosts nil})

(deftest ^:integration ingesting-two-works-with-updated-by-relations
  (testing "two works with one relationship are ingested and connected"
    (let [has-update-doi "10.1016/j.jvs.2015.07.001"
          updates-to-doi "10.1016/j.jvs.2016.01.001"
          dois #{has-update-doi updates-to-doi}
          testfiles ["has-update.relation.body" "updates-to.relation.body"]]

      (setup-api)

      (is (empty? (fetch-works)) "Index should be empty before test")
      (index-all-and-flush testfiles "application/vnd.crossref.unixsd+xml")
      (is (= dois (->> (fetch-works) (map :DOI) set)) "Files should be indexed")
      (is (= has-update-doi (-> (fetch-work updates-to-doi) :update-to first :DOI)) "updated dois do not match")
      (is (nil? (-> (fetch-work has-update-doi) :updated-by)) "DOI shound not be updated yet")
      (process-calculated-data)
      (flush-elastic)
      (Thread/sleep 2000)
      (is (seq (-> (fetch-work has-update-doi) :updated-by)) "DOI shound not be updated yet"))))

(def has-updated-by-rels
  {"10.1007/s11665-016-1940-8" #{"10.1007/s11665-016-1940-8"}
   "10.1007/978-3-030-36150-1" #{"10.1007/978-3-030-36150-1_57"}
   "10.1177/0267659115588021" #{}
   "10.1177/0267659113502835" #{"10.1177/0267659114536761"
                                "10.1177/0267659115588021"
                                "10.1177/0267659114547943"
                                "10.1177/0267659114557720"
                                "10.1177/0267659114547942"}
   "10.1177/0267659113498617" #{"10.1177/0267659114536761"
                                "10.1177/0267659115588021"
                                "10.1177/0267659114547943"
                                "10.1177/0267659114557720"
                                "10.1177/0267659114547942"}
   "10.1177/0267659114536761" #{"10.1177/0267659115588021"
                                "10.1177/0267659114547943"
                                "10.1177/0267659114557720"
                                "10.1177/0267659114547942"}
   "10.1080/10611940.2015.1042342" #{"10.1080/10611428.2017.1312977"}
   "10.1007/978-3-030-36150-1_57" #{}
   "10.1007/s12206-018-0653-3" #{}
   "10.1177/0267659113513823" #{"10.1177/0267659114536761"
                                "10.1177/0267659115588021"
                                "10.1177/0267659114547943"
                                "10.1177/0267659114557720"
                                "10.1177/0267659114547942"}
   "10.1177/0267659114522088" #{"10.1177/0267659114536761"
                                "10.1177/0267659115588021"
                                "10.1177/0267659114547943"
                                "10.1177/0267659114557720"
                                "10.1177/0267659114547942"}
   "10.1080/10611428.2017.1312977" #{}
   "10.1177/0267659114547943" #{"10.1177/0267659114536761"
                                "10.1177/0267659115588021"
                                "10.1177/0267659114557720"
                                "10.1177/0267659114547942"}
   "10.1017/s0025100316000141" #{}
   "10.1177/0267659114547942" #{"10.1177/0267659114536761"
                                "10.1177/0267659115588021"
                                "10.1177/0267659114547943"
                                "10.1177/0267659114557720"}
   "10.1017/s0025100315000407" #{"10.1017/s0025100316000141"}
   "10.1177/0267659114557720" #{"10.1177/0267659114536761"
                                "10.1177/0267659115588021"
                                "10.1177/0267659114547943"
                                "10.1177/0267659114547942"}
   "10.1007/s12206-017-1037-9" #{"10.1007/s12206-018-0653-3"}})

(deftest ^:integration ingesting-multiple-works-with-multiple-relations
  (testing "ingesting multiple works with multiple relations"
    (let [files (->> "dev-resources/ingest/works/data/updated-by-relations" io/file .list (map #(str "updated-by-relations/" %)))]
      (setup-api)
      (index-all-and-flush files "application/vnd.crossref.unixsd+xml")
      (process-calculated-data)
      (flush-elastic)
      (Thread/sleep 2000)
      (doseq [[doi rels] [(first has-updated-by-rels)]]
        (let [updated-by-dois (->> doi fetch-work :updated-by (map :doi) set)]
          (is (= updated-by-dois rels))))
      (index-all-and-flush files "application/vnd.crossref.unixsd+xml")
      (process-calculated-data)
      (flush-elastic)
      (Thread/sleep 2000)
      (doseq [[doi rels] has-updated-by-rels]
        (let [data (fetch-work doi)
              updated-by-dois (->> data :updated-by (map :DOI) set)]
          (is (= updated-by-dois rels)))))))


(deftest ^:integration error-dois-can-be-extracted-from-es-response
  (setup-api)
  (index-all-and-flush ["updated-by-relations/19b0c2bd669d1dc1b3dadda5813154e43974d078.xml"] "application/vnd.crossref.unixsd+xml")
  (testing "Error dois can be extracted from elastic search response"
    (is (=
         {"10.1007/s12206-017-1037-10" ["another_exception"]}
         (es-results->failed-dois es-response)))))

(deftest ^:integration relations-are-detected-from-es-doc
  (testing "Relations are detected from an elastic search document"
    (is (= '({:index {:_index "relupdates", :_type "relupdates"}}
             {:doi "10.1177/0267659114547942",
              :relation-type :has-update,
              :relation-info
              {:doi "10.5555/doi",
               :type "retraction",
               :label "Retraction",
               :date-extended "2014-10-28T00:00:00.000Z"}}
             {:index {:_index "relupdates", :_type "relupdates"}}
             {:doi "10.1177/0267659114522088",
              :relation-type :has-update,
              :relation-info
              {:doi "10.5555/doi",
               :type "retraction",
               :label "Retraction",
               :date-extended "2014-10-28T00:00:00.000Z"}})
           (find-relation-updates es-doc)))))

(deftest ^:integration ingesting-multiple-works-with-alias-relations
  (let [files (->> "dev-resources/ingest/works/data/alias-relations" io/file .list (map #(str "alias-relations/" %)))]
    (setup-api)
    (index-all-and-flush files "application/vnd.crossref.unixsd+xml")
    (process-calculated-data)
    (flush-elastic)
    (Thread/sleep 4000)
    (testing "aliased doi returns 30X and location header works"
      (let [{:keys [status headers]}
            (api-request "/v1/works/10.1007/978-1-59259-345-3_1" :get)]
        (is (= (headers "Location") "/works/10.1385/1-59259-345-3:03"))
        (is (= status 301)))
      (let [{:keys [status headers]}
            (api-request "/v1/works/10.1007/978-1-59259-345-3_2" :get)]
        (is (= (headers "Location") "/works/10.1385/1-59259-345-3:03"))
        (is (= status 301)))
      (let [{:keys [status headers]}
            (api-request "/v1/works/10.1007/978-1-59259-345-3_1/transform/application/x-bibtex" :get)]
        (is (= status 308))
        (is (= (headers "Location") "/works/10.1385/1-59259-345-3:03/transform/application/x-bibtex")))
      (let [{:keys [status headers]}
            (api-request "/v1/works/10.1007/978-1-59259-345-3_1/transform"
                         :get
                         {:headers {"accept" "application/x-bibtex"}})]
        (is (= status 308))
        (is (= (headers "Location") "/works/10.1385/1-59259-345-3:03/transform"))))
    (let [{:keys [aliases is-referenced-by-count]}
          (api-get "/v1/works/10.1385/1-59259-345-3:03")]
      (testing "prime dois contains list of aliased dois"
        (is (= (set aliases) #{"10.1007/978-1-59259-345-3_1"
                               "10.1007/978-1-59259-345-3_2"})))
      (testing "is-referenced-by-count agregates aliased doi counts"
        (is (= is-referenced-by-count 9))))

    (let [files (->> "dev-resources/ingest/works/data/updated-by-relations" io/file .list (map #(str "updated-by-relations/" %)))]
      (index-all-and-flush files "application/vnd.crossref.unixsd+xml")
      (process-calculated-data)
      (flush-elastic)
      (testing "has-alias filter works"
        (is (= 19 (-> (fetch-works) count)))
        (is (= 1 (-> (fetch-works "?filter=has-alias:true") count)))))))
