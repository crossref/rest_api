(ns cayenne.ingest.journals-test
  (:require [cayenne.api-fixture :refer [api-get setup-api]]
            [cayenne.tasks.journal :as journal]
            [clojure.java.io :refer [resource reader]]
            [clojure.test :refer [deftest testing is]]
            [user :refer [flush-elastic]]))

(deftest ^:unit generating-journal-es-update-command
  (testing "ES journal update command is correctly generated"
    (let [test-journal {:JournalTitle "An Awesome Journal"
                        :JournalID "45"
                        :Publisher "A Great Publisher"
                        :pissn "0544536X"
                        :eissn "21317494"
                        :additionalIssns "07993889; 07995199; 23095830"
                        :doi "10.1055/some-DOI"
                        (keyword "(year1)[volume1]issue1,issue2,issue3(year2)[volume2]issue4,issues5")
                        "(2013)[19]37,38(2014)[20]39,40,41"}
          command (journal/update-command test-journal)
          action (first command)
          body (:doc (second command))]
      
      (is (= 2 (count command)) "ES journal index command should have two elements")
      (is (= {:update {:_id 45}} action))
      (is (= 45 (:id body)))
      (is (= "An Awesome Journal" (:title body)))
      (is (= #{"an" "awesome" "journal"} (set (:token body))))
      (is (= "10.1055/some-doi" (:doi body)))
      (is (= "A Great Publisher" (:publisher body)))
      (is (= #{{:value "0799-3889", :type nil}
               {:value "0544-536X", :type "print"}
               {:value "2131-7494", :type "electronic"}
               {:value "0799-5199", :type nil}
               {:value "2309-5830", :type nil}}
             (set (:issn body)))))))

(deftest ^:integration ingesting-journals
  (testing "journals are indexed during the ingestion"
    (let [expected-count 3
          expected-issns #{"2141-6494" "0131-3878" "2412-950X" "0044-586X" "2107-7207"}]

      (setup-api)
      (is (empty? (-> "/v1/journals" api-get :items)) "Index should be empty before test")
      
      (with-open [rdr (-> "ingest/journals/journals.csv" resource reader)]
        (journal/index-journals rdr))
      
      (flush-elastic)
      
      (let [items (:items (api-get "/v1/journals"))]
        (is (= expected-count (count items)) "Journals should be indexed")
        (is (= expected-issns (set (mapcat :ISSN items))) "ISSNs should be in the index")))))
