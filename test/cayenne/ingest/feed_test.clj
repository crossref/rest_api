(ns cayenne.ingest.feed-test
  (:require [cayenne.api-fixture :refer [api-get frozen-time setup-api]]
            [cayenne.api.v1.feed :as feed]
            [cayenne.api.v1.update :as update]
            [cayenne.conf :as conf]
            [cayenne.elastic.convert :as convert]
            [cayenne.elastic.index :as es-index]
            [cayenne.elastic.update :as es-update]
            [clj-time.core :as dt]
            [clojure.java.io :as io]
            [clojure.test :refer [deftest testing is]]
            [me.raynes.fs :refer [copy-dir delete-dir]]
            [user :refer [flush-elastic index-work-files]]))

(deftest ^:unit constructing-feed-filename
  (testing "feed filenames are correctly constructured"
    (let [pref (partial str (conf/get-param [:dir :data]))
          type-unixsd "application/vnd.crossref.unixsd+xml"
          type-update "application/vnd.crossref.update+json"]
      
      (is (= (pref "/feed-in/crossref-unixsd-id-1.body")
             (feed/feed-filename "in" type-unixsd "crossref" "id-1")))
      (is (= (pref "/feed-processed/crossref-unixsd-id-2.body")
             (feed/feed-filename "processed" type-unixsd "crossref" "id-2")))
      (is (= (pref "/feed-failed/crossref-update-id-3.body")
             (feed/feed-filename "failed" type-update "crossref" "id-3"))))))

(deftest ^:unit parsing-feed-filename
  (testing "feed filenames are correctly parsed"
    (let [parse (fn [n] (->> n
                             (str (conf/get-param [:dir :data]) "/feed-in/")
                             feed/parse-feed-filename))]
    
      (is (= {:content-type "application/vnd.crossref.unixsd+xml"
              :provider "crossref"
              :id "id-1"}
             (parse "crossref-unixsd-id-1.body")))
      (is (= {:content-type "application/vnd.crossref.update+json"
              :provider "crossref"
              :id "id-2"}
             (parse "crossref-update-id-2.body"))))))
    
(deftest ^:unit generating-refby-count-es-command
  (testing "correct ES command for referenced-by count update is generated"
    ; run in a do-at context, so the time is fixed
    (dt/do-at
       frozen-time
       (let [doi "10.5555/some-random-doi"
             refby-count 57
             command (:work (es-update/update-referenced-by-count-command doi refby-count))]

         (is (= 2 (count command)) "ES command should have two elements")
         (is (= {:update {:_id doi}} (first command)))
         (is (= {:doc {:indexed (dt/now) :is-referenced-by-count refby-count}} (second command)))))))

(deftest ^:unit generating-work-index-command
  (testing "correct ES command for indexing works is generated"
    (let [doc {:doi "10.5555/some-random-doi" :name "Some Name"}
          command (with-redefs [convert/item->es-doc identity]
                    (-> doc es-index/bulk-index-command :work))]
      
      (is (= 2 (count command)) "ES command should have two elements")
      (is (= {:update {:_id "10.5555/some-random-doi"}} (first command)))
      (is (= doc (-> command second :upsert))))))

(deftest ^:unit parsing-update-vector
   (testing "update vector from ingest file is correctly parsed"
     (let [update-vector ["set" "10.5555/is-this-doi" "is-cited-by-count" 21]
           expected {:action :set
                     :subject-doi "10.5555/is-this-doi"
                     :subject-citation-id nil
                     :predicate :is-cited-by-count
                     :object 21}]

       (is (= expected (update/parse-update update-vector))))))

(defn setup-testdir []
  (let [feed-dir (.getPath (io/resource "ingest/works"))
        data-dir (str feed-dir "/data/")
        in-dir (str feed-dir "/feed-in/")
        processed-dir (str feed-dir "/feed-processed/")
        failed-dir (str feed-dir "/feed-failed/")]
    (delete-dir processed-dir)
    (delete-dir failed-dir)
    (delete-dir in-dir)
    (copy-dir data-dir in-dir)
    {:orig-dir data-dir :in-dir in-dir :processed-dir processed-dir :failed-dir failed-dir}))
            
(defn testfile-info [{:keys [orig-dir in-dir processed-dir failed-dir]} testfile]
  {:content-type (-> testfile feed/parse-feed-filename :content-type)
   :orig-file (str orig-dir testfile)
   :incoming-file (str in-dir testfile)
   :processed-file (str processed-dir testfile)
   :failed-file (str failed-dir testfile)})

(deftest ^:component ingesting-files
  (testing "file is moved to feed-processed after successful ingestion"
    (let [dirs (setup-testdir)
          testfiles ["crossref-unixsd-f0e08fdd-459c-4b59-96da-ede1a1483f81.body"
                     "crossref-unixsd-aa5498f1-345b-4e0f-a7e6-9fe7a3052465.body"
                     "crossref-update-example1.body"
                     "crossref-unixsd-379a5ac7-5b52-443d-9655-0fe3871d0baa.body"]
          testfile-infos (map (partial testfile-info dirs) testfiles)]
      
      (doseq [{:keys [orig-file incoming-file processed-file] :as info} testfile-infos]
        ; ES-related functions are redefined to make sure ES is not called.
        (with-redefs
          [es-index/bulk-index-items (constantly nil)
           es-update/index-updates (constantly nil)]
          (feed/process-with info))
        
        (is (not (.exists (io/as-file incoming-file))) "File should be removed from in-dir")
        (is (= (slurp orig-file) (slurp processed-file)) "File should be moved to processed-dir")))))

(defn index-all-and-flush [file-names content-type]
  (let [paths (map (partial str "dev-resources/ingest/works/data/") file-names)]
    (try
      (index-work-files paths content-type)
      (catch Exception _)))
  (flush-elastic))

(deftest ^:integration ingesting-works
  (testing "work file is indexed during ingestion"
    (let [testdoi "10.1080/08870446.2016.1247841"
          testfile "crossref-unixsd-f0e08fdd-459c-4b59-96da-ede1a1483f81.body"]
      
      (setup-api)

      (is (empty? (-> "/v1/works" api-get :items)) "Index should be empty before test")
      
      (index-all-and-flush [testfile] "application/vnd.crossref.unixsd+xml")
      
      (is (= testdoi (->> testdoi (str "/v1/works/") api-get :DOI))
        "File should be indexed"))))

(deftest ^:integration ingesting-updates
  (testing "ingested update results in changes in the indexed works"
    (let [workfiles ["crossref-unixsd-f0e08fdd-459c-4b59-96da-ede1a1483f81.body"
                     "crossref-unixsd-aa5498f1-345b-4e0f-a7e6-9fe7a3052465.body"
                     "crossref-unixsd-379a5ac7-5b52-443d-9655-0fe3871d0baa.body"]
          updatefile "crossref-update-example1.body"
          expected-refby-counts {"10.1080/08870446.2016.1247841" 27
                                 "10.1155/2016/6402942" 13
                                 "10.7717/peerj.1698" 5}]
     
      (setup-api)
      (index-all-and-flush workfiles "application/vnd.crossref.unixsd+xml")
      (index-all-and-flush [updatefile] "application/vnd.crossref.update+json")
      
      (doseq [[doi expected-count] expected-refby-counts]
        (let [actual-count (->> doi (str "/v1/works/") api-get :is-referenced-by-count)]
          (is (= expected-count actual-count)
            (str "Expected ref-by count for DOI " doi)))))))

(deftest ^:integration ingesting-updates-with-error
  (testing "Single error in file results in no entries getting processed"
    (let [workfiles ["crossref-unixsd-f0e08fdd-459c-4b59-96da-ede1a1483f81.body"
                     "crossref-unixsd-aa5498f1-345b-4e0f-a7e6-9fe7a3052465.body"
                     "crossref-unixsd-379a5ac7-5b52-443d-9655-0fe3871d0baa.body"]
          updatefile "crossref-update-example2.body"
          expected-refby-counts {"10.1080/08870446.2016.1247841" 0
                                 "10.1155/2016/6402942" 4
                                 "10.7717/peerj.1698" 0}]

      (setup-api)
      (index-all-and-flush workfiles "application/vnd.crossref.unixsd+xml")
      (index-all-and-flush [updatefile] "application/vnd.crossref.update+json")
      
      (doseq [[doi expected-count] expected-refby-counts]
        (let [actual-count (->> doi (str "/v1/works/") api-get :is-referenced-by-count)]
          (is (= expected-count actual-count)
            (str "Expected ref-by count for DOI " doi)))))))

(deftest ^:integration ingesting-publisher-update
  (testing "ingested publisher update does not results in changes in the indexed works because only ref-count is implemented so far"
    (let [workfiles ["crossref-unixsd-f0e08fdd-459c-4b59-96da-ede1a1483f81.body"
                     "crossref-unixsd-aa5498f1-345b-4e0f-a7e6-9fe7a3052465.body"
                     "crossref-unixsd-379a5ac7-5b52-443d-9655-0fe3871d0baa.body"]
          updatefile "crossref-update-example3.body"
          expected-publishers {"10.1080/08870446.2016.1247841" "Informa UK Limited"
                               "10.1155/2016/6402942" "Hindawi Limited"
                               "10.7717/peerj.1698" "PeerJ"}]
      
      (setup-api)
      (index-all-and-flush workfiles "application/vnd.crossref.unixsd+xml")
      (index-all-and-flush [updatefile] "application/vnd.crossref.update+json")
      
      (doseq [[doi expected] expected-publishers]
        (let [actual (->> doi (str "/v1/works/") api-get :publisher)]
          (is (= expected actual)
            (str "Expected publisher for DOI " doi)))))))
