(ns cayenne.util-tests
  (:require [clojure.test :refer [deftest testing is]]
            [cayenne.util :refer [->sha1]]
            [cayenne.util :as util]))

(deftest ^:unit convert-to-sha1
  (testing "converting to sha1 returns expected results"
    (is (= (->sha1 "The quick brown fox jumps over the lazy dog") "2fd4e1c67a2d28fced849ee1bb76e7391b93eb12"))
    (is (= (->sha1 "The quick brown fox jumps over the lazy cog") "de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3"))
    (is (= (->sha1 "") "da39a3ee5e6b4b0d3255bfef95601890afd80709"))))

(deftest ^:unit test-hyphenation
  (testing "all underscores should be replaced with hyphens"
    (is (= " the-quick-brown-fox-" (util/hyphenate " the_quick_brown-fox_")))
    (is (= " ------- " (util/hyphenate " -____-_ ")))
    (is (= "the-quick-brown-fox" (util/hyphenate "the_quick_brown_fox"))))
  (testing "no underscores should return original string even when empty or blank"
    (is (= "the quick brown fox" (util/hyphenate "the quick brown fox")))
    (is (= "" (util/hyphenate "")))
    (is (= " " (util/hyphenate " "))))
  (testing "nil or non string input should return nil"
    (is (= nil (util/hyphenate nil)))
    (is (= nil (util/hyphenate 1)))
    (is (= nil (util/hyphenate {:a 1})))
    (is (= nil (util/hyphenate ["a" "b"])))))

(deftest ^:unit test-remove-keys
  (testing "removing odds and evens"
    (is (= {:a 1} (util/remove-keys-by-pred {:a 1 :b 2} {number? even?})))
    (is (= {:b 2} (util/remove-keys-by-pred {:a 1 :b 2} {number? odd?})))
    (is (= {} (util/remove-keys-by-pred {:a 1 :b 2} {number? (some-fn odd? even?)}))))

  (testing "removing empty lists"
    (is (= {:a 1} (util/remove-keys-by-pred {:a 1 :b []} {vector? empty?}))))

  (testing "removing even in nested fields"
    (is (= {:a 1 :b {:c 1}} (util/remove-keys-by-pred {:a 1 :b {:c 1 :d 2}} {number? even?}))))

  (testing "removing nils"
    (is (= {:a 1 :c {}} (util/remove-keys-by-pred {:a 1 :b nil :c {:d nil}} {nil? (constantly true)})))))
