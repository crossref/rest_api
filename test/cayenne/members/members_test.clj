(ns cayenne.members.members-test
  (:require [cayenne.api-fixture :refer [api-get api-get-status api-fixture setup-api
                                         setup-test-members setup-members-coverage setup-test-works]]
            [clojure.set :refer [subset?]]
            [clojure.test :refer [use-fixtures deftest testing is]]))

(deftest ^:integration all-members
  (testing "members endpoint returns expected result"

    (let [items (-> "/v1/members" api-get :items)]
      (is (seq items))
      (doseq [item items]
        (is (contains? item :id))
        (is (contains? item :names))
        (is (seq (:prefix item)))
        (is (= (count (:prefix item)) (count (:prefixes item))))))))

(deftest ^:integration offset-members
  (testing "offset retrieves consecutive ranges of members"
    (doseq [[start range-1 range-2] [[0 27 48] [11 45 29] [3 59 0] [49 0 49] [21 0 0]]]
      (let [get-item-range (fn [start size] (-> (str "/v1/members?rows=" size "&offset=" start) api-get :items))
            items-all (get-item-range start (+ range-1 range-2))
            items-1 (get-item-range start range-1)
            items-2 (get-item-range (+ start range-1) range-2)]
        (is (= items-all (concat items-1 items-2)))))))

(deftest ^:integration cursor-members
  (testing "iteration with cursors retrieves the entire dataset"
    (let [iterate-members (fn it-m [cursor]
                            (let [members-page (->> cursor (str "/v1/members?rows=20&cursor=") api-get)
                                  items (:items members-page)]
                              (if (empty? items)
                                []
                                (lazy-cat items (it-m (:next-cursor members-page))))))
          cursor-members (set (iterate-members "*"))
          all-members (-> "/v1/members?rows=500" api-get :items set)]
      (is (= all-members cursor-members)))))

(deftest ^:integration filtering-members

  (testing "doi count filters correctly filter members"
    (setup-members-coverage)
    (doseq [[filter-name field] [["backfile-doi-count" :backfile-dois]
                                 ["current-doi-count" :current-dois]]]
      (let [doi-counts (->> (str "/v1/members?rows=500&filter=" filter-name ":0")
                            api-get
                            :items
                            (map :counts)
                            (map field)
                            set)]
        (is (= #{0} doi-counts)))))

  (testing "prefix filter correctly filters members"
    (let [prefix "10.1205"
          members (->> prefix
                       (str "/v1/members?rows=500&filter=prefix:")
                       api-get
                       :items
                       (map :prefixes))]
      (is (seq members))
      (is (every? (partial some #{prefix}) members)))))

(deftest ^:integration multiple-valued-filters
  (testing "filters of same type are OR-ed"
    (let [id-set (fn [f] (->> f (str "/v1/members?rows=500&filter=") api-get :items (map :id) set))]
      (doseq [[f-name val-1 val-2] [["current-doi-count" "1" "0"]
                                    ["backfile-doi-count" "1" "0"]
                                    ["prefix" "10.7811" "10.24824"]]]
        (let [ids-1 (id-set (str f-name ":" val-1))
              ids-2 (id-set (str f-name ":" val-2))
              ids-or (id-set (str f-name ":" val-1 "," f-name ":" val-2))]
          (is (subset? ids-1 ids-or))
          (is (subset? ids-2 ids-or)))))))

(deftest ^:integration querying-members

  (testing "members endpoint returns result for query"
    (doseq [query ["the"
                   "society+of"
                   "oxford+press"
                   "press+oxford"
                   "oxfo+pres"
                   "pre+oxf"
                   "Men%27s+Studies"
                   "A.+I.+Rosu+Cultural+Scientific+Foundation+(Fundatia+cultural-stiintifica+A.+I.+rosu)"]]
      (let [works (->> query (str "/v1/members?query=") api-get :items)]
        (is (seq works))))))

(deftest ^:integration filter-and-query
  (testing "queries and filters are AND-ed"
    (let [id-set (fn [query] (->> query (str "/v1/members?rows=500&") api-get :items (map :id) set))]
      (doseq [[wide-q narrow-q] [["filter=prefix:10.7811" "filter=prefix:10.7811,has-public-references:1"]
                                 ["query=the" "query=the&filter=has-public-references:1"]
                                 ["filter=has-public-references:1" "query=the&filter=has-public-references:1"]]]
        (is (subset? (id-set narrow-q) (id-set wide-q)))))))

(deftest ^:intgration filtering-works
  (testing "the same number of works is returned through /members/<id>/works route and the filter"
    (let [filter-total (-> "/v1/works?filter=member:78" api-get :total-results)
          route-total (-> "/v1/members/78/works" api-get :total-results)
          route-total-0-rows (-> "/v1/members/78/works?rows=0" api-get :total-results)]
      (is (> filter-total 0))
      (is (= filter-total route-total))
      (is (= filter-total route-total-0-rows)))))

(deftest ^:integration retrieving-members
  (testing "member can be retrieved by an id"
    (is (= 78 (-> "/v1/members/78" api-get :id))))
  
  (testing "retrieving a non-existent member returns 404"
    (is (= 404 (api-get-status "/v1/members/nonexistent")))
    (is (= 404 (api-get-status "/v1/members/nonexistent/works")))))

(use-fixtures
  :once
  (api-fixture setup-api setup-test-members setup-test-works))
