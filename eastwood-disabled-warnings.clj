(disable-warning
 {:linter :suspicious-expression
  :for-macro 'clojure.core/->
  :if-inside-macroexpansion-of #{'clojurewerkz.quartzite.schedule.cron/schedule}
  :reason "Issue in a third-party library: 'suspicious-expression: -> called with 1 args.  (-> x) always returns x.  Perhaps there are misplaced parentheses?'"})

(disable-warning
  {:linter :deprecations
   :symbol-matches #{#"^#'compojure\.handler\/api"}
   :reason "This namespace has been deprecated in favor of the ring-defaults (https://github.com/ring-clojure/ring-defaults) library"})

(disable-warning
 {:linter :constant-test
  :for-macro 'clojure.core/not=
  :if-inside-macroexpansion-of #{'clojure.core.async/go-loop}
  :reason "Issue in go-loop macro"})


(disable-warning
  {:linter :wrong-arity
   :function-symbol 'amazonica.aws.sqs/receive-message
   :arglists-for-linting '([a b c d e f g h])
   :reason "Issue with third party client library"})

(disable-warning
  {:linter :wrong-arity
   :function-symbol 'amazonica.aws.sqs/get-queue-attributes
   :arglists-for-linting '([a b c d])
   :reason "Issue with third party client library"})
