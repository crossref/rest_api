import boto3
import time
import json

config = {
    "region_name": "us-east-1",
    "aws_access_key_id": "ACCESS_ID",
    "aws_secret_access_key": "ACCESS_KEY",
    "endpoint_url": "http://localstack-integration:4566",
}

queue = boto3.client("sqs", **config)
queue.create_queue(
    QueueName="mdqueue", 
    tags={"string": "string"}
)

queue.create_queue(
    QueueName="doi-update-notifications", 
    tags={"string": "string"}
)

redrive_policy = {
    "deadLetterTargetArn": "arn:aws:sqs:us-east-1:000000000000:md-dlq",
    "maxReceiveCount": "5",
}

queue.set_queue_attributes(
    QueueUrl="http://localstack-integration:4566/000000000000/mdqueue",
    Attributes={"RedrivePolicy": json.dumps(redrive_policy)},
)

sns = client = boto3.client("sns", **config)
sns.create_topic(Name="mdtopic")
sns.subscribe(
    TopicArn="arn:aws:sns:us-east-1:000000000000:mdtopic",
    Protocol="sqs",
    Endpoint="arn:aws:sqs:us-east-1:000000000000:mdqueue",
)

s3 = boto3.resource("s3", **config)
bucket = s3.create_bucket(Bucket="md-bucket")
bucket.Notification().put(
    NotificationConfiguration={
        "TopicConfigurations": [
            {
                "Id": "md2sqs",
                "TopicArn": "arn:aws:sns:us-east-1:000000000000:mdtopic",
                "Events": ["s3:ObjectCreated:*"],
            },
        ]
    }
)
