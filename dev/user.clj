(ns user
  "User namespace for testing and development."
  (:import [java.io PushbackReader])
  (:require [cayenne.api.v1.feed :as feed]
            [cayenne.conf :refer [cores get-param set-param! start-core! stop-core!]]
            [cayenne.defaults]
            [cayenne.elastic.mappings :as elastic-mappings]
            [cayenne.rdf :as rdf]
            [cayenne.tasks.coverage :refer [check-journals check-members]]
            [cayenne.tasks.funder :refer [index-funders select-country-stmts]]
            [cayenne.tasks.journal :refer [index-journals]]
            [cayenne.tasks.member :refer [index-members]]
            [clj-http.client :as http]
            [clojure.edn :as edn]
            [clojure.java.io :as io :refer [file resource reader]]
            [clojure.pprint :as pprint]
            [clojure.string :as string]
            [me.raynes.fs :refer [copy-dir delete-dir]]
            [nio2.dir-seq :refer [dir-seq-glob]]
            [nio2.io :refer [path]]
            [qbits.spandex :as elastic]
            [robert.bruce :as bruce]))

(doseq [file-name ["apa.csl" "locales-en-US.xml"]]
  (assert (resource file-name) (str file-name " file missing. Make sure CSL style and locale files are available.")))

(defn client []
  (qbits.spandex/client {:hosts (get-param [:service :elastic :urls])}))

(defn- elastic-ready? []
  (try
    (-> (client)
        (elastic/request {:url [:_cat :indices] :method :get})
        :status
         #{200}
        boolean)

    (catch Exception e
      (println (.getMessage e))
      (.printStackTrace e)
      false)))

(defn wait-for-elastic?
  "Wait for Elastic to become ready, return success."
  []
  (bruce/try-try-again {:tries 100 :sleep 10000} elastic-ready?))

(defn delete-elastic-indexes
  "Restore Elastic Search to a initial state between tests."
  ; Implemented in user because there's no good reason to do this in production.
  []
  (-> (client)
      (elastic/request {:url [:_all ] :method :delete})))


(defn flush-elastic
  "Wait for all Elastic Search indexing activity to complete before proceeding."
  []
  (-> (client)
      (elastic/request {:url [:_all :_flush]
                        :method :post
                        :query-string
                        {:wait_if_ongoing true
                         :force true}}))
  (-> (client)
      (elastic/request {:url [:_all :_refresh]
                        :method :post})))

(defn create-elastic-indexes []
  (elastic-mappings/create-indexes (client)))

(def core-started? (atom false))

(defn start []
  ; For easier debugging ingest only one at once.
  (set-param! [:val :feed-concurrency] 1)

  (when-not (wait-for-elastic?)
    (println "Error: Can't start Elastic")
    (System/exit 1))

  ; Delete the Elastic index when starting, not stopping.
  ; This allows us to inspect it after tests for debugging.
  (delete-elastic-indexes)
  (create-elastic-indexes)

  (when (and (not @core-started?)
             (start-core! :default :api :feed-api))
    (reset! core-started? true)))

(defn reset []
  (stop-core! :default)
  (start-core! :default :api :feed-api))

(defn index-work-files
 "Simple ingest a list of files from resource filenames."
 ([filenames]
  (index-work-files filenames "application/vnd.crossref.unixsd+xml"))
 ([filenames content-type]
  (doseq [filename filenames]
    (with-open [rdr (-> filename file reader)]
      (feed/process! content-type rdr)))))

(defn index-work-files-dir
 [dirpath]
 (let [filenames (-> dirpath file .list)]
  (index-work-files (map (partial str dirpath "/") filenames))))

(defn index-journal-list
  [filename]
  (with-open [rdr (-> filename file reader)]
    (index-journals rdr)))

(defn index-member-list
 "Load member list from filename containing an EDN index structure."
 [members-filename prefixes-filename]
 (with-open [members-rdr (-> members-filename file reader PushbackReader.)
             prefixes-rdr (-> prefixes-filename file reader PushbackReader.)]
  (let [member-list (edn/read members-rdr)
        prefix-list (edn/read prefixes-rdr)
        get-test-prefix (fn [prefix-list member prefix]
                          (let [filtered (filter #(and (= member (:member-id %))
                                                       (= prefix (:value %)))
                                           prefix-list)]
                            (first filtered)))]
    (with-redefs [cayenne.tasks.member/get-member-list (constantly member-list)
                  cayenne.tasks.member/get-prefix-info (partial get-test-prefix prefix-list)]
      (index-members)))))

(defn index-funder-list [filename]
  (with-redefs
    [cayenne.tasks.funder/get-country-literal-name
     (fn [model node]
       (let [url (rdf/->uri (first (rdf/objects (select-country-stmts model node))))]
         (case url
           "http://sws.geonames.org/2921044/" "Germany"
           "http://sws.geonames.org/6252001/" "United States"
           "http://sws.geonames.org/2077456/" "Australia"
           "http://sws.geonames.org/337996/" "Ethiopia"
           "http://sws.geonames.org/1814991/" "China"
           "http://sws.geonames.org/2635167/" "United Kingdom"
           "http://sws.geonames.org/3144096/" "Norway"
           "http://sws.geonames.org/2661886/" "Sweden"
           "http://sws.geonames.org/1861060/" "Japan"
           url)))]
    (index-funders filename)))


(defn setup-feed
  "Build structure of corpus, feed in and feed out directories.
   Start by deleting the feed in and out directories.
   May be used either for testing or production."
  [& {:keys [source-dir] :or {source-dir "/corpus"}}]
  (let [feed-dir (.getPath (resource "feeds"))

        ; source-dir is configurable or '/corpus'
        feed-source-dir (str feed-dir source-dir)

        ; feed-in is where the 'pusher' places files to be indexed in production.
        feed-in-dir (str feed-dir "/feed-in")

        feed-processed-dir (str feed-dir "/feed-processed")
        feed-file-count (count (dir-seq-glob (path feed-source-dir) "*.body"))]
    (delete-dir feed-processed-dir)
    (delete-dir feed-in-dir)
    (set-param! [:dir :data] feed-dir)
    (set-param! [:dir :test-data] feed-dir)
    (println "Feed source dir is " feed-source-dir)
    {:feed-source-dir feed-source-dir
     :feed-in-dir feed-in-dir
     :feed-file-count feed-file-count}))

(defn body-file-count
  [dir]
  (->> dir
       io/file
       file-seq
       (map #(.getName %))
       (filter #(clojure.string/ends-with? % ".body"))
       count))

(defn index-feed
  "Set up an instance with the test corpus indexed."
  [& {:keys [source-dir]
      :or {source-dir (or (System/getenv "CAYENNE_API_TEST_CORPUS") "/corpus")}}]

  (let [{:keys [feed-source-dir feed-in-dir]} (setup-feed :source-dir source-dir)
        num-feed-files (body-file-count feed-source-dir)]

    (println "Indexing journals...")
    (index-journal-list (.getPath (resource "titles.csv")))

    (println "Indexing members...")
    (index-member-list
      (.getPath (resource "get-member-list.edn"))
      (.getPath (resource "get-prefix-info.edn")))

    (println "Indexing funders...")
    (index-funder-list (.getPath (resource "registry.rdf")))

    (println "Copying" num-feed-files "files of test data from" feed-source-dir "to" feed-in-dir)
    (copy-dir feed-source-dir feed-in-dir)

    (println "Indexing works...")
    (feed/feed-once!)

    ; Wait for all docs to be indexed because we're going to query them in coverage.
    (println "Flush indexes...")
    (flush-elastic)

    ; Build coverage.
    (println "Generate coverage for journals...")
    (check-journals)

    (println "Generate coverage for members...")
    (check-members)

    (println "Flush coverage indexes...")
    (flush-elastic)

    (println "Done creating instance.")))

(defn spit-resource [path data]
  (spit (str "dev-resources/" path) (with-out-str (pprint/pprint data))))

(def system @cores)

(defn remove-feed-references []
  (let [feed-dir (.getPath (resource "feeds"))
        feed-corpus-dir (str feed-dir "/corpus")
        dois (->> (http/get "http://localhost:3000/works?rows=1000&select=DOI" {:as :json})
                  :body
                  :message
                  :items
                  (map :DOI))]
    (doseq [doi dois]
      (let [clean-file-path (str feed-corpus-dir "/crossref-unixsd-" (str (java.util.UUID/randomUUID)) ".body")]
        (println "Downloading clean version of " doi)
        (spit clean-file-path (slurp (str "https://www.crossref.org/openurl/?pid={REPLACE-ME}&noredirect=true&format=unixsd&id=doi:" doi)))
        (Thread/sleep 1000)
        (println "Saved clean version of" doi "to" clean-file-path)))))
