stages:
  - lint
  - tests
  - release
  - build
  - package
  - deploy
  
image: docker:stable

services:
  - name: docker:dind

lint:
  image: alpinelinux/docker-compose:latest
  stage: lint
  variables:
    DOCKER_HOST: tcp://docker:2375
  before_script:
    - apk add --no-cache git
    - docker network create cayenne_default
    - git submodule update --init
  script:
    - docker-compose -f docker-compose.yml run api lein eastwood
  only:
    - branches
    - merge_requests

unit-tests:
  image: alpinelinux/docker-compose:latest
  stage: tests
  variables:
    DOCKER_HOST: tcp://docker:2375
  before_script:
    - apk add --no-cache git
    - docker network create cayenne_default
    - git submodule update --init
  script:
    - docker-compose -f docker-compose.yml run api lein difftest :unit
  only:
    - branches
    - merge_requests

component-tests:
  image: alpinelinux/docker-compose:latest
  stage: tests
  variables:
    DOCKER_HOST: tcp://docker:2375
  before_script:
    - apk add --no-cache git
    - docker network create cayenne_default
    - git submodule update --init
  script:
    - docker-compose -f docker-compose.yml run api lein test :component
  only:
    - branches
    - merge_requests

integration-tests:
  image: alpinelinux/docker-compose:latest
  stage: tests
  variables:
    DOCKER_HOST: tcp://docker:2375
  before_script:
    - apk add --no-cache git
    - docker network create cayenne_default
    - git submodule update --init
  script:
    - docker-compose -f docker-compose.yml run api lein test :integration
  only:
    - branches
    - merge_requests

get-semantic-version:
  image: public.ecr.aws/crossref/cicada:latest
  stage: release
  tags:
    - aws
    - ec2
    - amd64
  script:
    - npx semantic-release
  only:
    - labs
    - main
  artifacts:
    paths:
      - VERSION.txt
      - CHANGELOG.md

get-generic-version:
  stage: release
  except:
    refs:
      - main
      - alpha
      - $MAINT_BRANCH_PATTERN
      - $PREREL_BRANCH_PATTERN
  script:
    - echo "Using generic version"
    - echo build-$CI_PIPELINE_ID > VERSION.txt
  when: manual
  artifacts:
    paths:
      - VERSION.txt

build-jar:
  image: public.ecr.aws/docker/library/clojure:temurin-17-lein-bullseye-slim
  stage: build
  script:
    # Set the version so it's compiled in to the JAR file. The change isn't commited to source control. 
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - apt-get update
    - apt-get install -y git
    - export APP_VERSION="$(cat VERSION.txt)"
    - git submodule update --init
    - lein set-version $APP_VERSION
    - lein uberjar
  rules:
    - if: '$CI_COMMIT_TAG'
      when: never
    - if: '$CI_COMMIT_BRANCH == "main"'
      when: on_success
    - if: '$CI_MERGE_REQUEST_ID != null'
      when: never
    - if: '$CI_COMMIT_BRANCH != "main"'
      needs:
        - get-generic-version
  artifacts:
    paths:
      - VERSION.txt
      - target/cayenne*

build_amd64_image:
  image: public.ecr.aws/crossref/cicada:latest
  stage: package
  tags:
    - aws
    - ec2
    - amd64
  services:
    - docker:dind
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - build_image.sh amd64 "${CI_PROJECT_DIR}/Dockerfile.prod" cayenne $APP_VERSION $CI_PROJECT_DIR
  only:
    - main
    - labs
    - branches
  needs: ['build-jar']
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

build_arm64_image:
  image: public.ecr.aws/crossref/cicada:latest
  stage: package
  tags:
    - aws
    - ec2
    - arm64
  services:
    - docker:dind
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - build_image.sh arm64 "${CI_PROJECT_DIR}/Dockerfile.prod" cayenne $APP_VERSION $CI_PROJECT_DIR
  only:
    - labs
    - main
  needs: ['build-jar']
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

multi_arch_manifest_branches:
  image: public.ecr.aws/crossref/cicada:latest
  stage: package
  tags:
    - aws
    - ec2
    - amd64
  services:
    - docker:dind
  needs: ['build_amd64_image']
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - create_singlearch_manifest.sh cayenne $APP_VERSION amd64
  only:
    - branches
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

multi_arch_manifest:
  image: public.ecr.aws/crossref/cicada:latest
  stage: package
  tags:
    - aws
    - ec2
    - amd64
  services:
    - docker:dind
  needs: ['build_amd64_image', 'build_arm64_image']
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - create_manifest.sh cayenne $APP_VERSION
  only:
    - labs
    - main
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

deploy-jar:
  image: public.ecr.aws/docker/library/clojure:temurin-17-lein-bullseye-slim
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  before_script:
    - apt-get -o Acquire::ForceIPv4=true update
    - apt-get -o Acquire::ForceIPv4=true install -y maven
  script:
    # Set the version so it's compiled in to the JAR file. The change isn't commited to source control. 
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
    - lein set-version $APP_VERSION
    - lein uberjar
    - echo "Deploy JAR for version $APP_VERSION"
    - mvn deploy:deploy-file -DgroupId=org.crossref -DartifactId=cayenne -Dversion="$APP_VERSION" -Dpackaging=jar -Dfile=target/cayenne-$APP_VERSION.jar -DrepositoryId=gitlab-maven -Durl=https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/maven -s settings.xml
  only:
    - main

deploy-research-internal:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: research
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - deploy_container.sh internal-api-research internal-api-research $APP_VERSION
    - deploy_container.sh internal-indexer-research internal-indexer-research $APP_VERSION
    - deploy_container.sh internal-scheduled-tasks-research internal-scheduled-tasks-research $APP_VERSION
    - deploy_container.sh api-scanner-internal-research api-scanner-internal-research $APP_VERSION skip_service
  only:
    - labs
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

deploy-research-public:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: research
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - deploy_container.sh public-api-research public-api-research $APP_VERSION
    - deploy_container.sh public-indexer-research public-indexer-research $APP_VERSION
    - deploy_container.sh public-scheduled-tasks-research public-scheduled-tasks-research $APP_VERSION
    - deploy_container.sh api-scanner-public-research api-scanner-public-research $APP_VERSION skip_service
  only:
    - labs
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

deploy-research-polite:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: research
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - deploy_container.sh polite-api-research polite-api-research $APP_VERSION
    - deploy_container.sh polite-indexer-research polite-indexer-research $APP_VERSION
    - deploy_container.sh polite-scheduled-tasks-research polite-scheduled-tasks-research $APP_VERSION
    - deploy_container.sh api-scanner-polite-research api-scanner-polite-research $APP_VERSION skip_service
  only:
    - labs
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

deploy-research-plus:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: research
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - deploy_container.sh plus-api-research plus-api-research $APP_VERSION
    - deploy_container.sh plus-indexer-research plus-indexer-research $APP_VERSION
    - deploy_container.sh plus-scheduled-tasks-research plus-scheduled-tasks-research $APP_VERSION
    - deploy_container.sh api-scanner-plus-research api-scanner-plus-research $APP_VERSION skip_service
  only:
    - labs
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

deploy-staging-internal:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: staging
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - deploy_container.sh internal-api-staging internal-api-staging $APP_VERSION
    - deploy_container.sh internal-indexer-staging internal-indexer-staging $APP_VERSION
    - deploy_container.sh internal-scheduled-tasks-staging internal-scheduled-tasks-staging $APP_VERSION
    - deploy_container.sh api-scanner-internal-staging api-scanner-internal-staging $APP_VERSION skip_service
  rules:
    - if: '$CI_COMMIT_TAG'
      when: never
    - if: '$CI_COMMIT_BRANCH == "main"'
      when: on_success
    - if: '$CI_MERGE_REQUEST_ID != null'
      when: never
    - if: '$CI_COMMIT_BRANCH != "main"'
      needs: ['multi_arch_manifest_branches']
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

deploy-staging-public:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: staging
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - deploy_container.sh public-api-staging public-api-staging $APP_VERSION
    - deploy_container.sh public-indexer-staging public-indexer-staging $APP_VERSION
    - deploy_container.sh public-scheduled-tasks-staging public-scheduled-tasks-staging $APP_VERSION
    - deploy_container.sh api-scanner-public-staging api-scanner-public-staging $APP_VERSION skip_service
  rules:
    - if: '$CI_COMMIT_TAG'
      when: never
    - if: '$CI_COMMIT_BRANCH == "main"'
      when: on_success
    - if: '$CI_MERGE_REQUEST_ID != null'
      when: never
    - if: '$CI_COMMIT_BRANCH != "main"'
      needs: ['multi_arch_manifest_branches']
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

deploy-staging-polite:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: staging
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - deploy_container.sh polite-api-staging polite-api-staging $APP_VERSION
    - deploy_container.sh polite-indexer-staging polite-indexer-staging $APP_VERSION
    - deploy_container.sh polite-scheduled-tasks-staging polite-scheduled-tasks-staging $APP_VERSION
    - deploy_container.sh api-scanner-polite-staging api-scanner-polite-staging $APP_VERSION skip_service
  rules:
    - if: '$CI_COMMIT_TAG'
      when: never
    - if: '$CI_COMMIT_BRANCH == "main"'
      when: on_success
    - if: '$CI_MERGE_REQUEST_ID != null'
      when: never
    - if: '$CI_COMMIT_BRANCH != "main"'
      needs: ['multi_arch_manifest_branches']
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

deploy-staging-plus:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: staging
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - deploy_container.sh plus-api-staging plus-api-staging $APP_VERSION
    - deploy_container.sh plus-indexer-staging plus-indexer-staging $APP_VERSION
    - deploy_container.sh plus-scheduled-tasks-staging plus-scheduled-tasks-staging $APP_VERSION
    - deploy_container.sh api-scanner-plus-staging api-scanner-plus-staging $APP_VERSION skip_service
  rules:
    - if: '$CI_COMMIT_TAG'
      when: never
    - if: '$CI_COMMIT_BRANCH == "main"'
      when: on_success
    - if: '$CI_MERGE_REQUEST_ID != null'
      when: never
    - if: '$CI_COMMIT_BRANCH != "main"'
      needs: ['multi_arch_manifest_branches']
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

deploy-production-internal:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: production
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - deploy_container.sh internal-api-production internal-api-production $APP_VERSION
    - deploy_container.sh internal-indexer-production internal-indexer-production $APP_VERSION
    - deploy_container.sh internal-scheduled-tasks-production internal-scheduled-tasks-production $APP_VERSION
    - deploy_container.sh api-scanner-internal-production api-scanner-internal-production $APP_VERSION skip_service
  only:
    - main
  when: manual
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

deploy-production-public:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: production
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - deploy_container.sh public-api-production public-api-production $APP_VERSION
    - deploy_container.sh public-indexer-production public-indexer-production $APP_VERSION
    - deploy_container.sh public-scheduled-tasks-production public-scheduled-tasks-production $APP_VERSION
    - deploy_container.sh api-scanner-public-production api-scanner-public-production $APP_VERSION skip_service
  only:
    - main
  when: manual
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

deploy-production-polite:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: production
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - deploy_container.sh polite-api-production polite-api-production $APP_VERSION
    - deploy_container.sh polite-indexer-production polite-indexer-production $APP_VERSION
    - deploy_container.sh polite-scheduled-tasks-production polite-scheduled-tasks-production $APP_VERSION
    - deploy_container.sh api-scanner-polite-production api-scanner-polite-production $APP_VERSION skip_service
  only:
    - main
  when: manual
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt

deploy-production-plus:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: production
  before_script:
    - if [ ! -f "VERSION.txt" ]; then echo "VERSION.txt file missing, skipping deploy"; exit 0; fi
    - export APP_VERSION="$(cat VERSION.txt)"
  script:
    - deploy_container.sh plus-api-production plus-api-production $APP_VERSION
    - deploy_container.sh plus-indexer-production plus-indexer-production $APP_VERSION
    - deploy_container.sh plus-scheduled-tasks-production plus-scheduled-tasks-production $APP_VERSION
    - deploy_container.sh api-scanner-plus-production api-scanner-plus-production $APP_VERSION skip_service
  only:
    - main
  when: manual
  artifacts:
    paths:
      - CHANGELOG.md
      - VERSION.txt
