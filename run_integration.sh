case $1 in
test)
  docker-compose -f docker-compose-integration.yml run --service-ports api lein run :nrepl
  ;;
run)
  docker-compose -f docker-compose-integration.yml kill 
  docker-compose -f docker-compose-integration.yml rm --force
  docker-compose -f docker-compose-integration.yml up $2
  ;;
stop)
  docker-compose -f docker-compose-integration.yml kill 
  docker-compose -f docker-compose-integration.yml rm --force
  ;;
esac
