
## Facets

Summary statistics counts can be retrieved by enabling faceting. Facets are enabled by providing a facet field name along with a maximum number of returned term values. The larger the number of returned values, the longer the query will take. Some facet fields can accept a `*` as their maximum, which indicates that all values should be returned.

Note that facet counts use approximation based on assumptions about the data. Some facets count the number of relationships and double-count a record with the same relationship two or more times (e.g. a record with two published Corrections). They may therefore differ from exact counts obtained using filters.

##

For example, to get facet counts for all work types:

##
```
/works?facet=type-name:*
```

##

This endpoint supports the following facets:

##
