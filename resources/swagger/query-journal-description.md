
## Queries

Free form search queries can be made, for example, journals that include `pharmacy` and `health`:

##

```
/journals?query=pharmacy+health
```

