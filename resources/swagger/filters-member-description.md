
## Filters

Filters allow you to select items based on specific criteria. All filter results are lists.

##

Example:

##
```
/members?filter=current-doi-count:0
```
##

### Multiple filters

Multiple filters can be specified in a single query. In such a case, different filters will be applied with AND semantics, while specifying the same filter multiple times will result in OR semantics - that is, specifying the filters:

- `current-doi-count:0`
- `backfile-doi-count:0`
- `prefix:10.1296`
- `prefix:10.2481`

would locate members with no current DOIs and no backfile DOIs and with prefix 10.1296 or 10.2481. These filters would be specified by joining each filter together with a comma:

##
```
/members?filter=current-doi-count:0,backfile-doi-count:0,prefix:10.1296,prefix:10.2481
```
##

This endpoint supports the following filters:

##
