
## Parameter combinations

Any combination of `query`, `query.*`, `filter`, `facet`, `select` and `sort` can be used with offsets. Sampling cannot be combined with offsets.

##

Any combination of `query`, `query.*`, `filter`, `facet`, `select` and `sort` may also be used with deep paging cursors. `rows` may also be specified.

##

`offset` and `sample` cannot be used in combination with cursors.

##
