
## Deep paging

Deep paging using cursors can be used to iterate over large result sets, without any limits on their size.

##

To use deep paging make a query as normal, but include the `cursor` parameter with a value of `*`, for example:

##

```
/members/311/works?filter=type:journal-article&cursor=*
```

##

A `next-cursor` field will be provided in the JSON response. To get the next page of results, pass the value of `next-cursor` as the cursor parameter (remember to URL-encode). For example:

##

```
/members/311/works?filter=type:journal-article&cursor=<value of next-cursor parameter>
```

##

Clients should check the number of returned items. If the number of returned items is equal to the number of expected rows then the end of the result set has been reached. Using next-cursor beyond this point will result in responses with an empty items list. Cursors expire after 5 minutes if they are not used.

##
