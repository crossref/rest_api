FROM clojure:openjdk-17-lein

COPY . /usr/src/app
RUN mkdir /usr/src/app/data
WORKDIR /usr/src/app
RUN apt-get -o Acquire::ForceIPv4=true update && apt-get -o Acquire::ForceIPv4=true install -y git curl unzip
RUN git submodule update --init
RUN if [ -f "VERSION.txt" ]; then lein set-version "$(cat VERSION.txt)"; fi
RUN lein deps
CMD ["bash","docker-entrypoint.sh"]
