JAVA_OPTS="-XX:+UseG1GC -XX:MaxRAMPercentage=80"
VERSION=`cat VERSION.txt`
VERSION=${VERSION:-UNVERSIONED}

case $APPLICATION in
  "api")
    echo "running $APPLICATION"
    java $JAVA_OPTS -Dcayenne.version=$VERSION -jar cayenne.jar :api
    ;;
  "indexer")
    echo "running $APPLICATION"
    java $JAVA_OPTS -Dcayenne.version=$VERSION -jar cayenne.jar :create-mappings :update-index-settings :sqs-ingest
    ;;
  "scheduled-tasks")
    echo "running $APPLICATION"
    java $JAVA_OPTS -Dcayenne.version=$VERSION -jar cayenne.jar :create-mappings :update-index-settings :update-members :update-journals :update-funders :update-subjects :process-calculated-data :retraction-watch-fetcher
    ;;
  "scanner")
    echo "running $APPLICATION"
    java $JAVA_OPTS -Dcayenne.version=$VERSION -jar cayenne.jar :s3-sqs-produce-all
    ;;
  "all")
    echo "running $APPLICATION"
    java $JAVA_OPTS -Dcayenne.version=$VERSION -jar cayenne.jar :nrepl :api :deposit-api :create-mappings :update-index-settings :sqs-ingest :update-members :update-journals :update-funders :update-subjects :doi-update-service
    ;;
  *)
    echo "APPLICATION $APPLICATION unknown";;
esac
