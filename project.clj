(defproject crossref/cayenne "SNAPSHOT"
  :description "Index and serve CrossRef metadata"
  :url "http://github.com/CrossRef/cayenne"
  :repl-options {:port 7880 :init-ns cayenne.user :timeout 120000}
  :main cayenne.production
  :jvm-opts ["-XX:+UseG1GC" "-XX:MaxRAMPercentage=80"]
  :resource-paths ["csl/styles" "csl/locales" "resources"]
  :daemon {:cayenne {:ns cayenne.production
                     :pidfile "cayenne.pid"}}
  :profiles {:uberjar {:aot :all
                       :env {:production true}
                       :uberjar-name "cayenne.jar"
                       :omit-source true }
             :dev {:source-paths ["dev"]
                   :resource-paths ["dev-resources"]
                   :dependencies [[marge "0.11.0"]
                                  [djblue/portal "0.53.0"]]
                   :eastwood {:config-files ["eastwood-disabled-warnings.clj"]}}
             :itviewer {:main cayenne.item-tree-viewer}
             :prod { }}
  :test-selectors {:default (constantly true)
                   :unit :unit
                   :component :component
                   :integration :integration
                   :all (constantly true)
                   :manual :manual}
  :dependencies [[amazonica "0.3.157" :exclusions [*]]
                 [com.amazonaws/aws-java-sdk-signer "1.12.132"]
                 [org.clojure/algo.generic "0.1.2"]
                 [com.amazonaws/aws-java-sdk-s3 "1.12.132"]
                 [com.amazonaws/aws-java-sdk-sqs "1.12.132"]
                 [com.amazonaws/aws-java-sdk-s3control "1.12.132"]
                 [robert/hooke "1.3.0"]

                 [org.clojure/data.xml "0.2.1-SNAPSHOT"]
                 [org.clojure/google-closure-library "0.0-2029-2"]
                 [org.clojure/clojure "1.11.2"]
                 [org.clojure/core.async "1.6.681"]
                 [nrepl "1.1.0"]
                 [org.mozilla/rhino "1.7.7.1"]
                 [de.undercouch/citeproc-java "2.0.0"]
                 [info.hoetzel/clj-nio2 "0.1.1"]
                 [xml-apis "1.4.01"]
                 [me.raynes/fs "1.4.6"]
                 [com.taoensso/timbre "6.5.0"]
                 [com.cemerick/url "0.1.1"]
                 [clojurewerkz/quartzite "1.0.1"]
                 [enlive "1.1.1"]
                 [org.apache.jena/jena-core "4.3.1"]
                 [org.apache.jena/jena-arq "4.3.1"]
                 [clj-time "0.14.0"]
                 [clj-http "3.7.0"]
                 [org.clojure/core.incubator "0.1.2"]
                 [org.clojure/data.json "0.2.0"]
                 [org.clojure/data.xml "0.2.0-alpha6"]
                 [org.clojure/data.zip "0.1.1"]
                 [org.clojure/data.csv "0.1.2"]
                 [org.clojure/core.memoize "0.5.8"]
                 [org.clojure/math.combinatorics "0.0.4"]
                 [liberator "0.15.2"]
                 [compojure "1.6.0"]
                 [ring "1.9.3"]
                 [ring/ring-mock "0.3.2"]
                 [ring-logger "1.1.1"]
                 [ring/ring-codec "1.1.3"]
                 [bk/ring-gzip "0.3.0"]
                 [metosin/ring-swagger "0.26.2"]
                 [metosin/ring-swagger-ui "4.15.5"]
                 [ring-basic-authentication "1.0.5"]
                 [http-kit "2.7.0"]
                 [instaparse "1.4.1"]
                 [robert/bruce "0.8.0"]
                 [bigml/sampling "3.0"]
                 [digest "1.4.4"]
                 [cc.qbits/spandex "0.5.2"]
                 [environ "1.0.3"]
                 [slingshot "0.12.2"]
                 [org.apache.commons/commons-compress "1.19"]
                 [io.sentry/sentry-clj "3.1.138"]
                 [com.climate/claypoole "1.1.4"]
                 [com.github.amazon-archives/aws-request-signing-apache-interceptor "f39dfaf" :exclusions [com.amazonaws/aws-java-sdk-core]]

                 ;For keycloak authorization
                 [org.jetbrains.kotlin/kotlin-stdlib "1.5.31"]
                 [org.crossref/internalsdk "1.5.1"]
                 [commons-httpclient/commons-httpclient "3.1"]
                 [software.amazon.awssdk/cloudwatch "2.20.25"]
                 [org.jetbrains.kotlinx/kotlinx-coroutines-core "1.6.4"]

                 ; Only for tests.
                 [cheshire "5.8.1"]]

  :plugins [[jonase/eastwood "0.3.5"]
            [cider/cider-nrepl "0.50.2"]
            [mx.cider/enrich-classpath "1.19.3"]
            [lein-set-version "0.4.1"]
            [lein-difftest "2.0.0"]]

  :repositories [["jitpack.io" "https://jitpack.io"]
                 ["gitlab-maven" "https://gitlab.com/api/v4/projects/40776694/packages/maven"]])

