(ns cayenne.ids.doi
  (:require [clojure.string :as string]
            [cayenne.ids :as ids]))

(def funder-prefix "10.13039")

(defn is-long-doi?
  "Return true if s is a valid long DOI handle without URI prefix."
  [s]
  (not (nil? (re-find #"\A10\.[0-9]{4,}/[^\s]+\Z" s))))

(defn is-short-doi?
  "Return true if s is a valid short DOI handle without URI prefix."
  [s]
  (not (nil? (re-find #"\A10/[a-z0-9]+\Z" s))))

(defn is-any-doi? [s]
  (or (is-short-doi? s) (is-long-doi? s)))  

(defn extract-doi
  "Attempt to extract a DOI from the forms:
   http://dx.doi.org/<DOI>
   dx.doi.org/<DOI>
   doi:<DOI>
   <DOI>"
  [s]
  (or (re-find #"10\.[0-9]{4,}/[^\s]+" (or s "")) ""))

(defn extract-doi-prefix [s]
  (first (string/split (extract-doi s) #"/")))

(defn normalize-doi [s]
  (when s (.toLowerCase (extract-doi s))))

(defn to-doi-uri
  "Ensure a long DOI is in a normalized URI form."
  [s]
  (when s
    (let [normalized-doi (normalize-doi s)]
      (if (string/blank? normalized-doi)
        nil
        (ids/get-id-uri :doi normalized-doi)))))

(defn doi-uri-to-id
  "takes the last portion of a doi after the last /. Useful for funder identifiers"
  [doi]
  (last (string/split doi #"/")))

(defn with-prefix
  "used to convert a funder id to a funder doi"
  [doi prefix]
  (->> doi
       doi-uri-to-id ;in case we're passed a funder doi instead of just the ID
       (str prefix "/")))

(defn with-funder-prefix [doi]
  (with-prefix doi funder-prefix))

