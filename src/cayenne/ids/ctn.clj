(ns cayenne.ids.ctn
  (:require [clojure.string :as string]))

(defn normalize-ctn
  "Normalize a CTN for display."
  [ctn]
  (when ctn
    (.toLowerCase ctn)))

(defn ctn-proxy
  "Normalize a CTN to a proxy for search."
  [ctn]
  (->
    ctn
    (.toLowerCase)
    (string/replace #"[^a-z0-9]" "")))
