(ns cayenne.ids.container
  (:require [cayenne.ids :as ids]))

(defn journal-number->uri
  "Construct a container ID URI from a journalcite-id number"
  [v]
  (when v
    (ids/get-id-uri :journal-container v)))

(defn series-number->uri
  "Construct a container ID URI from a seriescite-id number"
  [v]
  (when v
    (ids/get-id-uri :series-container v)))

(defn book-number->uri
  "Construct a container ID URI from a bookcite-id number"
  [v]
  (when v
    (ids/get-id-uri :book-container v)))

(defn journal-uri->number
  "If the container URI is for a journalcite-id, extract the value"
    [v]
    (some->> v
    (ids/matches-type? :journal-container)
    (ids/get-id-non-uri :journal-container)))

(defn series-uri->number
  "If the container URI is for a seriescite-id, extract the value"
  [v]
    (some->> v
    (ids/matches-type? :series-container)
    (ids/get-id-non-uri :series-container)))

(defn book-uri->number
  "If the container URI is for a bookcite-id, extract the value"
  [v]
    (some->> v
    (ids/matches-type? :book-container)
    (ids/get-id-non-uri :book-container)))
