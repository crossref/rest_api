(ns cayenne.ids.type)

(defn bibo-type [clss]
  (str "http://purl.org/ontology/bibo/" clss))

(def type-dictionary {:journal-article {:index-id "Journal Article"
                                        :label "Journal Article"
                                        :bibo-type (bibo-type "Article")
                                        :csl-type "article-journal"
                                        :rpp-type "Journal Articles"}
                      :journal-issue {:index-id "Journal Issue"
                                      :label "Journal Issue"
                                      :bibo-type (bibo-type "Issue")}
                      :journal-volume {:index-id "Journal Volume" 
                                       :label "Journal Volume"
                                       :bibo-type (bibo-type "Collection")}
                      :journal {:index-id "Journal"
                                :label "Journal"
                                :bibo-type (bibo-type "Journal")}
                      :proceedings-article {:index-id "Conference Paper"
                                            :label "Proceedings Article"}
                      :proceedings-series {:index-id "Proceedings Series"
                                           :label "Proceedings Series"}
                      :proceedings {:index-id "Proceedings"
                                    :label "Proceedings"}
                      :database {:index-id "Database"
                                 :label "Database"}
                      :dataset {:index-id "Dataset"
                                :label "Dataset"}
                      :component {:index-id "Component"
                                  :label "Component"}
                      :report {:index-id "Report"
                               :label "Report"
                               :bibo-type (bibo-type "Report")}
                      :report-component {:index-id "Report Component"
                                         :label "Report Component"}
                      :report-series {:index-id "Report Series"
                                      :label "Report Series"}
                      :standard {:index-id "Standard"
                                 :label "Standard"}
                      :edited-book {:index-id "Edited Book"
                                    :label "Edited Book"}
                      :monograph {:index-id "Monograph"
                                  :label "Monograph"}
                      :reference-book {:index-id "Reference"
                                       :label "Reference Book"}
                      :book {:index-id "Book"
                             :label "Book"}
                      :book-series {:index-id "Book Series"
                                    :label "Book Series"}
                      :book-set {:index-id "Book Set"
                                 :label "Book Set"}
                      :book-chapter {:index-id "Chapter"
                                     :label "Book Chapter"}
                      :book-section {:index-id "Section"
                                     :label "Book Section"}
                      :book-part {:index-id "Part"
                                  :label "Part"}
                      :book-track {:index-id "Track"
                                   :label "Book Track"}
                      :reference-entry {:index-id "Entry"
                                        :label "Reference Entry"}
                      :dissertation {:index-id "Dissertation"
                                     :label "Dissertation"}
                      :posted-content {:index-id "Posted Content"
                                       :label "Posted Content"}
                      :peer-review {:index-id "Peer Review"
                                    :crossmark-unaware? true
                                    :label "Peer Review"}
                      :other {:index-id "Other"
                              :label "Other"}
                      :grant {:index-id "Grant"
                              :label "Grant"
                              :cn-transforms ["application/vnd.citationstyles.csl+json"
                                              "application/citeproc+json"
                                              "application/json"
                                              "application/vnd.crossref.unixref+xml"
                                              "application/unixref+xml"
                                              "application/vnd.crossref.unixsd+xml"]}})

(def reverse-dictionary
  (reduce 
   (fn [m [key value]]
     (assoc m (:index-id value) key))
   {}
   type-dictionary))

(defn ->type-id [index-str]
  (get reverse-dictionary index-str))
