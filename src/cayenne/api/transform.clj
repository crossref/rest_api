(ns cayenne.api.transform
  (:require [cayenne.conf :as conf]
            [cayenne.formats.rdf :as rdf]
            [cayenne.formats.ris :as ris]
            [cayenne.formats.citation :as citation]
            [clojure.data.json :as json]
            [org.httpkit.client :as hc]
            [clojure.string :as string])
  (:import [java.net URLEncoder]))

(def legacy-styles
  {"mla" "modern-language-association"
   "harvard3" "harvard1"})

(def csl-type
  {:book-chapter :chapter
   :book-part :chapter
   :book-section :chapter
   :book-series :book
   :book-set :book
   :component :figure
   :database :dataset
   :dissertation :thesis
   :edited-book :book
   :grant :article-journal
   :journal :periodical
   :journal-article :article-journal
   :journal-issue :periodical
   :journal-volume :periodical
   :monograph :book
   :other :webpage
   :peer-review :review
   :posted-content :article-journal
   :proceedings :paper-conference
   :proceedings-article :paper-conference
   :proceedings-series :periodical
   :reference-book :webpage
   :reference-entry :entry
   :report-component :report
   :report-series :report})

(defmulti ->format :media-type)

(defmethod ->format "text/turtle" [_ metadata]
  (rdf/->turtle metadata))

(defmethod ->format "text/n3" [_ metadata]
  (rdf/->n3 metadata))

(defmethod ->format "text/n-triples" [_ metadata]
  (rdf/->n-triples metadata))

(defmethod ->format "application/rdf+xml" [_ metadata]
  (rdf/->xml metadata))

(defmethod ->format "application/vnd.citationstyles.csl+json" [_ metadata]
  (json/write-str
    (cond-> metadata
      (-> metadata :type csl-type)
      (assoc :type (csl-type (:type metadata)))

      (seq (:title metadata))
      (assoc :title (first (:title metadata)))

      (seq (:container-title metadata))
      (assoc :container-title (first (:container-title metadata)))

      (seq (:short-container-title metadata))
      (assoc :container-title-short (first (:short-container-title metadata)))

      (seq (:event metadata))
      (assoc :event (get-in metadata [:event :name]))

      :always
      (dissoc :short-container-title :archive :issn-type))))

(defmethod ->format "application/x-research-info-systems" [_ metadata]
  (ris/->ris metadata))

(defmethod ->format "text/x-bibliography" [representation metadata]
  (let [args (concat
              (when-let [style (get-in representation [:parameters :style])]
                [:style (or (legacy-styles style) style)])
              (when-let [lang (get-in representation [:parameters :locale])]
                [:language lang])
              (when-let [format (get-in representation [:parameters :format])]
                [:format format]))]
    (apply citation/->citation metadata args)))

(defmethod ->format "application/x-bibtex" [representation metadata]
  (->format (-> representation
                (assoc :media-type "text/bibliography")
                (assoc-in [:parameters :style] "bibtex"))
            metadata))

;; legacy formats

(defmethod ->format "text/bibliography" [representation metadata]
  (->format (assoc representation :media-type "text/x-bibliography")
            (-> metadata
                (update :type #(-> % keyword csl-type (or %) name))
                (assoc :title (->> metadata
                                   ((juxt :title :subtitle))
                                   (map first)
                                   (remove string/blank?)
                                   (string/join ": ")
                                   list
                                   set))
                (assoc :collection-title (get-in metadata [:event :acronym])))))

(defmethod ->format "application/citeproc+json" [representation metadata]
  (->format (assoc representation :media-type "application/vnd.citationstyles.csl+json")
            metadata))

(defmethod ->format "application/json" [representation metadata]
  (->format (assoc representation :media-type "application/vnd.citationstyles.csl+json")
            metadata))

(defmethod ->format "application/unixref+xml" [representation metadata]
  (->format (assoc representation :media-type "application/vnd.crossref.unixref+xml")
            metadata))

(defmethod ->format "text/plain" [representation metadata]
  (->format (assoc representation :media-type "text/x-bibliography")
            metadata))

;; for now we retrieve original unixref and unixsd, but in future perhaps we
;; will generate from citeproc

(defmethod ->format "application/vnd.crossref.unixref+xml" [_ metadata]
  (-> metadata
      :DOI
      (URLEncoder/encode "UTF-8")
      ((partial str (conf/get-param [:upstream :unixref-url])))
      (hc/get {:timeout 4000})
      (deref)))

(defmethod ->format "application/vnd.crossref.unixsd+xml" [_ metadata]
  (-> metadata
      :DOI
      (URLEncoder/encode "UTF-8")
      ((partial str (conf/get-param [:upstream :unixsd-url])))
      (hc/get {:timeout 4000})
      (deref)))
