(ns cayenne.api.v1.filter
  (:require [clj-time.core :as dt]
            [clj-time.coerce :as dc]
            [clojure.string :as string]
            [cayenne.conf :as conf]
            [cayenne.ids :as ids]
            [cayenne.ids.type :as type-id]
            [cayenne.ids.prefix :as prefix]
            [cayenne.ids.issn :as issn]
            [cayenne.ids.isbn :as isbn]
            [cayenne.ids.orcid :as orcid]
            [cayenne.ids.doi :as doi-id]
            [cayenne.ids.member :as member-id]
            [cayenne.ids.ror-id :as ror-id]
            [cayenne.elastic.util :as elastic-util]
            [qbits.spandex :as elastic]))

;; Helpers
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn split-date [date-str]
  (let [date-parts (string/split date-str #"-")]
    {:year (Integer/parseInt (first date-parts))
     :month (Integer/parseInt (nth date-parts 1 "-1"))
     :day (Integer/parseInt (nth date-parts 2 "-1"))}))

(defn obj-date [date-str & {:keys [direction]}]
  (let [date-parts (split-date date-str)
        d (cond (not= (:day date-parts) -1)
                (dt/date-time (:year date-parts) (:month date-parts) (:day date-parts))
                (not= (:month date-parts) -1)
                (dt/date-time (:year date-parts) (:month date-parts))
                :else
                (dt/date-time (:year date-parts)))]
    (if (not= direction :until)
      d
      (let [add-period (cond (not= (:day date-parts) -1)
                             dt/days
                             (not= (:month date-parts) -1)
                             dt/months
                             :else
                             dt/years)]
        (-> d (dt/plus (add-period 1)) (dt/minus (dt/millis 1)))))))


;; Elastic filters
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn binary-values [val]
  (->> (if (sequential? val) val [val])
       (map #(.toLowerCase %))
       (map {"t" 1 "true" 1 "1" 1 "f" 0 "false" 0 "0" 0})
       set))

(defn greater-than-zero [field]
  (fn [val]
    (let [vals (binary-values val)]
      (cond (= #{1} vals)
            {:occurrence :filter
             :clause {:range {field {:gte 1}}}}
            (= #{0} vals)
            {:occurrence :filter
             :clause {:range {field {:lte 0}}}}))))

(defn range-nested-query [term value]
  (let [qry-hsh {:query {:bool {:must []}}}
        values (into [] (map (fn [v] {:range {term {:lte (Integer/parseInt v)}}}) value))]
    (assoc-in qry-hsh [:query :bool :must] values)))

(defn match-nested-query [term value]
  {:query {:terms {term value}}})

(defn existence [field]
  (fn [val]
    (let [vals (binary-values val)]
      (cond (= #{1} vals)
            {:occurrence :filter
             :clause {:exists {:field field}}}
            (= #{0} vals)
            {:occurrence :must-not
             :clause {:exists {:field field}}}))))

(defn bool
  "Return an anonymous function with a single `val` parameter where:
   if `val` translates to 1 then filter for `field` being true
   if `val` translates to 0 then filter for `field` being false
   if `val` translates to anything else then answer if `field` exists." 
  [field]
  (fn [val]
    (let [vals (binary-values val)]
      (cond (= #{1} vals)
            {:occurrence :filter
             :clause {:term {field true}}}
            (= #{0} vals)
            {:occurrence :filter
             :clause {:term {field false}}}
            :else
            {:occurrence :filter
             :clause {:exists {:field field}}}))))

(defn bool-equality [field value]
  (fn [bool-val]
    (let [vals (binary-values bool-val)]
      (cond (= #{1} vals)
            {:occurrence :filter
             :clause {:term {field value}}}
            (= #{0} vals)
            {:occurrence :must-not
             :clause {:term {field value}}}))))

(defn equality [field & {:keys [transformer] :or {transformer identity}}]
  (fn [val]
    (let [terms (if (sequential? val) val [val])]
      {:occurrence :filter :clause {:terms {field (map transformer terms)}}})))

(defn date-range [field end-point]
  (fn [val]
    (let [vals (if (sequential? val) val [val])
          dates (map #(obj-date % :direction end-point) vals)
          pred (fn [d1 d2]
                 (let [p (if (= :from end-point) dt/before? dt/after?)]
                   (if (p d1 d2) d1 d2)))
          union-date (reduce pred dates)
          direction (if (= :from end-point) :gte :lte)]
      {:occurrence :filter
       :clause {:range {field {direction (.toString union-date)}}}})))

(defn number-range [field direction]
  (fn [val]
    (let [value (->> (if (sequential? val) val [val])
                     (map #(Integer/parseInt %))
                     (apply (if (= :lte direction) max min)))]
      {:occurrence :filter
       :clause {:range {field {direction value}}}})))

(defn replace-keys [m kr]
  (into {} (map (fn [[k v]] (if-let [replacement (get kr k)] [replacement v] [k v])) m)))

(defn nested-terms [prefix suffixes & {:keys [transformers matchers] :or {transformers (constantly identity) matchers (constantly match-nested-query)}}]
  (fn [val]
    (map
      #(let [kw (-> % first keyword)
             field-name (->> kw suffixes name (str (name prefix) ".") keyword)
             value (map (transformers kw) (second %))
             nested-hsh {:occurrence :filter
                         :clause {:nested {:path prefix}}}]
          ;; allowing for different types of nested query searches, the default is the match query
          (update-in nested-hsh [:clause :nested] merge ((matchers kw match-nested-query) field-name value)))
      val)))

(defn nested [filter-fn path & paths]
  (fn [val]
    (let [{:keys [occurrence clause]} (filter-fn val)
          clause (reduce
                   #(hash-map :nested {:path %2 :query %1})
                   {:nested {:path path :query clause}}
                   paths)] 
      {:occurrence occurrence
       :clause clause})))

(defn nested-multi-field [suffix-fns]
  (fn [val]
    (map
      #(let [suffix (-> % first keyword)
             suffix-fn (suffix-fns suffix)]
         (suffix-fn (second %)))
      val)))

(defn or-ed
  "Note: We cannot use or-ed for truthful comparisons of functions
   that do not return a :filter occurrence"
  [filter-fn1 filter-fn2 & filter-fns]
  (fn [val]
    (let [filter-clauses (->> (concat [filter-fn1 filter-fn2] filter-fns)
                              (map #(% val)))]
      (when (some #(not= :filter (:occurrence %)) filter-clauses)
        (throw (Exception. "OR filter can only be applied to filters")))
      {:occurrence :filter
       :clause {:bool {:should (map :clause filter-clauses) :minimum_should_match 1}}})))

(defn member-ids
  ([doi-counts type] (member-ids doi-counts type 0 10000))
  ([doi-counts type offset size]
   (let [es-request {:method :get
                     :url (str (elastic-util/index-url-prefix :coverage) "_search")
                     :body {:_source [:subject-id]
                            :query {:bool {:filter [{:terms {type doi-counts}}
                                                    {:term {:subject-type "member"}}]}}
                            :sort [:subject-id]
                            :from offset
                            :size size}}
         es-response (elastic/request (conf/get-service :elastic) es-request)
         ids (->> [:body :hits :hits]
                  (get-in es-response)
                  (map :_source)
                  (map :subject-id))]
     (if (< (count ids) size)
       ids
       (lazy-cat ids (member-ids doi-counts type (+ offset size) size))))))

(defn members-doi-count [type]
  (fn [doi-count]
    (let [doi-counts (if (sequential? doi-count) doi-count [doi-count])
          ids (member-ids doi-counts type)]
      {:occurrence :filter :clause {:terms {:id ids}}})))

(defn has-award []
  (fn [val]
    (let [award-filter ((existence :award-keyword) val)
          award-nested-filter ((-> (existence :funder.award-keyword) (nested :funder)) val)
          vals (binary-values val)]
      (cond (= #{1} vals)
            {:occurrence :filter
             :clause {:bool {:should [(:clause award-filter) (:clause award-nested-filter)]
                             :minimum_should_match 1}}}
            (= #{0} vals)
            {:occurrence :must-not
             :clause [(:clause award-filter) (:clause award-nested-filter)]}
            :else
            {:occurrence :filter
             :clause {:match_all {}}}))))

;; Filter definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def compound-fields
  {:full-text ["type" "application" "version"]
   :license ["url" "version" "delay"]
   :award ["funder" "number"]
   :relation ["type" "object-type" "object"]})

(def std-filters 
  {"from-update-date"          (date-range :deposited :from)
   "until-update-date"         (date-range :deposited :until)
   "from-index-date"           (date-range :indexed :from)
   "until-index-date"          (date-range :indexed :until)
   "from-deposit-date"         (date-range :deposited :from)
   "until-deposit-date"        (date-range :deposited :until)
   "from-created-date"         (date-range :first-deposited :from)
   "until-created-date"        (date-range :first-deposited :until)
   "from-pub-date"             (date-range :published :from)
   "until-pub-date"            (date-range :published :until)
   "from-issued-date"          (date-range :issued :from)
   "until-issued-date"         (date-range :issued :until)
   "from-online-pub-date"      (date-range :published-online :from)
   "until-online-pub-date"     (date-range :published-online :until)
   "from-print-pub-date"       (date-range :published-print :from)
   "until-print-pub-date"      (date-range :published-print :until)
   "from-posted-date"          (date-range :posted :from)
   "until-posted-date"         (date-range :posted :until)
   "from-accepted-date"        (date-range :accepted :from)
   "until-accepted-date"       (date-range :accepted :until)
   "from-event-start-date"     (date-range :event.start :from)
   "until-event-start-date"    (date-range :event.start :until)
   "from-event-end-date"       (date-range :event.end :from)
   "until-event-end-date"      (date-range :event.end :until)
   "from-approved-date"        (date-range :approved :from)
   "until-approved-date"       (date-range :approved :until)
   "from-awarded-date"         (date-range :award-start :from)
   "until-awarded-date"        (date-range :award-start :until)
   "has-event"                 (existence :event)
   "has-alias"                 (-> (existence :calculated.alias-dois-refcount) (nested :calculated.alias-dois-refcount)(nested :calculated))
   "has-prime-doi"             (existence :prime-doi)
   "is-update"                 (or-ed
                                (-> (existence :update-to) (nested :update-to))
                                (-> (existence :calculated.rw-update-to) (nested :calculated.rw-update-to) (nested :calculated)))
   "has-update"                (or-ed (-> (existence :calculated.updated-by) (nested :calculated.updated-by) (nested :calculated))
                                      (-> (existence :calculated.rw-updated-by) (nested :calculated.rw-updated-by) (nested :calculated)))

   "content-domain"            (equality :domain)
   "has-content-domain"        (existence :domain)
   "has-domain-restriction"    (bool :domain-exclusive)
   "updates"                   (-> (equality :update-to.doi
                                             :transformer doi-id/to-doi-uri)
                                   (nested :update-to))
   "update-type"               (or-ed (-> (equality :update-to.type)
                                          (nested :update-to))
                                      (-> (equality :calculated.rw-update-to.type)
                                          (nested :calculated.rw-update-to)
                                          (nested :calculated)))
   "has-abstract"              (existence :abstract)
   "has-full-text"             (-> (existence :link) (nested :link))
   "has-license"               (-> (existence :license) (nested :license))
   "has-references"            (greater-than-zero :references-count)
   "has-update-policy"         (existence :update-policy)
   "has-archive"               (existence :archive)
   "has-orcid"                 (existence :contributor-orcid)
   "has-authenticated-orcid"   (or-ed (-> (bool :contributor.authenticated-orcid)
                                          (nested :contributor))
                                      (-> (bool :project.contributor.authenticated-orcid)
                                          (nested :project.contributor :project)))
   "has-affiliation"           (existence :contributor-affiliation)
   "has-funder"                (existence :funder-name)
   "has-funder-doi"            (existence :funder-doi)
   "has-award"                 (has-award)
   "has-ror-id"                (existence :institution-ror-id)
   "ror-id"                    (equality :institution-ror-id :transformer ror-id/to-ror-id-uri)
   "has-description"           (or-ed (existence :description)
                                      (-> (existence :project.description)
                                          (nested :project)))
   "has-relation"              (-> (existence :relation) (nested :relation))
   "funder-doi-asserted-by"    (or-ed (-> (equality :funder.doi-asserted-by) (nested :funder))
                                      (-> (equality :project.funding.funder-doi-asserted-by) (nested :project.funding :project)))
   "has-assertion"             (-> (existence :assertion) (nested :assertion))
   "has-clinical-trial-number" (-> (existence :clinical-trial) (nested :clinical-trial))
   "full-text"                 (nested-terms :link {:type        :content-type
                                                    :application :application
                                                    :version     :version})
   "license"                   (nested-terms :license {:url     :url
                                                       :version :version
                                                       :delay   :delay}
                                             :matchers
                                             {:delay range-nested-query})
   "archive"                   (equality :archive)
   "article-number"            (equality :article-number)
   "issn"                      (equality :issn.value :transformer issn/normalize-issn)
   "isbn"                      (equality :isbn.value :transformer isbn/extract-isbn)
   "type"                      (equality :type)
   "type-name"                 (equality :type :transformer #(get type-id/reverse-dictionary % ""))
   "orcid"                     (equality :contributor-orcid :transformer orcid/to-orcid-uri)
   "assertion"                 (-> (equality :assertion.name) (nested :assertion))
   "assertion-group"           (-> (equality :assertion.group-name) (nested :assertion))
   "doi"                       (equality :doi :transformer doi-id/normalize-doi)
   "group-title"               (equality :group-title)
   "container-title"           (equality :container-title)
   "clinical-trial-number"     (-> (equality :clinical-trial.number)
                                   (nested :clinical-trial))
   "alternative-id"            (equality :supplementary-id :transformer ids/to-supplementary-id-uri)
   "award"                     (nested-multi-field {:funder-doi (equality :funder-doi :transformer doi-id/with-funder-prefix)
                                                    :funder     (equality :funder-doi :transformer doi-id/with-funder-prefix)
                                                    :number     (or-ed
                                                                 (equality :award-keyword :transformer string/lower-case)
                                                                 (-> (equality :funder.award-keyword :transformer string/lower-case)
                                                                     (nested :funder)))})
   "relation"                  (nested-terms :relation {:type        :type
                                                        :object-type :object-type
                                                        :object-ns   :object-ns
                                                        :claimed-by  :claimed-by
                                                        :object      :object})
   "member"                    (equality :member-id
                                         :transformer member-id/extract-member-id)
   "prefix"                    (equality :owner-prefix
                                         :transformer prefix/extract-prefix)
   "funder"                    (equality :funder-doi
                                         :transformer doi-id/with-funder-prefix)
   "category-name"             (bool :subject_disabled) ;; this field will never exist so it always return an empty list
   "lte-award-amount"          (or-ed
                                (-> (number-range :project.award-amount :lte)
                                    (nested :project))
                                (-> (number-range :project.funding.award-amount :lte)
                                    (nested :project.funding :project)))
   "gte-award-amount"          (or-ed
                                (-> (number-range :project.award-amount :gte)
                                    (nested :project))
                                (-> (number-range :project.funding.award-amount :gte)
                                    (nested :project.funding :project)))
   "citation-id"               (equality :citation-id)})

(def member-filters
  {"prefix"                (equality :prefix.value)
   "backfile-doi-count"    (members-doi-count :backfile-dois)
   "current-doi-count"     (members-doi-count :current-dois)})

(def funder-filters
  {"location"   (equality :country)})
