(ns cayenne.api.v1.routes
  (:import [java.net URL URLDecoder])
  (:require [cayenne.ids.doi :as doi-id]
            [cayenne.ids.prefix :as prefix-id]
            [cayenne.ids.issn :as issn-id]
            [cayenne.ids.type :as type-id]
            [cayenne.defaults]
            [cayenne.data.core :as c]
            [cayenne.data.work :as work]
            [cayenne.data.funder :as funder]
            [cayenne.data.prefix :as prefix]
            [cayenne.data.member :as member]
            [cayenne.data.journal :as journal]
            [cayenne.data.license :as license]
            [cayenne.data.type :as data-types]
            [cayenne.data.csl :as csl]
            [cayenne.api.transform :as transform]
            [cayenne.api.v1.types :as t]
            [cayenne.api.v1.query :as q]
            [cayenne.api.v1.validate :as v]
            [cayenne.api.conneg :as conneg]
            [cayenne.util :refer [merge-uri-query]]
            [clojure.data.json :as json]
            [clojure.string :as string]
            [liberator.core :refer [defresource]]
            [liberator.representation :refer [ring-response]]
            [ring.util.response :refer [redirect] :as response]
            [compojure.core :refer [defroutes ANY]]))

(extend java.util.Date json/JSONWriter {:-write #(json/write (.toString %1) %2)})
(extend clojure.lang.Var json/JSONWriter {:-write #(json/write (.toString %1) %2)})
(extend java.lang.Object json/JSONWriter {:-write #(json/write (.toString %1) %2)})

(defn get-owner [context]
  (get-in context [:request :basic-authentication 0]))

(defn known-post-type?
  "Does the content type submitted match a known content type, if the
   method is POST? Otherwise, if not method POST, accept the request
   regardless of content type."
  [context post-types]
  (let [method (get-in context [:request :request-method])]
    (if (not= :post method)
      true
      (some #{(get-in context [:request :headers "content-type"])} post-types))))

(defn ->1
  "Helper that creates a function that calls f while itself taking one
   argument which is ignored."
  [f]
  (fn [_] (f)))

(defn abs-url
  "Create an absolute url to a resource relative to the given request url. The
   request url acts as the base of a path created by concatentating paths."
  [request & paths]
  (URL. (format "%s://%s:%s%s/%s"
                (name (:scheme request))
                (:server-name request)
                (:server-port request)
                (:uri request)
                (clojure.string/join "/" paths))))

(defn rel-url
  [& paths]
  (str "/" (clojure.string/join "/" paths)))

(defn truth-param [context param-name]
  (if (#{"t" "true" "1"}
       (-> context
           (get-in [:request :params param-name])
           (or "false")))
    true
    false))

(defn param [context param-name]
  (get-in context [:request :params param-name]))

(defresource csl-styles-resource
  :malformed? (v/malformed?)
  :handle-malformed :validation-result
  :allowed-methods [:get :options]
  :available-media-types t/json
  :handle-ok (->1 #(csl/fetch-all-styles)))

(defresource csl-locales-resource
  :malformed? (v/malformed?)
  :handle-malformed :validation-result
  :allowed-methods [:get :options]
  :available-media-types t/json
  :handle-ok (->1 #(csl/fetch-all-locales)))

(defresource cores-resource
  :malformed? (v/malformed?)
  :handle-malformed :validation-result
  :allowed-methods [:get :options]
  :available-media-types t/json
  :handle-ok (->1 #(c/fetch-all)))

(defresource core-resource [core-name]
  :malformed? (v/malformed? :singleton true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options]
  :available-media-types t/json
  :exists? (->1 #(c/exists? core-name))
  :handle-ok (->1 #(c/fetch core-name)))


(defresource works-resource
  :malformed? (v/malformed? :facet-validator v/validate-work-facets
                            :filter-validator v/validate-work-filters
                            :query-field-validator v/validate-work-query-fields
                            :select-validator v/validate-work-selects
                            :offset-validator v/validate-work-offsets
                            :deep-pagable true
                            :sortable true
                            :samplable true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :handle-ok #(work/fetch-async (q/->query-context %) (get-in % [:request :ring-respond])))

(defresource work-resource [doi]
  :malformed? (v/malformed? :singleton true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :exists? (->1 #(-> doi doi-id/normalize-doi string/blank? not))
  :handle-ok #(work/fetch-one-async
                (doi-id/normalize-doi doi)
                (get-in % [:request :ring-respond])))

(defresource work-agency-resource [doi]
  :malformed? (v/malformed? :singleton true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :exists? (->1 #(when-let [agency (work/parse-agency (work/get-agency doi))] {:agency agency}))
  :handle-ok #(work/->agency-response doi (:agency %)))

(defn force-exact-request-doi
  "Why? DOIs are case insensitive. CrossRef APIs try to always present DOIs
   lowercases, but out in the real world they may appear mixed-case. We want
   clients to be given RDF that describes the case-sensitive URI they requested,
   so we avoid the canonical lower-case DOI and present metadata for the DOI
   exactly as requested."
  [request doi]
  (assoc (get-in request [:work :message])
         :URL
         (->> doi
              (#(URLDecoder/decode % "UTF-8"))
              (doi-id/extract-doi)
              (str "http://dx.doi.org/"))))

(defn ->transform
  "Generates a transformation, expects the doi, the context and the format,
   If the work has a prime DOI it will redirect to the prime DOI transform.
  The reason we are responding with a 308 is because in case header are provided
  (eg Accept), they should be used as well in the redirection."
  [{{{:keys [prime-doi]} :message} :work
    {{path :*} :params} :request
    :as ctx}
   doi
   format]
  (if prime-doi
    {:headers {"Location" (str "/works/" prime-doi (->> path (re-find #"/transform(/[^/]+/[^/]+)?") first))}
     :status 308}
    (let [transformed (transform/->format format
                                          (force-exact-request-doi ctx doi))]
      (if (map? transformed)
        {:headers {}
         :status (:status transformed)
         :body (:body transformed)}
        {:headers {}
         :body transformed}))))

(defresource work-transform-resource [doi]
  :malformed? (v/malformed? :singleton true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :media-type-available? #(let [work (-> doi doi-id/normalize-doi work/fetch-one)
                                cts (if work
                                      (-> work
                                          :message
                                          :type
                                          keyword
                                          type-id/type-dictionary
                                          (get :cn-transforms t/work-transform))
                                      t/work-transform)]
                            (when-let [content-type ((conneg/content-type-matches cts) %)]
                              (assoc content-type :work work)))
  :exists? #(when (:work %) true)
  :handle-ok (fn [{{{:keys [prime-doi]} :message} :work :as ctx}]
               (ring-response (->transform ctx doi (:representation ctx)))))

(defresource explicit-work-transform-resource [doi content-type]
  :malformed? (v/malformed? :singleton true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :media-type-available? (->1 #(let [work (-> doi doi-id/normalize-doi work/fetch-one)
                                     cts (if work
                                           (-> work
                                               :message
                                               :type
                                               keyword
                                               type-id/type-dictionary
                                               (get :cn-transforms t/work-transform))
                                           t/work-transform)]
                                 (when (some #{content-type} cts)
                                   {:work work})))
  :exists? #(when (:work %) true)
  :handle-ok (fn [{{{:keys [prime-doi]} :message} :work :as ctx}]
               (ring-response (->transform ctx doi {:media-type content-type :parameters {}}))))

(defresource funders-resource
  :malformed? (v/malformed? :filter-validator v/validate-funder-filters
                            :deep-pagable true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :handle-ok #(funder/fetch-async (q/->query-context %) (get-in % [:request :ring-respond])))

(defresource funder-resource [funder-id]
  :malformed? (v/malformed? :singleton true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :exists? (->1 #(-> funder-id doi-id/doi-uri-to-id string/blank? not))
  :handle-ok #(funder/fetch-one-async
                (doi-id/doi-uri-to-id funder-id)
                (get-in % [:request :ring-respond])))

(defresource funder-works-resource [funder-id]
  :malformed? (v/malformed? :facet-validator v/validate-work-facets
                            :filter-validator v/validate-work-filters
                            :query-field-validator v/validate-work-query-fields
                            :select-validator v/validate-work-selects
                            :offset-validator v/validate-work-offsets
                            :deep-pagable true
                            :sortable true
                            :samplable true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :exists? (->1 #(-> funder-id doi-id/doi-uri-to-id funder/fetch-one nil? not))
  :handle-ok #(funder/fetch-works
                (->> funder-id (q/->query-context % :id))
                (get-in % [:request :ring-respond])))

(defresource prefix-resource [px]
  :malformed? (v/malformed? :singleton true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :exists? #(when-let [p (prefix/fetch-one
                          (q/->query-context % :id (prefix-id/normalize-prefix px)))]
              {:publisher p})
  :handle-ok :publisher)

(defresource prefix-works-resource [px]
  :malformed? (v/malformed? :facet-validator v/validate-work-facets
                            :filter-validator v/validate-work-filters
                            :query-field-validator v/validate-work-query-fields
                            :select-validator v/validate-work-selects
                            :offset-validator v/validate-work-offsets
                            :deep-pagable true
                            :sortable true
                            :samplable true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :exists? #(->> px
                 prefix-id/normalize-prefix
                 (q/->query-context % :singleton true :id)
                 prefix/fetch-one
                 nil?
                 not)
  :handle-ok #(prefix/fetch-works
                (q/->query-context % :id (prefix-id/normalize-prefix px))
                (get-in % [:request :ring-respond])))

(defresource members-resource
  :malformed? (v/malformed? :filter-validator v/validate-member-filters
                            :deep-pagable true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :handle-ok #(member/fetch-async (q/->query-context %) (get-in % [:request :ring-respond])))

(defresource member-resource [id]
  :malformed? (v/malformed? :singleton true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :exists? (->1 #(-> id string/blank? not))
  :handle-ok #(member/fetch-one-async id (get-in % [:request :ring-respond])))

(defresource member-works-resource [id]
  :malformed? (v/malformed? :facet-validator v/validate-work-facets
                            :filter-validator v/validate-work-filters
                            :query-field-validator v/validate-work-query-fields
                            :select-validator v/validate-work-selects
                            :offset-validator v/validate-work-offsets
                            :deep-pagable true
                            :sortable true
                            :samplable true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :exists? (->1 #(-> id member/fetch-one nil? not))
  :handle-ok #(member/fetch-works (q/->query-context % :id id) (get-in % [:request :ring-respond])))

(defresource journals-resource
  :malformed? (v/malformed? :deep-pagable true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :handle-ok #(journal/fetch-async (q/->query-context %) (get-in % [:request :ring-respond])))

(defresource journal-resource [issn]
  :malformed? (v/malformed? :singleton true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :exists? (->1 #(-> issn issn-id/normalize-issn string/blank? not))
  :handle-ok #(journal/fetch-one-async
                (q/->query-context % :id (issn-id/normalize-issn issn))
                (get-in % [:request :ring-respond])))

(defresource journal-works-resource [issn]
  :malformed? (v/malformed? :facet-validator v/validate-work-facets
                            :filter-validator v/validate-work-filters
                            :query-field-validator v/validate-work-query-fields
                            :select-validator v/validate-work-selects
                            :offset-validator v/validate-work-offsets
                            :deep-pagable true
                            :sortable true
                            :samplable true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :exists? #(->> issn
                 issn-id/normalize-issn
                 (q/->query-context % :singleton true :id)
                 journal/fetch-one
                 nil?
                 not)
  :handle-ok #(journal/fetch-works
                (q/->query-context % :id (issn-id/normalize-issn issn))
                (get-in % [:request :ring-respond])))

(defresource licenses-resource
  :malformed? (v/malformed? :deep-pagable true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :handle-ok #(license/fetch (q/->query-context %)))

(defresource types-resource
  :malformed? (v/malformed?)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :handle-ok (->1 #(data-types/fetch-all)))

(defresource type-resource [id]
  :malformed? (v/malformed? :singleton true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :exists? #(when-let [t (data-types/fetch-one (q/->query-context % :id id))]
              {:data-type t})
  :handle-ok :data-type)

(defresource type-works-resource [id]
  :malformed? (v/malformed? :facet-validator v/validate-work-facets
                            :filter-validator v/validate-work-filters
                            :query-field-validator v/validate-work-query-fields
                            :select-validator v/validate-work-selects
                            :offset-validator v/validate-work-offsets
                            :deep-pagable true
                            :sortable true
                            :samplable true)
  :handle-malformed :validation-result
  :allowed-methods [:get :options :head]
  :available-media-types t/json
  :exists? #(->> id
                 (q/->query-context % :singleton true :id)
                 data-types/fetch-one
                 nil?
                 not)
  :handle-ok #(data-types/fetch-works (q/->query-context % :id id) (get-in % [:request :ring-respond])))

(defresource reverse-lookup-resource
  :malformed? (v/malformed? :singleton true)
  :new? false
  :respond-with-entity? true
  :handle-malformed :validation-result
  :allowed-methods [:post :options :head]
  :available-media-types t/json
  :handle-ok #(work/fetch-reverse {:terms (-> (get-in % [:request :body])
                                              (.bytes)
                                              slurp)}))
(def not-found-deprecated
    (constantly
      (-> (response/response
                  (json/write-str
                    {:status :error
                    :message-type :route-not-found
                    :message-version "1.0.0"
                    :message "Route not found, see https://www.crossref.org/deprecated/"}))
                  (response/status 404)
                  (response/header "Content-Type" "application/json"))))


(defroutes api-routes
  (ANY "/reverse" []
    reverse-lookup-resource)
  (ANY "/styles" []
    csl-styles-resource)
  (ANY "/locales" []
    csl-locales-resource)
  (ANY "/funders" []
    funders-resource)
  (ANY "/funders/*" {{id :*} :params}
    (if (.endsWith id "/works")
      (funder-works-resource (string/replace id #"/works\z" ""))
      (funder-resource id)))
  (ANY "/members" []
    members-resource)
  (ANY "/members/:id" [id]
    (member-resource id))
  (ANY "/members/:id/works" [id]
    (member-works-resource id))
  (ANY "/journals" []
    journals-resource)
  (ANY "/journals/:issn" [issn]
    (journal-resource issn))
  (ANY "/journals/:issn/works" [issn]
    (journal-works-resource issn))
  (ANY "/licenses" []
    licenses-resource)
  (ANY "/prefixes/:prefix" [prefix]
    (prefix-resource prefix))
  (ANY "/prefixes/:prefix/works" [prefix]
    (prefix-works-resource prefix))
  (ANY "/works" []
    works-resource)
  (ANY "/works/*" params
    (let [doi (-> params :params :*)
          urlvars (-> params :params (dissoc :*))]
      (cond (.endsWith doi ".xml")
            (redirect (merge-uri-query
                       (str
                        "/works/"
                        (string/replace doi #".xml" "")
                        "/transform/application/vnd.crossref.unixsd+xml")
                       urlvars))
            (.endsWith doi "/agency")
            (work-agency-resource (string/replace doi #"/agency\z" ""))
            (.endsWith doi "/transform")
            (work-transform-resource (string/replace doi #"/transform\z" ""))
            (re-matches #".*/transform/.+\z" doi)
            (explicit-work-transform-resource
             (string/replace doi #"/transform/[^/]+/[^/]+\z" "")
             (second (re-matches #".*/transform/(.+)\z" doi)))
            :else
            (work-resource doi))))
  (ANY "/types" []
    types-resource)
  (ANY "/types/:id" [id]
    (type-resource id))
  (ANY "/types/:id/works" [id]
    (type-works-resource id))
  (ANY "/cores" []
    cores-resource)
  (ANY "/cores/:name" [name]
    (core-resource name))

  ; Removed deposits, serve up 404. 
  (ANY "/deposits" {body :body}
       not-found-deprecated)
  (ANY "/deposits/:id" [id]
       not-found-deprecated)
  (ANY "/deposits/:id/data" [id]
       not-found-deprecated))
