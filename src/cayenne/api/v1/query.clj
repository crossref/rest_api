(ns cayenne.api.v1.query
  (:require [cayenne.util :as util]
            [cayenne.api.v1.parameters :as p]
            [cayenne.api.v1.facet :as facet]
            [cayenne.api.v1.fields :as fields]
            [cayenne.elastic.util :as elastic-util]
            [clojure.string :as string]
            [clj-time.core :as t]
            [clj-time.coerce :as tc]))

(def default-offset 0)
(def default-rows 20)
(def max-rows 1000)
(def max-sample 100)
(def default-facet-rows 20)
(def max-facet-rows 1000)
(def cursor-expiration "5m")

;; todo this should be passed in to ->query-context https://gitlab.com/crossref/issues/-/issues/1068
(def sort-fields
  {"score"                  [:_score]
   "relevance"              [:_score]
   "updated"                [:deposited]
   "deposited"              [:deposited]
   "indexed"                [:indexed]
   "created"                [:first-deposited]
   "published-print"        [:published-print]
   "published-online"       [:published-online]
   "published"              [:published]
   "issued"                 [:issued]
   "is-referenced-by-count" [:is-referenced-by-count]
   "references-count"       [:references-count]})

;; todo this should be passed in to ->query-context https://gitlab.com/crossref/issues/-/issues/1068
(def select-fields
  {"DOI"                    [:doi]
   "member"                 [:member-id]
   "prefix"                 [:owner-prefix]
   "URL"                    [:doi]
   "issued"                 [:issued]
   "created"                [:first-deposited]
   "deposited"              [:deposited]
   "indexed"                [:indexed]
   "publisher"              [:publisher]
   "references-count"       [:references-count :type]
   "is-referenced-by-count" [:is-referenced-by-count :type]
   "type"                   [:type]
   "content-domain"         [:domain :domain-exclusive :type]
   "relation"               [:reference.* :relation.*]
   "published-online"       [:published-online]
   "published-print"        [:published-print]
   "published"              [:published]
   "posted"                 [:posted]
   "accepted"               [:accepted]
   "content-created"        [:content-created]
   "approved"               [:approved]
   "publisher-location"     [:publisher-location]
   "abstract"               [:abstract-xml]
   "article-number"         [:article-number]
   "volume"                 [:volume]
   "issue"                  [:issue]
   "ISBN"                   [:isbn.*]
   "ISSN"                   [:issn.*]
   "degree"                 [:degree]
   "subject"                [:subject]
   "alternative-id"         [:supplementary-id]
   "title"                  [:title :type]
   "short-title"            [:short-title :type]
   "original-title"         [:original-title :type]
   "subtitle"               [:subtitle :type]
   "container-title"        [:container-title :type]
   "short-container-title"  [:short-container-title :type]
   "group-title"            [:group-title]
   "archive"                [:archive]
   "update-policy"          [:update-policy]
   "update-to"              [:update-to.*]
   "updated-by"             [:updated-by.*]
   "license"                [:license.*]
   "link"                   [:link.*]
   "page"                   [:first-page :last-page]
   "funder"                 [:funder.*]
   "assertion"              [:assertion.*]
   "clinical-trial-number"  [:clinical-trial.*]
   "issn-type"              [:issn.*]
   "event"                  [:event.*]
   "reference"              [:reference.*]
   "author"                 [:contributor.*]
   "editor"                 [:contributor.*]
   "chair"                  [:contributor.*]
   "translator"             [:contributor.*]
   "standards-body"         [:standards-body.*]
   "score"                  [:_score]
   "resource"               [:resource-url]})

(defn clean-terms [terms & {:keys [remove-syntax] :or {remove-syntax false}}]
  (if-not remove-syntax
    terms
    (-> terms
        (string/replace #"[\\+!{}*\"\'\.\[\]\(\)\-:;\/%^&?=_,]+" " ")
        (string/replace #"\|\|" " ")
        (string/replace #"&&" " ")
        (string/replace #"\s(?i)not\s" " ")
        (string/replace #"\s(?i)or\s" " ")
        (string/replace #"\s(?i)and\s" " "))))

(defn parse-rows-val [val]
  (int (cond
         (nil? val)
         default-rows
         (= (type val) java.lang.String)
         (util/parse-int-safe val))))

(defn parse-offset-val [val]
  (int (cond
         (nil? val)
         default-offset
         (= (type val) java.lang.String)
         (util/parse-int-safe val))))

(defn parse-sample-val
  "Returns a sample count or 0, indicating that no sample
   is to be taken."
  [val]
  (int (cond
         (nil? val)
         0
         (= (type val) java.lang.String)
         (util/parse-int-safe val))))

(defn get-filters
  "Turns a filter value string such as a.b:val,c:val2 into
   a map representation, such as {\"a\" {\"b\" val} \"c\" val2}."
  [params]
  (when (get params :filter)
    (->> (string/split (get params :filter) #",")
         (reduce #(if (re-find #":" %2)
                    (conj %1 %2)
                    (conj (drop-last %1) (apply str (last %1) "," %2)))
                 [])
         (map #(let [parts (string/split % #":")
                     k (first parts)
                     val (string/join ":" (rest parts))
                     path (string/split k #"\.")]
                 [path val]))
         (reduce (fn [m [path val]] (update-in m path #(conj %1 val))) {}))))

;; facet spec is
;; field1:count1,...fieldn:countn
;; *, t, T, 1 all signify 'all fields'
(defn get-facets
  [params]
  (if-let [facet-params (get params :facet)]
    (map
     #(let [[field count] (string/split % #":")]
        {:field field
         :count (if (= count "*")
                  "*"
                  (max 1
                       (min (or (util/parse-int-safe count)
                                default-facet-rows)
                            max-facet-rows)))})
     (string/split facet-params #","))
    []))

(defn get-rels
  [params]
  (if-let [rel-params (get params :rel)]
    (map
     #(let [[rel value] (string/split % #":" 2)]
        {:rel (keyword rel)
         :value value
         :any (= value "*")})
     (string/split rel-params #","))
    []))

(defn parse-sort-order [params]
  (if (get params :order)
    (let [val (-> (get params :order)
                  string/trim
                  string/lower-case)]
      (cond (some #{val} ["1" "asc"])
            :asc
            (some #{val} ["-1" "desc"])
            :desc
            :else
            :desc))
    :desc))

(defn parse-sort [params]
  (when-let [sort-params (get params :sort)]
    (-> sort-params
        string/trim
        string/lower-case
        sort-fields)))

(defn get-selectors [params]
  (when (get params :select)
    (string/split (get params :select) #",")))

(defn get-field-queries [params]
  (->> params
       (filter #(.startsWith (-> % first name) "query."))
       (map #(vector (-> % first name (string/replace #"query." ""))
                     (-> % second (clean-terms :remove-syntax true))))))

(defn ->query-context [resource-context & {:keys [id filters singleton]
                                           :or {id nil filters {} singleton false}}]
  (let [params (p/get-parameters resource-context)]
    (if singleton
      {:id id
       :rows 1
       :debug (:debug params)}
      {:id id
       :sample (parse-sample-val (:sample params))
       :terms (:query params)
       :field-terms (get-field-queries params)
       :cursor (:cursor params)
       :offset (parse-offset-val (:offset params))
       :rows (parse-rows-val (:rows params))
       :select (get-selectors params)
       :facets (get-facets params)
       :order (parse-sort-order params)
       :sort (parse-sort params)
       :rels (get-rels params)
       :filters (merge filters (get-filters params))
       :debug (:debug params)})))

(defn with-source-fields [es-body query-context]
  (if (empty? (:select query-context))
    es-body
    (assoc es-body :_source (->> (:select query-context)
                                 (map select-fields)
                                 (apply concat)))))

(defn with-filters [es-body query-context & {:keys [filters]}]
  (let [filter-clauses (flatten (map #((-> % first name filters)
                                       (second %))
                                     (:filters query-context)))
        filter-occurrence (->> filter-clauses
                               (filter #(= (:occurrence %) :filter))
                               (map :clause))
        must-not-occurrence (->> filter-clauses
                                 (filter #(= (:occurrence %) :must-not))
                                 (map :clause))]
    (-> es-body
        (assoc-in [:query :bool :filter] filter-occurrence)
        (assoc-in [:query :bool :must_not] must-not-occurrence))))

(defn with-query [es-body query-context & {:keys [id-field filters]}]
  (cond-> es-body
          (-> query-context :terms string/blank? not)
          (assoc-in [:query :bool :must]
            [{:match {:metadata-content-text {:query (fields/clean-query (:terms query-context))
                                              :minimum_should_match "20%"}}}])

          (-> query-context :prefix-terms string/blank? not)
          (assoc-in
            [:query :bool :must]
            (-> query-context
                :prefix-terms
                (string/replace #"[,\.\-\"]" " ")
                (string/split #"\s+")
                ((partial map #(hash-map :match_phrase_prefix {(:prefix-field query-context) %})))))

          (-> query-context :filters seq)
          (with-filters query-context :filters filters)

          ;; todo could be rewritten to use /type/type/id https://gitlab.com/crossref/issues/-/issues/1073
          id-field
          (update-in [:query :bool :filter] conj {:term {id-field (:id query-context)}})))

(defn with-paging [es-body query-context & {:keys [paged count-only]}]
  (cond
    paged
    (-> es-body
        (assoc :from (or (:offset query-context) 0))
        (assoc :size (:rows query-context)))

    count-only
    (assoc es-body :size 0)

    :else
    es-body))

(defn with-sort-fields [es-body query-context]
  (cond
    (-> query-context :sort empty?)
    es-body

    (-> query-context :order sequential?)
    (assoc
      es-body
      :track_scores true
      :sort (map #(hash-map %1 {:order %2}) (:sort query-context) (:order query-context)))

    :else
    (assoc
      es-body
      :track_scores true
      :sort (map #(hash-map % {:order (:order query-context)}) (:sort query-context)))))

(defn with-random-sort [es-body query-context]
  (if (and (:sample query-context) (not= 0 (:sample query-context)))
    (let [current-query (:query es-body)
          ; random sorting must have an explicit query, otherwise there are no scores to alter
          current-query (if (get-in current-query [:bool :must])
                          current-query
                          (assoc-in current-query [:bool :must] {:match_all {}}))]
      (-> es-body
          (dissoc :query)
          (assoc-in [:query :function_score :query] current-query)
          (assoc-in [:query :function_score :functions]
                    [{:random_score {:seed (tc/to-long (t/now))
                                     :field :_seq_no}}])
          (assoc :from 0)
          (assoc :size (:sample query-context))))
    es-body))

(defn with-scroll [es-body query-context]
  (if (and (-> query-context :cursor string/blank? not)
           (-> query-context :cursor (not= "*")))
    {:scroll cursor-expiration
     :scroll_id (:cursor query-context)}
    es-body))

(defn ->es-request [query-context
                    & {:keys [url-prefix paged id-field filters count-only]
                       :or {url-prefix (elastic-util/index-url-prefix :work) paged true id-field nil filters {}
                            count-only false}}]
  {:method :get
   :url (cond
          (-> query-context :cursor string/blank?)
          (str url-prefix "_search")
          (and (-> query-context :cursor string/blank? not)
               (-> query-context :cursor (not= "*")))
          "/_search/scroll"
          (-> query-context :cursor (= "*"))
          (str url-prefix "_search?scroll=" cursor-expiration))
   :body
   (-> {}
       (with-source-fields query-context)
       (with-sort-fields query-context)
       (with-query query-context :id-field id-field :filters filters)
       (with-paging query-context :paged paged :count-only count-only)
       (facet/with-aggregations query-context)
       (fields/with-field-queries query-context)
       (with-random-sort query-context)
       (with-scroll query-context))})

(defn prefix-query-context [query-context prefix-field]
  (-> query-context
      (assoc :prefix-terms (:terms query-context))
      (assoc :prefix-field prefix-field)
      (dissoc :terms)))

