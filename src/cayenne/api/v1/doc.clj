(ns cayenne.api.v1.doc
  (:require [cayenne.api.v1.schema :as sc]
            [cayenne.api.v1.filter :refer [std-filters compound-fields funder-filters member-filters]]
            [cayenne.api.v1.fields :refer [work-fields]]
            [cayenne.api.v1.facet :refer [std-facets]]
            [cayenne.api.v1.query :refer [select-fields sort-fields]]
            [ring.util.response :refer [redirect] :as response]
            [compojure.core :refer [defroutes GET]]
            [clojure.data.json :as json]
            [clojure.java.io :refer [resource]]
            [clojure.string :as string]
            [ring.swagger.swagger-ui :refer [swagger-ui]]
            [ring.swagger.swagger2 :as rs]
            [schema.core :as s]
            [cayenne.version :refer [version]]))

(def info
  {:info
   {:version version
    :title "Crossref REST API"
    :description (slurp (resource "swagger/description.md"))
    :contact {:name "Crossref"
              :email "support@crossref.org"
              :url "https://crossref.org"}}})

(def tags
  {:tags
   [{:name "Funders"
     :description "Endpoints that expose funder related data"}
    {:name "Journals"
     :description "Endpoints that expose journal related data"}
    {:name "Works"
     :description "Endpoints that expose works related data"}
    {:name "Prefixes"
     :description "Endpoints that expose prefix related data"}
    {:name "Members"
     :description "Endpoints that expose member related data"}
    {:name "Types"
     :description "Endpoints that expose type related data"}
    {:name "Licenses"
     :description "Endpoints that expose license related data"}
    {:name "Deposits"
     :description "Endpoints that expose deposit related data. *Note* **These endpoints will be deprecated in the near future.**"}]})

(defn- subfields [compound-fields field field-docs]
  (let [c-fields (get compound-fields (keyword field))
        full-fields (map (partial str field ".") c-fields)
        docstrings (map #(when (get field-docs %) (str " - " (get field-docs %))) full-fields)]
    (string/join (map #(str "\n  + `" %1 "`" %2) full-fields docstrings))))

(defn- fields-description
  ([title fields]
   (fields-description title fields {}))
  ([title fields field-docs]
   (fields-description title fields field-docs {}))
  ([title fields field-docs compound-fields]
   (->> fields
        sort
        (map #(let [docstring (when (contains? field-docs %) (str " - " (get field-docs %)))
                    subfields-enum (subfields compound-fields % field-docs)]
                (str "\n+ `" % "`" docstring subfields-enum)))
        string/join
        (str title))))

(defn- filters-description []
  (fields-description
    (slurp (resource "swagger/filters-description.md"))
    (keys std-filters)
    {"has-funder"                "[0 or 1], metadata which includes/does not include one or more funder entry"
     "funder"                    "metadata which include given funder id in FundRef data"
     "prefix"                    "metadata belonging to a given DOI owner prefix (e.g. 10.1016)"
     "member"                    "metadata belonging to a given Crossref member"
     "from-index-date"           "[date], metadata indexed since given date (inclusive)"
     "until-index-date"          "[date], metadata indexed before given date (inclusive)"
     "from-deposit-date"         "[date], metadata last (re)deposited since given date (inclusive)"
     "until-deposit-date"        "[date], metadata last (re)deposited before given date (inclusive)"
     "from-update-date"          "[date], metadata updated since given date (inclusive), currently the same as `from-deposit-date`"
     "until-update-date"         "[date], metadata updated before given date (inclusive), currently the same as `until-deposit-date`"
     "from-created-date"         "[date], metadata first deposited since given date (inclusive)"
     "until-created-date"        "[date], metadata first deposited before given date (inclusive)"
     "from-pub-date"             "[date], metadata where published date is since given date (inclusive)"
     "until-pub-date"            "[date], metadata where published date is before given date (inclusive)"
     "from-online-pub-date"      "[date], metadata where online published date is since given date (inclusive)"
     "until-online-pub-date"     "[date], metadata where online published date is before given date (inclusive)"
     "from-print-pub-date"       "[date], metadata where print published date is since given date (inclusive)"
     "until-print-pub-date"      "[date], metadata where print published date is before given date (inclusive)"
     "from-posted-date"          "[date], metadata where posted date is since given date (inclusive)"
     "until-posted-date"         "[date], metadata where posted date is before given date (inclusive)"
     "from-accepted-date"        "[date], metadata where accepted date is since given date (inclusive)"
     "until-accepted-date"       "[date], metadata where accepted date is before given date (inclusive)"
     "from-awarded-date"         "[date], metadata where award date is since given date (inclusive)"
     "until-awarded-date"        "[date], metadata where award date is before given date (inclusive)"
     "has-license"               "[0 or 1], metadata that includes/does not include any `license_ref` elements"
     "license.version"           "metadata where the `license_ref`'s `applies_to` attribute equals given version"
     "license.url"               "metadata where `license_ref` value equals given url"
     "license.delay"             "metadata where difference between publication date and the `license_ref`'s `start_date` attribute is <= than given delay (in days)"
     "lte-award-amount"          "metadata where award is less than or equals given number"
     "gte-award-amount"          "metadata where award is greater than or equals given number"
     "has-full-text"             "[0 or 1], metadata that includes/does not include any full text `resource` elements"
     "full-text.version"         "metadata where `resource` element's `content_version` attribute equals given version"
     "full-text.type"            "metadata where `resource` element's `content_type` attribute equals given version mime type (e.g. application/pdf)"
     "full-text.application"     "[text-mining, similarity-checking or unspecified], metadata where `resource` link has given application"
     "has-references"            "[0 or 1], metadata for works that have/don't have a list of references"
     "has-archive"               "[0 or 1], metadata which includes/does not include name of archive partner"
     "archive"                   "metadata where value of archive partner equals given archive name"
     "has-orcid"                 "[0 or 1], metadata which includes/does not include one or more ORCIDs"
     "has-authenticated-orcid"   "[0 or 1], metadata which includes/does not include one or more ORCIDs where the depositing publisher claims to have witness the ORCID owner authenticate with ORCID"
     "orcid"                     "metadata where there is a contributor with given ORCID"
     "issn"                      "metadata with given ISSN, format is xxxx-xxxx"
     "isbn"                      "metadata with given ISBN"
     "type"                      "metadata records whose type equals given type, type must be an ID value from the list of types returned by the /types resource"
     "doi"                       "metadata describing given DOI"
     "ror-id"                    "metadata with given ROR ID"
     "updates"                   "metadata for records that represent editorial updates to given DOI"
     "is-update"                 "[0 or 1], metadata for records that represent/do not represent editorial updates"
     "has-update-policy"         "[0 or 1], metadata for records that include/do not include a link to an editorial update policy"
     "container-title"           "metadata with a publication title that exactly equals given title"
     "category-name"             "metadata for records with category label equal to given name, category labels come from the list published by Scopus"
     "type-name"                 "metadata for records with type name equal to given name"
     "award.number"              "metadata for records with award number equal to given number, optionally combine with `award.funder`"
     "award.funder"              "metadata for records with award funder equal to given funder, optionally combine with `award.number`"
     "has-assertion"             "[0 or 1], metadata for records with/without assertions"
     "assertion-group"           "metadata for records with an assertion in a given group"
     "assertion"                 "metadata for records with a given named assertion"
     "has-affiliation"           "[0 or 1], metadata for records with/without affiliation information"
     "alternative-id"            "metadata for records with the given alternative ID, which may be a publisher-specific ID, or any other identifier a publisher may have provided"
     "article-number"            "metadata for records with a given article number"
     "has-abstract"              "[0 or 1], metadata for records with/without an abstract"
     "has-clinical-trial-number" "[0 or 1], metadata for records with/without a clinical trial number"
     "content-domain"            "metadata where the publisher records a given domain name as the location Crossmark content will appear"
     "has-content-domain"        "[0 or 1], metadata where the publisher records/does not record a domain name location for Crossmark content"
     "has-domain-restriction"    "[0 or 1], metadata where the publisher restricts/does not restrict Crossmark usage to content domains"
     "has-relation"              "[0 or 1], metadata for records that either assert/do not assert or are/are not the object of a relation"
     "relation.type"             "metadata for records with a relation with the given type from the Crossref relations schema (e.g. is-referenced-by, is-parent-of, is-preprint-of)"
     "relation.object"           "metadata for records with a relation, where the object identifier matches given identifier"
     "relation.object-type"      "metadata for records with a relation, where the object type matches given type from the Crossref relations schema (e.g. doi, issn)"
     "clinical-trial-number"     "metadata for records with given clinical trial number"
     "from-approved-date"        "[date], metadata where approved date is since given date (inclusive)"
     "from-event-end-date"       "[date], metadata where event end date is since given date (inclusive)"
     "from-event-start-date"     "[date], metadata where event start date is since given date  (inclusive)"
     "from-issued-date"          "[date], metadata where issued date is since given date  (inclusive)"
     "funder-doi-asserted-by"    "metadata where funder DOI was asserted by given body"
     "group-title"               "metadata with given group title"
     "has-award"                 "[0 or 1], metadata for records with/without award"
     "has-event"                 "[0 or 1], metadata for records with/without event"
     "has-funder-doi"            "[0 or 1], metadata for records with/without funder DOI"
     "has-update"                "[0 or 1], metadata for records with/without update information"
     "has-ror-id"                "[0 or 1], metadata for records with/without ROR ID"
     "until-approved-date"       "[date], metadata where approved date is before given date (inclusive)"
     "until-event-end-date"      "[date], metadata where event end date is before given date (inclusive)"
     "until-event-start-date"    "[date], metadata where event start date is before given date  (inclusive)"
     "until-issued-date"         "[date], metadata where issued date is before given date  (inclusive)"
     "update-type"               "metadata with given update type"}
    
    compound-fields))

(defn- filters-member-description []
  (fields-description
    (slurp (resource "swagger/filters-member-description.md"))
    (keys member-filters)
    {"backfile-doi-count"    "members with given count of DOIs for material published more than two years ago"
     "current-doi-count"     "members with given count of DOIs for material published within last two years"
     "prefix"                "members with given prefix"}))

(defn- filters-funders-description []
  (fields-description
    (slurp (resource "swagger/filters-funder-description.md"))
    (keys funder-filters)
    {"location" "funders located in given country"}))


(defn- facets-description []
  (fields-description
    (slurp (resource "swagger/facets-description.md"))
    (keys std-facets)
    {"type-name"        "work type name, such as journal-article or book-chapter"
     "published"        "earliest year of publication"
     "container-title"  "[max value 100], work container title, such as journal title, or book title"
     "funder-name"      "funder literal name as deposited alongside DOI"
     "funder-doi"       "funder DOI"
     "orcid"            "[max value 100], contributor ORCID"
     "issn"             "[max value 100], journal ISSN (any - print, electronic, link)"
     "publisher-name"   "publisher name of work"
     "license"          "license URI of work"
     "archive"          "archive location"
     "update-type"      "significant update type"
     "relation-type"    "relation type described by work or described by another work with work as object"
     "affiliation"      "author affiliation"
     "assertion"        "custom Crossmark assertion name"
     "assertion-group"  "custom Crossmark assertion group name"
     "category-name"    "category name of work"
     "journal-issue"    "journal issue number"
     "journal-volume"   "journal volume"
     "link-application" "intended application of the full text link"
     "source"           "source of the DOI"
     "ror-id"           "institution ROR ID"}))

(defn- selects-description []
  (fields-description
    (slurp (resource "swagger/selects-description.md"))
    (keys select-fields)))

(defn- sorts-description []
  (fields-description
    (slurp (resource "swagger/sorts-description.md"))
    (keys sort-fields)
    {"score"                  "sort by relevance score"
     "relevance"              "sort by relevance score"
     "updated"                "sort by date of most recent change to metadata, currently the same as deposited"
     "deposited"              "sort by time of most recent deposit"
     "indexed"                "sort by time of most recent index"
     "published-print"        "sort by print publication date"
     "published-online"       "sort by online publication date"
     "published"              "sort by publication date"
     "issued"                 "sort by issued date (earliest known publication date)"
     "is-referenced-by-count" "sort by number of times this DOI is referenced by other Crossref DOIs"
     "references-count"       "sort by number of references included in the references section of the document identified by this DOI"
     "created"                "sort by created date"}))

(defn- query-description []
  (slurp (resource "swagger/query-description.md")))

(defn- query-funder-description []
  (slurp (resource "swagger/query-funder-description.md")))

(defn- query-journal-description []
  (slurp (resource "swagger/query-journal-description.md")))

(defn- query-member-description []
  (slurp (resource "swagger/query-member-description.md")))

(defn- query-license-description []
  (slurp (resource "swagger/query-license-description.md")))

(defn- field-query-description []
  (fields-description
    (slurp (resource "swagger/field-query-description.md"))
    (map (partial str "query.") (keys work-fields))
    {"query.bibliographic"          "query bibliographic information, useful for citation look up, includes titles, authors, ISSNs and publication years"
     "query.container-title"        "query container title aka. publication name"
     "query.affiliation"            "query contributor affiliations"
     "query.author"                 "query author given and family names"
     "query.editor"                 "query editor given and family names"
     "query.chair"                  "query chair given and family names"
     "query.translator"             "query translator given and family names"
     "query.contributor"            "query author, editor, chair and translator given and family names"
     "query.degree"                 "query degree"
     "query.event-acronym"          "query acronym of the event"
     "query.event-location"         "query location of the event"
     "query.event-name"             "query name of the event"
     "query.event-sponsor"          "query sponsor of the event"
     "query.event-theme"            "query theme of the event"
     "query.title"                  "query title"
     "query.funder-name"            "query name of the funder"
     "query.publisher-location"     "query location of the publisher"
     "query.publisher-name"         "query publisher name"
     "query.standards-body-acronym" "query acronym of the standards body"
     "query.standards-body-name"    "query standards body name"
     "query.description"            "query description"}))

(defn- offset-limited-description []
  (slurp (resource "swagger/offset-limited-description.md")))

(defn- offset-examples []
  (slurp (resource "swagger/offset-examples.md")))

(defn- offset-description []
  (slurp (resource "swagger/offset-description.md")))

(defn- cursor-description []
  (slurp (resource "swagger/cursor-description.md")))

(defn- sample-description []
  (slurp (resource "swagger/sample-description.md")))

(defn- works-combinations-description []
  (slurp (resource "swagger/works-combinations-description.md")))

(defn- works-description [title]
  (str
    title
    (query-description)
    (field-query-description)
    (sorts-description)
    (facets-description)
    (filters-description)
    (selects-description)
    (offset-limited-description)
    (offset-examples)
    (cursor-description)
    (sample-description)
    (works-combinations-description)))

(defn- funders-description [title]
  (str
    title
    (query-funder-description)
    (filters-funders-description)
    (offset-description)
    (offset-examples)
    (cursor-description)))

(defn- journals-description [title]
  (str
    title
    (query-journal-description)
    (offset-description)
    (offset-examples)
    (cursor-description)))

(defn- members-description [title]
  (str
    title
    (query-member-description)
    (filters-member-description)
    (offset-description)
    (offset-examples)
    (cursor-description)))

(defn- types-description [title]
  (str
    title
    (offset-description)
    (offset-examples)))

(defn- licenses-description [title]
  (str
    title
    (query-license-description)
    (offset-limited-description)
    (offset-examples)
    (cursor-description)))


(def works
  {"/works"
   {:get {:description (works-description "Returns a list of all works (journal articles, conference proceedings, books, components, etc), 20 per page.")
          :parameters (merge-with merge sc/WorksQuery sc/FilterParams sc/DeepPagingParams sc/QueryParams sc/BasicParams)
          :responses {200 {:schema sc/WorksMessage
                           :description "A list of works"}}
          :tags ["Works"]}}
   "/works/:doi"
   {:get {:description "Returns metadata for the specified Crossref DOI, as an example use DOI 10.5555/12345678"
          :parameters {:path {:doi sc/WorkDoi}}
          :responses {200 {:schema sc/WorkMessage
                           :description "The work identified by {doi}."}
                      404 {:description "The work identified by {doi} does not exist."}}
          :tags ["Works"]}}
   "/works/:doi/agency"
   {:get {:description "Gets the agency associated with a specific work by its DOI, as an example use DOI 10.5555/12345678"
          :parameters {:path {:doi sc/WorkDoi}}
          :responses {200 {:schema sc/AgencyMessage
                           :description "The agency associated with work identified by {doi}."}
                      404 {:description "The work identified by {doi} does not exist."}}
          :tags ["Works"]}}})

(def funders
  {"/funders"
   {:get {:description (funders-description "Returns a list of all funders in the [Funder Registry](https://gitlab.com/crossref/open_funder_registry).")
          :parameters (merge-with merge sc/FundersFilter sc/DeepPagingParams sc/QueryParams sc/BasicParams)
          :responses {200 {:schema sc/FundersMessage
                           :description "A list of funders."}}
          :tags ["Funders"]}}
   "/funders/:id"
   {:get {:description "Returns metadata for specified funder **and** its suborganizations, as an example use id 501100006004"
          :parameters {:path {:id sc/FunderId}}
          :responses {200 {:schema sc/FunderMessage
                           :description "The funder identified by {id}."}
                      404 {:description "The funder identified by {id} does not exist."}}
          :tags ["Funders"]}}
   "/funders/:id/works"
   {:get {:description (works-description "Returns list of works associated with the specified {id}.")
          :parameters (merge-with merge sc/WorksQuery sc/FilterParams sc/DeepPagingParams sc/QueryParams sc/BasicParams {:path {:id sc/FunderId}})
          :responses {200 {:schema sc/WorksMessage
                           :description "A list of works"}}
          :tags ["Funders"]}}})

(def journals
  {"/journals"
   {:get {:description (journals-description "Return a list of journals in the Crossref database.")
          :parameters (merge-with merge sc/DeepPagingParams sc/QueryParams sc/BasicParams)
          :responses {200 {:schema sc/JournalsMessage
                           :description "A list of journals"}}
          :tags ["Journals"]}}
   "/journals/:issn"
   {:get {:description "Returns information about a journal with the given ISSN, as an example use ISSN 03064530"
          :parameters {:path {:issn sc/JournalIssn}}
          :responses {200 {:schema sc/JournalMessage
                           :description "The journal identified by {issn}."}
                      404 {:description "The journal identified by {issn} does not exist."}}
          :tags ["Journals"]}}
   "/journals/:issn/works"
   {:get {:description (works-description "Returns a list of works in the journal identified by {issn}.")
          :parameters (merge-with merge sc/WorksQuery sc/FilterParams sc/DeepPagingParams sc/QueryParams sc/BasicParams {:path {:issn sc/JournalIssn}})
          :responses {200 {:schema sc/WorksMessage
                           :description "A list of works"}}
          :tags ["Journals"]}}})

(def members
  {"/members"
   {:get {:description (members-description "Returns a list of all Crossref members (mostly publishers).")
          :parameters (merge-with merge sc/DeepPagingParams sc/FilterParams sc/QueryParams sc/BasicParams)
          :responses {200 {:schema sc/MembersMessage
                           :description "A collection of members"}}
          :tags ["Members"]}}
   "/members/:id"
   {:get {:description "Returns metadata for a Crossref member, as an example use id 324"
          :parameters {:path {:id s/Int}}
          :responses {200 {:schema sc/MemberMessage
                           :description "The prefix data identified by {id}."}
                      404 {:description "The prefix data identified by {id} does not exist."}}
          :tags ["Members"]}}
   "/members/:id/works"
   {:get {:description (works-description "Returns list of works associated with a Crossref member (deposited by a Crossref member) with {id}.")
          :parameters (merge-with merge sc/WorksQuery sc/FilterParams sc/DeepPagingParams sc/QueryParams sc/BasicParams {:path {:id s/Int}})
          :responses {200 {:schema sc/WorksMessage
                           :description "A list of works"}}
          :tags ["Members"]}}})

(def prefixes
  {"/prefixes/:prefix"
   {:get {:description "Returns metadata for the DOI owner prefix, as an example use prefix 10.1016"
          :parameters {:path {:prefix s/Str}}
          :responses {200 {:schema sc/PrefixMessage
                           :description "The prefix data identified by {prefix}."}
                      404 {:description "The prefix data identified by {prefix} does not exist."}}
          :tags ["Prefixes"]}}
   "/prefixes/:prefix/works"
   {:get {:description (works-description "Returns list of works associated with specified {prefix}.")
          :parameters (merge-with merge sc/WorksQuery sc/FilterParams sc/DeepPagingParams sc/QueryParams sc/BasicParams {:path {:prefix s/Str}}
)
          :responses {200 {:schema sc/WorksMessage
                           :description "A list of works"}}
          :tags ["Prefixes"]}}})

(def types
  {"/types"
   {:get {:description (types-description "Returns a list of valid work types.")
          :parameters sc/BasicParams
          :responses {200 {:schema sc/TypesMessage
                           :description "A collection of types"}}
          :tags ["Types"]}}
   "/types/:id"
   {:get {:description "Returns information about a metadata work type, as an example use `monograph`"
          :parameters {:path {:id s/Int}}
          :responses {200 {:schema sc/TypeMessage
                           :description "The type identified by {id}."}
                      404 {:description "The type identified by {id} does not exist."}}
          :tags ["Types"]}}
   "/types/:id/works"
   {:get {:description (works-description "returns list of works of type {id}.")
          :parameters (merge-with merge sc/WorksQuery sc/FilterParams sc/DeepPagingParams sc/QueryParams sc/BasicParams {:path {:id s/Int}})
          :responses {200 {:schema sc/WorksMessage
                           :description "A list of works"}}
          :tags ["Types"]}}})

(def licenses
  {"/licenses"
   {:get {:description (licenses-description "Returns a list of licenses.")
          :parameters (merge-with merge sc/LicensesQuery sc/QueryParams sc/BasicParams)
          :responses {200 {:schema sc/LicensesMessage
                           :description "A list of licenses"}}
          :tags ["Licenses"]}}})

(def paths
  {:paths
   (merge
     works
     funders
     members
     journals
     prefixes
     types
     licenses)})

(defroutes api-doc-routes
 (swagger-ui
  {:path "/swagger-ui"
  :swagger-docs "/swagger-docs"})
 (GET "/swagger-docs" []
  (-> (json/write-str
	   (s/with-fn-validation
		(rs/swagger-json
		 (merge
		  info
		  tags
		  paths))))
   (response/response)
   (response/header "Content-Type" "application/json"))))
