(ns cayenne.api.v1.facet
  (:require [cayenne.ids.doi :as doi]
            [cayenne.ids.issn :as issn]
            [cayenne.ids.type :as type]
            [clojure.string :refer [join]]
            [cayenne.util :refer [assoc-exists]]))

(def std-facets
  {"type-name"               {:internal-field-name ["type"]
                              :allow-unlimited-values true
                              :transformer #(get-in type/type-dictionary [(keyword %) :index-id])}
   "published"               {:internal-field-name ["issued-year"]
                              :allow-unlimited-values true}
   "container-title"         {:internal-field-name ["container-title"]}
   "funder-name"             {:internal-field-name ["funder-name"]
                              :allow-unlimited-values true}
   "funder-doi"              {:internal-field-name ["funder-doi"]
                              :allow-unlimited-values true
                              :transformer doi/to-doi-uri}
   "orcid"                   {:internal-field-name ["contributor-orcid"]
                              :allow-unlimited-values true}
   "issn"                    {:internal-field-name ["issn.value"]
                              :allow-unlimited-values true
                              :transformer issn/to-issn-uri}
   "publisher-name"          {:internal-field-name ["publisher"]
                              :allow-unlimited-values true}
   "license"                 {:internal-field-name ["license-url"]
                              :allow-unlimited-values true}
   "archive"                 {:internal-field-name ["archive"]
                              :allow-unlimited-values true}
   "update-type"             {:internal-field-name ["update-to" "type"]
                              :allow-unlimited-values true}
   "relation-type"           {:internal-field-name ["relation-type"]
                              :allow-unlimited-values true}
   "affiliation"             {:internal-field-name ["contributor-affiliation"]
                              :allow-unlimited-values true}
   "assertion"               {:internal-field-name ["assertion-name"]
                              :allow-unlimited-values true}
   "assertion-group"         {:internal-field-name ["assertion-group-name"]
                              :allow-unlimited-values true}
   "link-application"        {:internal-field-name ["link-application"]}
   "journal-volume"          {:internal-field-name ["volume"]
                              :allow-unlimited-values true}
   "journal-issue"           {:internal-field-name ["issue"]
                              :allow-unlimited-values true}
   "category-name"           {:internal-field-name ["subject"]
                              :allow-unlimited-values true}
   "source"                  {:internal-field-name ["source"]
                              :allow-unlimited-values true}
   "ror-id"                  {:internal-field-name ["institution-ror-id"]
                              :allow-unlimited-values true}})

(defn facet-value-limit [field specified-limit]
  (cond (and (= specified-limit "*")
             (get-in std-facets [field :allow-unlimited-values]))
        100000
        (= specified-limit "*")
        1000
        :else
        specified-limit))

(defn generate-agg-query [field cnt]
  (let [[first-field & rest-fields :as nested-paths] (->> field (get std-facets) :internal-field-name reverse)
        limited-count (facet-value-limit field cnt)]
    (loop [[first-path & rst-path] nested-paths
           agg-val {:aggs {(keyword field) {:terms {:field (join "." (reverse nested-paths)) :size limited-count}}}}]
      (let [nested-field (when rst-path (keyword field
                                        ; (str "nested-" (first rst-path) "-agg")
                                                 ))
            nested-map {:aggs {nested-field (merge {:nested {:path (->> rst-path reverse (join "."))}}
                                                   agg-val)}}]
        (if rst-path
          (recur rst-path nested-map)
          agg-val)))))

(defn with-aggregations [es-body {:keys [facets]}]
  (reduce
   (fn [es-body {:keys [field count]}]
     (let [limited-count (facet-value-limit field count)
           {:keys [internal-field-name]} (get std-facets field)
           agg-query (generate-agg-query field count)]
       (merge-with merge
                   es-body
                   agg-query)))
   es-body
   facets))

(defn find-buckets [[k agg-map]]
  (loop [m agg-map]
    (if (:buckets m)
      (:buckets m)
      (recur (k m)))))

(defn ->response-facet [aggregation]
  (let [external-field-name (first aggregation)
        buckets (find-buckets aggregation)
        facet-config (std-facets (name external-field-name))
        transformer (or (:transformer facet-config) identity)
        values (into {} (map
                         #(vector (transformer (:key %)) (:doc_count %)) buckets))
        sorted-values (into
                       (sorted-map-by
                        (fn [key1 key2]
                          (compare [(get values key2) key2][(get values key1) key1]))) values)]
    [(name external-field-name)
     {:value-count (count buckets)
      :values sorted-values}]))

(defn ->response-facets [aggregations]
  (into {} (map ->response-facet aggregations)))
