(ns cayenne.api.v1.schema
  (:require [ring.swagger.json-schema :refer [field]]
            [schema.core :as s]))

;; Generic
(s/defschema Query
  {:start-index s/Int :search-terms s/Str})

(s/defschema DateParts
  {:date-parts [[s/Int s/Int s/Int]]})

(s/defschema Date
  (merge DateParts {:date-time s/Inst
                    :timestamp s/Int}))

(s/defschema DateAndVersion
  (merge Date {:version s/Str}))

(s/defschema Message
  {:status s/Str
   :message-type s/Str
   :message-version s/Str})

(s/defschema BasicParams
  {:query {(s/optional-key :rows) 
           (field s/Int {:description "The number of rows per page"})
           (s/optional-key :mailto) 
           (field s/Str 
                  {:pattern #"^[A-Za-z0-9._%+-]+@[A-Za-z0-9-]+\.[A-Za-z]{2,6}$" 
                   :description "The email address to identify yourself and be in the \"polite pool\""})
           (s/optional-key :offset) 
           (field s/Int {:description "The number of rows to skip before returning"})}})

(s/defschema DeepPagingParams
  {:query {(s/optional-key :cursor) 
           (field s/Str {:description "Exposes the ability to deep page through large result sets, where offset would cause performance problems"})}})

(s/defschema QueryParams
  {:query {(s/optional-key :query) 
           (field s/Str {:description "Exposes the ability to free text query certain fields"})}})

(s/defschema FilterParams
  {:query {(s/optional-key :filter) 
           (field s/Str {:description "Exposes the ability to filter by certain fields, supports a comma separated list of lucene filters, e.g. `content-domain:psychoceramics.labs.crossref.org`"})}})

(s/defschema IdAndLabel
  {:id s/Str :label s/Str})

(s/defschema Affiliation
  {:name s/Str})

(s/defschema Author
  {(s/optional-key :ORCID) s/Str
   (s/optional-key :authenticated-orcid) Boolean
   (s/optional-key :given) s/Str
   (s/optional-key :prefix) s/Str
   (s/optional-key :suffix) s/Str
   (s/optional-key :name) s/Str
   (s/optional-key :family) s/Str
   :sequence s/Str
   :affiliation [Affiliation]})

(s/defschema CoverageFull
  {:affiliations-current s/Num
   :funders-backfile s/Num
   :licenses-backfile s/Num
   :funders-current s/Num
   :affiliations-backfile s/Num
   :resource-links-backfile s/Num
   :orcids-backfile s/Num
   :update-policies-current s/Num
   :orcids-current s/Num
   :references-backfie s/Num
   :award-numbers-backfile s/Num
   :update-policies-backfile s/Num
   :licenses-current s/Num
   :award-numbers-current s/Num
   :abstracts-backfile s/Num
   :resource-links-current s/Num
   :abstracts-current s/Num
   :references-current s/Num
   :similarity-checking-current s/Num
   :similarity-checking-backfile s/Num
   :ror-ids-current s/Num
   :ror-ids-backfile s/Num
   :descriptions-current s/Num
   :descriptions-backfile s/Num})

(s/defschema Coverage
  {:last-status-check-time s/Int
   :affiliations s/Num
   :funders s/Num
   :update-policies s/Num
   :orcids s/Num
   :licenses s/Num
   :award-numbers s/Num
   :resource-links s/Num
   :abstracts s/Num
   :references s/Num
   :similarity-checking s/Num
   :ror-ids s/Num
   :descriptions s/Num})

(s/defschema Flags
  {:deposits-abstracts-current Boolean
   :deposits-orcids-current Boolean
   :deposits Boolean
   :deposits-affiliations-backfile Boolean
   :deposits-update-policies-backfile Boolean
   :deposits-award-numbers-current Boolean
   :deposits-resource-links-current Boolean
   :deposits-articles Boolean
   :deposits-affiliations-current Boolean
   :deposits-funders-current Boolean
   :deposits-references-backfile Boolean
   :deposits-abstracts-backfile Boolean
   :deposits-licenses-backfile Boolean
   :deposits-award-numbers-backfile Boolean
   :deposits-references-current Boolean
   :deposits-resource-links-backfile Boolean
   :deposits-orcids-backfile Boolean
   :deposits-funders-backfile Boolean
   :deposits-update-policies-current Boolean
   :deposits-licenses-current Boolean
   :deposits-ror-ids-backfile Boolean
   :deposits-ror-ids-current Boolean
   :deposits-descriptions-backfile Boolean
   :deposits-descriptions-current Boolean})

(s/defschema DoiCounts
  {:total-dois s/Int
   :current-dois s/Int
   :backfile-dois s/Int})

;; Funders
(s/defschema FunderId (field s/Str {:description "The id of the funder"}))
(s/defschema Funder {:id FunderId
                     :location (field s/Str {:description "The geographic location of the funder"})
                     :name s/Str
                     :alt-names (field [s/Str] {:description "Other names this funder may be identified with"})
                     :uri s/Str
                     :replaces [s/Str]
                     :replaced-by [s/Str]
                     :tokens [s/Str]})

(s/defschema FunderHierarchy {s/Str s/Any
                              (s/optional-key :more) Boolean})

(s/defschema HierarchyNamesObject
  {s/Str s/Str})

(s/defschema FunderFull
  (merge Funder {:descendant-work-count s/Int
                 :work-count s/Int
                 :hierarchy-names HierarchyNamesObject
                 :hierarchy FunderHierarchy
                 :descendants [s/Str]}))

(s/defschema FunderMessage
  (merge Message {:message-type #"funder" :message FunderFull}))

(s/defschema Funders
  {:items-per-page s/Int
   :query Query
   :total-results s/Int
   :items [Funder]})

(s/defschema FundersMessage
  (merge Message {:message-type #"funder-list"
                  :message Funders}))

(s/defschema
  FundersFilter
  {:query
   {(s/optional-key :filter) 
    (field s/Str {:description "Exposes the ability to search funders by location using a Lucene based syntax"
                  :pattern "location:.*"})}})

(s/defschema BreakdownsObject
  {:dois-by-issued-year [[s/Int s/Int]]})

(s/defschema CoverageObject
  {s/Str Coverage})

(s/defschema CoverageTypeObject
  {:all CoverageObject
   :current CoverageObject
   :backfile CoverageObject})

;; Journals
(s/defschema JournalIssn (field [s/Str] {:description "The ISSN identifier associated with the journal"}))
(s/defschema JournalIssnType
  {:value s/Str
   :type s/Str})

(s/defschema Journal
  {:title (field s/Str {:description "The title of the journal"})
   :publisher (field s/Str {:description "The publisher of the journal"})
   :last-status-check-time s/Int
   :counts DoiCounts
   :breakdowns BreakdownsObject
   :coverage CoverageFull
   :coverage-type CoverageTypeObject
   :flags Flags
   :subjects [s/Str]
   :issn-type JournalIssnType
   :ISSN JournalIssn})

(s/defschema JournalMessage
  (merge Message {:message-type #"journal" :message Journal}))

(s/defschema Journals
  {:items-per-page s/Int
   :query Query
   :total-results s/Int
   :items [Journal]})

(s/defschema JournalsMessage
  (merge Message {:message-type #"journal-list"
                  :message Journals}))

;; works
(s/defschema
  WorksQuery
  {:query 
   {(s/optional-key :select) 
    (field s/Str {:description "Exposes the ability to select certain fields, supports a comma separated list of fields, e.g. `DOI,volume`"
                  :pattern #"^\w+(,\w+)*$"})
    (s/optional-key :facet) 
    (field s/Str {:description "Exposes the ability to retrieve counts for pre-defined facets e.g. `type-name:*` returns counts of all works by type"})
    (s/optional-key :query) 
    (field s/Str {:description "Exposes the ability to free text query certain fields"})
    (s/optional-key :sample) 
    (field s/Int {:description "Exposes the ability to return `N` randomly sampled items"})
    (s/optional-key :sort) 
    (field s/Str {:description "Exposes the ability to sort results by a certain field, e.g. `score`"})
    (s/optional-key :order) 
    (field s/Str {:description "Combined with sort can be used to specify the order of results, e.g. asc or desc"
                  :pattern #"(asc|desc)"})}})

(s/defschema Agency IdAndLabel)

(s/defschema PrimaryResource
  {:URL s/Str})

(s/defschema SecondaryResource
  {:URL s/Str
   :label s/Str})

(s/defschema Resources
  {:primary PrimaryResource
   (s/optional-key :secondary) [SecondaryResource]})

(s/defschema WorkDoi (field s/Str {:description "The DOI identifier associated with the work"}))

(s/defschema VersionInfoDescription
  {(s/optional-key :language) s/Str
   (s/optional-key :description) s/Str})

(s/defschema VersionInfo
  {:version s/Str
   (s/optional-key :language) s/Str
   (s/optional-key :version-description) [VersionInfoDescription]})

(s/defschema WorkLink
  {:URL s/Str
   :content-type s/Str
   :content-version s/Str
   :intended-application s/Str})

(s/defschema WorkLicense
  {:URL s/Str
   (s/optional-key :start) Date
   :delay-in-days s/Int
   :content-version s/Str})

(s/defschema WorkDomain
  {:domain [s/Str]
   (s/optional-key :crossmark-restriction) Boolean})

(s/defschema WorkReview
  {(s/optional-key :type) s/Str
   (s/optional-key :running-number) s/Str
   (s/optional-key :revision-round) s/Str
   (s/optional-key :stage) s/Str
   (s/optional-key :competing-interest-statement) s/Str
   (s/optional-key :recommendation) s/Str
   (s/optional-key :language) s/Str})

(s/defschema WorkInstitution
  {:name s/Str
   (s/optional-key :place) [s/Str]
   (s/optional-key :department) [s/Str]
   (s/optional-key :acronym) [s/Str]})

(s/defschema WorkRelationObject
  {:id-type s/Str
   :id s/Str
   :asserted-by s/Str})


(s/defschema WorkRelation
  {s/Str WorkRelationObject})

(s/defschema WorkISSNType
  {:type s/Str
   :value [s/Str]})

(s/defschema WorkStandardsBody
  {(s/optional-key :name) s/Str
   (s/optional-key :acronym) [s/Str]})

(s/defschema Reference
  {(s/optional-key :key) s/Str
   (s/optional-key :doi) s/Str
   (s/optional-key :doi-asserted-by) s/Str
   (s/optional-key :issn) s/Str
   (s/optional-key :issn-type) s/Str
   (s/optional-key :isbn) s/Str
   (s/optional-key :isbn-type) s/Str
   (s/optional-key :author) s/Str
   (s/optional-key :volume) s/Str
   (s/optional-key :issue) s/Str
   (s/optional-key :first-page) s/Str
   (s/optional-key :year) s/Str
   (s/optional-key :edition) s/Str
   (s/optional-key :component) s/Str
   (s/optional-key :standard-designator) s/Str
   (s/optional-key :standards-body) s/Str
   (s/optional-key :unstructured) s/Str
   (s/optional-key :article-title) s/Str
   (s/optional-key :series-title) s/Str
   (s/optional-key :volume-title) s/Str
   (s/optional-key :journal-title) s/Str
   (s/optional-key :type) s/Str})

(s/defschema WorkEvent
  {:name s/Str
   (s/optional-key :theme) s/Str
   (s/optional-key :location) s/Str
   (s/optional-key :acronym) s/Str
   (s/optional-key :number) s/Str
   (s/optional-key :sponsor) s/Str
   (s/optional-key :start) DateParts
   (s/optional-key :end) DateParts})

(s/defschema WorkClinicalTrial
  {:clinical-trial-number s/Str
   :registry s/Str
   (s/optional-key :type) s/Str})

(s/defschema WorkAssertionGroup
  {:name s/Str
   (s/optional-key :label) s/Str})

(s/defschema WorkAssertionExplanation
  {:URL s/Str})

(s/defschema WorkAssertion
  {(s/optional-key :group) WorkAssertionGroup
   (s/optional-key :explanation) WorkAssertionExplanation
   :name s/Str
   (s/optional-key :value) s/Str
   (s/optional-key :URL) s/Str
   (s/optional-key :order) s/Int})

(s/defschema FunderIdentifier
  {:id s/Str
   :id-type s/Str
   :asserted-by s/Str})

(s/defschema WorkFunder
  {(s/optional-key :name) s/Str
   (s/optional-key :DOI) s/Str
   (s/optional-key :doi-asserted-by) s/Str
   (s/optional-key :award) [s/Str]
   (s/optional-key :id) [FunderIdentifier]})

(s/defschema AffiliationIdentifier
  {:id s/Str
   :id-type s/Str
   :asserted-by s/Str})

(s/defschema InvestigatorAffiliation
  {:id [AffiliationIdentifier]
   :name s/Str})

(s/defschema Investigator
  {(s/optional-key :ORCID) s/Str
   (s/optional-key :authenticated-orcid) s/Bool
   (s/optional-key :prefix) s/Str
   (s/optional-key :suffix) s/Str
   (s/optional-key :name) s/Str
   (s/optional-key :given) s/Str
   (s/optional-key :family) s/Str
   (s/optional-key :alternate-name) s/Str
   (s/optional-key :sequence) s/Str
   :affiliation [InvestigatorAffiliation]
   (s/optional-key :role-start) DateParts
   (s/optional-key :role-end) DateParts})

(s/defschema AwardAmount
  {:amount s/Num
   (s/optional-key :currency) s/Str
   (s/optional-key :percentage) s/Int})

(s/defschema Funding
  {:type s/Str
   (s/optional-key :scheme) s/Str
   (s/optional-key :award-amount) AwardAmount
   :funder [WorkFunder]})

(s/defschema ProjectTitle
  {:title s/Str
   (s/optional-key :language) s/Str})

(s/defschema WorkProject
  {:project-title [ProjectTitle]
   (s/optional-key :investigator) [Investigator]
   (s/optional-key :lead-investigator) [Investigator]
   (s/optional-key :co-lead-investigator) [Investigator]
   (s/optional-key :award-amount) AwardAmount
   (s/optional-key :award-start) [DateParts]
   (s/optional-key :award-end) [DateParts]
   (s/optional-key :award-planned-start) [DateParts]
   (s/optional-key :award-planned-end) [DateParts]
   (s/optional-key :funding) [Funding]})

(s/defschema WorkUpdate
  {:label s/Str
   :DOI s/Str
   :type s/Str
   :source s/Str
   :updated Date})

(s/defschema StatusDescription
  {(s/optional-key :language) s/Str
   (s/optional-key :description) s/Str})

(s/defschema PostedContentStatus
  {:type s/Str
   :update DateParts
   :status-description [StatusDescription]})

(s/defschema WorkJournalIssue
  {:issue s/Str})

(s/defschema WorkFreeToRead
  {(s/optional-key :start-date) DateParts
   (s/optional-key :end-date) DateParts})

(s/defschema Work
  {:DOI WorkDoi
   :URL s/Str
   (s/optional-key :author) [Author]
   (s/optional-key :content-domain) WorkDomain
   :created Date
   :deposited Date
   :indexed DateAndVersion
   (s/optional-key :is-referenced-by-count) s/Int
   :issued DateParts
   :member s/Str
   :prefix s/Str
   :publisher s/Str
   (s/optional-key :reference-count) s/Int
   (s/optional-key :references-count) s/Int
   :score Long
   :source s/Str
   (s/optional-key :title) [s/Str]
   :type s/Str
   :resource Resources
   (s/optional-key :version) VersionInfo
   (s/optional-key :ISBN) [s/Str]
   (s/optional-key :ISSN) [s/Str]
   (s/optional-key :abstract) s/Str
   (s/optional-key :accepted) DateParts
   (s/optional-key :alternative-id) [s/Str]
   (s/optional-key :approved) DateParts
   (s/optional-key :archive) [s/Str]
   (s/optional-key :article-number) s/Str
   (s/optional-key :assertion) [WorkAssertion]
   (s/optional-key :chair) [Author]
   (s/optional-key :component-number) s/Str
   (s/optional-key :clinical-trial-number) [WorkClinicalTrial]
   (s/optional-key :container-title) [s/Str]
   (s/optional-key :issue-title) [s/Str]
   (s/optional-key :content-created) DateParts
   (s/optional-key :content-updated) DateParts
   (s/optional-key :degree) s/Str
   (s/optional-key :edition-number) s/Str
   (s/optional-key :editor) [Author]
   (s/optional-key :free-to-read) WorkFreeToRead
   (s/optional-key :funder) [WorkFunder]
   (s/optional-key :group-title) [s/Str]
   (s/optional-key :institution) [WorkInstitution]
   (s/optional-key :isbn-type) [WorkISSNType]
   (s/optional-key :issn-type) [WorkISSNType]
   (s/optional-key :issue) s/Str
   (s/optional-key :journal-issue) WorkJournalIssue
   (s/optional-key :language) s/Str
   (s/optional-key :license) [WorkLicense]
   (s/optional-key :link) [WorkLink]
   (s/optional-key :original-title) [s/Str]
   (s/optional-key :page) s/Str
   (s/optional-key :part-number) s/Str
   (s/optional-key :posted) DateParts
   (s/optional-key :project) WorkProject
   (s/optional-key :published) DateParts
   (s/optional-key :published-online) DateParts
   (s/optional-key :published-other) DateParts
   (s/optional-key :published-print) DateParts
   (s/optional-key :publisher-location) s/Str
   (s/optional-key :reference) Reference
   (s/optional-key :relation) WorkRelation
   (s/optional-key :review) WorkReview
   (s/optional-key :short-container-title) s/Str
   (s/optional-key :short-title) [s/Str]
   (s/optional-key :standards-body) [WorkStandardsBody]
   (s/optional-key :subject) [s/Str]
   (s/optional-key :subtitle) [s/Str]
   (s/optional-key :subtype) s/Str
   (s/optional-key :translator) [Author]
   (s/optional-key :update-policy) s/Str
   (s/optional-key :update-to) [WorkUpdate]
   (s/optional-key :updated-by) [WorkUpdate]
   (s/optional-key :volume) s/Str
   (s/optional-key :status) PostedContentStatus
   (s/optional-key :proceedings-subject) s/Str})


(s/defschema WorkMessage
  (merge Message
         {:message-type #"work" 
          :message Work}))

(s/defschema Works
  {:items-per-page s/Int
   :query Query
   :total-results s/Int
   (s/optional-key :next-cursor) (field s/Str {:description "Used to navigate to the next page of results when using cursor deep paging"})
   :items [Work]})

(s/defschema WorksMessage
  (merge Message
         {:message-type #"work-list"
          :message Works}))

(s/defschema DoiAgency
  {:DOI WorkDoi
   :agency Agency})

(s/defschema AgencyMessage
  (merge Message {:message-type #"work-agency" :message DoiAgency}))

;; Prefixes
(s/defschema Prefix
  {:member s/Str
   :name s/Str
   :prefix s/Str})

(s/defschema PrefixMessage
  (merge Message {:message-type #"prefix" :message Prefix}))

(s/defschema MemberPrefix
  {:name s/Str
   :value s/Str})

(s/defschema MemberCountObject
  {s/Str s/Int})

(s/defschema MemberCountsType
  {:all MemberCountObject
   :current MemberCountObject
   :backfile MemberCountObject})

;; Members
(s/defschema Member
  {:id s/Int
   :primary-name s/Str
   :last-status-check-time s/Int
   :counts DoiCounts
   :counts-type MemberCountsType
   :breakdowns BreakdownsObject
   :prefixes [s/Str]
   :coverage CoverageFull
   :coverage-type CoverageTypeObject
   :flags Flags
   :tokens [s/Str]
   :names [s/Str]
   :location s/Str
   :prefix [MemberPrefix]})

(s/defschema Members
  {:items-per-page s/Int
   :query Query
   :total-results s/Int
   :items [Member]})

(s/defschema MembersMessage
  (merge Message
         {:message-type #"member-list"
          :message Members}))

(s/defschema MemberMessage
  (merge Message
         {:message-type #"member" 
          :message Member}))

;; Types
(s/defschema Type IdAndLabel)

(s/defschema Types
  {:items-per-page s/Int
   :query Query
   :total-results s/Int
   :items [Type]})

(s/defschema TypesMessage
  (merge Message
         {:message-type #"type-list"
          :message Types}))

(s/defschema TypeMessage
  (merge Message
         {:message-type #"type" 
          :message Type}))

;; Licenses
(s/defschema
  LicensesQuery
  {:query 
   {(s/optional-key :query) 
    (field s/Str {:description "Exposes the ability to free text query certain works fields, supports a comma separated list of lucene filters, e.g. `title:cortisol`"})
    (s/optional-key :cursor) 
    (field s/Str {:description "Exposes the ability to deep page through large result sets, where offset would cause performance problems"})}})

(s/defschema License
  {:URL s/Str
   :work-count s/Int})

(s/defschema Licenses
  {:total-results s/Int
   :items [License]})

(s/defschema LicensesMessage
  (merge Message
         {:message-type #"license-list"
          :message Licenses}))

(s/defschema Deposit
  {:owner s/Str
   :filename s/Str
   :pingback-url s/Str
   :citation-count s/Int
   :matched-citation-count s/Int
   :length s/Int
   :batch-id s/Str
   :test s/Bool
   :submitted-at s/Str
   :status s/Str})

(s/defschema Deposits
  {:total-results s/Int
   :items [Deposit]})

(s/defschema DepositMessage
  (merge Message
         {:message-type #"deposit" 
          :message Deposit}))

(s/defschema DepositsMessage
  (merge Message
         {:message-type #"deposit-list"
          :message Deposits}))

(s/defschema DepositPostQueryParams
  {:query {(s/optional-key :test) (field s/Bool {:description "Is the deposit for test purposes?"})}
   :body s/Str})
