(ns cayenne.api.auth.apikey
  (:require [ring.util.response :as ring]
            [clojure.data.json :as json]
            [clojure.string :as string]
            [cayenne.sentry :refer [send-sentry-event]]
            [cayenne.conf :as conf])

  (:import [org.crossref.authz CrossrefAuthorizationInterface AWSApiMetrics]))

(defonce auth-interface
 (if (conf/get-param [:service :api-key-auth])
  (let [cai (CrossrefAuthorizationInterface.
             (conf/get-param [:service :keycloak :client])
             (conf/get-param [:service :keycloak :client-secret])
             (conf/get-param [:service :keycloak :api-key-salt])
             (conf/get-param [:service :keycloak :realm])
             (conf/get-param [:service :keycloak :url])
             (AWSApiMetrics. "cayenne" 300 nil))]
   (.setCache cai 10000 60)
   cai)))

(defonce api-key-headers #{"crossref-api-key" "authorization" "crossref-plus-api-token"})

(defn- request->api-key [r]
  (some->> r
           :headers
           (filter (comp api-key-headers string/lower-case key))
           first
           val
           (#(string/replace % #"^Bearer " ""))))

(defn- check-keycloak-permission [resource api-key]
  (let [pair (.checkPermission auth-interface resource api-key nil nil)]
    (.getFirst pair)))

(defn- authenticated? [r]
  (let [api-key (request->api-key r)]
     (if (or (nil? api-key) (nil? auth-interface))
       false
       (check-keycloak-permission "metadata-plus-data" api-key))))

(defn- wrap-auth [handler]
  (fn [request]
    (try
      (if (authenticated? request)
        (handler request)
        (-> (ring/response (json/write-str
                            {:status :error
                             :message-type :access-denied
                             :message-version "1.0.0"
                             :message "Access Denied"}))
            (ring/status 401)))
      (catch Exception e
        (send-sentry-event {} e)
        (-> (ring/response (json/write-str
                            {:status :error
                             :message-type :exception
                             :message-version "1.0.0"
                             :message "Auth service exception"}))
            (ring/status 503))))))

(defn wrap-auth-when-enabled
  ([handler enabler]
   (if (enabler)
     (wrap-auth handler)
     (fn
       ([request] (handler request))
       ([request _ _] (handler request))))))
