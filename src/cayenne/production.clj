(ns cayenne.production
  (:gen-class :main true)
  (:require [cayenne.conf :as conf]
            [cayenne.defaults]
			[taoensso.timbre :as timbre]
            [cayenne.schedule :as schedule]
            [cayenne.startup-tasks]
            [cayenne.version :refer [version]]
            [clojure.data.json :as json]
            [clojure.pprint :refer [pprint]]
            [org.httpkit.client :as http]
            [sentry-clj.core :as sentry]))

(defn slack-format
  [{:keys [level ns throwable message]}]
  (format "_%s_ `%s` %s%s"
          (-> level name)
          ns
          (if message
            (with-out-str (pprint message))
            "")
          (if throwable
            (str "\n" (.toString throwable))
            "")))

(defn send-to-slack [log-event]
  (let [payload {"username" "cayennebot"
                 "icon_emoji" ":ghost:"
                 "text" (slack-format log-event)}]
    (http/post (conf/get-param [:upstream :slack-logging])
               {:form-params {:payload (json/write-str payload)}})))

(def timbre-slack
  {:doc "Spits to #cayenne slack channel."
   :enabled? true
   :async? true
   :fn send-to-slack})

(def termination (promise))

(defn -main [& args]
  ; Drop leading colon from profile names.
  (let [profiles (map #(->> % (drop 1) (apply str) keyword) args)]

    (sentry/init!
      (conf/get-param [:sentry :dsn])
      {:environment (conf/get-param [:sentry :env]) :release version})
    
    (schedule/start)

    (conf/create-core-from! :production :default)
    (conf/set-core! :production)

    (apply conf/start-core! :production profiles)

    @termination))

(defn stop []
  (deliver termination true))
