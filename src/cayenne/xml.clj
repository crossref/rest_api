(ns cayenne.xml
  (:require [clojure.data.xml :as xml]
           [clojure.string :as string]))

(defn node->tagname
  "Given an xml node extracts the tag name removing the namespace"
  [n]
  (let [tag (:tag n)]
    (cond
      (string? tag)
      tag
      (keyword? tag)
      (name tag)
      :else
      nil)))

(defn node->str [node]
  (loop [output ""
         [f & contents] (-> node :content)]
    (cond
      (string? f)
      (recur (str output f) contents)
      (nil? f)
      output
      :else
      (recur output (concat (:content f) contents)))))

(defn- node->xml [node]
  (-> node
      xml/emit-str
      (string/replace #"^<\?xml[^>]+><[^>]+>" "")
      (string/replace #"<[^>]+>$" "")))

(defn- node->plain [node]
  (->> node
       :content
       (filter string?)
       (apply str)
       (.trim)))

(defn- children-by-tagname [node tag]
  (filter #(or (= tag (node->tagname %)) (= tag "*")) (:content node)))

(defn- descendant-seq [nodes where-fn]
  (flatten (map #(filter where-fn (xml-seq %)) nodes)))

(defn process-xml
  "Parse an XML document and apply supplied function all instances of the named element."
  [^java.io.Reader rdr tag-name process-fn]
  (let [element (xml/parse rdr)]
    {:tag :xml
     :content [(->> (xml-seq element)
                    (filter #(= tag-name (node->tagname %)))
                    first
                    process-fn)]}))


(defn get-elements
  "Retrieve the elements with the given tag name."
  [^java.io.Reader rdr tag-name]
  (let [result (new java.util.ArrayList)]
    (process-xml rdr tag-name (fn [r] (.add result r)))
    result))

(defn- xselect-result [out-val]
  (if (and (map? out-val) (contains? out-val :nodes))
    (or (:nodes out-val) [])
    out-val))

(defn- get-attr [node attr-name]
  (let [parts (string/split attr-name #":")
        attrs (->> node
                   :attrs
                   (map (fn [[k v]] [(name k) v]))
                   (into {}))]
    (cond
      (= 1 (count parts))
      (attrs attr-name)
      (= 2 (count parts))
      (attrs (second parts))
      :else
      nil)))

(defn- xselect* [context selector]
  (let [nodes (:nodes context)
        descending? (:descending? context)]
    (cond
      (vector? selector)
      (let [f (first selector)]
        (cond
          (= :has f)
          {:nodes  (filter #(not= (get-attr % (second selector)) nil) nodes)
           :descending? false}
          (= := f)
          {:nodes (filter
                   #(= (-> % (get-attr (second selector))) (nth selector 2)) nodes)
           :descending? false}
          (= :has-not f)
          {:nodes (filter #(= (get-attr % (second selector)) nil) nodes)
           :descending? false}
          :else
          (map #(-> % (get-attr f)) nodes)))

      (= :> selector)
      (assoc context :descending? true)

      (= :text selector)
      (map node->str nodes)

      (= :plain selector)
      (map node->plain nodes)

      (= :xml selector)
      (map node->xml nodes)

      (and descending? (= java.lang.String (type selector)))
      {:nodes (descendant-seq nodes #(= selector (node->tagname %)))
       :descending? false}

      (string? selector)
      {:nodes (flatten (map #(children-by-tagname % selector) nodes))
       :descending? false})))

(defn xselect [node & path]
  (if-not node
    []
    (let [nodes (if (and (not (map? node)) (coll? node)) node (cons node nil))
          ctx {:nodes nodes :descending? false}
          result (reduce xselect* ctx path)]
      (xselect-result result))))

(defn xselect1
  "Select a single element or attribute trimming string values.
   To select an attribute provide its name as a string vector"
  [& args]
  (let [res (first (apply xselect args))]
    (if (string? res)
      (string/trim res)
      res)))
