(ns cayenne.dates
  (:require [clj-time.format :as tf]
            [clj-time.coerce :as tc]
            [clj-time.core :as t]
            [cayenne.xml :as xml]
            [cayenne.util :refer [parse-int-safe]]
            [clojure.string :as str]))

(defn- ymd->date-string
  "Use all valid date parts and round down any missing or invalid parts.
   Returns an ISO-8601 formatted date-time string."
  [y m d]
  (let [year (when-let [yy (parse-int-safe y)] 
               (when (> yy 0) yy))
        month (when-let [mm (parse-int-safe m)] 
                (when (<= mm 12) mm))
        day (parse-int-safe d)
        date (cond
               (and year month day)
               (if (< (t/number-of-days-in-the-month year month) day)
                 (t/date-time year month)
                 (t/date-time year month day))
               (and year month)
               (t/date-time year month)
               year
               (t/date-time year))]
    (tc/to-string date)))

(defn ymd-keep
  [year month day]
  (cond (and year month day)
        [year month day]
        (and year month)
        [year month]
        :else
        [year]))

(defn- particle
  "Create a new date-time particle.
   A particle is a map holding date-time information."
  [date-parts date-time source]
  {:date-parts (apply ymd-keep date-parts)
   :date-time date-time
   :timestamp (when date-time (tc/to-long (tf/parse date-time)))
   :source source})

(defn parse-month [month]
  (when-let [month-val (or (when (integer? month) month)
                           (try (Integer/parseInt month) (catch Exception _ nil)))]
    (if (and (>= month-val 1) (<= month-val 34))
      month-val
      nil)))

(defn parse-date
  "Parse 'print' or 'online' publication dates. Can return nil"

  ([date-loc]
   (let [day-val (xml/xselect1 date-loc "day" :text)
         month-val (xml/xselect1 date-loc "month" :text)
         year-val (xml/xselect1 date-loc "year" :text)]
     (parse-date year-val month-val day-val)))

  ([year-val month-val day-val]
   (when (and year-val (> (parse-int-safe year-val) 0))
     (let [date-parts [(parse-int-safe year-val)
                       (parse-month month-val)
                       (parse-int-safe day-val)]
           date-time (apply ymd->date-string date-parts)]
       (particle date-parts date-time "date-parts")))))

(defn parse-date-string
  "Parse 'print' or 'online' publication dates. Can return nil"
  [s]
  (when-not (str/blank? s)
    (let [d (tf/parse (tf/formatters :date-time-no-ms) s)
          date-parts [(t/year d) (t/month d) (t/day d)]
          date-time (tc/to-string d)]
      (particle date-parts date-time "date-time"))))

(defn parse-start-date [start-date-loc]
  (let [month-val (xml/xselect1 start-date-loc ["start_month"])
        day (xml/xselect1 start-date-loc ["start_day"])
        month (parse-month month-val)
        year (xml/xselect1 start-date-loc ["start_year"])]
    (parse-date year month day)))

(defn parse-end-date [end-date-loc]
  (let [month-val (xml/xselect1 end-date-loc ["end_month"])
        day (xml/xselect1 end-date-loc ["end_day"])
        month (parse-month month-val)
        year (xml/xselect1 end-date-loc ["end_year"])]
    (parse-date year month day)))

(defn particle->date-time [particle]
  (let [year (:year particle)
        month (when-let [m (:month particle)] (when (and m (<= m 12)) m))
        day (:day particle)
        hour (:hour particle)
        minute (:minute particle)
        sec (:second particle)]
    (cond (and year month day hour minute sec)
          (t/date-time year month day hour minute sec)
          (and year month day)
          (if (< (t/number-of-days-in-the-month year month) day)
            (t/date-time year month)
            (t/date-time year month day))
          (and year month)
          (t/date-time year month)
          year
          (t/date-time year)))
  (:date-time particle))

(defn particle->date-str [particle]
  (let [[year m day] (:date-parts particle)
        month (when (and m (<= m 12)) m)]
    (cond
      (and year month day)
      (if (< (t/number-of-days-in-the-month year month) day)
        (format "%d-%02d" year month)
        (format "%d-%02d-%02d" year month day))
      (and year month)
      (format "%d-%02d" year month)
      year
      (str year))))

(defn particle->year [particle] (-> particle :date-parts first))