(ns cayenne.tasks.retraction-watch
  (:require [clojure.data.json :as json]
            [clojure.data.csv :as csv]
            [clojure.string :as s]
            [clojure.java.io :as io]
            [clojure.java.shell :refer [sh]]
            [cayenne.state :as state]
            [cayenne.conf :as conf]
            [clj-time.format :as tf]
            [cayenne.elastic.index :as index]
            [cayenne.ids.doi :refer [is-long-doi?]]
            [taoensso.timbre :as timbre :refer [info error]]))

(def rw-db-commit-url "https://gitlab.com/crossref/retraction-watch-data/-/raw/%s/retraction_watch.csv")

(defn- reformat-date [s]
  (try
    (->> s
         (re-find #"^[0-9]+/[0-9]+/[0-9]+ [0-9]+:[0-9]+")
         (tf/parse (tf/formatter "MM/dd/YYYY HH:mm"))
         (tf/unparse (tf/formatters :date-time)))
    (catch Exception _)))

(defn- process-row
  ([r]
   (process-row r false))
  ([r delete]
   (if (< (count r) 20)
     (error "Error processing retraction watch row, ID:" (first r))
     (let [[record-id _ _ _ _ _ _ _ _ _ retraction-date retraction-doi
            _ _ original-paper-doi _ retraction-nature _ _ _ & more] r
           rel-info {:record-id record-id
                     :retraction-date (reformat-date retraction-date)
                     :retraction-nature retraction-nature
                     :delete delete}]
       (when  (and (seq record-id) (is-long-doi? retraction-doi) (is-long-doi? original-paper-doi))
         [{:index {:_index "relupdates" :_type "relupdates"}}
          {:doi (s/lower-case retraction-doi)
           :relation-type :rw-retracts
           :relation-info (assoc rel-info :retracts original-paper-doi)}
          {:index {:_index "relupdates" :_type "relupdates"}}
          {:doi (s/lower-case original-paper-doi)
           :relation-type :rw-retracted
           :relation-info (assoc rel-info :retracted-by retraction-doi)}])))))

(defn- get-last-commit []
  (let [url (str (conf/get-param [:update :schedule :retraction-watch-url]) "commits")]
    (-> url
        slurp
        json/read-str
        first
        (get "id"))))

(defn- download-commit [commit]
  (when-not (.exists (io/file commit))
    (with-open [reader (io/reader (format rw-db-commit-url commit))
                writer (io/writer commit)]
      (doseq [line (line-seq reader)]
        (.write writer (str line "\n"))))))

(defn- read-commit
  ([commit]
   (when-not (.exists (io/file commit))
     (download-commit commit))
   (let [reader (io/reader commit)]
     (read-commit reader (csv/read-csv reader))))
  ([reader data]
   (let [n (first data) r (rest data)]
     (if n
       (lazy-seq (cons n (read-commit reader r)))
       (.close reader)))))

(defn- deleted-record->map-id-record [record]
  (when-let [id  (some->> record
                          (re-find #"^<([\s0-9]+),")
                          second
                          s/trim)]
    {id record}))

(defn- updated-record->map-id-record [record]
  (when-let [id  (some->> record
                          (re-find #"^>([\s0-9]+),")
                          second
                          s/trim)]
    {id record}))

(defn- compare-commits [c1 c2]
  (download-commit c1)
  (download-commit c2)
  (let [diff (-> (sh "diff" c1 c2)
                 :out
                 (s/split #"\n"))
        updated-records (reduce merge (keep updated-record->map-id-record diff))
        deleted-records (reduce dissoc
                                (reduce merge (keep deleted-record->map-id-record diff))
                                (keys updated-records))

        to-csv-rows (fn [v]
                      (->> v
                           (map (fn [[id r]] (s/replace r #"^[^,]+" id)))
                           (s/join "\n")
                           (java.io.StringReader.)
                           io/reader
                           csv/read-csv))]
    {:updated (to-csv-rows updated-records) :deleted (to-csv-rows deleted-records)}))

(defn process-retraction-watch-git-repo []
  (let [last-processed-commit (state/get-cayenne-state [:retraction-watch :commit])
        last-commit (get-last-commit)
        {:keys [updated deleted]} (if last-processed-commit
                                    (compare-commits last-processed-commit last-commit)
                                    {:updated (rest (read-commit last-commit)) :deleted []})
        data (->> (concat
                   (for [r updated]
                     (process-row r))
                   (for [r deleted]
                     (process-row r true)))
                  flatten
                  (filter map?))]
    (if (seq data)
      (do
        (info (format "RW - processing new commit [%s] #entries: %d" last-commit (-> data count (/ 2))))
        (index/bulk-index-items :relupdates data :error-msg "Failed pushing RW updates into relupdates index")
        (when last-processed-commit
          (-> last-processed-commit io/file .delete))
        (state/set-cayenne-state [:retraction-watch :commit] last-commit))
      (info "RW - No new commits found on Git"))))
