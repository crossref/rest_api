(ns cayenne.tasks.journal
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [cayenne.conf :as conf]
            [cayenne.util :as util]
            [cayenne.sentry :refer [with-sentry-reporting]]
            [cayenne.ids.doi :as doi-id]
            [qbits.spandex :as elastic]
            [taoensso.timbre :refer [info error]]
            [cayenne.ids.issn :as issn]
            [cayenne.elastic.util :as elastic-util]))

(defn additional-issns 
  "Splits and trims the additionalIssns value.
   The value can contain multiple ISSNs separated by hyphens ';'
   and may also contains some unnecessary whitespace.
   Returns a list of string values or nil."
  [csv-row]
  (when-let [issns (:additionalIssns csv-row)]
    (->> (string/split issns #";")
         (map string/trim)
         (filter not-empty))))

(defn issns [csv-row]
  (let [p-issn (:pissn csv-row)
        e-issn (:eissn csv-row)
        additional (additional-issns csv-row)]
    (cond-> []
      (not (string/blank? p-issn))
      (conj {:value (issn/normalize-issn p-issn) :type "print"})
      (not (string/blank? e-issn))
      (conj {:value (issn/normalize-issn e-issn) :type "electronic"})
      (not-empty additional)
      (concat (map #(hash-map :value (issn/normalize-issn %) :type nil) additional)))))

; Journals are updated rather than indexed, so that "subject" field,
; which is updated by a separate process, stays intact.
(defn update-command
  "Convert a parsed CSV row and turn into an Elastic Search bulk update command."
  [csv-row]
  (let [title (:JournalTitle csv-row)
        journal-id (:JournalID csv-row)]
    (try
      (with-sentry-reporting
        {"journal-csv-row" (str csv-row)}
        [{:update {:_id (Long/parseLong journal-id)}}
         {:doc {:title     title
                :token     (util/tokenize-name title)
                :id        (Long/parseLong journal-id)
                :doi       (-> csv-row :doi doi-id/normalize-doi)
                :publisher (:Publisher csv-row)
                :issn      (issns csv-row)}
          :doc_as_upsert true}])
      (catch Exception e
        (error "Exception while processing journal CSV row:" csv-row (.getMessage e))
        (throw e)))))

(defn fetch-titles
  "Given an open reader, return a sequence of title entries as hashmaps.
   Remember to consume the lazy seq before closing the reader!"
  [rdr]
  (let [rows (csv/read-csv rdr)
        ; Header rows as keywords.
        header (->> rows first (map keyword))
        entries (rest rows)]
    (map #(apply merge (map hash-map header %)) entries)))

(def chunk-size
  "Items in an index command. Note that this must be divisible by 2,
   as it's concatenated pairs of index-command, body."
  100)

(defn index-journals
  ([]
   (with-sentry-reporting
     {}
     (with-open [rdr (io/reader (conf/get-param [:location :cr-titles-csv]))]
       (index-journals rdr))))
  ([rdr]
   (try
     (with-sentry-reporting
       {}
       (let [titles (fetch-titles rdr)
             update-items (mapcat update-command titles)
             cnt (atom 0)]
         (doseq [chunk (partition-all chunk-size update-items)]
           (elastic-util/with-retry
             {:sleep 10000 :tries 5 :decay :double}
             "Elasticsearch journal update failed"
             (elastic/request
               (conf/get-service :elastic)
               {:method :post
                :url (str (elastic-util/index-url-prefix :journal) "_bulk")
                :body (elastic-util/raw-jsons chunk)}))
           (swap! cnt + (/ chunk-size 2))
           (info "Updated" @cnt "journals"))))
     (catch Exception e
       (error "Exception while updating journals:" (.getMessage e))
       (throw e)))))
