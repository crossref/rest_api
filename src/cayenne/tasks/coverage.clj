(ns cayenne.tasks.coverage
  (:require [cayenne.ids.type :as type-id]
            [cayenne.conf :as conf]
            [cayenne.data.work :as works]
            [cayenne.api.v1.filter :refer [std-filters]]
            [cayenne.util :as util]
            [cayenne.sentry :refer [with-sentry-reporting]]
            [clj-time.core :as dt]
            [clj-time.format :as df]
            [taoensso.timbre :as timbre :refer [error info]]
            [qbits.spandex :as elastic]
            [cayenne.elastic.util :as elastic-util]))

(def year-date-format (df/formatter "yyyy"))

(defn current-start-year []
  (df/unparse year-date-format (dt/minus (dt/now) (dt/years 2))))

(defn make-id-filter [type id]
  (cond (= type :member)
        {:member [(str id)]}
        (= type :journal)
        {:issn (map :value id)}))

(defn make-id-filter-es [type id]
  (cond (= type :member)
        {:member-id [(str id)]}
        (= type :journal)
        {:issn.value (map :value id)}))

(defn coverage [total-count check-count]
  (if (zero? total-count)
    0.0
    (double (/ check-count total-count))))

(defn check-breakdowns [record & {:keys [type id-field]}]
  (let [record-id (get record id-field)
        works (works/fetch {:rows (int 0)
                            :facets [{:field "published" :count "*"}]
                            :filters (make-id-filter type record-id)})]
    {:breakdowns
     {:dois-by-issued-year
      (reduce
        (fn [a [k v]] (conj a [(util/parse-int k) v]))
        []
        (-> works
            (get-in [:message :facets "published" :values])))}}))

(defn content-types [type record-id]
  (if (= type :member)
    (-> {:rows (int 0)
         :facets [{:field "type-name" :count "*"}]
         :filters (make-id-filter type record-id)}
        works/fetch
        (get-in [:message :facets "type-name" :values])
        keys
        ((partial map type-id/->type-id))
        (or [])
        ((partial filter identity))
        ((partial map keyword))
        (conj :all :journal-article)
        set)
    [:all :journal-article]))

(def checkles
  [[:affiliations        "has-affiliation"   ["true"]]
   [:abstracts           "has-abstract"      ["true"]]
   [:update-policies     "has-update-policy" ["true"]]
   [:references          "has-references"    ["true"]]
   [:licenses            "has-license"       ["true"]]
   [:resource-links      "full-text"         {:application ["text-mining" "unspecified"]}]
   [:orcids              "has-orcid"         ["true"]]
   [:award-numbers       "has-award"         ["true"]]
   [:funders             "has-funder"        ["true"]]
   [:similarity-checking "full-text"         {:application ["similarity-checking"]}]
   [:ror-ids             "has-ror-id"        ["true"]]
   [:descriptions        "has-description"   ["true"]]])

(defn all-coverage [aggregation total]
  (->> aggregation
       :checkles
       :buckets
       (map #(vector (first %) (->> %
                                    second
                                    :doc_count
                                    (coverage total))))
       (into {:_count total})
       (hash-map :all)))

(defn type-coverage [aggregation type]
  (if-let [type-aggregation (->> aggregation
                                 :type
                                 :buckets
                                 (filter #(= (name type) (:key %)))
                                 first)]
    (->> type-aggregation
         :checkles
         :buckets
         (map #(vector (first %) (->> %
                                      second
                                      :doc_count
                                      (coverage (:doc_count type-aggregation)))))
         (into {:_count (:doc_count type-aggregation)})
         (hash-map type))
    (->> checkles
         (map #(vector (first %) 0.0))
         (into {:_count 0})
         (hash-map type))))

(defn es-aggregation-request [id-filter checkles-filters from-date]
  (let [aggregations {:published
                      {:date_range {:field "issued"
                                    :ranges [{:key "backfile" :to from-date}
                                             {:key "current" :from from-date}]}
                       :aggs {:checkles {:filters {:filters checkles-filters}}
                              :type {:terms {:field "type" :size 50}
                                     :aggs {:checkles {:filters {:filters checkles-filters}}}}}}}]
    {:query {:bool {:filter [{:terms id-filter}]}}
     :size 0
     :aggs aggregations}))

(defn es-aggregation-request-all [id-filter checkles-filters]
  (let [aggregations {:checkles {:filters {:filters checkles-filters}}
                      :type {:terms {:field "type" :size 50}
                             :aggs {:checkles {:filters {:filters checkles-filters}}}}}]
    {:query {:bool {:filter [{:terms id-filter}]}}
     :size 0
     :aggs aggregations}))

(defn es-aggregation-coverage [aggregation record-types]
  (->> aggregation
       :body
       :aggregations
       :published
       :buckets
       (map #(let [all-cov (all-coverage % (:doc_count %))
                   type-cov (map (partial type-coverage %) record-types)]
               [(keyword (:key %)) (apply merge all-cov type-cov)]))
       (into {})))

(defn es-aggregation-coverage-all [aggregation record-types]
  (let [total (-> aggregation :body :hits :total)
        aggregation (-> aggregation :body :aggregations)
        all-cov (all-coverage aggregation total)
        type-cov (map (partial type-coverage aggregation) record-types)]
    {:all (apply merge all-cov type-cov)}))

(defn check-type-coverage [record & {:keys [type id-field]}]
  (let [record-id (get record id-field)
        record-types (remove #{:all} (content-types type record-id))
        id-filter (make-id-filter-es type record-id)
        from-date (str (current-start-year) "-01-01")
        checkles-filters (reduce
                           (fn [m [c-name f-name f-value]]
                             (->> f-value
                                  ((std-filters f-name))
                                  (#(if (sequential? %) (first %) %))
                                  :clause
                                  (assoc m c-name)))
                           {}
                           checkles)
        query (es-aggregation-request id-filter checkles-filters from-date)
        response (elastic-util/with-retry
                   {:sleep 10000 :tries 5 :decay :double}
                   (format "Searching for coverage for type %s and id %s failed" type record-id)
                   (elastic/request
                     (conf/get-service :elastic)
                     {:method :get
                      :url (str (elastic-util/index-url-prefix :work) "_search")
                      :body query}))
        query-all (es-aggregation-request-all id-filter checkles-filters)
        response-all (elastic-util/with-retry
                       {:sleep 10000 :tries 5 :decay :double}
                       (format "Searching for coverage for type %s and id %s failed" type record-id)
                       (elastic/request
                         (conf/get-service :elastic)
                         {:method :get
                          :url (str (elastic-util/index-url-prefix :work) "_search")
                          :body query-all}))]
    (merge (es-aggregation-coverage-all response-all record-types)
           (es-aggregation-coverage response record-types))))

(defn check-record-counts [type-coverage]
  (let [backfile-count (-> type-coverage :backfile :all :_count)
        current-count (-> type-coverage :current :all :_count)]
    {:backfile-dois backfile-count
     :current-dois current-count
     :total-dois (+ backfile-count current-count)}))

(defn index-coverage-command [record & {:keys [type index-id cov-id]}]
  (try
    (with-sentry-reporting
      {"coverage-type" type "coverage-id" (get (:_source record) index-id)
       "function" "cayenne.tasks.coverage.index-coverage-command"}
      (let [started-date (dt/now)
            record-source (:_source record)
            id (get record-source index-id)
            coverage (check-type-coverage record-source :type type :id-field cov-id)
            record-counts (check-record-counts coverage)
            breakdowns (check-breakdowns record-source :type type :id-field cov-id)]
        [{:index {:_id (str (name type) "-" id)}}
         {:subject-type  (name type)
          :subject-id    id
          :started       started-date
          :finished      (dt/now)
          :total-dois    (:total-dois record-counts)
          :backfile-dois (:backfile-dois record-counts)
          :current-dois  (:current-dois record-counts)
          :breakdowns    breakdowns
          :coverage      coverage}]))
    (catch Exception e
      (error "Exception while processing coverage:" record (.getMessage e))
      (throw e))))

(defn all-items
  ([index-name index-id cov-id]
   (all-items index-name index-id cov-id nil))
  ([index-name index-id cov-id search-after]
   (let [request (cond-> {:method :get
                          :url (str (elastic-util/index-url-prefix index-name) "_search")
                          :body {:_source [index-id cov-id]
                                 :query {:match_all {}}
                                 :size 500
                                 :sort [:id]}}
                         search-after
                         (assoc-in [:body :search_after] search-after))
         response (elastic-util/with-retry
                   {:sleep 10000 :tries 5 :decay :double}
                   (str "Iterating over " index-name " failed")
                   (elastic/request (conf/get-service :elastic) request))
         items (get-in response [:body :hits :hits])
         next-search-after (:sort (last items))]
     (if (< (count items) 500)
       items
       (lazy-cat items (all-items index-name index-id cov-id next-search-after))))))

(defn check-index
  "Generage coverage and save to Elastic Search."
  [index-name & {:keys [index-id cov-id]}]
  (try
    (with-sentry-reporting
      {}
      (let [cnt (atom 0)
            all (all-items index-name index-id cov-id)
            parts (partition-all 100 all)]
        (doseq [some-records parts]
          (try
            (let [body (->> some-records
                            (pmap #(index-coverage-command
                                     %
                                     :type index-name
                                     :index-id index-id
                                     :cov-id cov-id
                                     ))
                            flatten
                            elastic-util/raw-jsons)]
              (with-sentry-reporting
                {}
                (elastic-util/with-retry
                  {:sleep 10000 :tries 5 :decay :double}
                  "Elasticsearch coverage update failed"
                  (elastic/request
                    (conf/get-service :elastic)
                    {:method :post
                     :url (str (elastic-util/index-url-prefix :coverage) "_bulk")
                     :body body}))))
            (catch Exception e
              (error "Exception while calculating coverage:" (.getMessage e))))
          (swap! cnt #(+ % (count some-records)))
          (info "Done" @cnt index-name "coverage checks..."))))
    (catch Exception e
      (error "Exception while calculating coverage:" (.getMessage e))
      (throw e))))

(defn check-members [] (check-index :member :index-id :id :cov-id :id))

(defn check-journals [] (check-index :journal :index-id :id :cov-id :issn))
