(ns cayenne.tasks.calculated-data
  (:require [cayenne.conf :as conf]
            [cayenne.sentry :refer [with-sentry-reporting]]
            [cayenne.elastic.util :as elastic-util]
            [cayenne.elastic.convert :refer [citeproc-date]]
            [cayenne.ids.update-type :refer [label->type]]
            [clojure.string :as string]
            [clojure.core.async :as async]
            [taoensso.timbre :refer [info error warn]]
            [cayenne.elastic.index :as es-index]
            [clojure.set :as cset]
            [qbits.spandex :as elastic]))

(defn- dois->calculated-data [ids]
  (->> (elastic/request (conf/get-service :elastic)
                       {:url (str "/work/_mget")
                        :body {:docs (map #(hash-map :_id (string/lower-case %) :_source [:calculated]) ids)} })
       :body
       :docs
       (into {} (map #(vector (:_id %) ((comp :calculated :_source) %))))))

(defn- update-obj-in-vector
  "Allows to update a specific object within a map of objects, in order to identify the object
  the the values associated to the keys specified in the arguments must match."
  [v obj keys]
  (let [key-values ((apply juxt keys) obj)
             pred #(not= key-values ((apply juxt keys) %))
        vec-without-obj (filter pred v)]
    (conj vec-without-obj obj)))

;; Functions to add calculated data to dois
(defmulti update-doi-calculated-data
  "given a work it will return a new calculated data based on its old-calculated data + relations info"
  (fn [[[doi type] relations] old-calculated-data]
    type))

;; updates are supposed to be a list of DOIs that update a given DOI, it is
;; basically a relation, a doi can point back to a set of dois that represent an
;; update to itself
(defmethod  update-doi-calculated-data "has-update"
  [[[doi _] relations] {:keys [updated-by] :as old-calculated-data}]

  (let [new-calculated-data (reduce #(update-obj-in-vector %1 %2 [:doi :type])
                                    (or updated-by [])
                                    relations)]
    (assoc old-calculated-data :updated-by new-calculated-data)))

(defmethod update-doi-calculated-data "rw-retracts"
  [[[doi _] relations] {:keys [rw-update-to] :as old-calculated-data}]
  (let [rw-updates (->> relations
                        (filter (complement :delete))
                        (map #(hash-map :source "retraction-watch"
                                        :DOI (:retracts %)
                                        :label (:retraction-nature %)
                                        :type (label->type (:retraction-nature %))
                                        :record-id (:record-id %)
                                        :updated (-> % :retraction-date citeproc-date)))
                        (filter :type))
        rw-deletes (into #{} (map :record-id (filter :delete relations)))

        new-calculated-data (reduce #(update-obj-in-vector %1 %2 [:record-id])
                                    (or rw-update-to [])
                                    rw-updates)

        new-calculated-data (->> new-calculated-data
                                 (filter (comp (complement rw-deletes) :record-id)))]
    (assoc old-calculated-data :rw-update-to new-calculated-data)))

(defmethod update-doi-calculated-data "rw-retracted"
  [[[doi _] relations] {:keys [rw-updated-by] :as old-calculated-data}]
  (let [rw-updates (->> relations
                        (filter (complement :delete))
                        (map #(hash-map :source "retraction-watch"
                                        :DOI (:retracted-by %)
                                        :label (:retraction-nature %)
                                        :type (label->type (:retraction-nature %))
                                        :record-id (:record-id %)
                                        :updated (-> % :retraction-date citeproc-date)))
                        (filter :type))
        rw-deletes (into #{} (map :record-id (filter :delete relations)))

        new-calculated-data (reduce #(update-obj-in-vector %1 %2 [:record-id])
                                    (or rw-updated-by [])
                                    rw-updates)

        new-calculated-data (->> new-calculated-data
                                 (filter (comp (complement rw-deletes) :record-id)))]
    (assoc old-calculated-data :rw-updated-by new-calculated-data)))

(defmethod  update-doi-calculated-data "alias"
  [[[doi _] relations] {{:keys [dois counts]} :alias-dois-refcount :as old-calculated-data}]
  (let [data (apply merge (map #(hash-map (:doi %) (:ref-count %)) relations))
             old-counts (into {} (map vector dois counts))
             new-alias-cnt (seq (merge old-counts data))]
         (assoc old-calculated-data :alias-dois-refcount {:dois (map first new-alias-cnt)
                                                          :counts (map second new-alias-cnt)})))


(defn update-calculated-data-map
  "We process updates in batches (in this case 500). There can be the case that a DOI
  calculated data gets updated twice. This function transforms the data provided by
  dois->calculated-data (m) which will be used by the multimethod update-doi-calculated-data
  repeatedly"
  [m obj]
  (let [[[doi type] relations] obj
        rels-data (map (comp :relation-info :_source) relations)
        new-doi-calc-data (update-doi-calculated-data [[doi type] rels-data] (get m doi))]
    (assoc m doi new-doi-calc-data)))

(defn es-results->failed-dois
  [results]
  (as-> results $
   (:body $)
   (:items $)
   (group-by #(-> % :update :_id) $)
   (update-vals $ #(->> % (map (comp :type :error :update)) (remove nil?) seq))
   (filter #(-> % second nil? not) $)
   ;; updates to unexisting dois will be deleted otherwise they accumulate
   (filter #(not= (second %) (list "document_missing_exception")) $)
   (into {} $)))

(defn- group-updates
  "This function will accept ES documents and will group them by [doi rel_type] -> [doc1 ... docN]
  so that we process multiple documents for the same DOI type in only one transaction."
  [es-result]
  (->> es-result
       :body :hits :hits
       (group-by #(vector (-> % :_source :doi) (-> % :_source :relation-type)))))

(defn- update-group->es-commands [grp]
  (let [doi-to-calculated (dois->calculated-data (map #(first (first %)) grp))]
    (->> (reduce update-calculated-data-map doi-to-calculated grp)
         (mapcat (fn [[doi calc-data]]
                   (vector
                    {:update {:_id doi}}
                    {:doc {:calculated calc-data}}))))))

(defn process-calculated-data
  "This function will be called periodically using cron. It will iterate over
  all documents in relupdates index and perform updates on documents in the work
  index"
  []
  (let [grouped-updates (-> (elastic/request (conf/get-service :elastic)
                                              {:url "/relupdates/_search"
                                               :body {:query {:match_all {}}
                                                      :size (conf/get-param [:update :calculated-data :batch-size])}})
                            group-updates)]
    (if (seq grouped-updates)
      (do
        (info "DOIs with updates found: " (count grouped-updates))
        (doseq [part (partition 500 500 [] grouped-updates)]
          (let [doi-to-id (as-> part $
                            (map second $)
                            (flatten $)
                            (group-by (comp :doi :_source) $)
                            (update-vals $ #(map :_id %)))
                grouped-commands (update-group->es-commands part)
                results (try
                          (es-index/bulk-index-items :work grouped-commands :error-msg "Error adding calculated data")
                          (catch clojure.lang.ExceptionInfo e
                            (ex-data e)))
                failed-dois (es-results->failed-dois results)
                success-dois (-> doi-to-id
                                 keys
                                 set
                                 (cset/difference (set (keys failed-dois))))]
            (doseq [doi failed-dois]
              (warn "Failed processing calculated data: " doi))
            (try
              (->> success-dois
                   (map #(get doi-to-id %))
                   flatten
                   (es-index/bulk-delete-docs :relupdates))
              (catch Exception e
                (prn (ex-data e)))))))
      (info "No calculated data updates found"))))
