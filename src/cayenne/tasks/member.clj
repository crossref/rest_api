(ns cayenne.tasks.member
  (:require [cayenne.conf :as conf]
            [cayenne.util :as util]
            [cayenne.sentry :refer [with-sentry-reporting]]
            [cayenne.elastic.util :as elastic-util]
            [clojure.data.xml :as xml]
            [clojure.zip :as zip]
            [clojure.string :as string]
            [clojure.data.zip.xml :as zx]
            [clojure.data.json :as json]
            [clojure.java.io :as io]
            [clj-http.client :as http]
            [taoensso.timbre :refer [info error]]
            [qbits.spandex :as elastic]))

(defn get-member-list
  "Get a list of members from the Crossref prefix information API."
  []
  (let [url (str (conf/get-param [:upstream :prefix-info-url]) "all")
        response (http/get url
                           {:connection-manager (conf/get-service :conn-mgr)
                            :throw-exceptions false
                            :as :string})]
    (when (= 200 (:status response))
      (-> response :body (json/read-str :key-fn keyword)))))

(defn get-prefix-info
  "Return information about an owner prefix from the Crossref prefix information
   API."
  [member-id prefix]
  (let [url (str
             (conf/get-param [:upstream :prefix-info-url])
             prefix)
        response (http/get url {:connection-manager (conf/get-service :conn-mgr)
                                :throw-exceptions false
                                :as :byte-array})
        root (when (= 200 (:status response))
               (-> (:body response) io/reader xml/parse zip/xml-zip))]
    (when root
      {:value prefix
       :member-id member-id
       :name (zx/text (zx/xml1-> root :publisher :prefix_name))
       :location (zx/text (zx/xml1-> root :publisher :publisher_location))})))

(defn index-command
  "Turn a member record from the Crossref prefix information API into
   a command, document pair that will index the member in ES."
  [member]
  (try
    (with-sentry-reporting
      {"member" (str member)}
      (let [member-id (:memberId member)
            prefixes (filter (complement string/blank?) (:prefixes member))
            prefixes (map (partial get-prefix-info member-id) prefixes)
            publisher-location (first (filter (complement nil?)
                                        (map :location prefixes)))]
        [{:index {:_id member-id}}
         {:id member-id
          :primary-name (:name member)
          :location publisher-location
          :token (util/tokenize-name (:name member))
          :prefix prefixes}]))
    (catch Exception e
      (error "Exception while ingesting member:" member (.getMessage e))
      (throw e))))

(defn index-members
  "Index members into ES."
  []
  (try
    (with-sentry-reporting
      {}
      (let [cnt (atom 0)]
        (doseq [some-members (partition-all 100 (get-member-list))]
          (let [bulk-body (->> some-members
                               (map index-command)
                               flatten)]
            (elastic-util/with-retry
              {:sleep 10000 :tries 5 :decay :double}
              "Elasticsearch member update failed"
              (elastic/request
                (conf/get-service :elastic)
                {:method :post
                 :url (str (elastic-util/index-url-prefix :member) "_bulk")
                 :body (elastic-util/raw-jsons bulk-body)})))
          (swap! cnt + 100)
          (info "Updated" @cnt "members"))))
    (catch Exception e
      (error "Exception while updating members:" (.getMessage e))
      (throw e))))
