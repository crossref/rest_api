(ns cayenne.util
		"Utility functions. These are all pure, and don't require any external external dependencies, 
		making them suitable for includin in extlib."
  (:require [clojure.string :as string]
            [clojure.walk :as wk]
            [ring.util.codec :as codec])
  (:import  [java.security MessageDigest]
            [java.net URI]))

(defn parse-uri [uri]
  (let [parsed (URI. uri)]
    {:scheme (.getScheme parsed)
     :host (.getHost parsed)
     :path (.getPath parsed)
     :query (when-let [p (.getRawQuery parsed)] (codec/form-decode p))
     :fragment (.getFragment parsed)}))

(defn unparse-uri [{:keys [scheme host path query fragment]}]
  (.toString (URI. scheme host path (when query (->> query (map (fn [[k v]] (str (name k) "=" v))) (string/join "&"))) fragment)))

(defn merge-uri-query [uri query-map]
  (let [parts (parse-uri uri)
        merged (update parts :query #(seq (merge % query-map)))]
    (unparse-uri merged)))

(defn assoc-when
  "Like assoc but only assocs when value is truthy"
  [m & kvs]
  (assert (even? (count kvs)))
  (into m
        (for [[k v] (partition 2 kvs)
              :when v]
          [k v])))

(defn assoc-str
  "Like assoc but only assocs when value is a non-blank string. Value is assoced
   as a trimmed string."
  [m & kvs]
  (assert (even? (count kvs)))
  (into m
        (for [[k v] (partition 2 kvs)
              :when (not (string/blank? v))]
          [k (string/trim v)])))

(defn assoc-exists
  "Like assoc except only performs the assoc if value is
   a non-empty string, non-empty list or a non-nil value."
  ([m key value]
   (assoc-exists m key value value))
  ([m key value assoc-value]
   (cond (= (type value) java.lang.String)
         (if (clojure.string/blank? value)
           m
           (assoc m key assoc-value))
         (sequential? value)
         (if (empty? value)
           m
           (assoc m key assoc-value))
         (nil? value)
         m
         :else
         (assoc m key assoc-value))))

(declare parse-int-safe)

(defn assoc-int
  "Like assoc but only assocs when value is not nil. Value is assoced
   as a parsed int."
  [m & kvs]
  (assert (even? (count kvs)))
  (into m
        (for [[k v] (partition 2 kvs)
              :when (parse-int-safe v)]
          [k (parse-int-safe v)])))

(defn keys-in
  "Return all keys in nested maps."
  [m]
  (when (map? m)
    (concat
     (keys m)
     (mapcat (comp keys-in (partial get m)) (keys m)))))

(defn map-diff
  "Produce the list of keys in a but not in b."
  [a b]
  (filter #(not (get b %)) (keys a)))

(defn map-intersect
  "Produce a list of keys present in a and b."
  [a b]
  (filter #(get a %) (keys b)))

(defn without-nil-vals
  "Dissoc any key val pairs where the val is nil."
  [record]
  (reduce (fn [m [k v]] (if v m (dissoc m k))) record record))

(defn without-keyword-vals
  "Convert all map values that are keywords into Java strings."
  [record]
  (reduce (fn [m [k v]] (if (keyword? v) (assoc m k (name v)) m)) record record))

(defn with-java-array-vals
  "Convert all clojure vectors and seqs in a map to Java arrays."
  [record]
  (reduce
   (fn [m [k v]]
     (if (or (vector? v) (seq? v)) (assoc m k (into-array v)) m)) record record))

(defn patherize [coll]
  (reduce #(conj %1 (conj (vec (last %1)) %2)) [] coll))

(defn update-keys [m f]
  (zipmap (map f (keys m)) (vals m)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Control flow stuff taken from Prismatic's plumbing lib

(defmacro ?>>
  "Conditional double-arrow operation (->> nums (?>> inc-all? map inc))"
  [do-it? f & args]
  `(if ~do-it?
     (~f ~@args)
     ~(last args)))

(defmacro ?>
  "Conditional single-arrow operation (-> m (?> add-kv? assoc :k :v))"
  [arg do-it? f & rest]
  `(if ~do-it?
     (~f ~arg ~@rest)
     ~arg))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; File utils

(defn file-of-kind?
  "Does the path point to a file that ends with kind?"
  [kind path]
  (and (.isFile path) (.endsWith (.getName path) kind)))

(defn file-kind-seq
  "Return a seq of all xml files under the given directory."
  [kind file-or-dir count]
  (if (= count :all)
    (->> (file-seq file-or-dir)
         (filter #(file-of-kind? kind %)))
    (->> (file-seq file-or-dir)
         (filter #(file-of-kind? kind %))
         (take count))))

(defn file-ext [f]
  (last (clojure.string/split "." (.getName f))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Parse utils

(defn parse-int [s]
  (if (number? s)
    s
    (Integer/valueOf (re-find #"-?\d+" s))))

(defn parse-int-safe [s]
  (if (number? s)
    s
    (try (Integer/valueOf (re-find #"-?\d+" s)) (catch Exception _ nil))))

(defn parse-float [s]
  (if (number? s)
    s
    (Float/valueOf (re-find #"-?[\d.]+" s))))

(defn parse-float-safe [s]
  (if (number? s)
    s
    (try (Float/valueOf (re-find #"-?[\d.]+" s)) (catch Exception _ nil))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Misc

(defn simplify-name [name]
  (-> (.toLowerCase name)
      (.trim)
      (.replaceAll "\\)" "")
      (.replaceAll "\\(" "")
      (.replaceAll "," "")
      (.replaceAll "\\." "")
      (.replaceAll "'" "")
      (.replaceAll "\"" "")
      (.replaceAll "-" " ")))

(defn tokenize-name [name]
  (-> (simplify-name name)
      (string/replace #"[\(\)]" " ")
      (string/replace #"&" "& and")
      (string/split #"\s+")))

(defn slugify [uri]
  (when uri
    (string/replace uri #"[^a-zA-Z0-9]" "_")))

(defn ?-
  "Return a fn that tries to take k out of a map, or returns
   a placeholder string if missing."
  [k]
  (fn [m]
    (if-let [v (get m k)]
      v
      "-")))

(defn ?fn-
  "Return a fn that tries to take k out of a map, or returns
   a placeholder string if missing."
  [k]
  (fn [m]
    (if-let [v (k m)]
      v
      "-")))

(defn dissoc-in [m [k & ks]]
  (if ks
    (if-let [nextmap (clojure.core/get m k)]
      (let [newmap (dissoc-in nextmap ks)]
        (if (seq newmap)
          (assoc m k newmap)
          (dissoc m k)))
      m)
    (dissoc m k)))

(defn safe-trim [s]
  (when s (string/trim s)))

(defn tree-seq-depth
  "Returns a lazy sequence of vectors of the nodes in a tree and their
  depth as [node depth], via a depth-first walk.  branch? must be a fn
  of one arg that returns true if passed a node that can have
  children (but may not).  children must be a fn of one arg that
  returns a sequence of the children. Will only be called on nodes for
  which branch? returns true. Root is the root node of the tree."
  [branch? children root]
  (let [walk (fn walk [depth node]
               (lazy-seq
                (cons [node depth]
                      (when (branch? node)
                        (mapcat (partial walk (inc depth)) (children node))))))]
    (walk 0 root)))

(defn dissoc-all [m ks]
  (reduce (fn [ma [k va]]
            (if (map? va)
              (assoc ma k (dissoc-all va ks))
              (if (not (some #{k} ks))
                (assoc ma k va)
                ma))) {} m))

(defn get-all-in [m ks]
  (reduce (fn [col [k va]]
            (if (map? va)
              (concat col (get-all-in va ks))
              (if (some #{k} ks)
                (cons va col)
                col))) [] m))

(defn empty-string->nil [s]
  (if (string/blank? s)
    nil
    s))

(defn remove-nil-values
  "remove nil values in a hashmap"
  [hsh-map]
  (into {} (filter (comp some? val) hsh-map)))

(defn get-empty-collections
  "generates a hashmap of empty collections"
  [h]
  (into {}
    (filter
      ; checks if it's a collection and if the value of the hashmap is an empty hash or an empty vector
      #(and (coll? (second %)) (or (not (seq (second %))) (empty? (filter some? (second %))))) h)))

(defn remove-empty-collections
  "removes keys of anything with an empty collection from the hashmap sent to the function"
   [h]
   (let [empty-keys (keys (get-empty-collections h))]
    (apply dissoc h empty-keys)))

(defn remove-empty-strings [h]
  (into {} (filter #(not= "" (val %)) h)))

(defn ->sha1 [data]
  (apply str 
         (map 
           #(.substring 
              (Integer/toString 
                (+ (bit-and % 0xff) 0x100) 16) 1) 
           (.digest (MessageDigest/getInstance "sha1") (.getBytes data)))))

(defn remove-keys-by-pred
  "provides a way to remove keys based on predicates executed on values
  The pred-map input is a map where the key will be a function that checks the type
  and the value a function that will theck a property of the value.
  The reason is that you cannot execute empty? on a number or odd? on a collection.

  example of pred-map: {number? odd? coll? empty?}"
  [mp pred-map]
  (wk/postwalk (fn [e] (if (map-entry? e)
                          (let [[x y] e
                                pred-res ((->> pred-map
                                               (map #(apply every-pred %))
                                               (apply some-fn)) y)]
                            (if pred-res nil e))
                          e))
                 mp))

(defn hyphenate
  "Accepts a string s and replaces every instance of an underscore with a hyphen\n
   returning the new resulting string value.\n
   Returns nil if s is not a string or is nil"
  [s]
  (when (and (not (nil? s)) (string? s))
    (string/replace s "_" "-")))
