(ns cayenne.ingest.s3-storage
  (:require [amazonica.aws.s3 :as s3]
            [cayenne.util :as util]
            [cayenne.ingest.prefix-storage :refer [PrefixStorage files-by-prefix]]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [taoensso.timbre :refer [info]]))

(defrecord S3 [endpoint client-config]
  PrefixStorage
  (file-by-key [this bucket k]
    (->> (s3/get-object this bucket k)
         :object-content
         slurp))
  (files-by-prefix [this bucket prefix suffix continuation-token]
    (let [response (s3/list-objects-v2 this
                     (if continuation-token
                       {:bucket-name bucket 
                        :continuation-token continuation-token
                        :prefix prefix}
                       {:bucket-name bucket 
                        :prefix prefix}))
          summaries (if suffix 
                      (filter 
                        #(string/ends-with? (:key %) suffix) 
                        (:object-summaries response))
                      (:object-summaries response))
          next-continuation-token (:next-continuation-token response)]
      (concat (filter 
                  (comp (partial < 0) :size)
                  summaries)
              (lazy-seq (when next-continuation-token (files-by-prefix this bucket prefix suffix next-continuation-token))))))
  (files-by-sha1-prefix [this bucket prefix suffix continuation-token]
    (files-by-prefix this bucket (if prefix (util/->sha1 (string/lower-case prefix)) "") suffix continuation-token))
  (content-by-prefix [this bucket prefix suffix]
    (map
      (fn [{:keys [key]}]
        (->> (s3/get-object this bucket key)
             :object-content
             slurp)) 
      (files-by-prefix this bucket prefix suffix nil)))
  (apply-by-prefix [this bucket prefix suffix f]
    (doseq [{:keys [key]} (files-by-prefix this bucket prefix suffix nil)]
      (info "Processing" key)
      (let [content (->> (s3/get-object this bucket key) :object-content slurp)]
        (with-open [rdr (io/reader (char-array content))]
          (f rdr)))))
  (put-object [this bucket k s]
    (s3/put-object this bucket k s)))

