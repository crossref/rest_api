(ns cayenne.ingest.local-storage
  (:require [cayenne.util :as util]
            [cayenne.ingest.prefix-storage :refer [PrefixStorage files-by-prefix]]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [taoensso.timbre :refer [info]]))

(defn- local-path->key [file bucket]
  (let [k (string/replace-first (.getAbsolutePath file) (str (System/getProperty "user.dir") "/") "")
        k (string/replace-first k bucket "")]
    (string/replace-first k #"^/" "")))

(defrecord LocalS3 []
  PrefixStorage
  (file-by-key [_ bucket k]
    (slurp (str bucket "/" k)))
  (files-by-prefix [_ bucket prefix suffix _]
    (let [files (drop 1 (file-seq (clojure.java.io/file bucket)))
          no-dirs (remove #(.isDirectory %) files)
          with-keys (map #(assoc {} :key (local-path->key % bucket)) no-dirs)
          matching-prefix (if prefix
                            (filter 
                              #(string/starts-with? (:key %) prefix) 
                              with-keys)
                            with-keys)
          matching-suffix (if suffix
                            (filter 
                              #(string/ends-with? (:key %) suffix) 
                              matching-prefix)
                            matching-prefix)]
      matching-suffix))
  (files-by-sha1-prefix [this bucket prefix suffix continuation-token]
    (files-by-prefix this bucket (if prefix (util/->sha1 (string/lower-case prefix)) "") suffix continuation-token))
  (content-by-prefix [this bucket prefix suffix]
    (map
      (fn [{:keys [key]}]
        (slurp (str bucket "/" key))) 
      (files-by-prefix this bucket prefix suffix nil)))
  (apply-by-prefix [this bucket prefix suffix f]
    (doseq [{:keys [key]} (files-by-prefix this bucket prefix suffix nil)]
      (info "Processing" key)
      (let [content (slurp (str bucket "/" key))]
        (with-open [rdr (io/reader (char-array content))]
          (f rdr))))))
