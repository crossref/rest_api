(ns cayenne.ingest.sqs
  (:import [java.util UUID])
  (:require [amazonica.aws.sqs :as sqs]
            [cayenne.api.v1.update :as update]
            [cayenne.conf :as conf]
            [cayenne.elastic.index :as index]
            [cayenne.formats.unixsd :as unixsd]
            [cayenne.item-tree :as itree]
            [cayenne.ingest.prefix-storage :as prefix-storage]
            [cayenne.sentry :refer [with-sentry-reporting send-sentry-event]]
            [cayenne.xml :as xml]
            [clojure.core.async :refer [go-loop chan timeout alts!! >! <!!]]
            [clojure.set :as set]
            [clojure.string :as str]
            [clojure.data.json :as json]
            [clojure.java.io :as io]
            [com.climate.claypoole :as cp]
            [com.climate.claypoole.lazy :as lazy]
            [taoensso.timbre :as log]))

(defn subject-apply-to
  "After subject codes deprecation we don't want breaking changes, so we keep a dummy function
  that returns an empty list instead"
  ([item]
   (if (= :journal (itree/get-item-subtype item))
     (assoc item :category [])
     item))
  ([id item]
   [id (subject-apply-to item)]))

(defn sqs-message [{:keys [bucket object-key index-all]}]
  (let [record (-> {}
                   (assoc-in [:s3 :bucket :name] bucket)
                   (assoc-in [:s3 :object :key] object-key)
                   (assoc-in [:index-all] index-all))]
    (json/write-str {:Message (json/write-str {:Records [record]})})))

(defn record-s3-objects [sqs-record]
  (let [bucket (get-in sqs-record [:s3 :bucket :name])
        key (get-in sqs-record [:s3 :object :key])
        basic-object {:bucket bucket :object-key key}]
    (if (:index-all sqs-record)
      [basic-object
       {:bucket bucket
        :object-key (str/replace key #"unixsd.xml$" "citation-update.json")
        :ignore-errors true}]
      [basic-object])))

(defn s3-objects [message]
  (-> message
      :body
      (json/read-str :key-fn keyword)
      :Message
      (json/read-str :key-fn keyword)
      :Records
      ((partial mapcat record-s3-objects))))

(defn s3-object-content [{:keys [bucket object-key ignore-errors]
                          :or {ignore-errors false}
                          :as record}]
  (if ignore-errors
    (try
      (log/info "Retrieving record" record)
      (-> (conf/get-param [:s3 :metadata-storage-service])
          (prefix-storage/file-by-key bucket object-key)
          char-array
          (vector record))
      (catch Exception _))
    (try
      (with-sentry-reporting
        {:record record}
        (log/info "Retrieving record" record)
        (-> (conf/get-param [:s3 :metadata-storage-service])
            (prefix-storage/file-by-key bucket object-key)
            char-array
            (vector record)))
      (catch Exception ex 
        (.printStackTrace ex)
        (log/error "Exception retrieving record" record (.getMessage ex))))))

(defn index-command-xml [rdr object-key]
  (let [index-commands (atom [])
        f #(let [parsed (as-> % $
                             (unixsd/unixsd-record-parser $)
                             (apply subject-apply-to $)
                             (conj $ :filters {:type :work})
                             (apply itree/centre-on $))
                 doi (first (itree/get-item-ids parsed :doi))
                 index-command (index/bulk-index-command parsed)]
              (cond
                ; nil doi means the Item Tree parser did not fully parse the XML
                ; this happens when we process an unsupported content type
                (nil? doi)
                (log/error "Null DOI in parsed XML" object-key)
                
                ; this check is to make sure we do not try to index pending publications
                ; pending publications are supported by the Item Tree parser,
                ; but are NOT (yet) supported by the Item Tree -> ES doc converter
                ; see also https://crossref.atlassian.net/browse/CR-876
                (or (-> index-command :work first :update :_id nil?)
                    (-> index-command :work second nil?))
                (log/error "Null converted ES JSON" object-key)
                
                :else
                (do
                  (doseq [crm-item (unixsd/missing-crm-items %)]
                    (log/warn "Missing crm-item" crm-item "for DOI" doi))
                    ;(send-sentry-event
                    ;  {:doi doi :crm-item crm-item}
                    ;  (Exception. (str "Missing crm-item " crm-item " for DOI " doi))))
                  (swap! index-commands conj index-command)
                  (log/info "Parsed file for DOI" doi))))]
    (xml/process-xml rdr "crossref_result" f)
    @index-commands))

(defn index-command-update [rdr]
  (let [index-commands (->> rdr
                            update/read-updates-message
                            (map update/update-as-elastic-command)
                            (filter (comp not str/blank? :_id :update first :work)))]
    (doseq [command index-commands]
      (log/info "Parsed update for DOI" (-> command :work first :update :_id)))
    index-commands))

(defn index-command [message ]
  (let [records (s3-objects message)
        s3-files (map s3-object-content records)
        index-fn (fn [[file {:keys [object-key] :as record}]]
                   (try
                     (with-sentry-reporting
                       {:record record}
                       (log/info "Handling record" record)
                       (with-open [rdr (io/reader file)]
                         (cond
                           (str/ends-with? object-key "unixsd.xml")
                           (index-command-xml rdr object-key)
                           (str/ends-with? object-key "citation-update.json")
                           (index-command-update rdr))))
                     (catch Exception ex 
                       (.printStackTrace ex)
                       (log/error "Exception processing record" record (.getMessage ex)))))]
    {:commands (flatten (map index-fn s3-files)) :message message}))

(defn extract-errors [ex-message es-prefix bulk-commands]
  (if (str/starts-with? ex-message es-prefix)
    (-> ex-message
        (subs (count es-prefix))
        str/trim
        (json/read-str :key-fn keyword)
        ((partial map #(let [op-type (if (:update %) :update :index)
                             doi (:_id (op-type %))
                             error (op-type %)]
                         (hash-map :doi doi :op-type op-type :error error)))))
    (map
      #(let [op-type (if (:update (first %)) :update :index)
             doi (:_id (op-type (first %)))]
         (hash-map :doi doi :op-type op-type :error (str (name op-type) " failed")))
      bulk-commands)))

(defn es-response->messages-map
  "Given es-reponse and es-input including messages returns
  {:sucessful-messages [messages] :failed-messages [messages]}"

  [response es-input]
  (let [items (->> response :body :items)
        updated-ids (->> items
                         (keep :update)
                         (filter #(#{200 201} (:status %)))
                         (map :_id)
                         set)
        update-objects (->> es-input (filter (comp :update first :work first :commands)))

        succesful-update-msgs (->> update-objects
                                   (keep (fn [{:keys [commands message]}]
                                           (when (some updated-ids (-> commands first :work first :update :_id list))
                                             message))))

        successful-messages (set succesful-update-msgs)
        failed-messages (as-> es-input $
                          (map :message $)
                          (set $)
                          (set/difference $ successful-messages))]

    {:successful-messages (seq successful-messages) :failed-messages (seq failed-messages)}))

(defn delete-sqs-messages [messages]
  (doseq [part (partition 10 10 [] (set messages))]
    (let [entries (map-indexed (fn [idx {:keys [receipt-handle]}]
                         {:id idx
                          :receipt-handle receipt-handle}) part)]
      (sqs/delete-message-batch {:entries entries
                                 :queue-url (conf/get-param [:service :sqs :url])}))))

(defn index-data
  "We are doing the indexing of relationship at this point, that should not be
  the case but because of the way xml parsing is made it is very complex doing it
  in any other place. TODO: after the redesign of pusher checker this sqs file
  disappears and we can move it all to the new system"
  [objects]
  (let [bulk-commands (->> objects (map :commands ) flatten)
        works (->> bulk-commands (map :work) flatten)
        relations-updates (->> bulk-commands (map :updates) flatten)
        uuid (str (UUID/randomUUID))
        error-msg "Elasticsearch work index failed"
        relupdates-error-msg "Elasticsearch relation updates index failed"]
    (log/info "Indexing" (count bulk-commands) "items - " uuid)
    (try
      (let [response (try (index/bulk-index-items :work works :error-msg error-msg)
                          (catch clojure.lang.ExceptionInfo e (ex-data e)))
            updates-response (when (seq relations-updates)
                               (do
                                 (log/info "#Relations found:" (count relations-updates))
                                 (try (index/bulk-index-items :relupdates relations-updates :error-msg relupdates-error-msg)
                                                    (catch clojure.lang.ExceptionInfo e (ex-data e)))))
            {:keys [successful-messages failed-messages]} (es-response->messages-map response objects)]

        (log/info "Removing from Queue" (count successful-messages) "messages - " uuid)
        (delete-sqs-messages successful-messages)
        (when failed-messages
          (do
            (run! #(log/info (json/write-str {:type "ERROR"
                                              :message "Error indexing work"
                                              :detail (->> response
                                                           :body :items
                                                           (filter (fn [l] (or (get-in l [:update :error] ) (get-in l [:index :error]))))
                                                           (map vals)
                                                           flatten
                                                           (map (fn [d] {:id (:_id d) :error (get-in d [:error :reason]) :shard (get-in d [:error :shard])})))
                                              :obj (->> % s3-objects (map :object-key))}))
                  failed-messages)
            (delete-sqs-messages failed-messages))))
      (catch Exception ex
        (.printStackTrace ex)
        (log/error "Exception indexing data" (.getMessage ex))
        (doseq [error-info (extract-errors (.getMessage ex) error-msg bulk-commands)]
          (when-not (and (= :update (:op-type error-info))
                         (= "document_missing_exception" (get-in error-info [:error :error :type])))
            (send-sentry-event {} (Exception. (str "Indexing item failed " (json/write-str error-info)))))
          (log/error "Indexing item failed" (json/write-str error-info)))))))

(defn consume-notifications []
  (let [index-ch (chan)]
    (go-loop []
      (log/debug "Polling SQS")
      (let [messages (:messages (sqs/receive-message
                                 :queue-url (conf/get-param [:service :sqs :url])
                                 :wait-time-seconds 10
                                 :max-number-of-messages 10
                                 :delete false))]
        (log/info "Got" (count messages) "messages")

        (doseq [commands (cp/pmap 10 index-command messages)]
          (>! index-ch commands)))
      (recur))
    
    (loop [bulk-commands []
           command (<!! index-ch)]
      (when (or (>= (count bulk-commands) 200)
                (and (nil? command) (seq bulk-commands)))
        (index-data bulk-commands))
      (let [next-bulk-commands (cond
                                 ; timeout
                                 (nil? command) []

                                 ; Error parsing xml we ignore it
                                 (->> command :commands first nil?)
                                 (do
                                   (log/info (json/write-str {:type "ERROR"
                                                              :message "Error parsing s3 obj"
                                                              :obj (->> command :message s3-objects (map :object-key))}))
                                   (delete-sqs-messages [(:message command)])
                                   bulk-commands)
                                 
                                 ; the item list is full
                                 (>= (count bulk-commands) 200) (vector command)

                                 :else (conj bulk-commands command))
            [next-command _] (alts!! [index-ch (timeout (* (conf/get-param [:val :indexing-timeout]) 1000))])]
        (recur next-bulk-commands next-command)))))

(defn all-keys [suffix]
  (let [bucket (conf/get-param [:s3 :metadata-bucket])
        service (conf/get-param [:s3 :metadata-storage-service])]
    (cond
      (seq (conf/get-param [:s3 :dois]))
      (apply concat (map
                      #(prefix-storage/files-by-sha1-prefix service bucket % suffix nil)
                      (conf/get-param [:s3 :dois])))
      
      (seq (conf/get-param [:s3 :prefixes]))
      (apply concat (map
                      #(prefix-storage/files-by-prefix service bucket % suffix nil)
                      (conf/get-param [:s3 :prefixes])))
      
      :else
      (prefix-storage/files-by-sha1-prefix service bucket nil suffix nil))))

(defn produce-all-keys [suffix]
  (let [bucket (conf/get-param [:s3 :metadata-bucket])
        produce (fn [data msg]
                  (log/info "Producing" msg)
                  (sqs/send-message
                    {:endpoint (conf/get-param [:aws :endpoint])}
                    (conf/get-param [:service :sqs :url])
                    (sqs-message data)))]
        
    (cp/pdoseq 10 [[first {:keys [key]}]
                   (->> (all-keys suffix)
                        (cons {:key "unixsd.xml"})
                        (partition 2 1))]
      (try
        (while (> (-> (sqs/get-queue-attributes
                        :queue-url (conf/get-param [:service :sqs :url])
                        :attribute-names ["ApproximateNumberOfMessages"])
                      :ApproximateNumberOfMessages
                      (Integer/parseInt))
                  50000)
          (Thread/sleep 60000))
        (let [[first-hash _] (str/split (:key first) #"/")
              [second-hash second-name] (str/split key #"/")]
          (cond
            (and (= second-name "unixsd.xml")
                 (not= first-hash second-hash))
            (produce {:object-key key :bucket bucket} key)

            (and (= second-name "unixsd.xml")
                 (= first-hash second-hash))
            (produce {:object-key key :bucket bucket :index-all true} (str key " and citation update"))

            (= "citation-update.json" suffix)
            (produce {:object-key key :bucket bucket} key)))
        (catch Exception ex 
          (.printStackTrace ex)
          (log/error "Exception sending key" (:key key) (.getMessage ex)))))

    (log/info "Scanning finished.")
    (System/exit 0)))

(conf/with-core :default
  (conf/add-startup-task
    :sqs-ingest
    (fn [_]
      (future
        (conf/set-service!
          :sqs-ingest
          (consume-notifications))))))

(conf/with-core :default
  (conf/add-startup-task
    :s3-sqs-produce-xml
    (fn [_]
      (future 
        (conf/set-service!
          :s3-sqs-produce-xml
          (produce-all-keys "unixsd.xml"))))))

(conf/with-core :default
  (conf/add-startup-task
    :s3-sqs-produce-update
    (fn [_]
      (future 
        (conf/set-service!
          :s3-sqs-produce-update
          (produce-all-keys "citation-update.json"))))))

(conf/with-core :default
  (conf/add-startup-task
    :s3-sqs-produce-all
    (fn [_]
      (future 
        (conf/set-service!
          :s3-sqs-produce-all
          (produce-all-keys nil))))))

