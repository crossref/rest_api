(ns cayenne.ingest.prefix-storage)

(defprotocol PrefixStorage
  (file-by-key [this bucket k])
  (files-by-prefix [this bucket prefix suffix continuation-token])
  (files-by-sha1-prefix [this bucket prefix suffix continuation-token])
  (content-by-prefix [this bucket prefix suffix])
  (apply-by-prefix [this bucket prefix suffix f])
  (put-object [this bucket k s]))
