(ns cayenne.ingest.s3
  (:require [cayenne.api.v1.feed :as feed]
            [cayenne.version]
            [cayenne.ingest.prefix-storage :refer [apply-by-prefix]]
            [cayenne.conf :as conf]
            [cayenne.util :as util]
            [clojure.string :as str]))

(defn index-keys [prefix suffix content-type & {:keys [prefix-f] :or {prefix-f identity}}]
  (apply-by-prefix 
    (conf/get-param [:s3 :metadata-storage-service]) 
    (conf/get-param [:s3 :metadata-bucket]) 
    (prefix-f prefix)
    suffix
    (partial feed/process! content-type)))

(defn index-all [suffix content-type]
  (cond
    (seq (conf/get-param [:s3 :dois]))
    (doseq [doi (conf/get-param [:s3 :dois])]
      (index-keys doi suffix content-type :prefix-f (comp util/->sha1 str/lower-case)))
    
    (seq (conf/get-param [:s3 :prefixes]))
    (doseq [prefix (conf/get-param [:s3 :prefixes])]
      (index-keys prefix suffix content-type))
    
    :else
    (index-keys nil suffix content-type)))

(conf/with-core :default
  (conf/add-startup-task
    :s3-ingest-xml
    (fn [_]
      (future 
        (conf/set-service!
          :s3-ingest-xml
          (index-all "unixsd.xml" "application/vnd.crossref.unixsd+xml"))))))

(conf/with-core :default
  (conf/add-startup-task
    :s3-ingest-update
    (fn [_]
      (future 
        (conf/set-service!
          :s3-ingest-update
          (index-all "citation-update.json" "application/vnd.crossref.update+json"))))))

