(ns cayenne.data.work
  (:require [cayenne.conf :as conf]
            [cayenne.api.v1.query :as query]
            [cayenne.api.v1.response :as r]
            [cayenne.api.v1.filter :as filter]
            [cayenne.api.v1.facet :as facet]
            [cayenne.ids.doi :as doi-id]
            [org.httpkit.client :as http]
            [clojure.string :as string]
            [clojure.data.json :as json]
            [qbits.spandex :as elastic]
            [taoensso.timbre :as timbre]
            [clojure.core.memoize :as memoize]
            [clojure.core.async :refer [go <!]]
            [cayenne.elastic.convert :as convert]
            [cayenne.elastic.util :as elastic-util])
  (:import [java.net URLEncoder]))

(def default-work-filters
  {"has-prime-doi" '("false")})

(defn render-record [query-context doc]
  (into
   {}
   (filter
     #(not (if (coll? (second %)) (empty? (second %)) (nil? (second %))))
     (if (empty? (:select query-context))
       (-> doc
           convert/es-doc->citeproc)
       (-> doc
           convert/es-doc->citeproc
           (select-keys (map keyword (:select query-context))))))))

(defn indexed
  [s]
  (map vector (iterate inc 0) s))

(defn positions [pred coll]
  (for [[idx elt] (indexed coll) :when (pred elt)] idx))

(defn reordered-preprints [records]
  (let [reordering-records (take 20 records)]
    (concat
     (reduce
      #(let [has-preprint-dois (->> (get (:relation %2) "has-preprint")
                                    (map :id))
             preprint-positions (positions (fn [a]
                                             (some #{(:DOI a)}
                                                   has-preprint-dois)) %1)
             first-preprint-pos (if (empty? preprint-positions)
                                  -1
                                  (apply min preprint-positions))]
         (cond
           (empty? has-preprint-dois)
           (conj %1 %2)

           (not= first-preprint-pos -1)
           (vec
            (concat
             (subvec %1 0 first-preprint-pos)
             [%2]
             (subvec %1 first-preprint-pos)))

           :else
           (conj %1 %2)))
      []
      reordering-records)
     (drop 20 records))))

(defn es->response [query-context es-request es-response doc-list & {:keys [total] :or {total nil}}]
  (-> (r/api-response :work-list)
      (r/with-debug-info query-context es-request)
      (r/with-result-facets (-> es-response
                                (get-in [:body :aggregations])
                                facet/->response-facets))
      (r/with-result-items
        (if total total (get-in es-response [:body :hits :total]))
        (-> (map render-record (repeat query-context) doc-list)
            reordered-preprints)
        :next-cursor (get-in es-response [:body :_scroll_id]))
      (r/with-query-context-info query-context)))

(defn fetch-data-async [query-context respond & {:keys [id-field] :or {id-field nil}}]
  (let [es-request (query/->es-request query-context
                     :id-field id-field
                     :filters filter/std-filters)
        tb (System/currentTimeMillis)
        es-response-ch (elastic/request-chan (conf/get-service :elastic) es-request)]
    (go
      (try
        (let [es-response (<! es-response-ch)
              _ (timbre/info (json/write-str {"request" query-context
                                                     "type" "es-time"
                                                     "ms" (- (System/currentTimeMillis) tb)}))
              doc-list (get-in es-response [:body :hits :hits])
              response  (if (instance? Exception es-response)
                          es-response
                          (es->response query-context es-request es-response doc-list))]
          (respond response))
        (catch Exception e
          (respond e))))
    ; this is returned to let the handler know that the request is being responded to here
    "ASYNC"))

(defn max-citation-id
  "the largest citation-id of an indexed work"
  []
  (let [es-request {:method :get
                    :url (str (elastic-util/index-url-prefix :work) "_search")
                    :body {:_source [:citation-id]
                           :query {:match_all {}}
                           :sort [{:citation-id {:order :desc}}]
                           :size 1}}
        es-response (elastic/request (conf/get-service :elastic) es-request)]
    (-> es-response
        (get-in [:body :hits :hits])
        first
        (get-in [:_source :citation-id]))))

(def max-citation-id-memo (memoize/ttl max-citation-id :ttl/threshold (* 6 60 60 1000)))

(defn citation-id-sample
  "draws a sample of citation ids from the range 1-max_cid (inclusive)"
  [size]
  (let [max-cid (max-citation-id-memo)]
    (if (>= size max-cid)
      (into [] (range 1 (inc max-cid)))
      (let [random-cids (repeatedly #(inc (rand-int max-cid)))
            samples (map #(set (take % random-cids)) (range))]
        (->> samples
             (filter #(= size (count %)))
             first
             (into []))))))

(defn fetch-sample-async [query-context respond
                          & {:keys [es-sample-size attempts-left results id-field]
                             :or {es-sample-size nil attempts-left 2 results [] id-field nil}}]
  (let [sample-size (:sample query-context)
        es-sample-size (if es-sample-size es-sample-size (max 5 (* 2 sample-size)))
        citation-ids (citation-id-sample es-sample-size)
        sampling-query-context (-> query-context
                                   (assoc :sample 0)
                                   (assoc :rows es-sample-size)
                                   (assoc-in [:filters "citation-id"] citation-ids))
        es-request (query/->es-request sampling-query-context
                     :id-field id-field
                     :filters filter/std-filters)
        es-response-ch (elastic/request-chan (conf/get-service :elastic) es-request)]
    (go
      (try
        (let [es-response (<! es-response-ch)
              doc-list (get-in es-response [:body :hits :hits])
              cumulated-results (->> doc-list
                                     (concat results)
                                     (reduce #(assoc %1 (:_id %2) %2) {})
                                     (reduce-kv (fn [m _ v] (conj m v)) []))
              predicted-cumulated-size (cond
                                         (= attempts-left 0) (count cumulated-results)
                                         (= attempts-left 1) (* 2.4 (count cumulated-results))
                                         (= attempts-left 2) (* 7 (inc (count cumulated-results))))]
          (timbre/info (json/write-str {"original-request" query-context
                                               "sampling-request" sampling-query-context
                                               "type" "sampling"
                                               "es-sample-size" es-sample-size
                                               "attempts-left" attempts-left
                                               "returned-size" (count doc-list)
                                               "cumulated-size" (count cumulated-results)
                                               "predicted-size" predicted-cumulated-size}))
          (cond
            ; ES returned an exception
            (instance? Exception es-response)
            (respond es-response)
            
            ; the number of the results gathered so far is equal to the requested sample size,
            ; we respond to the client immediately
            (= sample-size (count cumulated-results))
            (respond (es->response query-context es-request es-response cumulated-results :total sample-size))
            
            ; the number of the results gathered so far is larger than the requested sample size,
            ; we sample down to get the right size
            (< sample-size (count cumulated-results))
            (respond (es->response
                       query-context
                       es-request
                       es-response
                       (take sample-size cumulated-results)
                       :total sample-size))
            
            ; the number of the results gathered so far is smaller than the requested sample size,
            ; but we predict that we still can gather the requested number within allowed attempts,
            ; we try asking ES again for twice as many works as before
            (and (> sample-size (count cumulated-results))
                 (>= predicted-cumulated-size sample-size)
                 (> attempts-left 0))
            (fetch-sample-async
              query-context
              respond
              :es-sample-size (* 2 es-sample-size)
              :attempts-left (dec attempts-left)
              :results cumulated-results
              :id-field id-field)
            
            ; all allowed attempts resulted in too few works,
            ; we give up and revert to the original sampling
            :else
            (do
              (timbre/info (json/write-str {"original-request" query-context
                                                   "sampling-request" sampling-query-context
                                                   "type" "sampling"
                                                   "es-sample-size" es-sample-size
                                                   "attempts-left" attempts-left
                                                   "returned-size" (count doc-list)
                                                   "cumulated-size" (count cumulated-results)
                                                   "predicted-size" predicted-cumulated-size
                                                   "decision" "revert"}))
              (fetch-data-async query-context respond :id-field id-field))))
        (catch Exception e
          (respond e))))
    ; this is returned to let the handler know that the request is being responded to here
    "ASYNC"))

(defn fetch-async [query-context respond & {:keys [id-field] :or {id-field nil}}]
  (let [query-context (update query-context :filters #(merge default-work-filters %))]
    (if (pos? (:sample query-context))
      (fetch-sample-async query-context respond :id-field id-field)
      (fetch-data-async query-context respond :id-field id-field))))

(defn fetch [query-context & {:keys [id-field] :or {id-field nil}}]
  (let [query-context (update query-context :filters #(merge default-work-filters %))]
    (let [es-request (query/->es-request query-context
                                         :id-field id-field
                                         :filters filter/std-filters)
          response (elastic/request (conf/get-service :elastic) es-request)
          doc-list (get-in response [:body :hits :hits])]
      (-> (r/api-response :work-list)
          (r/with-debug-info query-context es-request)
          (r/with-result-facets (-> response
                                    (get-in [:body :aggregations])
                                    facet/->response-facets))
          (r/with-result-items
            (get-in response [:body :hits :total])
            (-> (map render-record (repeat query-context) doc-list)
                reordered-preprints)
            :next-cursor (get-in response [:body :_scroll_id]))
          (r/with-query-context-info query-context)))))

(defn fetch-reverse [query-context]
  (let [terms (:terms query-context)
        es-request (query/->es-request {:field-terms {"bibliographic" terms}
                                        :rows 1})
        response (elastic/request (conf/get-service :elastic) es-request)
        doc-list (get-in response [:body :hits :hits])]
    (if (zero? (count doc-list))
      (r/api-response :nothing)
      (let [doc (-> doc-list first convert/es-doc->citeproc)]
        (if (or (< (count (string/split terms #"\s")) 4) (< (:_score doc) 2))
          (r/api-response :nothing)
          (r/api-response :work :content doc))))))

(defn fetch-one
  "Fetch a known DOI."
  [doi]
  (when (not (string/blank? doi))
    (let [response
          (elastic/request
            (conf/get-service :elastic)
            {:method :get
             :url (str (elastic-util/index-url-prefix :work) (URLEncoder/encode doi "UTF-8"))
             :exception-handler (fn [e] (when (not (string/includes? (.getMessage e) "404"))
                                          (throw e)))})]
      (when-let [doc (:body response)]
        (r/api-response :work :content (-> doc
                                           (assoc :_score 1)
                                           convert/es-doc->citeproc))))))

(defn fetch-one-async [doi respond]
  (when (not (string/blank? doi))
    (let [tb (System/currentTimeMillis)
          es-response-ch
          (elastic/request-chan
            (conf/get-service :elastic)
            {:method :get
             :url (str (elastic-util/index-url-prefix :work) (URLEncoder/encode doi "UTF-8"))})]
      (go
        (try
          (let [es-response (<! es-response-ch)
                _ (timbre/info (json/write-str {"retrieve-doi" doi
                                                "type" "es-time"
                                                "ms" (- (System/currentTimeMillis) tb)}))
                response (if (instance? Exception es-response)
                           es-response
                           (r/api-response :work :content (-> es-response
                                                              :body
                                                              (assoc :_score 1)
                                                              convert/es-doc->citeproc)))
                {{:keys [prime-doi]} :message} response]
            (when prime-doi
              (throw (ex-info "prime doi detected" {:redirect (str "/works/" prime-doi)})))
            (respond response))
          (catch Exception e
            (respond e))))
          ; this is returned to let the handler know that the request is being responded to here
      "ASYNC")))

(defn get-agency [doi]
  @(http/get (str (conf/get-param [:upstream :doi-ra-url])
                  doi)))

(defn parse-agency [{:keys [body error]}]
  (when-not error
    (-> body
        (json/read-str :key-fn keyword)
        first
        :RA)))

(defn ->agency-response [doi agency]
  (r/api-response
   :work-agency
   :content {:DOI (doi-id/normalize-doi doi)
             :agency {:id (clojure.string/lower-case agency)
                      :label agency}}))

(defn fetch-agency [doi]
  (let [extracted-doi (doi-id/normalize-doi doi)
        agency-fn (comp parse-agency get-agency)]
    (->agency-response doi (agency-fn extracted-doi))))
