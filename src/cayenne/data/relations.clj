(ns cayenne.data.relations
  (:require [clojure.set :as set]
            [clojure.string :as string]
            [cayenne.ids.doi :as doi]))

(def inter-relations
  [:is-derived-from
   :has-derivation
   :is-review-of
   :has-review
   :is-comment-on
   :has-comment
   :is-reply-to
   :has-reply
   :based-on-data
   :is-data-basis-for
   :has-related-material
   :is-related-material
   :is-compiled-by
   :compiles
   :is-documented-by
   :documents
   :is-supplement-to
   :is-supplemented-by
   :is-continued-by
   :continues
   :is-part-of
   :has-part
   :references
   :is-referenced-by
   :is-based-on
   :is-basis-for
   :requires
   :is-required-by
   :finances
   :is-financed-by])

(def intra-relations
  [:has-translation
   :is-translation-of
   :has-preprint
   :is-preprint-of
   :has-manuscript
   :is-manuscript-of
   :has-expression
   :is-expression-of
   :has-manifestation
   :is-manifestation-of
   :replaces
   :is-replaced-by
   :is-same-as
   :is-identical-to
   :is-original-form-of
   :is-varient-form-of
   :has-version
   :is-version-of
   :has-format])

(def relations (concat inter-relations intra-relations))

(def relation-antonym
  (let [antonyms {:is-derived-from      :has-derivation
                  :is-review-of         :has-review
                  :is-comment-on        :has-comment
                  :is-reply-to          :has-reply
                  :based-on-data        :is-data-basis-for
                  :has-related-material :is-related-material
                  :is-compiled-by       :compiles
                  :is-documented-by     :documents
                  :is-supplement-to     :is-supplemented-by
                  :is-continued-by      :continues
                  :is-part-of           :has-part
                  :references           :is-referenced-by
                  :is-based-on          :is-basis-for
                  :requires             :is-required-by
                  :finances             :is-financed-by

                  :has-translation      :is-translation-of
                  :has-preprint         :is-preprint-of
                  :has-manuscript       :is-manuscript-of
                  :has-expression       :is-expression-of
                  :has-manifestation    :is-manifestation-of
                  :replaces             :is-replaced-by
                  :has-version          :is-version-of

                  :is-original-form-of  :is-varient-form-of

                  :is-same-as           :is-same-as
                  :is-identical-to      :is-identical-to}]
    (into antonyms (map #(vector (second %) (first %)) antonyms))))

;; defines relations that don't have a reciprocal representation in
;; crossref's relations.xsd schema
(def unworkable-relations
  (set/difference (set relations) (set (keys relation-antonym))))

(def relation-update-fns
  "This is an array of functions to detect relationships on newly indexed
  documents the output of the functions in this array will be update commands on
  documents that will hold those relationships"
  [(fn index-object->has-update-relation-update
     [{:keys [doi update-to]}]
     (when (seq update-to)
       (map #(into {} {:doi (-> % :doi doi/extract-doi)
                       :relation-type :has-update
                       :relation-info (assoc % :doi doi)})
            update-to)))

   (fn index-object->aliases-relation-update
     [{:keys [doi prime-doi is-referenced-by-count]}]
     (when prime-doi
       {:doi prime-doi
        :relation-type :alias
        :relation-info {:doi doi
                        :ref-count is-referenced-by-count}}))])

(defn find-relation-updates
  "This function iterates over a set of elastic search commands in order
  to find whether there are relations present or not and therefore creating
  new commands to update works. Such updates are calculated by running the
  functions within relation-update-fns definition"
  [es-doc]
  (let [find-rels (apply juxt relation-update-fns)]
    (->> es-doc
         find-rels
         flatten
         (remove nil?)
         (map #(vector  {:index {:_index "relupdates" :_type "relupdates"}} %))
         flatten)))
