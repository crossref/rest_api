(ns cayenne.data.csl
  (:require [cayenne.conf :as conf]
            [cayenne.api.v1.response :as r]))

(defonce locale-cache
  (delay
    (do
      (-> (conf/get-resource :locales)
          slurp
          read-string))))

(defonce style-cache
  (delay
    (do
      (-> (conf/get-resource :styles)
          slurp
          read-string))))

(defn fetch-all-styles []
  (-> (r/api-response :style-list)
      (r/with-result-items (count @style-cache) @style-cache)))

(defn fetch-all-locales []
  (-> (r/api-response :locale-list)
      (r/with-result-items (count @locale-cache) @locale-cache)))

