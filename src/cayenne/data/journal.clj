(ns cayenne.data.journal
  (:require [cayenne.conf :as conf]
            [cayenne.data.work :as work]
            [cayenne.data.coverage :as coverage]
            [cayenne.api.v1.query :as query]
            [cayenne.api.v1.response :as r]
            [cayenne.elastic.util :as elastic-util]
            [qbits.spandex :as elastic]
            [taoensso.timbre :as timbre]
            [clojure.data.json :as json]
            [clojure.core.async :refer [go <!]]
            [clj-time.coerce :as dc]))

(defn ->response-doc [journal-doc & {:keys [coverage-doc]}]
  (cond-> {:title (:title journal-doc)
           :publisher (:publisher journal-doc)
           :ISSN (map :value (:issn journal-doc))
           :issn-type (:issn journal-doc)
           :subjects []}   ;; legacy, we'll keep it empty for non breaking changes
          (not (nil? coverage-doc))
          (merge
            (merge-with
              merge
              (coverage/coverage coverage-doc :current)
              (coverage/coverage coverage-doc :backfile))
            {:breakdowns             (get-in coverage-doc [:breakdowns :breakdowns])
             :counts                 (select-keys coverage-doc [:current-dois
                                                                :backfile-dois
                                                                :total-dois])
             :coverage-type          (coverage/coverage-type-journal coverage-doc)
             :last-status-check-time (-> coverage-doc :finished dc/to-long)})))

(defn get-coverage [subject-type subject-ids]
  (let [query (-> {}
                  (assoc-in [:query :bool :must]
                            [{:term {:subject-type subject-type}}])
                  (assoc-in [:query :bool :minimum_should_match] 1)
                  (assoc-in [:query :bool :should]
                            (map (fn [subject-id]
                                   {:term {:subject-id subject-id}}) subject-ids))
                  (assoc :size (count subject-ids)))]
    (-> (elastic/request
         (conf/get-service :elastic)
         {:method :get
          :url (str (elastic-util/index-url-prefix :coverage) "_search")
          :body query})
        (get-in [:body :hits :hits])
        (->> (map :_source)))))

(defn fetch-one [query-context]
  (when (:id query-context)
    (when-let [journal-doc (-> (elastic/request
                                 (conf/get-service :elastic)
                                 (query/->es-request query-context
                                   :id-field :issn.value
                                   :url-prefix (elastic-util/index-url-prefix :journal)))
                               (get-in [:body :hits :hits])
                               first
                               :_source)]
      (r/api-response :journal
        :content
        (->response-doc journal-doc
          :coverage-doc
          (first (get-coverage :journal [(:id journal-doc)])))))))

(defn fetch-one-async [query-context respond]
  (let [tb (System/currentTimeMillis)
        es-response-ch (elastic/request-chan
                          (conf/get-service :elastic)
                          (query/->es-request query-context
                            :id-field :issn.value
                            :url-prefix (elastic-util/index-url-prefix :journal)))]
    (go
      (try
        (let [es-response (<! es-response-ch)
              _ (timbre/info (json/write-str {"retrieve-journal" query-context
                                                     "type" "es-time"
                                                     "ms" (- (System/currentTimeMillis) tb)}))
              response (if (instance? Exception es-response)
                         es-response
                         (if-let [journal-doc (-> es-response
                                                  (get-in [:body :hits :hits])
                                                  first
                                                  :_source)]
                           (r/api-response
                             :journal
                             :content
                             (->response-doc
                               journal-doc
                               :coverage-doc
                               (first (get-coverage :journal [(:id journal-doc)]))))
                           (ex-info "Resource not found." {:status 404})))]
          (respond response))
        (catch Exception e
          (respond e))))
    ; this is returned to let the handler know that the request is being responded to here
    "ASYNC"))

(defn es-request-with-journal-sort [query-context]
  (let [context-with-sort (assoc query-context :sort [:_score :id] :order [:desc :asc])]
    (query/->es-request
      (query/prefix-query-context context-with-sort :title)
      :url-prefix (elastic-util/index-url-prefix :journal))))

(defn fetch [query-context]
  ;; Stable sorting needs to be applied to journals to make it possible
  ;; to retrieve the entire dataset using offsets, even if there are multiple shards.
  (let [es-request (es-request-with-journal-sort query-context)
        response (elastic/request
                   (conf/get-service :elastic)
                   es-request)
        docs (->> [:body :hits :hits]
                  (get-in response)
                  (map :_source))
        find-coverage (->> docs
                           (map :id)
                           (get-coverage :journal)
                           (partial (fn [coverages id]
                                      (some
                                        #(when (= (:subject-id %) id) %) coverages))))]
    (-> (r/api-response :journal-list)
        (r/with-query-context-info query-context)
        (r/with-debug-info query-context es-request)
        (r/with-result-items
          (get-in response [:body :hits :total])
          (map #(->response-doc
                 %
                 :coverage-doc (find-coverage (:id %))) docs)
          :next-cursor (get-in response [:body :_scroll_id])))))

(defn fetch-async [query-context respond]
  ;; Stable sorting needs to be applied to journals to make it possible
  ;; to retrieve the entire dataset using offsets, even if there are multiple shards.
  (let [es-request (es-request-with-journal-sort query-context)
        tb (System/currentTimeMillis)
        es-response-ch (elastic/request-chan
                         (conf/get-service :elastic)
                         es-request)]
    (go
      (try
        (let [es-response (<! es-response-ch)
              _ (timbre/info (json/write-str {"request" query-context
                                                     "type" "es-time"
                                                     "ms" (- (System/currentTimeMillis) tb)}))
              response (if (instance? Exception es-response)
                         es-response
                         (let [docs (->> [:body :hits :hits]
                                         (get-in es-response)
                                         (map :_source))
                               find-coverage (->> docs
                                                  (map :id)
                                                  (get-coverage :journal)
                                                  (partial (fn [coverages id]
                                                             (some
                                                               #(when (= (:subject-id %) id) %) coverages))))]
                           (-> (r/api-response :journal-list)
                               (r/with-query-context-info query-context)
                               (r/with-debug-info query-context es-request)
                               (r/with-result-items
                                 (get-in es-response [:body :hits :total])
                                 (map #(->response-doc
                                         %
                                         :coverage-doc (find-coverage (:id %))) docs)
                                 :next-cursor (get-in es-response [:body :_scroll_id])))))]
          (respond response))
        (catch Exception e
          (respond e))))
    ; this is returned to let the handler know that the request is being responded to here
    "ASYNC"))

(defn fetch-works
  ([query-context]
   (work/fetch query-context :id-field :issn.value))
  ([query-context respond]
   (work/fetch-async query-context respond :id-field :issn.value)))

