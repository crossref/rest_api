(ns cayenne.data.member
  (:require [cayenne.conf :as conf]
            [cayenne.api.v1.query :as query]
            [cayenne.api.v1.response :as r]
            [cayenne.api.v1.filter :as filter]
            [cayenne.data.work :as work]
            [cayenne.data.coverage :as coverage]
            [cayenne.elastic.util :as elastic-util]
            [clojure.string :as string]
            [taoensso.timbre :as timbre]
            [clojure.data.json :as json]
            [clojure.core.async :refer [go <!]]
            [clj-time.coerce :as dc]
            [qbits.spandex :as elastic])
  (:import [java.net URLEncoder]))

(defn ->response-doc [member-doc & {:keys [coverage-doc]}]
  (cond-> {:id           (:id member-doc)
           :primary-name (:primary-name member-doc)
           :names        (->> (:prefix member-doc)
                              (map (comp string/trim :name))
                              (cons (:primary-name member-doc))
                              distinct)
           :prefixes     (map :value (:prefix member-doc))
           :prefix       (->> member-doc
                              :prefix
                              (map #(select-keys % [:name :value])))
           :location     (-> member-doc :location string/trim)
           :tokens       (:token member-doc)}
    (not (nil? coverage-doc))
    (merge
     (merge-with
      merge
      (coverage/coverage coverage-doc :current)
      (coverage/coverage coverage-doc :backfile))
     {:breakdowns             (get-in coverage-doc [:breakdowns :breakdowns])
      :counts                 (select-keys coverage-doc [:current-dois
                                                         :backfile-dois
                                                         :total-dois])
      :coverage-type          (coverage/coverage-type coverage-doc)
      :counts-type            (coverage/type-counts coverage-doc)
      :last-status-check-time (-> coverage-doc :finished dc/to-long)})))

(defn get-coverage [subject-type subject-ids]
  (let [query (-> {}
                  (assoc-in [:query :bool :must]
                            [{:term {:subject-type subject-type}}])
                  (assoc-in [:query :bool :minimum_should_match] 1)
                  (assoc-in [:query :bool :should]
                            (map (fn [subject-id]
                                   {:term {:subject-id subject-id}}) subject-ids))
                  (assoc :size (count subject-ids)))]
    (-> (elastic/request
         (conf/get-service :elastic)
         {:method :get
          :url (str (elastic-util/index-url-prefix :coverage) "_search")
          :body query})
        (get-in [:body :hits :hits])
        (->> (map :_source)))))

(defn fetch-one [id]
  (when (not (string/blank? id))
    (let [response
          (elastic/request
            (conf/get-service :elastic)
            {:method :get
             :url (str (elastic-util/index-url-prefix :member) (URLEncoder/encode id "UTF-8"))
             :exception-handler (fn [e] (when (not (string/includes? (.getMessage e) "404"))
                                          (throw e)))})]
      (when-let [member-doc (:_source (:body response))]
        (r/api-response
          :member
          :content
          (->response-doc
            member-doc
            :coverage-doc
            (first (get-coverage :member [(:id member-doc)]))))))))

(defn fetch-one-async [id respond]
  (when (not (string/blank? id))
    (let [tb (System/currentTimeMillis)
          es-response-ch
          (elastic/request-chan
            (conf/get-service :elastic)
            {:method :get
             :url (str (elastic-util/index-url-prefix :member) (URLEncoder/encode id "UTF-8"))})]
      (go
        (try
          (let [es-response (<! es-response-ch)
                _ (timbre/info (json/write-str {"retrieve-member" id
                                                       "type" "es-time"
                                                       "ms" (- (System/currentTimeMillis) tb)}))
                response (if (instance? Exception es-response)
                           es-response
                           (let [member-doc (:_source (:body es-response))]
                             (r/api-response
                               :member
                               :content
                               (->response-doc
                                 member-doc
                                 :coverage-doc
                                 (first (get-coverage :member [(:id member-doc)]))))))]
            (respond response))
          (catch Exception e
            (respond e))))
      ; this is returned to let the handler know that the request is being responded to here
      "ASYNC")))

(defn fetch-works
  ([query-context]
   (work/fetch query-context :id-field :member-id))
  ([query-context respond]
   (work/fetch-async query-context respond :id-field :member-id)))

(defn es-request-with-member-sort [query-context]
  (let [context-with-sort (assoc query-context :sort [:_score :id] :order [:desc :asc])]
    (query/->es-request
      (query/prefix-query-context context-with-sort :primary-name)
      :url-prefix (elastic-util/index-url-prefix :member)
      :filters filter/member-filters)))

(defn fetch [query-context]
  ;; Stable sorting needs to be applied to members to make it possible
  ;; to retrieve the entire dataset using offsets, even if there are multiple shards.
  (let [es-request (es-request-with-member-sort query-context)
        response (elastic/request
                   (conf/get-service :elastic)
                   es-request)
        docs (->> [:body :hits :hits]
                  (get-in response)
                  (map :_source))
        find-coverage (->> docs
                           (map :id)
                           (get-coverage :member)
                           (partial (fn [coverages id]
                                      (some
                                        #(when (= (:subject-id %) id) %) coverages))))]
    (-> (r/api-response :member-list)
        (r/with-query-context-info query-context)
        (r/with-debug-info query-context es-request)
        (r/with-result-items
          (get-in response [:body :hits :total])
          (map #(->response-doc
                 %
                 :coverage-doc (find-coverage (:id %))) docs)
          :next-cursor (get-in response [:body :_scroll_id])))))

(defn fetch-async [query-context respond]
  ;; Stable sorting needs to be applied to members to make it possible
  ;; to retrieve the entire dataset using offsets, even if there are multiple shards.
  (let [es-request (es-request-with-member-sort query-context)
        tb (System/currentTimeMillis)
        es-response-ch (elastic/request-chan
                         (conf/get-service :elastic)
                         es-request)]
    (go
      (try
        (let [es-response (<! es-response-ch)
              _ (timbre/info (json/write-str {"request" query-context
                                                     "type" "es-time"
                                                     "ms" (- (System/currentTimeMillis) tb)}))
              response  (if (instance? Exception es-response)
                          es-response
                          (let [docs (->> [:body :hits :hits]
                                          (get-in es-response)
                                          (map :_source))
                                find-coverage (->> docs
                                                   (map :id)
                                                   (get-coverage :member)
                                                   (partial (fn [coverages id]
                                                              (some
                                                                #(when (= (:subject-id %) id) %) coverages))))]
                            (-> (r/api-response :member-list)
                                (r/with-query-context-info query-context)
                                (r/with-debug-info query-context es-request)
                                (r/with-result-items
                                  (get-in es-response [:body :hits :total])
                                  (map #(->response-doc
                                          %
                                          :coverage-doc (find-coverage (:id %))) docs)
                                  :next-cursor (get-in es-response [:body :_scroll_id])))))]
          (respond response))
        (catch Exception e
          (respond e))))
    ; this is returned to let the handler know that the request is being responded to here
    "ASYNC"))

