(ns cayenne.data.funder
  (:import [java.net URLEncoder])
  (:require [cayenne.conf :as conf]
            [cayenne.api.v1.query :as query]
            [cayenne.api.v1.response :as r]
            [cayenne.api.v1.filter :as filter]
            [cayenne.data.work :as work]
            [cayenne.util :as util]
            [cayenne.ids.doi :as doi-id]
            [cayenne.elastic.util :as elastic-util]
            [taoensso.timbre :as timbre]
            [clojure.data.json :as json]
            [clojure.core.async :refer [go <!]]
            [clojure.string :as string]
            [qbits.spandex :as elastic]))

(defn fetch-descendant-dois
  "Get all descendant funder ids for a funder."
  [funder-doi]
  (-> (elastic/request
       (conf/get-service :elastic)
       {:method :get
        :url (str (elastic-util/index-url-prefix :funder) (URLEncoder/encode funder-doi "UTF-8"))})
      (get-in [:body :_source :descendant])))

(defn fetch-descendant-work-count [funder-doi]
  ;funders are indexed with the funder id as their database id, they are linked, however, with their
  ;doi (with the funder prefix) so we must turn the doi to an id to do the query
  (let [funder-dois (-> funder-doi
                        doi-id/doi-uri-to-id
                        fetch-descendant-dois
                        (conj funder-doi)
                        ((partial map doi-id/with-funder-prefix)))
        nested-query {:bool
                      {:should
                       (map #(hash-map :term {:funder.doi %}) funder-dois)}}]
    (-> (elastic/request
          (conf/get-service :elastic)
          {:method :get
           :url (str (elastic-util/index-url-prefix :work) "_count")
           :body {:query {:nested {:path :funder :query nested-query}}}})
        (get-in [:body :count]))))

(defn fetch-work-count [funder-doi]
  (-> (elastic/request
        (conf/get-service :elastic)
        {:method :get
         :url (str (elastic-util/index-url-prefix :work) "_count")
         :body (assoc-in
                 {}
                 [:query :nested]
                 {:path :funder
                  :query {:term {:funder.doi (doi-id/with-funder-prefix funder-doi)}}})})
      (get-in [:body :count])))

(defn ->response-doc [funder-doc]
  {:id          (:id funder-doc)
   :location    (:country funder-doc)
   :name        (:primary-name funder-doc)
   :alt-names   (:name funder-doc)
   :uri         (-> funder-doc :doi doi-id/to-doi-uri)
   :replaces    (:replaces funder-doc)
   :replaced-by (:replaced-by funder-doc)
   :tokens      (:token funder-doc)})

(defn- build-hierarchy [funder-doc]
  (util/dissoc-all (:hierarchy funder-doc) [:id :name]))

(defn ->extended-response-doc [funder-doc]
  (let [funder-doi (:id funder-doc)]
    (merge
     (->response-doc funder-doc)
     {:work-count            (fetch-work-count funder-doi)
      :descendant-work-count (fetch-descendant-work-count funder-doi)
      :descendants           (:descendant funder-doc)
      :hierarchy             (build-hierarchy funder-doc)
      :hierarchy-names       (:hierarchy-names funder-doc)})))

(defn fetch-one [funder-id]
  (when (not (string/blank? funder-id))
    (let [response
          (elastic/request
            (conf/get-service :elastic)
            {:method :get
             :url (str (elastic-util/index-url-prefix :funder) (URLEncoder/encode funder-id "UTF-8"))
             :exception-handler (fn [e] (when (not (string/includes? (.getMessage e) "404"))
                                          (throw e)))})]
      (when-let [funder-doc (:_source (:body response))]
        (r/api-response
          :funder
          :content
          (->extended-response-doc funder-doc))))))

(defn fetch-one-async [funder-id respond]
  (when (not (string/blank? funder-id))
    (let [tb (System/currentTimeMillis)
          es-response-ch
          (elastic/request-chan
            (conf/get-service :elastic)
            {:method :get
             :url (str (elastic-util/index-url-prefix :funder) (URLEncoder/encode funder-id "UTF-8"))})]
      (go
        (try
          (let [es-response (<! es-response-ch)
                _ (timbre/info (json/write-str {"retrieve-funder" funder-id
                                                       "type" "es-time"
                                                       "ms" (- (System/currentTimeMillis) tb)}))
                response (if (instance? Exception es-response)
                           es-response
                           (r/api-response
                             :funder
                             :content
                             (->extended-response-doc (:_source (:body es-response)))))]
            (respond response))
          (catch Exception e
            (respond e))))
      ; this is returned to let the handler know that the request is being responded to here
      "ASYNC")))

(defn es-request-with-funder-sort [query-context]
  (let [context-with-sort (assoc query-context :sort [:_score :level :id] :order [:desc :asc :asc])]
    (query/->es-request
      (query/prefix-query-context context-with-sort :all-names-text)
      :url-prefix (elastic-util/index-url-prefix :funder)
      :filters filter/funder-filters)))

(defn fetch
  "Retrieve funders from ES."
  [query-context]
  ;; Stable sorting needs to be applied to funders to make it possible
  ;; to retrieve the entire dataset using offsets, even if there are multiple shards.
  (let [es-request (es-request-with-funder-sort query-context)
        response (elastic/request (conf/get-service :elastic) es-request)
        docs (->> (get-in response [:body :hits :hits]) (map :_source))]
    (-> (r/api-response :funder-list)
        (r/with-query-context-info query-context)
        (r/with-debug-info query-context es-request)
        (r/with-result-items
          (get-in response [:body :hits :total])
          (map ->response-doc docs)
          :next-cursor (get-in response [:body :_scroll_id])))))

(defn fetch-async [query-context respond]
  ;; Stable sorting needs to be applied to funders to make it possible
  ;; to retrieve the entire dataset using offsets, even if there are multiple shards.
  (let [es-request (es-request-with-funder-sort query-context)
        tb (System/currentTimeMillis)
        es-response-ch (elastic/request-chan (conf/get-service :elastic) es-request)]
    (go
      (try
        (let [es-response (<! es-response-ch)
              _ (timbre/info (json/write-str {"request" query-context
                                                     "type" "es-time"
                                                     "ms" (- (System/currentTimeMillis) tb)}))
              docs (->> (get-in es-response [:body :hits :hits]) (map :_source))
              response  (if (instance? Exception es-response)
                          es-response
                           (-> (r/api-response :funder-list)
                               (r/with-query-context-info query-context)
                               (r/with-debug-info query-context es-request)
                               (r/with-result-items
                                 (get-in es-response [:body :hits :total])
                                 (map ->response-doc docs)
                                 :next-cursor (get-in es-response [:body :_scroll_id]))))]
          (respond response))
        (catch Exception e
          (respond e))))
    ; this is returned to let the handler know that the request is being responded to here
    "ASYNC"))

(defn fetch-works
  "for a given set of funders in a funders/works query, fetch all the descendant funders,
   then fetch all works with those funders."
  ([query-context]
   (let [funder-doi  (doi-id/doi-uri-to-id (:id query-context))
         filter-dois (conj (fetch-descendant-dois funder-doi) funder-doi)
         filter-dois-with-prefix (map doi-id/with-funder-prefix filter-dois)
         funder-filters (merge (:filters query-context) {"funder" filter-dois-with-prefix})]
     (work/fetch
       (-> query-context    ;works linking to (including funders linking to funders by realations
           ;funders need their ids to be dois
           (assoc :filters funder-filters)))))
  ([query-context respond]
   ; funders are fetched using their ids
   (let [funder-doi  (doi-id/doi-uri-to-id (:id query-context))
         filter-dois (conj (fetch-descendant-dois funder-doi) funder-doi)
         filter-dois-with-prefix (map doi-id/with-funder-prefix filter-dois)
         funder-filters (merge (:filters query-context) {"funder" filter-dois-with-prefix})]
     (work/fetch-async
       (-> query-context    ;works linking to (including funders linking to funders by realations
           ;funders need their ids to be dois
           (assoc :filters funder-filters))
       respond))))
