(ns cayenne.data.prefix
  (:require [cayenne.conf :as conf]
            [cayenne.api.v1.response :as r]
            [cayenne.api.v1.query :as query]
            [cayenne.data.work :as work]
            [cayenne.ids.member :refer [to-member-id-uri]]
            [cayenne.ids.prefix :refer [to-prefix-uri]]
            [cayenne.elastic.util :as elastic-util]
            [qbits.spandex :as elastic]))

(defn fetch-works
  ([query-context]
   (work/fetch query-context :id-field :owner-prefix))
  ([query-context respond]
   (work/fetch-async query-context respond :id-field :owner-prefix)))

(defn fetch-one [query-context]
  (when (:id query-context)
    (when-let [member-doc (-> (elastic/request
                                (conf/get-service :elastic)
                                (query/->es-request query-context
                                  :id-field :prefix.value
                                  :url-prefix (elastic-util/index-url-prefix :member)))
                              (get-in [:body :hits :hits])
                              first
                              :_source)]
      (let [prefix (->> member-doc
                        :prefix
                        (filter #(= (:value %) (:id query-context)))
                        first)]
        (r/api-response :prefix :content {:member (to-member-id-uri (:id member-doc))
                                          :name (:primary-name member-doc)
                                          :prefix (to-prefix-uri (:value prefix))})))))
