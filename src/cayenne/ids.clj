(ns cayenne.ids
  (:require [cayenne.conf :as conf]
            [clojure.string :as string]))

(defn get-id-uri [id-type id-value]
  (if-let [prefix (conf/get-param [:id (keyword id-type) :path])]
    (str prefix id-value)
    (str (conf/get-param [:id-generic :path]) (name id-type) "/" id-value)))

(defn get-id-non-uri
  "For an ID represented as a URI, remove the path prefix."
  [id-type id-value]
  (let [prefix (conf/get-param [:id id-type :path])]
    (.replace id-value (or prefix "") "")))

(defn matches-type?
  "If the id matches the type, return. Else nil."
  [id-type id-value]
  (let [regex (conf/get-param [:id id-type :regex])]
    (when (re-matches regex id-value)
      id-value)))

(defn get-data-uri [id-type id-value]
  (if-let [prefix (conf/get-param [:id (keyword id-type) :data-path])]
    (str prefix id-value)
    (str (conf/get-param [:id-generic :data-path]) (name id-type) "/" id-value)))

(defn to-supplementary-id-uri [id-value]
  (str (conf/get-param [:id :supplementary :path]) id-value))

(defn extract-supplementary-id [id-uri]
  (when id-uri
    (string/replace id-uri #"\Ahttps?:\/\/id\.crossref\.org\/supp\/" "")))

(defn id-uri-type [id-uri]
  (when id-uri
    (condp #(re-matches %1 %2) id-uri
      (conf/get-param [:id :issn :regex]) :issn
      (conf/get-param [:id :isbn :regex]) :isbn
      (conf/get-param [:id :orcid :regex]) :orcid
      (conf/get-param [:id :owner-prefix :regex]) :owner-prefix
      (conf/get-param [:id :journal-container :regex]) :journal-container
      (conf/get-param [:id :book-container :regex]) :book-container
      (conf/get-param [:id :series-container :regex]) :series-container
      (conf/get-param [:id :supplementary :regex]) :supplementary
      (conf/get-param [:id :contributor :regex]) :contributor
      (conf/get-param [:id :member :regex]) :member
      (conf/get-param [:id :ror :regex]) :ror
      (conf/get-param [:id :isni :regex]) :isni
      (conf/get-param [:id :wikidata :regex]) :wikidata
      (conf/get-param [:id :doi :regex]) :doi
      (conf/get-param [:id :pmid :regex]) :pmid
      (conf/get-param [:id :pmcid :regex]) :pmcid
      (conf/get-param [:id :purl :regex]) :purl
      (conf/get-param [:id :arxiv :regex]) :arxiv
      (conf/get-param [:id :ark :regex]) :ark
      (conf/get-param [:id :handle :regex]) :handle
      (conf/get-param [:id :uuid :regex]) :uuid
      (conf/get-param [:id :ecli :regex]) :ecli
      (conf/get-param [:id :accession :regex]) :accession
      (conf/get-param [:id :other :regex]) :other
      (conf/get-param [:id :uri :regex]) :uri
    :unknown)))
