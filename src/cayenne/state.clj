(ns cayenne.state
  (:require [cayenne.conf :as conf]
            [cayenne.ingest.prefix-storage :as prefix-storage]))

(defn get-cayenne-state [conf-path]
  (let [storage-service (conf/get-param [:s3 :metadata-storage-service])
        bucket (conf/get-param [:s3 :cayenne-state :bucket])
        key (conf/get-param [:s3 :cayenne-state :key])
        object (try
                 (-> storage-service
                     (prefix-storage/file-by-key bucket key)
                     read-string)
                 (catch Exception e {}))]
    (get-in object conf-path)))

(defn set-cayenne-state [conf-path value]
  (let [storage-service (conf/get-param [:s3 :metadata-storage-service])
        bucket (conf/get-param [:s3 :cayenne-state :bucket])
        key (conf/get-param [:s3 :cayenne-state :key])
        object (try
                 (-> storage-service
                     (prefix-storage/file-by-key bucket key)
                     read-string)
                 (catch Exception e {}))
        output-str (-> object
                       (assoc-in conf-path value)
                       prn-str)]
    (prefix-storage/put-object storage-service bucket key output-str)))
