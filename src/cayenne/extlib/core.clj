(ns cayenne.extlib.core
  "Expose Cayenne functionality as a Java library."
  (:require [cayenne.formats.unixsd :as unixsd]
            [cayenne.conf :as conf]
            [cayenne.elastic.convert :as elastic-convert]
            [cayenne.xml :as xml]
            [cayenne.item-tree :as itree]
            [clojure.java.io :as io]
            [clojure.data.json :as json])

  ; For export in extlib
  (:gen-class
    :name org.crossref.cayenne
    :methods [#^{:static true} [parseXml [java.io.Reader java.lang.String] java.lang.Iterable]
              #^{:static true} [parseXmlCentred [java.io.Reader java.lang.String] java.lang.Iterable]
              #^{:static true} [boot [] void]
              #^{:static true} [esDocToCiteproc [java.lang.Object] java.lang.Object]
              #^{:static true} [itemToEsDoc [java.lang.Object] java.lang.Object]
              #^{:static true} [writeJsonString [java.lang.Object] java.lang.String]
              #^{:static true} [writeJsonWriter [java.lang.Object java.io.Writer] void]]))

(defn -parseXml
  "Parse XML, returning a list of [primary-identifier, item-tree] pairs. Note that the primary-identifier 
   may be null depending on whether the XML came from UNIXSD. The getDepositedDois function can find all
   relevant DOIs in the whole deposit.
   Root node is the same as it would be for the XML, e.g. the Journal.
   elementName is the root element to look for, i.e. 'query_result'.
   Don't close the reader (which allows it to be called on e.g. a non-file reader)."
  [rdr elementName]
  (let [xml-reader (io/reader rdr)
        elements (xml/get-elements xml-reader elementName)
        parsed (map unixsd/unixsd-record-parser elements)]
    parsed))

(defn stringify-dates [_ value]
  (if (instance? org.joda.time.DateTime value) (str value) value))

(defn roundtrip-json
    "Round-trip via JSON to ensure that the file has no clojure-specific types 
    (e.g. org.joda.time.DateTime) that can't be serialized to JSON by external code."
    [input]
    (-> 
        input
        (json/write-str :value-fn stringify-dates)
        (json/read-str :key-fn keyword)))

(defn -parseXmlCentred
  "Parse XML, returning a list of item-trees centred on the primary identifier item to make it
   the root node of the tree.
   elementName is the root element to look for, i.e. 'query_result'.
   Don't close the reader (which allows it to be called on e.g. a non-file reader)."
  [rdr elementName]
  (let [xml-reader (io/reader rdr)
        elements (xml/get-elements xml-reader elementName)]
    (map #(as-> % $
            (unixsd/unixsd-record-parser $)
			(conj $ :filters {:type :work})
            (apply itree/centre-on $)
            (roundtrip-json $))
        elements)))


(defn -esDocToCiteproc
    "Directly call the transform function to build an Elastic Search indexable document from an Item Tree.
    The Elastic Search document is really a wrapper with some extra fields for indexing."
    [elasticsearchDoc]
    (-> ; Simulate it coming back from Elastic with a :_source.
        {:_source elasticsearchDoc}        
        (roundtrip-json)
        (elastic-convert/es-doc->citeproc)))

(defn -itemToEsDoc
    "Directly call the transform function to build a Citeproc-JSON document from an Elastic Search document."
    [item]
    (elastic-convert/item->es-doc item))

(defn -writeJsonString
    "Serialize to JSON String"
    [obj]
    (json/write-str obj :value-fn stringify-dates))

(defn -writeJsonWriter
    "Serialize JSON to writer."
    [obj writer]
    (json/write obj writer :value-fn stringify-dates))

(defn -boot
    "Put Cayenne in the expected configuration state. Register identifier patterns."
    []
    ; Compare with cayenne.identifier-defaults. These ones use HTTPS and the correct current DOI resolver.
    (conf/with-core :identifier-defaults
        (conf/set-param! [:id :issn :path] "https://id.crossref.org/issn/")
        (conf/set-param! [:id :isbn :path] "https://id.crossref.org/isbn/")
        (conf/set-param! [:id :orcid :path] "https://orcid.org/")
        (conf/set-param! [:id :owner-prefix :path] "https://id.crossref.org/prefix/")
        (conf/set-param! [:id :journal-container :path] "https://id.crossref.org/container/J")
        (conf/set-param! [:id :book-container :path] "https://id.crossref.org/container/B")
        (conf/set-param! [:id :series-container :path] "https://id.crossref.org/container/S")
        (conf/set-param! [:id :doi :path] "https://doi.org/")
        (conf/set-param! [:id :supplementary :path] "https://id.crossref.org/supp/")
        (conf/set-param! [:id :contributor :path] "https://id.crossref.org/contributor/")
        (conf/set-param! [:id :member :path] "https://id.crossref.org/member/")
        (conf/set-param! [:id :ror :path] "https://ror.org/")
        (conf/set-param! [:id :isni :path] "https://www.isni.org/")
        (conf/set-param! [:id :wikidata :path] "https://www.wikidata.org/entity/")

        (conf/set-param! [:id-generic :path] "https://id.crossref.org/")
        (conf/set-param! [:id-generic :data-path] "https://data.crossref.org/")
        
        (conf/set-param! [:id :uri :path] "")

        (conf/set-param! [:id :pmid :path] "https://www.ncbi.nlm.nih.gov/pubmed/")
        (conf/set-param! [:id :pmcid :path] "https://europepmc.org/articles/")
        (conf/set-param! [:id :purl :path] "https://purl.org/")
        (conf/set-param! [:id :arxiv :path] "https://arxiv.org/abs/")
        (conf/set-param! [:id :ark :path] "https://n2t.net/ark:")
        (conf/set-param! [:id :handle :path] "https://hdl.handle.net/")
        (conf/set-param! [:id :uuid :path] "http://id.crossref.org/uuid/")
        (conf/set-param! [:id :ecli :path] "http://id.crossref.org/ecli/")
        (conf/set-param! [:id :accession :path] "http://id.crossref.org/accession/")
        (conf/set-param! [:id :other :path] "http://id.crossref.org/other/"))

    (conf/set-core! :identifier-defaults))

