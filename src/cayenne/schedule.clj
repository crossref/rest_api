(ns cayenne.schedule
  (:require [cayenne.api.v1.feed :refer [delete-processed-feed-files]]
            [cayenne.conf :as conf]
            [cayenne.state :as state]
            [cayenne.tasks.funder :as funder]
            [cayenne.tasks.calculated-data :as calc]
            [cayenne.tasks.member :as member]
            [cayenne.tasks.coverage :as coverage]
            [cayenne.tasks.journal :as journal]
            [cayenne.tasks.retraction-watch :as rw]
            [clj-time.core :as time]
            [clj-time.format :as timef]
            [clj-time.coerce :as timec]
            [org.httpkit.client :as http]
            [clojurewerkz.quartzite.scheduler :as qs]
            [clojurewerkz.quartzite.triggers :as qt]
            [clojurewerkz.quartzite.jobs :as qj :refer [defjob]]
            [clojurewerkz.quartzite.schedule.cron :as cron]
            [taoensso.timbre :as timbre :refer [info error]]))

;; This file defines scheduled tasks, in order to create a new task you need to use the macro
;; define-task, that will require the following map:
;; {:id "name"    ->  This will be the name of the task 
;;  :cron-config  Str/Vector -> You can pass either a cron string or a vector path for the config in defaults
;;  :code        ->  The code to be executed
;; }
;; In order to enable the task, you must add the name in the lein-run command line, eg:
;; lein run with-profile dev :api :deposit-api :task-name

(def last-modified-format (timef/formatter "EEE, dd MMM YYYY HH:mm:ss zz"))

(defmacro define-task [{:keys [id cron-config code info error-msg]}]
  (let [trigger (symbol (str id "-trigger"))
        idsymbol (symbol id)
        startfn (symbol (str "start-" id))]
    `(do
       (defonce ~trigger
         (delay
           (do
             (qt/build
               (qt/with-identity (qt/key ~id))
               (qt/with-schedule
                 (cron/schedule
                   (cron/cron-schedule
                    ~(if (vector? cron-config)
                       `(conf/get-param ~cron-config)
                       cron-config))))))))

       (defjob ~idsymbol [~'ctx]
         ~code)

       (defn ~startfn []
         (qs/schedule
          (qj/build
            (qj/of-type ~idsymbol)
            (qj/with-identity (qj/key ~id)))
          (deref ~trigger)))

       (conf/with-core :default
         (conf/add-startup-task
          ~(keyword id)
          (fn [~'_]
            (~startfn)))))))

(define-task
  {:id "retraction-watch-fetcher"
   :cron-config  [:update :schedule :retraction-watch]
   :code (try
           (info "Updating retraction watch information")
           (rw/process-retraction-watch-git-repo)
           (catch Exception e
             (error "Failed to process retraction watch git repository" e)))})

(define-task
  {:id "process-calculated-data"
   :cron-config [:update :schedule :calculated-data]
   :code (try
           (info "Processing calculated data updates")
           (calc/process-calculated-data)
           (catch Exception e (error e "Failed to update calculated data")))})

(define-task
  {:id "update-members"
   :cron-config [:update :schedule :members]
   :code (do
           (try
             (info "Updating members collection")
             (member/index-members)
             (info "Updating members collection done")
             (catch Exception e (error e "Failed to update members collection")))
           (Thread/sleep (* 5 60 1000))
           (try
             (info "Updating member flags and coverage values")
             (coverage/check-members)
             (info "Updating member flags and coverage values done")
             (catch Exception e (error e "Failed to update member flags and coverage values"))))})

(define-task
  {:id "update-journals"
   :cron-config [:update :schedule :journals]
   :code (do
           (try
             (info "Updating journals collection")
             (journal/index-journals)
             (info "Updating journals collection done")
             (catch Exception e (error e "Failed to update journals collection")))
           (Thread/sleep (* 5 60 1000))
           (try
             (info "Updating journal flags and coverage values")
             (coverage/check-journals)
             (info "Updating journal flags and coverage values done")
             (catch Exception e (error e "Failed to update journal flags and coverage values"))))})

(define-task
  {:id "update-funders"
   :cron-config [:update :schedule :funders]
   :code (try
           (info "Updating funders from new RDF")
           (let [current-etag (-> @(http/head (conf/get-param [:location :cr-funder-registry]))
                                  :headers :etag)
                 last-known-etag (state/get-cayenne-state [:funders :etag])]
             (info "Registry funders last modified etag " current-etag)
             (info "API funders last modified etag " last-known-etag)
             (if (not (= current-etag last-known-etag))
               (do
                 (funder/index-funders)
                 (state/set-cayenne-state [:funders :etag] current-etag)
                 (info "Updating funders done"))
               (info "Funders are up to date")))
           (catch Exception e (error e "Failed to update funders from RDF")))})

(define-task
  {:id "cleanup-feed-files"
   :cron-config "0 10 * * * ?"
   :code (delete-processed-feed-files 120)})

(defn start []
  (qs/initialize)
  (qs/start))
