(ns cayenne.item-tree-viewer
  (:gen-class :main true)
  (:require [cayenne.defaults]
            [cheshire.core :as json]
            [cayenne.formats.unixsd :as unixsd]
            [cayenne.xml :as xml]
            [clojure.string :as str]
            [clojure.java.io :as io]))

(defn dot-node-label [node]
  (if (instance? java.util.Map node)
    (let [prop-keys (-> node
                        (dissoc :rel)
                        (dissoc :dot-label)
                        keys)
          prop-keys (as-> prop-keys $
                          (if (some #{:subtype} $)
                            (cons :subtype (remove #{:subtype} $))
                            $)
                          (if (some #{:type} $)
                            (cons :type (remove #{:type} $))
                            $)
                          (remove #{:id} $)
                          (cons :id $))
          prop-vals (map #(-> (json/generate-string (% node) {:pretty true})
                              (str/replace "{" "\\{")
                              (str/replace "}" "\\}")) prop-keys)]
      (-> (str "{" (str/join "|" prop-keys) "}|{" (str/join "|" prop-vals) "}")
          (str/replace "\"" "")
          (str/replace "\n" " ")
          (str/replace "<" "\\<")
          (str/replace ">" "\\>")))
    node))

(defn dot-node-name [node]
  (str "rec" (or (:dot-label node) (str "lit" node))))

(defn enquote [s]
  (str "\"" s "\""))

(defn dot-node-stmt [name label]
  (str (enquote name) " [shape=record label=" (enquote label) "]\n"))

(defn dot-rel-stmt [subject-name object-name rel-label]
  (str (enquote subject-name) " -> " (enquote object-name) " [label=" (enquote rel-label) "]\n"))

(defn children-rel-data [node]
  (reduce-kv
    (fn [rel-data rel objects]
      (concat rel-data (map
                         #(hash-map
                            :rel rel
                            :object-name (dot-node-name %)
                            :object-label (dot-node-label %))
                         objects)))
    []
    (:rel node)))

(defn children-dot-stmts [node]
  (let [parent-node-name (dot-node-name node)
        parent-node-label (dot-node-label node)
        stmts (->> node
                   children-rel-data
                   (mapcat #(let [rel (:rel %)
                                  child-object-name (:object-name %)
                                  child-object-label (:object-label %)]
                              [(dot-node-stmt child-object-name child-object-label)
                               (dot-rel-stmt parent-node-name child-object-name rel)])))]
    (if (= 1 (:dot-label node))
      (concat [(dot-node-stmt parent-node-name parent-node-label)] stmts)
      stmts)))

(defn dot-graph [itree]
  (->> itree
       :rel
       vals
       flatten
       (mapcat dot-graph)
       (concat (children-dot-stmts itree))))

(defn remove-nulls [node]
  (if (instance? java.util.Map node)
    (reduce-kv
      (fn [m k v] (cond
                    (nil? v) m
                    (instance? java.util.Map v) (assoc m k (remove-nulls v))
                    (instance? java.util.Collection v) (assoc m k (map remove-nulls v))
                    :else (assoc m k v)))
      {}
      node)
    node))

(defn trim-strings [n node]
  (cond
    (instance? String node) (subs node 0 (min n (count node)))
    (instance? java.util.Collection node) (map (partial trim-strings n) node)
    (instance? java.util.Map node) (reduce-kv
                                     (fn [m k v] (if (= :id k)
                                                   (assoc m k v)
                                                   (assoc m k (trim-strings n v))))
                                     {}
                                     node)
    :else node))

(defn trim-rels [n node]
  (if (:rel node)
    (let [rel-keys (keys (:rel node))
          rel-values (map
                       (fn [k] (->> node
                                    :rel
                                    k
                                    (take n)
                                    (map (partial trim-rels n))))
                       rel-keys)]
      (assoc node :rel (apply hash-map (mapcat vector rel-keys rel-values))))
    node))

(defn prepare-itree [itree remove-nulls-f trim-strings-f prune-rels-f]
  (cond->> itree
           remove-nulls-f remove-nulls
           trim-strings-f (trim-strings trim-strings-f)
           prune-rels-f (trim-rels prune-rels-f)))

(defn add-dot-labels [node i]
  (if (instance? java.util.Map node)
    (let [add-rel-labels (fn [rels]
                           (reduce-kv
                             (fn [m k v] (assoc m k (map #(add-dot-labels % i) v)))
                             {}
                             rels))]
      (-> node
          (assoc :dot-label (swap! i inc))
          (update :rel add-rel-labels)))
    node))

(defn dot-data [rdr & {:keys [remove-nulls-f trim-strings-f prune-rels-f]
                       :or {remove-nulls-f true
                            trim-strings-f 50
                            prune-rels-f 2}}]
  (let [itree (-> rdr
                  (xml/process-xml "crossref_result" identity)
                  (.getRootElement)
                  unixsd/unixsd-record-parser
                  second
                  (prepare-itree remove-nulls-f trim-strings-f prune-rels-f))]
    (->> itree
         (#(add-dot-labels % (atom 0)))
         dot-graph
         (str/join "")
         (#(str "digraph { " % " }")))))

(defn run-dot [filename]
  (let [p (.exec (Runtime/getRuntime) (into-array ["dot" "-Tjpg" "-O" filename]))]
    (.waitFor p)))

(defn -main []
  (let [itree-graph (with-open [rdr (io/reader (System/getenv "FILE"))]
                      (dot-data
                        rdr
                        :remove-nulls-f (= "1" (or (System/getenv "REMOVE_NULLS") "1"))
                        :trim-string-f (Integer/parseInt (System/getenv "TRIM_STRINGS"))
                        :prune-rels-f (Integer/parseInt (System/getenv "PRUNE_RELS"))))
        dot-file (str (System/getenv "FILE") ".dot")]
    (spit dot-file itree-graph)
    (run-dot dot-file)))


