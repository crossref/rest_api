(ns cayenne.defaults
  (:import [java.util.concurrent Executors])
  (:require [cayenne.version]
            [cayenne.ingest.s3-storage :as s3-storage]
            [cayenne.conf :as conf]
            [cayenne.identifier-defaults]
            [cayenne.elastic.util :as es-util]
            [clj-http.conn-mgr :as conn]
            [clj-http.client :as http]
            [nrepl.server :as nrepl]
            [clojure.string :as str]
            [cider.nrepl :refer (cider-nrepl-handler)]))

(defn getenv-int [env-var default]
  (if-let [value (System/getenv env-var)]
    (Integer/parseInt value)
    default))


; The default for Cayenne in REST API is derived from the intrinsic defaults.
(conf/create-core-from! :default :identifier-defaults)

(conf/with-core :default
  (conf/set-param! [:env] :none)
  (conf/set-param! [:status] :stopped)
  (conf/set-param! [:aws :endpoint] (or (System/getenv "AWS_ENDPOINT") "http://localhost:4556"))
  (conf/set-param! [:version] cayenne.version/version)
  (conf/set-param! [:dir :home] (System/getProperty "user.dir"))
  (conf/set-param! [:dir :data] (str (conf/get-param [:dir :home]) "/data"))
  (conf/set-param! [:dir :test-data] (str (conf/get-param [:dir :home]) "/test-data"))
  (conf/set-param! [:dir :tmp] (str (conf/get-param [:dir :home]) "/tmp"))

  (conf/set-param! [:s3 :metadata-storage-service] (apply s3-storage/->S3
                                                          (if (System/getenv "METADATA_LOCAL_STORAGE")
                                                            [(conf/get-param [:aws :endpoint])
                                                             {:path-style-access-enabled true}]
                                                            [nil nil])))

  (conf/set-param! [:s3 :metadata-bucket] (System/getenv "METADATA_BUCKET"))
  (conf/set-param! [:s3 :dois] (if (System/getenv "UPDATE_DOIS")
                                 (str/split (System/getenv "UPDATE_DOIS") #",")
                                 []))
  (conf/set-param! [:s3 :prefixes] (if (System/getenv "UPDATE_S3_PREFIXES")
                                     (str/split (System/getenv "UPDATE_S3_PREFIXES") #",")
                                     []))

  (conf/set-param! [:s3 :snapshot-storage-service] (apply s3-storage/->S3
                                                          (if (System/getenv "SNAPSHOT_LOCAL_STORAGE")
                                                            [(conf/get-param [:aws :endpoint])
                                                             {:path-style-access-enabled true}]
                                                            [nil nil])))

  (conf/set-param! [:s3 :snapshot-bucket] (System/getenv "SNAPSHOT_BUCKET"))
  (conf/set-param! [:s3 :snapshot-bucket-ttl-mins] (or (System/getenv "SNAPSHOT_BUCKET_TTL_MINS") 30))
  (conf/set-param! [:s3 :snapshot-bucket-file-expiry-mins] (or (System/getenv "SNAPSHOT_BUCKET_FILE_EXPIRY_MINS") 10))
  (conf/set-param! [:s3 :cayenne-state :bucket] (System/getenv "CAYENNE_STATE_S3_BUCKET"))
  (conf/set-param! [:s3 :cayenne-state :key] (System/getenv "CAYENNE_STATE_S3_KEY"))

  (conf/set-param! [:service :api-key-auth] (if (System/getenv "API_KEY_AUTH") true false))

  (conf/set-param! [:service :keycloak :url] (or (System/getenv "KEYCLOAK_URL") "http://keycloak:8888/auth"))
  (conf/set-param! [:service :keycloak :client-secret] (or (System/getenv "KEYCLOAK_CLIENT_SECRET") "**********"))
  (conf/set-param! [:service :keycloak :api-key-salt] (or (System/getenv "KEYCLOAK_APIKEY_SALT") ""))
  (conf/set-param! [:service :keycloak :realm] "crossref")
  (conf/set-param! [:service :keycloak :client] "crossref-resources")

  (conf/set-param! [:service :sqs :url] (System/getenv "SQS_QUEUE_URL"))

  (conf/set-param! [:service :elastic :number_of_shards :default] (getenv-int "ELASTICSEARCH_SHARDS" 1))
  (conf/set-param! [:service :elastic :number_of_shards :work] (getenv-int "ELASTICSEARCH_SHARDS_WORK" 6))
  (conf/set-param! [:service :elastic :number_of_replicas :default] (getenv-int "ELASTICSEARCH_REPLICAS" 0))
  (conf/set-param! [:service :elastic :max_result_window :default] (getenv-int "ELASTICSEARCH_RESULT_WINDOW" 100000))
  (conf/set-param! [:service :elastic :max_result_window :work] (getenv-int "ELASTICSEARCH_RESULT_WINDOW_WORK" 10000))
  (conf/set-param! [:service :elastic :refresh_interval :default] (or (System/getenv "ELASTICSEARCH_REFRESH_INTERVAL") "30s"))
  (conf/set-param! [:service :elastic :search :default] {:slowlog {:threshold {:query {:warn "5s" :info "2s"}}}})
  
  (conf/set-param! [:service :elastic :index :work] (or (System/getenv "ELASTICSEARCH_INDEX_WORK") "work"))
  (conf/set-param! [:service :elastic :index :member] (or (System/getenv "ELASTICSEARCH_INDEX_MEMBER") "member"))
  (conf/set-param! [:service :elastic :index :journal] (or (System/getenv "ELASTICSEARCH_INDEX_JOURNAL") "journal"))
  (conf/set-param! [:service :elastic :index :funder] (or (System/getenv "ELASTICSEARCH_INDEX_FUNDER") "funder"))
  (conf/set-param! [:service :elastic :index :subject] (or (System/getenv "ELASTICSEARCH_INDEX_SUBJECT") "subject"))
  (conf/set-param! [:service :elastic :index :coverage] (or (System/getenv "ELASTICSEARCH_INDEX_COVERAGE") "coverage"))
  (conf/set-param! [:service :elastic :index :relupdates] (or (System/getenv "ELASTICSEARCH_INDEX_RELUPDATES") "relupdates"))
  
  (conf/set-param! [:service :elastic :urls] [(or (System/getenv "ELASTICSEARCH_URL") "http://elasticsearch:9200")])
  (conf/set-param! [:service :elastic :no-auth] (if (System/getenv "ELASTICSEARCH_NO_AUTH") true false))
  
  (conf/set-param! [:service :api :port] (getenv-int "API_PORT" 3000))
  (conf/set-param! [:service :queue :host] "5.9.51.150")
  (conf/set-param! [:service :logstash :host] "5.9.51.2")
  (conf/set-param! [:service :logstash :port] 4444)
  (conf/set-param! [:service :logstash :name] "cayenne-api")
  (conf/set-param! [:service :nrepl :port] (getenv-int "NREPL_PORT" 7888))
  (conf/set-param! [:service :nrepl :host] (if (System/getenv "NREPL_DOCKER") "0.0.0.0" "127.0.0.1"))
  
  (conf/set-param! [:update :schedule :members] (or (System/getenv "UPDATE_SCHEDULE_MEMBERS") "0 0 23 ? * *"))
  (conf/set-param! [:update :schedule :journals] (or (System/getenv "UPDATE_SCHEDULE_JOURNALS") "0 0 0 ? * *"))
  (conf/set-param! [:update :schedule :funders] (or (System/getenv "UPDATE_SCHEDULE_FUNDERS") "0 0 * * * ?"))
  (conf/set-param! [:update :schedule :retraction-watch] (or (System/getenv "UPDATE_RETRACTION_WATCH") "0 0 * ? * *"))
  (conf/set-param! [:update :schedule :retraction-watch-url]
                   (or (System/getenv "RETRACTION_WATCH_GIT_URL")
                       "https://gitlab.com/api/v4/projects/crossref%2fretraction-watch-data/repository/"))
  (conf/set-param! [:update :schedule :calculated-data] (or (System/getenv "UPDATE_SCHEDULE_CALCULATEDATA") "0 0/5 * ? * *"))

  (conf/set-param! [:update :calculated-data :batch-size] (getenv-int "UPDATE_CALCULATEDATA_BATCHSIZE" 10000))
  
  (conf/set-param! [:res :tld-list] "tlds.txt")
  (conf/set-param! [:res :funders] "funders.csv")
  (conf/set-param! [:res :locales] "locales.edn")
  (conf/set-param! [:res :styles] "styles.edn")
  (conf/set-param! [:res :tokens] "tokens.edn")
  
  (conf/set-param! [:location :cr-titles-csv] "http://ftp.crossref.org/titlelist/titleFile.csv")
  (conf/set-param! [:location :cr-funder-registry] "https://gitlab.com/crossref/open_funder_registry/-/raw/main/registry.rdf")
  
  (conf/set-param! [:test :doi] "10.5555/12345678")
  
  (conf/set-param! [:upstream :pdf-service] "http://46.4.83.72:3000/pdf")
  (conf/set-param! [:upstream :doi-url] "http://doi.crossref.org/search/doi?pid=cnproxy@crossref.org&format=unixsd&doi=")
  (conf/set-param! [:upstream :doi-ra-url] "https://doi.org/doiRA/")
  (conf/set-param! [:upstream :unixref-url] "http://doi.crossref.org/search/doi?pid=cnproxy@crossref.org&format=unixref&doi=")
  (conf/set-param! [:upstream :unixsd-url] "http://doi.crossref.org/search/doi?pid=cnproxy@crossref.org&format=unixsd&doi=")
  (conf/set-param! [:upstream :prefix-info-url] "http://doi.crossref.org/getPrefixPublisher/?prefix=")
  (conf/set-param! [:upstream :crossref-auth] "https://doi.crossref.org/info")
  (conf/set-param! [:upstream :crossref-test-auth] "http://test.crossref.org/info")
  
  (conf/set-param! [:val :feed-concurrency] (- (.. Runtime getRuntime availableProcessors) 1))
  (conf/set-param! [:val :indexing-timeout] (getenv-int "INDEXING_TIMEOUT" 30))


  (conf/set-param! [:log :level] :debug)
  (conf/set-param! [:sentry :dsn] (or (System/getenv "SENTRY_DSN") ""))
  (conf/set-param! [:sentry :env] (or (System/getenv "ENV") "staging")))

(conf/with-core :default
  (conf/add-startup-task
   :base
   (fn [_]
     (conf/set-service! :executor
                        (Executors/newScheduledThreadPool 40))
     (conf/set-service! :conn-mgr
                        (conn/make-reusable-conn-manager {:timeout 120 :threads 10}))

     (when-let [ecs-metadata-uri (System/getenv "ECS_CONTAINER_METADATA_URI_V4")]
       (let [task-info (-> ecs-metadata-uri
                           (str "/task")
                           (http/get {:connection-manager (conf/get-service :conn-mgr)
                                      :throw-exceptions false
                                      :as :json})
                           :body)]
         (conf/set-param! [:sentry :server] (:Family task-info))
         (conf/set-param! [:sentry :task] (:TaskARN task-info))
         (conf/set-param! [:debug :task-metadata] (select-keys task-info [:Cluster :TaskARN :Family :Revision]))))
     
     (conf/set-service! :elastic (es-util/es-client)))))

(conf/with-core :default
  (conf/add-startup-task
   :nrepl
   (fn [_]
     (conf/set-service!
      :nrepl
      (nrepl/start-server :bind (conf/get-param [:service :nrepl :host])
                          :port (conf/get-param [:service :nrepl :port])
                          :handler cider-nrepl-handler)))))

(conf/set-core! :default)
