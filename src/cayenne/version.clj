(ns cayenne.version)

(def version (or (System/getProperty "cayenne.version") "UNVERSIONED"))
(assert version "Failed to detect version.")

