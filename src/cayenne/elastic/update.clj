(ns cayenne.elastic.update
  (:require [clj-time.core :as dt]
            [qbits.spandex :as elastic]
            [cayenne.conf :as conf]
            [cayenne.elastic.util :as elastic-util]))

(defn update-referenced-by-count-command [subject-doi count]
  {:work
   [{:update {:_id subject-doi}}
    {:doc {:indexed (dt/now) :is-referenced-by-count count}}]
   :updates '()})

(defn update-reference-doi-command [subject-doi _ _]
  {:work [{:update {:_id subject-doi}}
          {:doc {}}]
   :updates '()})

(defn index-updates [update-commands & {:keys [error-msg]
                                        :or {error-msg "Elasticsearch work update failed"}}]
  (let [tries (atom 5)]
    (elastic-util/with-retry
      {:sleep 10000
       :decay :double
       :error-hook (partial elastic-util/retry-error-hook tries error-msg)}
      error-msg
      (elastic/request
        (conf/get-service :elastic)
        {:method :post
         :url (str (elastic-util/index-url-prefix :work) "_bulk")
         :body (-> update-commands flatten elastic-util/raw-jsons)}))))
