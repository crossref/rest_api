(ns cayenne.elastic.mappings
  (:require [qbits.spandex :as elastic]
            [cayenne.conf :as conf]
            [cayenne.sentry :as sentry]
            [cayenne.elastic.util :as es-util]
            [taoensso.timbre :as timbre :refer [info error]]
            [slingshot.slingshot :refer [try+]]))

(def affiliation-properties
  {:ror-id     {:type "keyword" :copy_to :institution-ror-id}
   :isni       {:type "keyword"}
   :wikidata   {:type "keyword"}
   :name       {:type "keyword"}
   :country    {:type "keyword"}
   :acronym    {:type "keyword"}
   :place      {:type "text"}
   :department {:type "text"}})

(def contributor-properties
  {:contribution        {:type "keyword"}
   :given-name          {:type "text"}
   :family-name         {:type "text"}
   :alternate-name      {:type "text"}
   :org-name            {:type "text"}
   :prefix              {:type "text"}
   :suffix              {:type "text"}
   :sequence            {:type "text"}
   :orcid               {:type "keyword" :copy_to :contributor-orcid}
   :affiliation         {:type "keyword" :copy_to [:contributor-affiliation :affiliation-text]}
   :affiliation-struct  {:type "object" :properties affiliation-properties}
   :authenticated-orcid {:type "boolean"}
   :start-date          {:type "date" :format "date_optional_time"}
   :end-date            {:type "date" :format "date_optional_time"}})

(def issn-properties
  {:value {:type "keyword"}
   :type  {:type "keyword"}})

(def isbn-properties
  {:value {:type "keyword"}
   :type  {:type "keyword"}})

(def funder-id-properties
  {:id {:type "keyword"}
   :id-type {:type "keyword"}
   :asserted-by {:type "keyword"}})

(def work-funder-properties
  {:name            {:type "keyword" :copy_to [:funder-name :funder-name-text]}
   :doi             {:type "keyword" :copy_to :funder-doi}
   :doi-asserted-by {:type "keyword"}
   :award           {:type "text"} ; deprecated; do not use
   :award-original  {:type "keyword"}
   :award-keyword   {:type "keyword"}
   :id     {:type "nested" :properties funder-id-properties}})

(def update-properties
  {:doi           {:type "keyword"}
   :type          {:type "keyword" :copy_to :update-type}
   :label         {:type "keyword"}
   :source        {:type "keyword"}
   :date          {:type "date"}
   :date-extended {:type "date" :format "date_optional_time"}})

;; Calculated properties is data that did not come from the ingestion of the
;; work itself, but calculated from other sources
(def rw-update-properties
  {:doi           {:type "keyword"}
   :type          {:type "keyword"}
   :label         {:type "keyword"}
   :source        {:type "keyword"}
   :date          {:type "date"}
   :record-id     {:type "keyword"}
   :date-extended {:type "date" :format "date_optional_time"}})

(def alias-doi-refcount
  {:dois {:type "text"}
   :counts {:type "long"}})

(def calculated-properties
  {:updated-by                 {:type "nested" :properties update-properties}
   ; TO BE DELETED after reindexing
   ; :aliases                    {:type "keyword"}
   :rw-updated-by              {:type "nested" :properties rw-update-properties}
   :rw-update-to               {:type "nested" :properties rw-update-properties}
   :alias-dois-refcount        {:type "nested" :properties alias-doi-refcount}})

(def clinical-trial-properties
  {:number   {:type "text"}
   :registry {:type "keyword"}
   :type     {:type "keyword"}})

(def link-properties
  {:content-type {:type "keyword"}
   :url          {:type "keyword"}
   :version      {:type "keyword"}
   :application  {:type "keyword" :copy_to :link-application}})

(def event-properties
  {:name     {:type "text"}
   :theme    {:type "text"}
   :location {:type "text"}
   :sponsor  {:type "text"}
   :acronym  {:type "text"}
   :number   {:type "text"}
   :start    {:type "date" :format "date_optional_time||year_month||year"}
   :end      {:type "date" :format "date_optional_time||year_month||year"}})

(def license-properties
  {:version        {:type "keyword"}
   :url            {:type "keyword" :copy_to :license-url}
   :delay          {:type "long"}
   :start          {:type "date"}
   :start-extended {:type "date" :format "date_optional_time"}})

(def assertion-properties
  {:name            {:type "keyword" :copy_to :assertion-name}
   :label           {:type "text"}
   :group-name      {:type "keyword" :copy_to :assertion-group-name}
   :group-label     {:type "text"}
   :url             {:type "keyword"}
   :value           {:type "text"}
   :order           {:type "integer"}
   :explanation-url {:type "keyword"}})

(def relation-properties
  {:type        {:type "keyword" :copy_to :relation-type}
   :object      {:type "keyword"}
   :object-type {:type "keyword"}
   :object-ns   {:type "keyword"}
   :claimed-by  {:type "keyword"}})

(def reference-properties
  {:key                  {:type "keyword"}
   :doi                  {:type "keyword"}
   :doi-asserted-by      {:type "keyword"}
   :issn                 {:type "keyword" :index false}
   :issn-type            {:type "keyword" :index false}
   :author               {:type "text" :index false}
   :issue                {:type "text" :index false}
   :first-page           {:type "text" :index false}
   :year                 {:type "text" :index false}
   :isbn                 {:type "keyword" :index false}
   :isbn-type            {:type "keyword" :index false}
   :series-title         {:type "text" :index false}
   :volume-title         {:type "text" :index false}
   :edition              {:type "keyword" :index false}
   :component            {:type "keyword" :index false}
   :volume               {:type "keyword" :index false}
   :article-title        {:type "text" :index false}
   :journal-title        {:type "text" :index false}
   :standards-body       {:type "text" :index false}
   :standards-designator {:type "keyword" :index false}
   :unstructured         {:type "text" :index false}})

(def resource-multiple-url
  {:label {:type "keyword"}
   :url {:type "keyword"}})

(def standards-body-properties
  {:name    {:type "text"}
   :acronym {:type "text"}})

(def peer-review-properties
  {:running-number {:type "text" :index false}
   :revision-round {:type "text" :index false}
   :stage {:type "text" :index false}
   :recommendation {:type "text" :index false}
   :competing-interest-statement {:type "text" :index false}
   :type {:type "text" :index false}
   :language {:type "text" :index false}})

(def journal-issue-properties
  {:published-print {:type "date" :format "date_optional_time||year_month||year"}
   :published-online {:type "date" :format "date_optional_time||year_month||year"}
   :issue {:type "text"}})

(def version-info-properties
  {:version {:type "keyword"}
   :language {:type "keyword"}
   :version-description {:type "object" :properties {:description {:type "text"}
                                                     :language {:type "keyword"}}}})

(def title-properties
  {:title    {:type "text"}
   :language {:type "keyword"}})

(def project-description-properties
  {:description   {:type "text"  :copy_to :description-text}
   :language      {:type "keyword"}})

(def posted-status-description-properties
  {:description   {:type "text" :index false}
   :language      {:type "keyword"}})

(def posted-status-properties
  {:updated {:type "date" :format "date_optional_time||year_month||year"}
   :type {:type "keyword"}
   :description {:type "object" :properties posted-status-description-properties}})

(def funding-properties
  {:type                   {:type "keyword"}
   :scheme                 {:type "text"}
   :award-amount           {:type "float"}
   :award-currency         {:type "keyword"}
   :award-percentage       {:type "integer"}
   :funder-name            {:type "keyword" :copy_to [:funder-name :funder-name-text]}
   :funder-doi             {:type "keyword" :copy_to :funder-doi}
   :funder-doi-asserted-by {:type "keyword"}
   :id                     {:type "nested" :properties funder-id-properties}})

(def project-properties
  {:title               {:type "object" :properties title-properties}
   :description         {:type "object" :properties project-description-properties}
   :contributor         {:type "nested" :properties contributor-properties}
   :award-amount        {:type "float"}
   :award-currency      {:type "keyword"}
   :award-start         {:type "date" :format "date_optional_time"}
   :award-end           {:type "date" :format "date_optional_time"}
   :award-planned-start {:type "date" :format "date_optional_time"}
   :award-planned-end   {:type "date" :format "date_optional_time"}
   :funding             {:type "nested" :properties funding-properties}})

(def institution-properties
  {:ror-id     {:type "keyword" :copy_to :institution-ror-id}
   :isni       {:type "keyword"}
   :wikidata   {:type "keyword"}
   :name       {:type "text"}
   :country    {:type "keyword"}
   :acronym    {:type "text"}
   :place      {:type "text"}
   :department {:type "text"}})

(def work-properties
  {:prime-doi                  {:type "keyword"}
   :metadata-content-text      {:type "text"}
   :source                     {:type "keyword" :copy_to :source-text}
   :source-text                {:type "text"}
   :bibliographic-content-text {:type "text"}
   :container-title-text       {:type "text"}
   :group-title-text           {:type "text"}
   :author-text                {:type "text"}
   :editor-text                {:type "text"}
   :chair-text                 {:type "text"}
   :translator-text            {:type "text"}
   :contributor-text           {:type "text"}
   :publisher-text             {:type "text"}
   :publisher-location-text    {:type "text"}
   :degree-text                {:type "text"}
   :affiliation-text           {:type "text"}
   :funder-name-text           {:type "text"}
   :abstract                   {:type "text"}
   :abstract-xml               {:type "text"}
   :type                       {:type "keyword"}
   :subtype                    {:type "keyword"}
   :doi                        {:type "keyword"}
   :prefix                     {:type "keyword"}
   :owner-prefix               {:type "keyword"}
   :member-id                  {:type "integer"}
   :journal-id                 {:type "integer"}
   :book-id                    {:type "integer"}
   :citation-id                {:type "integer"}
   :supplementary-id           {:type "keyword"}
   :issued-year                {:type "integer"}
   :title                      {:type "text"}
   :original-title             {:type "text"}
   :container-title            {:type "keyword" :copy_to :container-title-text}
   :issue-title                {:type "text"}
   :short-container-title      {:type "text"}
   :short-title                {:type "text"}
   :group-title                {:type "keyword" :copy_to :group-title-text}
   :subtitle                   {:type "keyword"}
   :volume                     {:type "keyword"}
   :issue                      {:type "keyword"}
   :first-page                 {:type "keyword"}
   :last-page                  {:type "keyword"}
   :description                {:type "keyword" :copy_to :description-text}
   :description-text           {:type "text"}
   :is-referenced-by-count     {:type "long"}
   :references-count           {:type "long"}
   :article-number             {:type "keyword"}
   :first-deposited            {:type "date"}
   :deposited                  {:type "date"}
   :indexed                    {:type "date"}
   :cayenne-version            {:type "keyword"}
   :issued                     {:type "date" :format "date_optional_time||year_month||year"}
   :published                  {:type "date" :format "date_optional_time||year_month||year"}
   :published-online           {:type "date" :format "date_optional_time||year_month||year"}
   :published-print            {:type "date" :format "date_optional_time||year_month||year"}
   :published-other            {:type "date" :format "date_optional_time||year_month||year"}
   :posted                     {:type "date" :format "date_optional_time||year_month||year"}
   :accepted                   {:type "date" :format "date_optional_time||year_month||year"}
   :content-created            {:type "date" :format "date_optional_time||year_month||year"}
   :content-updated            {:type "date" :format "date_optional_time||year_month||year"}
   :approved                   {:type "date" :format "date_optional_time||year_month||year"}
   :subject                    {:type "keyword"}
   :publication                {:type "keyword"}
   :archive                    {:type "keyword"}
   :publisher                  {:type "keyword" :copy_to :publisher-text}
   :publisher-location         {:type "keyword" :copy_to :publisher-location-text}
   :degree                     {:type "keyword" :copy_to :degree-text}
   :edition-number             {:type "keyword"}
   :part-number                {:type "keyword"}
   :component-number           {:type "keyword"}
   :language                   {:type "text"}
   :update-policy              {:type "keyword"}
   :domain                     {:type "keyword"}
   :domain-exclusive           {:type "boolean"}
   :index-context              {:type "keyword"}
   :funder-name                {:type "keyword"}
   :funder-doi                 {:type "keyword"}
   :contributor-orcid          {:type "keyword"}
   :institution-ror-id         {:type "keyword"}
   :license-url                {:type "keyword"}
   :update-type                {:type "keyword"}
   :relation-type              {:type "keyword"}
   :contributor-affiliation    {:type "keyword"}
   :assertion-name             {:type "keyword"}
   :assertion-group-name       {:type "keyword"}
   :link-application           {:type "keyword"}
   :award                      {:type "text"} ; deprecated; do not use
   :award-original             {:type "keyword"}
   :award-keyword              {:type "keyword"}
   :resource-url               {:type "keyword"}
   :resource-multi-url         {:type "keyword"}
   :resource-multiple-url      {:type "object" :properties resource-multiple-url}
   :award-start                {:type "date" :format "date_optional_time"}
   :standards-body             {:type "object" :properties standards-body-properties}
   :issn                       {:type "object" :properties issn-properties}
   :isbn                       {:type "object" :properties isbn-properties}
   :contributor                {:type "nested" :properties contributor-properties}
   :funder                     {:type "nested" :properties work-funder-properties}
   :calculated                 {:type "nested" :properties calculated-properties}
   :update-to                  {:type "nested" :properties update-properties}
   :clinical-trial             {:type "nested" :properties clinical-trial-properties}
   :event                      {:type "object" :properties event-properties}
   :link                       {:type "nested" :properties link-properties}
   :license                    {:type "nested" :properties license-properties}
   :assertion                  {:type "nested" :properties assertion-properties}
   :relation                   {:type "nested" :properties relation-properties}
   :reference                  {:type "object" :properties reference-properties}
   :peer-review                {:type "object" :properties peer-review-properties}
   :institution                {:type "object" :properties institution-properties}
   :free-to-read               {:type "object"}
   :journal-issue              {:type "object" :properties journal-issue-properties}
   :version-information        {:type "object" :properties version-info-properties}
   :project                    {:type "nested" :properties project-properties}
   :status                     {:type "object" :properties posted-status-properties}})

(def prefix-properties
  {:value                {:type "keyword"}
   :member-id            {:type "integer"}
   :location             {:type "text"}
   :name                 {:type "text"}})

(def member-properties
  {:primary-name {:type "text" :copy_to :suggest}
   :suggest      {:type "completion"}
   :location     {:type "text"}
   :id           {:type "long"}
   :token        {:type "keyword"}
   :prefix       {:type "object" :properties prefix-properties}})

(def funder-properties
  {:doi             {:type "keyword"}
   :suggest         {:type "completion"}
   :id              {:type "keyword"}
   :level           {:type "integer"}
   :parent          {:type "keyword"}
   :ancestor        {:type "keyword"}
   :child           {:type "keyword"}
   :descendant      {:type "keyword"}
   :hierarchy       {:type "object" :enabled false}
   :hierarchy-names {:type "object" :enabled false}
   :affiliated      {:type "keyword"}
   :country         {:type "keyword"}
   :all-names-text  {:type "text"}
   :primary-name    {:type "text" :copy_to :suggest}
   :name            {:type "text" :copy_to :suggest}
   :replaces        {:type "keyword"}
   :replaced-by     {:type "keyword"}
   :token           {:type "keyword"}})

(def subject-properties
  {:high-code   {:type "integer"}
   :code        {:type "integer"}
   :name        {:type "keyword"}})

(def journal-properties
  {:title     {:type "text" :copy_to :suggest}
   :suggest   {:type "completion"}
   :token     {:type "keyword"}
   :id        {:type "long"}
   :doi       {:type "keyword"}
   :publisher {:type "text"}
   :subject   {:type "object" :properties subject-properties}
   :issn      {:type "object" :properties issn-properties}})

(def coverage-properties
  ; Individual exemptions from the index.
  {:subject-type  {:type "keyword"}
   :subject-id    {:type "long"}
   :started       {:type "date"}
   :finished      {:type "date"}
   :total-dois    {:type "long"}
   :backfile-dois {:type "long"}
   :current-dois  {:type "long"}
   ; Breakdowns and coverage are particularly large.
   :breakdowns    {:type "object" :enabled false}
   :coverage      {:type "object" :enabled false}})

(def relations-updates-properties
  {:doi           {:type "keyword"}
   :relation-type {:type "keyword"}
   :relation-info {:type "object" :dynamic false}})

(def index-mappings
  {"work"     {:properties work-properties}
   "member"   {:properties member-properties}
   "funder"   {:properties funder-properties}
   "subject"  {:properties subject-properties}
   "coverage" {:properties coverage-properties}
   "journal"  {:properties journal-properties}
   "relupdates" {:properties relations-updates-properties}}
  )

(defn index-settings [index]
  (->> [:number_of_shards :number_of_replicas :max_result_window :refresh_interval :search]
       (map #(vector % (or (conf/get-param [:service :elastic % (keyword index)])
                           (conf/get-param [:service :elastic % :default]))))
       (into {})))

(def analyzers
  {:analysis
   {:analyzer
    {"bibliographic-stopwords"
     {:type "standard"
      :stopwords ["a" "an" "and" "are" "as" "at" "be" "but" "by" "for" "if" "in" "into"
                  "is" "it" "no" "not" "of" "on" "or" "s" "such" "t" "that" "the" "their"
                  "then" "there" "these" "they" "this" "to" "was" "will" "with" "journal"
                  "proceedings" "vol" "no" "p" "pp"]}}
    :normalizer
    {"keyword-lowercase"
     {:type "custom"
      :filter ["lowercase"]}}}})

(defn client[]
    (elastic/client {:hosts (conf/get-param [:service :elastic :urls])}))

(defn create-indexes
  "Creates an index per top-level document type - in preparation for ES 6+
   compatibility (which will remove multi-type per fields, making
   multiple types per index unworkable.)"
  [conn]
  (doseq [[index-name index-data] index-mappings]
    (info "Ensure Elastic Search index" index-name)

    ; Be tolerant of pre-existing indexes.
    ; If one does already exist, continue to create others.
    (try+
      (elastic/request conn
                       {:url (str "/" (conf/get-param [:service :elastic :index (keyword index-name)]))
                        :method :put
                        :body {:settings (merge (index-settings index-name) analyzers)
                               :mappings {index-name index-data}}})
      (catch #(-> % :body :error :root_cause first :type #{"resource_already_exists_exception"}) _
        (info "Index" index-name "already exists, skipping."))

      (catch qbits.spandex.Response exception
        (error "Error with mappings"
          (-> exception :body :error :root_cause))
        (throw exception))))

  (info "ES indexes:" (elastic/request conn {:url "/_cat/indices" :method :get}))
  (info "ES indexes settings:" (elastic/request conn {:url "/_all/_settings" :method :get})))

(defn update-index-mappings [conn]
  (doseq [[index-name index-data] index-mappings]
    (info "Updating index mappings for" index-name)
    (sentry/with-sentry-reporting
      {}
      (es-util/with-retry
        {:sleep 10000 :tries 5 :decay :double}
        "Elasticsearch index mappings update failed"
        (elastic/request
          conn
          {:url (str "/" (conf/get-param [:service :elastic :index (keyword index-name)]) "/_mapping/" index-name)
           :method :put
           :body index-data})))))

(defn update-index-settings [conn]
  (doseq [[index-name _] index-mappings]
    (info "Updating index settings for" index-name)
    (sentry/with-sentry-reporting
      {}
      (es-util/with-retry
        {:sleep 10000 :tries 5 :decay :double}
        "Elasticsearch index settings update failed"
        (elastic/request
          conn
          {:url (str "/" (conf/get-param [:service :elastic :index (keyword index-name)]) "/_settings")
           :method :put
           :body {:index (dissoc (index-settings index-name) :number_of_shards)}}))))
  (info "ES indexes:" (elastic/request conn {:url "/_cat/indices" :method :get}))
  (info "ES indexes settings:" (elastic/request conn {:url "/_all/_settings" :method :get})))

(defn update-index-analyzers [conn]
  (doseq [[index-name _] index-mappings]
    (info "Updating analyzers for" index-name)
    (elastic/request
      conn
      {:url (str "/" (conf/get-param [:service :elastic :index (keyword index-name)]) "/_close")
       :method :post})
    (sentry/with-sentry-reporting
      {}
      (es-util/with-retry
        {:sleep 10000 :tries 5 :decay :double}
        "Elasticsearch index settings update failed"
        (elastic/request
          conn
          {:url (str "/" (conf/get-param [:service :elastic :index (keyword index-name)]) "/_settings")
           :method :put
           :body {:index analyzers}})))
    (elastic/request
      conn
      {:url (str "/" (conf/get-param [:service :elastic :index (keyword index-name)]) "/_open")
       :method :post}))
  (info "ES indexes:" (elastic/request conn {:url "/_cat/indices" :method :get}))
  (info "ES indexes settings:" (elastic/request conn {:url "/_all/_settings" :method :get})))

(conf/with-core :default
  (conf/add-startup-task
   :create-mappings
   (fn [_]
     (let [conn (es-util/es-client)]
       (create-indexes conn)
       (update-index-mappings conn)))))

(conf/with-core :default
  (conf/add-startup-task
   :update-index-settings
   (fn [_] (update-index-settings (es-util/es-client)))))

(conf/with-core :default
  (conf/add-startup-task
   :update-index-analyzers
   (fn [_] (update-index-analyzers (es-util/es-client)))))
