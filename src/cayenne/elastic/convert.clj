(ns cayenne.elastic.convert
  (:require [cayenne.ids.doi :as doi-id]
            [clojure.set :as set]
            [cayenne.item-tree :as itree]
            [cayenne.util :as util]
            [clj-time.core :as t]
            [cayenne.data.relations]
            [cayenne.ids.issn :as issn-id]
            [cayenne.ids.isbn :as isbn-id]
            [cayenne.ids.prefix :as prefix-id]
            [cayenne.ids.container :as container-id]
            [cayenne.ids.member :as member-id]
            [cayenne.ids.type :as type-id]
            [cayenne.ids.update-type :as update-id]
            [cayenne.ids :as ids]
            [clojure.string :as string]
            [clj-time.format :as tf]
            [clj-time.coerce :as tc]
            [cayenne.dates :refer [particle->date-str particle->date-time particle->year]]
            [cayenne.version :refer [version]]))

(def contributions [:author :chair :editor :translator :contributor
                    :investigator :lead-investigator :co-lead-investigator])

(defn- select-keys-in
  "Like select-keys but allows to execute get-in (nested keys allowed)"
  [m ks]
  (let [ks-lst (map #(if (sequential? %) % [%]) ks)]
    (into {}
          (map #(let [v (get-in m %)]
                  (if (nil? v) nil [(first %) v])) ks-lst))))

(defn- list->vect-recur
  "Makes sure all lists in a complex datastructure become vectors"
  [elem]
  (cond
    (map? elem) (into {} (map #(vector (first %) (list->vect-recur (second %))) elem))
    (sequential? elem) (mapv list->vect-recur elem)
    :default elem))

(defn xml-string->text
  "Accepts a string s containing xml tags 
   and returns a trimmed string without xml tags.
   
   Example: (xml-string->text \"\\<jats:p\\> The value of\\</jats:p\\>\") ->
   \"The value of\""
  [s]
  (-> s (string/replace #"<[^>]+>" "") (string/trim)))

(defn maybe-int [int-as-string]
  (try
    (Integer/parseInt int-as-string)
    (catch NumberFormatException _
      nil)))

(defn item-id [item id-type & {:keys [converter]
                               :or {converter identity}}]
  (when-let [id (-> item (itree/get-item-ids id-type) first)]
    (converter id)))

(defn item-doi [item]
  (item-id item :doi :converter doi-id/extract-doi))

(defn item-issn [item]
  (item-id item :issn :converter issn-id/extract-issn))

(defn item-isbn [item]
  (item-id item :isbn :converter isbn-id/extract-isbn))

(defn item-owner-prefix [item]
  (item-id item :owner-prefix :converter prefix-id/extract-prefix))

(defn item-member-id [item]
  (item-id item :member :converter member-id/extract-member-id))

(defn item-orcid [item]
  (item-id item :orcid))

(defn item-ror-id [item]
  (item-id item :ror))

(defn item-isni [item]
  (item-id item :isni))

(defn item-wikidata [item]
  (item-id item :wikidata))

(defn item-type [item]
  (let [subtype (itree/get-item-subtype item)
        [subsubtype mapping] (cond
                               (= :book subtype) [(:book-type item) itree/book-type-labels]
                               (= :book-component subtype) [(:book-component-type item) itree/book-component-type-labels]
                               :else [subtype itree/subtype-labels])]
    (when-let [i-type (-> subsubtype mapping type-id/->type-id)]
      (name i-type))))

(defn item-version-info [item]
  (let [{{:keys [version language version-description]} :version-info} item]
   (when version
      {:version version
       :language language
       :version-description (mapv #(update-keys % {:value :description :language :language}) version-description)})))

(defn item-issued-date 
  "Collect a set of possible dates, sort them in ascending order and pick the first one (earliest date).
   
   The dates that are collected are: 
   - -> item [:posted :published-print :published-online :published-other :published :content-created]
   - -> item :project :awarded first :started
   - -> item :project :awarded first :planned-started
   
   `as-str`: boolean, if true then return a date-str otherwise return a date particle."
  [item & {as-str :as-str}]
  (let [date (->> [(map item [:posted :published-print :published-online :published-other :published :content-created])
                   (-> item (itree/get-item-rel :awarded) first :started)
                   (map #(-> % (itree/get-item-rel :awarded) first :started) (itree/get-item-rel item :project))
                   (map #(-> % (itree/get-item-rel :awarded) first :planned-started) (itree/get-item-rel item :project))]
                  flatten
                  (remove nil?)
                  (sort-by particle->date-time)
                  first)]
    (if as-str
      (particle->date-str date)
      date)))

(defn item-published-date-str 
  "Collect a set of possible dates, sort them in ascending order and pick the first one (earliest date).
   
   The dates that are collected are: 
   - -> item [:posted :published-print :published-online :published-other :published :content-created]"
  [item]
  (->>  (map item [:posted :published-print :published-online :published-other :published :content-created])
        flatten
        (remove nil?)
        (sort-by particle->date-time)
        first
        particle->date-str))

(defn item-date [item date-rel] 
  (when-let [date (-> item date-rel)]
    (if (vector? date)
      (first date)
      date)))

(defn item-date-str [item date-rel] 
  (when-let [date (-> item date-rel)]
    (if (vector? date)
      (particle->date-str (first date))
      (particle->date-str date))))

(defn item-plain-abstract [item]
  (let [abstract (-> item :abstract first :value)]
    (when (not (string/blank? abstract))
      (xml-string->text abstract))))

(defn item-xml-abstract [item]
  (let [abstract (-> item :abstract first :value)]
    (when (not (string/blank? abstract))
      abstract)))

(defn journal-volume [item]
  (-> item (itree/find-item-of-subtype :journal-volume) :volume))

(defn journal-issue [item]
  (-> item (itree/find-item-of-subtype :journal-issue) :issue))

(defn item-titles
  ([item]
   (mapcat
      #(map :value (% item))
      [:title-secondary :title-original :title-long :title-short :title :title-group]))
  ([item subtype]
     (mapv :value (subtype item))))

(defn item-container-titles [item & {:keys [subtype exclude only]}]
  (let [subtypes (if subtype
                   [subtype]
                   [:title-secondary :title-original :title-long :title-short :title :title-group])
        ancestor (let [ancestor (itree/get-item-rel item :ancestor)]
                   (cond
                     exclude (filter #(not= exclude (:subtype %)) ancestor)
                     only (filter #(= only (:subtype %)) ancestor)
                     :default ancestor))]
    (mapcat
     #(map :value (mapcat % ancestor))
     subtypes)))

(defn item-standards-body [item]
  (when-let [standards-body (-> item
                                (itree/get-tree-rel :standards-body)
                                first)]
    (select-keys-in standards-body [:name
                                    [:acronym 0 :value]])))

(defn item-issns [item]
  (map
   #(hash-map :value (-> % :id first issn-id/extract-issn)
              :type  (:kind %))
   (itree/get-tree-rel item :issn)))

(defn item-isbns [item]
  (cond->> (itree/get-item-rel item :isbn)

    (= :book-component (:subtype item))
    (concat (itree/get-item-rel (itree/find-item-of-subtype item :book) :isbn))

    true
    (map #(hash-map
           :value (-> % :id first isbn-id/extract-isbn)
           :type  (:kind %)))))

(defn item-update-policy [item]
  (when-let [policy (-> item (itree/get-tree-rel :update-policy) first)]
    (-> policy :id first)))

(defn item-institution [institution]
  (let [[object & more] (itree/get-item-rel institution :object)]
        {:ror-id (item-ror-id object)
         :isni (item-isni object)
         :wikidata (item-wikidata object)
         :name (:name institution)
         :country (:country institution)
         :acronym (map :value (:acronym institution))
         :place (map :value (:location institution))
         :department (map :name (itree/get-item-rel institution :component))}))

(defn item-affiliation [institution]
  (let [[object & more] (itree/get-item-rel institution :object)]
    {:ror-id (item-ror-id object)
     :isni (item-isni object)
     :wikidata (item-wikidata object)
     :name (:name institution)
     :country (:country institution)
     :acronym (map :value (:acronym institution))
     :place (map :value (:location institution))
     :department (map :name (itree/get-item-rel institution :component))}))

(defn contributor-affiliations [contributor]
  (map item-affiliation (itree/get-item-rel contributor :affiliation)))

(defn item-contributors 
  [item]
  (mapcat
   (fn [contributor-rel]
     (map
      #(let [[object & more] (itree/get-item-rel % :object)]
         (hash-map
          :contribution        (name contributor-rel)
          :given-name          (:first-name %)
          :family-name         (:last-name %)
          :alternate-name      (:alternate-name %)
          :org-name            (:name %)
          :suffix              (:suffix %)
          :prefix              (:prefix %)
          :orcid               (item-orcid object)
          :authenticated-orcid (:orcid-authenticated object)
          :sequence            (:sequence %)
          :affiliation         (as-> % $
                                 (itree/get-item-rel $ :affiliation)
                                 (map :name $))
          :affiliation-struct  (contributor-affiliations %)
          :start-date          (item-date-str % :started)
          :end-date            (item-date-str % :ended)))
      (itree/get-item-rel item contributor-rel)))
   contributions))

(defn normalise-es-doc-ids
  "Accept a list of ids in the form of {:id ... :id-type ... :asserted-by ...}
   and map each id-type to a predefined transformer function."
  [ids]
  (map  (fn [id]
          (case (:id-type id)
            "DOI" (assoc id :id (item-doi {:id [(doi-id/to-doi-uri (-> id :id first))]}))
            "ROR" (assoc id :id (-> id :id first))
            id))
        ids))

(defn item-funders [item]
  (map
   (fn [funder]
     (let [awards (->> (itree/get-tree-rel funder :awarded)
                       (map itree/get-item-ids)
                       (map first))]
       (-> {:name (:name funder)
            :doi (item-doi (assoc funder :id (map doi-id/to-doi-uri (->> funder :id-assertion (map #(-> % :id first))))))
            :doi-asserted-by (:doi-asserted-by funder)
            :award-original awards
            :award-keyword (map string/lower-case awards)}
           (util/assoc-exists :id (normalise-es-doc-ids (:id-assertion funder))))))
   (itree/get-item-rel item :funder)))

(defn item-clinical-trials [item]
  (let [clinical-trial (map
                        #(hash-map
                          :number   (:ctn %)
                          :registry (:registry %)
                          :type     (:ctn-type %))
                        (itree/get-tree-rel item :clinical-trial-number))]
    (map util/remove-nil-values clinical-trial)))

(defn item-events [item]
  (when-let [event (-> item (itree/get-tree-rel :about) first)]
    (let [wanted-keys [:name
                       :theme
                       [:location 0 :value]
                       [:acronym 0 :value]
                       :number
                       :sponsor]
          start (item-date-str event :start)
          end   (item-date-str event :end)
          event (->
                 (select-keys-in event wanted-keys)
                 (assoc :start start)
                 (assoc :end end))
          cleanedup-event (util/remove-nil-values event)]
      (util/remove-empty-collections cleanedup-event))))

(defn item-links [item]
  (map
   #(hash-map
     :content-type    (:content-type %)
     :url             (:value %)
     :version         (:content-version %)
     :application     (:intended-application %))
   (itree/get-item-rel item :resource-fulltext)))

(defn item-licenses [item]
  (letfn [(difference-in-days [a b]
            (if (or (nil? a) (nil? b) (t/after? a b))
              0
              (-> (t/interval a b)
                  (t/in-days))))]
     (map
      #(let [issued-date (-> (item-issued-date item) :date-time tf/parse)
             start-date (or (-> (item-date % :start) :date-time tf/parse) issued-date)] 
         {:version        (:content-version %)
          :url            (:value %)
          :delay          (difference-in-days issued-date start-date)
          :start-extended start-date})
      (itree/get-tree-rel item :license))))

(defn item-assertions [item]
  (map
   #(-> %
        (select-keys  [:name :label :group-name
                       :group-label :url :explanation-url
                       :value :order])
        (util/assoc-int :order (:order %)))
   (itree/get-tree-rel item :assertion)))

(def relation-types
  (set cayenne.data.relations/relations))



(defn get-relations-schema-relations
  "Filter all relationships on this item, as defined on the 'relations' schema, becuase those ones are special.
   These are relationships asserted as subject (i.e. in the unixml). 
   Extract those and convert into entries as found in the 'relation' field."
  [item]
  (let [rels (filter #(-> % first relation-types) (:rel item))]
    (mapcat (fn [[rel-type objects]]
              (map #(let [obj-id (-> % :id first)
                          typ (ids/id-uri-type obj-id)
                        ; Convert the URI back to a non-uri form, as this is the standard.
                          non-uri (ids/get-id-non-uri typ obj-id)]
                      {:type rel-type
                       :object non-uri
                       :object-type typ
                       :object-ns nil
                       :claimed-by :subject}) objects))
            rels)))

(defn item-posted-status-info [{:keys [updated description type] :as status}]
  (when status
    {:type type
     :updated (particle->date-str updated)
     :description (map #(hash-map :description (:value %) :language (:language %)) description)}))

(defn item-relations [item]
  (map
   #(hash-map
     :type        (-> % :subtype name)
     :object      (:object %)
     :object-type (:object-type %)
     :object-ns   (:object-namespace %)
     :claimed-by  (-> % :claimed-by name))
   (itree/get-tree-rel item :relation)))

(defn item-references [item]
  (let [convert-reference (fn [item-ref]
                            (as-> item-ref $
                              (select-keys $ [:doi :doi-asserted-by :key :issn :issn-type
                                              :isbn :isbn-type :author :volume :issue
                                              :first-page :year :edition :component
                                              :standard-designator :standards-body
                                              :unstructured :article-title :series-title
                                              :volume-title :journal-title :cited-work-type])
                              (if (and (:doi $) (not (string/starts-with? (:doi $) "10."))) (dissoc $ :doi) $)
                              (if (:doi $) $ (dissoc $ :doi-asserted-by))
                              (if (:issn $) $ (dissoc $ :issn-type))
                              (if (:isbn $) $ (dissoc $ :isbn-type))))]
    (as-> item $
      (itree/get-tree-rel $ :cites)
      (map convert-reference $)
      (map util/remove-nil-values $))))

(defn item-update-tos [item] 
  (map
   #(let [update-type (itree/get-item-subtype %)
          update-type-label (update-id/update-label update-type)
          label (cond
                  update-type-label update-type-label
                  (:label %) (:label %)
                  :else update-type)]
      {:doi           (:value %)
       :type          update-type
       :label         label
       :source        "publisher"
       :date-extended (-> % :updated :date-time)})
   (itree/find-items-of-type item :update)))

(defn contributor-name [contributor]
  (or
   (:org-name contributor)
   (str (:given-name contributor) " " (:family-name contributor))))

(defn contributor-initials [contributor]
  (letfn [(initials [first-name]
            (when first-name
              (as-> first-name $
                (string/split $ #"[\s\-]+")
                (map first $)
                (string/join " " $))))]
    (or
     (:org-name contributor)
     (str
      (-> contributor :given-name initials)
      " "
      (:family-name contributor)))))

(defn item-base-content [item]
  (let [published-year (when-let [date (item-date item :published-print)] (particle->year date)) 
        issued-year (when-let [date (item-issued-date item)] (particle->year date))] 
    (->>
     (vector
      issued-year
      published-year
      (journal-issue item)
      (journal-volume item)
      (:first-page item)
      (:last-page item))
     (concat (map :value (item-issns item)))
     (concat (map :value (item-isbns item)))
     (concat (item-titles item))
     (concat (item-container-titles item))
     (string/join " "))))

(defn item-contributor-names [item & {:keys [contribution]}]
  (let [contributors (concat
                      (item-contributors item)
                      (mapcat item-contributors (itree/get-item-rel item :project)))]
    (cond->> contributors
      (not (nil? contribution))
      (filter #(= (name contribution) (:contribution %)))
      :always
      (mapcat #(concat (:alternate-name %) (vector (:given-name %) (:family-name %) (:org-name %)))))))

(defn item-peer-review [item]
  (when-let [{:keys [running-number revision-round stage recommendation
                     competing-interest-statement review-type language]} (:review item)]
    {:running-number running-number
     :revision-round revision-round
     :stage stage
     :recommendation recommendation
     :competing-interest-statement competing-interest-statement
     :type review-type
     :language language}))

(defn item-institutions [item]
  (map item-institution (itree/get-tree-rel item :institution)))

(defn item-journal-issue [journal-issue]
  (when journal-issue
    {:published-print (item-date-str journal-issue :published-print)
     :published-online (item-date-str journal-issue :published-online)
     :issue (:issue journal-issue)
     :numbering (:numbering journal-issue)}))

(defn item-article-number [item]
  (->> item
       :number
       (filter #(= "article-number" (:kind %)))
       (map :value)
       first))

(defn project-titles [project]
  (map
   #(hash-map
      :title    (:value %)
      :language (:language %))
   (:title project)))

(defn project-descriptions [project]
  (->> project
       :description
       (map #(hash-map
              :description (:value %)
              :language    (:language %)))
       (filter (comp not string/blank? :description))))

(defn project-fundings [project] 
  (map
   #(let [funder (-> % (itree/get-item-rel :funder) first)]
      {:type (:subtype %)
       :scheme (:scheme %)
       :award-amount (-> % (itree/get-item-rel :awarded) first :amount)
       :award-currency (-> % (itree/get-item-rel :awarded) first :currency)
       :award-percentage (-> % (itree/get-item-rel :awarded) first :percentage)
       :funder-name (funder :name)
       :funder-doi (->> funder :id-assertion (map :id) first (hash-map :id) item-doi)
       :funder-doi-asserted-by (funder :doi-asserted-by)
       :id (normalise-es-doc-ids (:id-assertion funder))})
   (itree/get-item-rel project :funding)))

(defn item-projects [item]
  (let [project (itree/get-item-rel item :project)] 
    (map
     #(hash-map
       :title               (project-titles %)
       :description         (project-descriptions %)
       :contributor         (item-contributors %)
       :award-amount        (-> % (itree/get-item-rel :awarded) first :amount)
       :award-currency      (-> % (itree/get-item-rel :awarded) first :currency)
       :award-start         (-> % (itree/get-item-rel :awarded) first (item-date-str :started))
       :award-end           (-> % (itree/get-item-rel :awarded) first (item-date-str :ended))
       :award-planned-start (-> % (itree/get-item-rel :awarded) first (item-date-str :planned-started))
       :award-planned-end   (-> % (itree/get-item-rel :awarded) first (item-date-str :planned-ended))
       :funding             (project-fundings %))
     project)))

(defn grant-base-content [item & {:keys [:funders :initials] :or {funders true initials true}}]
  (let [projects (item-projects item)
        award (-> item (itree/get-item-rel :awarded) first)
        titles (->> projects (mapcat :title) (map :title))
        funders (if funders (->> projects (mapcat :funding) (map :funder-name)) [])
        contributors (->> projects
                          (mapcat :contributor)
                          (map (if initials contributor-initials contributor-name)))] 
    (concat
     [(:number award)]
     (when-let [started (item-date award :started)] [(-> started :date-parts first)])
     titles
     funders
     contributors)))

(defn item-bibliographic-content
  "Fields related to bibliographic citation look up"
  [item]
  (string/join
   " "
   (-> [(item-base-content item)]
       (concat (map contributor-initials (item-contributors item)))
       (concat (grant-base-content item)))))

(defn item-metadata-content
  "A default set of search fields"
  [item]
  (string/join
   " "
   (-> [(item-base-content item)]
       (conj (get-in item [:description 0 :value])) 
       (concat (map ids/extract-supplementary-id (:supplementary-id item []))) ; plain supp ids
       (concat (grant-base-content item :funders false :initials false))
       (concat (map contributor-name (item-contributors item))) ; full names
       (concat (mapcat itree/get-item-ids (itree/get-tree-rel item :awarded))) ; grant numbers
       (concat (map :name (itree/get-tree-rel item :funder)))))) ; funder names

(defn item-resource-urls [item subtype]
  (let [items (->> (itree/get-item-rel item :resource-resolution)
                   (filter #(= :resolution (:type %)))
                   (filter #(= subtype (:subtype %))))]
    (if (= subtype :multi)
        (map #(hash-map
               :label (:label %)
               :url (-> % (itree/get-item-rel :object) first :id first)) items)
        (mapcat #(-> % (itree/get-item-rel :object) first :id) items))))



(defn get-journal-number
  [container-rels]
  (first
    (keep #(container-id/journal-uri->number (-> % :id first))
          container-rels)))

(defn get-series-number
  [container-rels]
  (first
    (keep #(container-id/series-uri->number (-> % :id first))
          container-rels)))

(defn get-book-number
  [container-rels]
  (first
    (keep #(container-id/book-uri->number (-> % :id first))
          container-rels)))

(defn get-funder-ror-ids
  "Extract ROR ids from an item's funder, if there is one.
   Checks if :id-type == 'ROR'.
   Returns list of strings or ()."
  [item]
  (->> 
   (itree/get-item-rel item :funder)
   (mapcat :id-assertion)
   (filter #(= "ROR" (:id-type %)))
   (map #(-> % :id first))))

(defn get-grant-ror-ids
  "Extract ROR ids from an item's project:funding entries, if there are any.
   Checks if :id-type == 'ROR'.
   Returns list of strings or ()."
  [item]
  (->>
   (itree/get-item-rel item :project)
   (mapcat #(itree/get-item-rel % :funding))
   (mapcat #(itree/get-item-rel % :funder))
   (mapcat :id-assertion)
   (filter #(= "ROR" (:id-type %)))
   (map #(-> % :id first))))

(defn get-all-ror-ids
  "Return a (string) list of any ROR ids found within funders and/or grants funding."
  [item]
  (concat
   (get-funder-ror-ids item)
   (get-grant-ror-ids item)))

(defn item->es-doc [item] 
 (let [item (list->vect-recur item)]
    (when (not= :pending-publication (itree/get-item-subtype item))
      (let [doi            (item-doi item)
            steward        (-> item (itree/get-tree-rel :steward) first)
            prime-doi      (-> item (itree/get-tree-rel :prime-doi) first)
            container-rels     (-> item (itree/get-tree-rel :container))
            journal-number (get-journal-number container-rels)
            book-number (get-book-number container-rels)

            prefix         (-> item (itree/get-tree-rel :prefix) first)

            ; The chain of containers that originally contained this item.
            ancestors       (itree/get-tree-rel item :ancestor)
            book-metadata   (itree/find-item-of-subtype item :book)
            proceedings-metadata (itree/find-item-of-subtype item :proceedings)

                                        ; This is the <publisher> element from the submitted UNIXML at some level of container.
                                        ; Expect to find this in ancestor containers, but if this is a container then take it from here. 
            publisher      (or
                            (first (keep :publisher ancestors))
                            (:publisher item))
            journal        (itree/find-item-of-subtype item :journal)
            journal-issue  (itree/find-item-of-subtype item :journal-issue)
            journal-volume (itree/find-item-of-subtype item :journal-volume)

                                        ; Data from the "relations" schema in the UNIXSD (i.e. asserted by the subject) are stored as
                                        ; conventional direct relationships in the item graph.
            subject-relations (get-relations-schema-relations item)

                                        ; Relationships that come from the "relations" schema and attached to their object via <crm-items> in 
                                        ; UNIXSD are recorded as relationships to reified items. 
            object-relations (item-relations item)

            all-relations (concat subject-relations object-relations)

            es-doc
            {:doi              doi
             :prime-doi        (item-doi prime-doi)
             :source           "Crossref"
             :type             (item-type item)
             :prefix           (doi-id/extract-doi-prefix doi)
             :owner-prefix     (item-owner-prefix prefix)
             :member-id        (maybe-int (item-member-id steward))
             :journal-id       (maybe-int journal-number)
             :citation-id      (maybe-int (:citation-id item))
             :book-id          (maybe-int book-number)
             :supplementary-id (itree/get-tree-ids item :supplementary)

             :journal-issue    (item-journal-issue journal-issue)
             :issued           (item-issued-date item :as-str true)
                                        ; used for coverage breakdown by year
             :issued-year      (when-let [date (item-issued-date item)] (particle->year date))

             :published        (item-published-date-str item)
             :published-online (item-date-str item :published-online)
             :published-print  (or (item-date-str item :published-print) (item-date-str item :published))
             :published-other  (item-date-str item :published-other)

             :posted           (item-date-str item :posted)
             :accepted         (item-date-str item :accepted)
             :content-created  (or (-> item :content-created first particle->date-str)
                                   (-> ancestors first :content-created first particle->date-str))
             :content-updated  (or (-> item :content-updated first particle->date-str)
                                   (-> ancestors first :content-updated first particle->date-str))
             :approved         (or (-> item :approved first particle->date-str)
                                   (-> ancestors first :approved first particle->date-str))
             :deposited        (-> item :deposited particle->date-time)
             :first-deposited  (or (-> item :first-deposited particle->date-time)
                                   (-> item :deposited particle->date-time))
             :indexed          (tc/to-string (t/now))
             :cayenne-version  version

             :is-referenced-by-count (:cited-count item)
             :references-count       (-> item (itree/get-tree-rel :cites) count)

             :publisher              (or (:name publisher) (:name steward))
             :publisher-location     (get-in publisher [:location 0 :value])

             :title                 (item-titles item :title-long)
             :short-title           (item-titles item :title-short)
             :original-title        (item-titles item :title-original)
             :group-title           (first (item-titles item :title-group))
             :subtitle              (item-titles item :title-secondary)
             :container-title       (item-container-titles item :subtype :title-long :exclude :journal-issue)
             :issue-title           (item-container-titles item :subtype :title-long :only :journal-issue)
             :short-container-title (item-container-titles item :subtype :title-short)

             :first-page         (:first-page item)
             :last-page          (:last-page item)
             :issue              (:issue journal-issue)
             :volume             (or (:volume journal-volume) (:volume proceedings-metadata))
             :description        (util/empty-string->nil (get-in item [:description 0 :value]))
             :article-number     (item-article-number item)
             :degree             (map :value (:degree item))
             :part-number        (:part-number (itree/find-item-of-subtype item :book-set))
             :edition-number     (:edition-number (itree/find-item-of-subtype item :book))
             ;; :component-number
             :language           (or (:language journal) (:language book-metadata))
             :free-to-read       {:start (-> item (itree/get-tree-rel :free-to-read-start) first particle->date-time)
                                  :end (-> item (itree/get-tree-rel :free-to-read-end) first particle->date-time)}

             :resource-url       (item-resource-urls item :primary)
             :resource-multiple-url (item-resource-urls item :multi)
             :update-policy      (item-update-policy item)
             :domain             (:crossmark-domains item)
             :domain-exclusive   (or (:crossmark-domain-exclusive item) false)
             :archive            (map :name (itree/get-tree-rel item :archived-with))

             :abstract              (item-plain-abstract item)
             :abstract-xml          (item-xml-abstract item)
             :version-info       (item-version-info item)

             :metadata-content-text      (item-metadata-content item)
             :bibliographic-content-text (item-bibliographic-content item)
             :author-text                (item-contributor-names item :contribution :author)
             :editor-text                (item-contributor-names item :contribution :editor)
             :chair-text                 (item-contributor-names item :contribution :chair)
             :translator-text            (item-contributor-names item :contribution :translator)
             :contributor-text           (item-contributor-names item)

             :isbn             (item-isbns item)
             :issn             (item-issns item)
             :reference        (item-references item)
             :license          (item-licenses item)
             :link             (item-links item)
             :update-to        (item-update-tos item)
             :assertion        (item-assertions item)
             :relation         all-relations
             :contributor      (item-contributors item)
             :funder           (item-funders item)
             :clinical-trial   (item-clinical-trials item)
             :event            (item-events item)
             :standards-body   (item-standards-body item)
             :peer-review      (item-peer-review item)
             :institution      (item-institutions item)
             :subject          [] ; subject codes deprecation: non-breaking changes
             :subtype          (:content-type item)
             :award-original   (-> item (itree/get-item-rel :awarded) first :number)
             :award-keyword    (-> item (itree/get-item-rel :awarded) first :number (#(if % (string/lower-case %) %)))
             :award-start      (-> item (itree/get-item-rel :awarded) first (item-date-str :started))
             :proceedings-subject (:subject proceedings-metadata)
             :project          (item-projects item)
             :institution-ror-id (get-all-ror-ids item)
             :status           (item-posted-status-info (-> item :status))}]

        (if (:prime-doi es-doc)
          (select-keys es-doc [:prime-doi :is-referenced-by-count :doi])
          es-doc)))))

(defn citeproc-date [date-str]
  (when date-str 
    (let [instant (tf/parse (tf/formatters :date-time) date-str)]
      {:date-parts [[(t/year instant) (t/month instant) (t/day instant)]]
       :date-time (tf/unparse (tf/formatters :date-time-no-ms) instant)
       :timestamp (tc/to-long instant)})))

(defn citeproc-date-parts [date-str]
  (when date-str
    (let [date-parts (-> date-str
                         str
                         (string/split #"-"))]
      {:date-parts [(into [] (map util/parse-int-safe date-parts))]})))

(defn citeproc-pages [{:keys [first-page last-page]}]
  (cond (and (not (string/blank? last-page))
             (not (string/blank? first-page)))
        (str first-page "-" last-page)
        (not (string/blank? first-page))
        first-page
        :else
        nil))

(defn citeproc-ids [item ids asserted-bys]
  (reduce
   (fn [ret-ids [id asserted-by]]
     (if (get item id)
       (conj ret-ids {:id (get item id)
                      :id-type (get {:ror-id "ROR" :funder-doi "DOI" :doi "DOI" :isni "ISNI" :wikidata "wikidata"} id)
                      :asserted-by (or (get item asserted-by) "publisher")})
       ret-ids))
   []
   (map vector ids asserted-bys)))

(defn citeproc-institution [institution]
  (-> {}
      (util/assoc-exists :id (citeproc-ids
                              institution
                              [:ror-id :isni :wikidata]
                              [:asserted-by :asserted-by :asserted-by]))
      (util/assoc-exists :name (:name institution))
      (util/assoc-exists :country (:country institution))
      (util/assoc-exists :acronym (:acronym institution))
      (util/assoc-exists :place (:place institution))
      (util/assoc-exists :department (:department institution))))

(defn citeproc-affiliations [contributor]
  (if (:affiliation-struct contributor)
    (map citeproc-institution (:affiliation-struct contributor))
    (map #(hash-map :name %) (:affiliation contributor))))

(defn citeproc-contributors [es-doc & {:keys [contribution]}]
  (cond->> (:contributor es-doc)
    contribution
    (filter #(= contribution (-> % :contribution keyword)))
    :always
    (map
     #(-> {}
          (util/assoc-exists :ORCID (:orcid %))
          (util/assoc-exists :authenticated-orcid (:authenticated-orcid %) (boolean (:authenticated-orcid %)))
          (util/assoc-exists :prefix (:prefix %))
          (util/assoc-exists :suffix (:suffix %))
          (util/assoc-exists :name (:org-name %))
          (util/assoc-exists :given (:given-name %))
          (util/assoc-exists :family (:family-name %))
          (util/assoc-exists :alternate-name (:alternate-name %))
          (util/assoc-exists :sequence (:sequence %))
          (assoc :affiliation (citeproc-affiliations %))
          (util/assoc-exists :role-start (citeproc-date-parts (:start-date %)))
          (util/assoc-exists :role-end (citeproc-date-parts (:end-date %)))))))

(defn citeproc-events [es-doc]
  (when (:event es-doc)
    (-> (:event es-doc)
        (update-in [:start] citeproc-date-parts)
        (update-in [:end] citeproc-date-parts)
        util/remove-nil-values
        util/remove-empty-strings)))

(defn citeproc-references [es-doc]
  (when (:reference es-doc)
    (let [citeproc-ref (map
                        #(-> %
                             (dissoc :doi)
                             (assoc :DOI (:doi %))
                             (dissoc :issn)
                             (assoc :ISSN (:issn %))
                             (dissoc :isbn)
                             (assoc :ISBN (:isbn %))
                             (dissoc :cited-work-type)
                             (assoc :type (util/hyphenate (:cited-work-type %))))
                        (:reference es-doc))]
      (map util/remove-nil-values citeproc-ref))))

(defn citeproc-relations [es-doc]
  (->> (:relation es-doc)
       (map #(hash-map
              :id (:object %)
              :id-type (:object-type %)
              :asserted-by (:claimed-by %)
              :rel (:type %)))
       (group-by :rel)
       (map #(vector (first %) (map (fn [a] (dissoc a :rel)) (second %))))
       (into {})))

(defn citeproc-licenses [es-doc]
  (map #(-> %
            (assoc :start (or (-> % :start citeproc-date) (-> % :start-extended citeproc-date)))
            (dissoc :start-extended)
            (dissoc :version)
            (assoc :content-version (:version %))
            (dissoc :delay)
            (assoc :delay-in-days (:delay %))
            (dissoc :url)
            (assoc :URL (:url %))
            util/remove-nil-values)
       (:license es-doc)))

(defn citeproc-assertions [es-doc]
  (map #(-> {}
            (util/assoc-exists :value (:value %))
            (util/assoc-exists :URL (:url %))
            (util/assoc-exists :order (:order %))
            (util/assoc-exists :name (:name %))
            (util/assoc-exists :label (when (:label %) (clojure.string/trim (:label %))))
            (util/assoc-exists :explanation (:explanation-url %) {:URL (:explanation-url %)})
            (util/assoc-exists :group (:group-name %) (util/assoc-exists {:name (:group-name %)} :label (:group-label %))))
       (:assertion es-doc)))

(defn citeproc-links [es-doc]
  (map #(-> {}
            (util/assoc-exists :URL (:url %))
            (util/assoc-exists :content-type (:content-type %))
            (util/assoc-exists :content-version (:version %))
            (util/assoc-exists :intended-application (:application %)))
       (:link es-doc)))

(defn citeproc-clinical-trials [es-doc]
  (when (:clinical-trial es-doc)
    (let [dk {:number :clinical-trial-number, :registry :registry, :type :type}]
      (map #(set/rename-keys % dk) (:clinical-trial es-doc)))))

(defn citeproc-updates [updates]
  (map #(hash-map
         :DOI (doi-id/extract-doi (:doi %))
         :type (:type %)
         :label (:label %)
         :source (or (:source %) "publisher")
         :updated (or (-> % :date citeproc-date) (-> % :date-extended citeproc-date)))
       updates))

(defn citeproc-funders [es-doc]
  (when-let [funders (:funder es-doc)]
    (map
     #(-> {}
          (util/assoc-exists :DOI (:doi %))
          (util/assoc-exists :name (:name %))
          (util/assoc-exists :doi-asserted-by (:doi-asserted-by %))
          (util/assoc-exists :award (:award-original %))
          (util/assoc-exists :id (or (:id %)
                                     (citeproc-ids % [:doi] [:doi-asserted-by])))) funders)))

(defn citeproc-peer-review [es-doc]
  (when-let [{:keys [running-number revision-round stage
                     competing-interest-statement recommendation
                     language] :as peer-review} (:peer-review es-doc)]
    (-> {}
        (util/assoc-exists :type (:type peer-review))
        (util/assoc-exists :running-number running-number)
        (util/assoc-exists :revision-round revision-round)
        (util/assoc-exists :stage stage)
        (util/assoc-exists :competing-interest-statement competing-interest-statement)
        (util/assoc-exists :recommendation recommendation)
        (util/assoc-exists :language language))))

(defn citeproc-journal-issue [es-doc]
  (when-let [{:keys [issue published-online published-print]} (:journal-issue es-doc)]
    (when issue
      (-> {}
          (util/assoc-exists :issue issue)
          (util/assoc-exists :published-online published-online (citeproc-date-parts published-online))
          (util/assoc-exists :published-print published-print (citeproc-date-parts published-print))))))

(defn citeproc-free-to-read [es-doc]
  (when-let [{:keys [start end]} (:free-to-read es-doc)]
    (when (or start end)
      (-> {}
          (util/assoc-exists :start-date (citeproc-date-parts start))
          (util/assoc-exists :end-date (citeproc-date-parts end))))))

(defn citeproc-institutions [es-doc]
  (map citeproc-institution (:institution es-doc)))

(defn citeproc-content-domains [{:keys [crossmark-unaware?]} es-doc]
  (merge
   {:domain (or (get es-doc :domain) [])}
   (when-not crossmark-unaware?
     {:crossmark-restriction (:domain-exclusive es-doc)})))

(defn citeproc-project-titles [es-project]
  (map
   #(-> {:title (:title %)}
        (util/assoc-exists :language (:language %)))
   (:title es-project)))

(defn citeproc-project-descriptions [es-project]
  (map
   #(-> {:description (:description %)}
        (util/assoc-exists :language (:language %)))
   (:description es-project)))

(defn citeproc-project-fundings [es-project]
  (map
   #(-> {:type (:type %)}
        (util/assoc-exists :scheme (:scheme %))
        (util/assoc-exists :award-amount (:award-amount %) {:amount (:award-amount %)
                                                            :currency (:award-currency %)
                                                            :percentage (:award-percentage %)})
        (util/assoc-exists :funder (or (:funder-name %) (:id %) (:funder-doi %)) 
                           (-> {:id (or (:id %)
                                        (citeproc-ids % [:funder-doi] [:funder-doi-asserted-by]))}
                               (util/assoc-exists :name (:funder-name %) (:funder-name %)))))
   (:funding es-project)))

(defn citeproc-projects [es-doc]
  (when-let [projects (:project es-doc)]
    (map #(-> {:project-title (citeproc-project-titles %)}
              (util/assoc-exists :project-description  (citeproc-project-descriptions %))
              (util/assoc-exists :investigator         (citeproc-contributors % :contribution :investigator))
              (util/assoc-exists :lead-investigator    (citeproc-contributors % :contribution :lead-investigator))
              (util/assoc-exists :co-lead-investigator (citeproc-contributors % :contribution :co-lead-investigator))
              (util/assoc-exists :award-amount         (:award-amount %) {:amount (:award-amount %)
                                                                          :currency (:award-currency %)})
              (util/assoc-exists :award-start          (-> % :award-start citeproc-date-parts))
              (util/assoc-exists :award-end            (-> % :award-end citeproc-date-parts))
              (util/assoc-exists :award-planned-start  (-> % :award-planned-start citeproc-date-parts))
              (util/assoc-exists :award-planned-end    (-> % :award-planned-end citeproc-date-parts))
              (util/assoc-exists :funding              (citeproc-project-fundings %)))
         projects)))

(defn citeproc-resource [es-doc]
  (when (:resource-url es-doc)
    (-> {}
        (util/assoc-exists :primary {:URL (-> es-doc :resource-url first)})
        (util/assoc-exists :secondary (when-let [rmu (:resource-multiple-url es-doc)]
                                        (map (fn [{:keys [url label]}] {:URL url :label label}) rmu))))))

(defn citeproc-status [{{type :type updated :updated descs :description :as st} :status}]
  (when st
    {:type type
     :updated (citeproc-date-parts updated)
     :status-description descs}))

(defn clean [doc]
  (cond-> doc
    (= "grant" (:type doc))
    (dissoc :reference-count :references-count :is-referenced-by-count
            :content-domain :short-container-title :title :container-title
            :original-title :subtitle :short-title)

    (-> doc :updated-by seq not)
    (dissoc :updated-by)

    (-> doc :update-to seq not)
    (dissoc :update-to)))

(defn enrich-from-calculated [doc calculated-data]
  (->  doc
       (util/assoc-exists :updated-by             (citeproc-updates (-> calculated-data :updated-by)))
       (util/assoc-exists :aliases                (->> calculated-data :alias-dois-refcount :dois))
       (update            :updated-by concat      (:rw-updated-by calculated-data))
       (update            :update-to concat       (:rw-update-to calculated-data))
       (update            :is-referenced-by-count #(apply + (or % 0) (-> calculated-data :alias-dois-refcount :counts)))))

(defn es-doc->citeproc [es-doc]
  (let [source-doc (:_source es-doc)
        type-key (keyword (:type source-doc))]
    (-> source-doc

        (select-keys [:source :group-title :issue :volume
                      :degree :update-policy :archive :prefix
                      :references-count :language
                      :publisher-location :article-number :edition-number
                      :part-number :component-number])

        (->> (reduce (fn [acc i] (util/assoc-exists acc (first i) (last i))) {}))

        (assoc :is-referenced-by-count (:is-referenced-by-count source-doc))
        (assoc :type                   (:type source-doc))
        (assoc :publisher              (:publisher source-doc))
        (assoc :title                  (distinct (:title source-doc)))
        (assoc :subtitle               (distinct (:subtitle source-doc)))
        (assoc :short-title            (distinct (:short-title source-doc)))
        (assoc :container-title        (distinct (get source-doc :container-title [])))
        (assoc :short-container-title  (distinct (get source-doc :short-container-title [])))
        (assoc :original-title         (distinct (:original-title source-doc)))
        (assoc :reference-count        (:references-count source-doc))
        (assoc :DOI                    (:doi source-doc))
        (assoc :URL                    (-> source-doc :doi doi-id/to-doi-uri))
        (assoc :issued                 (or (-> source-doc :issued citeproc-date-parts) {:date-parts [[nil]]}))
        (assoc :prefix                 (:owner-prefix source-doc))
        (assoc :member                 (when (:member-id source-doc) (str (:member-id source-doc))))
        (assoc :indexed                (-> source-doc
                                           :indexed
                                           citeproc-date
                                           (util/assoc-exists :version (:cayenne-version source-doc))))
        (assoc :relation               (citeproc-relations source-doc))
        (assoc :content-domain         (-> type-id/type-dictionary
                                           (get type-key)
                                           (citeproc-content-domains source-doc)))

        (util/assoc-exists :issue-title            (distinct (:issue-title source-doc)))
        (util/assoc-exists :prime-doi              (:prime-doi source-doc))
        (util/assoc-exists :special_numbering      (-> source-doc :journal-issue :numbering))
        (util/assoc-exists :abstract               (:abstract-xml source-doc))
        (util/assoc-exists :alternative-id         (->> source-doc :supplementary-id (map ids/extract-supplementary-id)))
        (util/assoc-exists :ISSN                   (->> source-doc :issn (map :value)))
        (util/assoc-exists :ISBN                   (->> source-doc :isbn (map :value)))
        (util/assoc-exists :issn-type              (:issn source-doc))
        (util/assoc-exists :isbn-type              (:isbn source-doc))
        (util/assoc-exists :page                   (citeproc-pages source-doc))
        (util/assoc-exists :published              (-> source-doc :published citeproc-date-parts))
        (util/assoc-exists :published-print        (-> source-doc :published-print citeproc-date-parts))
        (util/assoc-exists :published-online       (-> source-doc :published-online citeproc-date-parts))
        (util/assoc-exists :published-other        (-> source-doc :published-other citeproc-date-parts))
        (util/assoc-exists :posted                 (-> source-doc :posted citeproc-date-parts))
        (util/assoc-exists :accepted               (-> source-doc :accepted citeproc-date-parts))
        (util/assoc-exists :approved               (-> source-doc :approved citeproc-date-parts))
        (util/assoc-exists :deposited              (-> source-doc :deposited citeproc-date))
        (util/assoc-exists :created                (-> source-doc :first-deposited citeproc-date))
        (util/assoc-exists :content-created        (-> source-doc :content-created citeproc-date-parts))
        (util/assoc-exists :content-updated        (-> source-doc :content-updated citeproc-date-parts))
        (util/assoc-exists :author                 (citeproc-contributors source-doc :contribution :author))
        (util/assoc-exists :editor                 (citeproc-contributors source-doc :contribution :editor))
        (util/assoc-exists :translator             (citeproc-contributors source-doc :contribution :translator))
        (util/assoc-exists :chair                  (citeproc-contributors source-doc :contribution :chair))
        (util/assoc-exists :standards-body         (:standards-body source-doc))
        (util/assoc-exists :reference              (citeproc-references source-doc))
        (util/assoc-exists :event                  (citeproc-events source-doc))
        (util/assoc-exists :clinical-trial-number  (citeproc-clinical-trials source-doc))
        (util/assoc-exists :assertion              (citeproc-assertions source-doc))
        (util/assoc-exists :link                   (citeproc-links source-doc))
        (util/assoc-exists :funder                 (citeproc-funders source-doc))
        (util/assoc-exists :license                (citeproc-licenses source-doc))
        (util/assoc-exists :update-to              (citeproc-updates (:update-to source-doc)))
        (util/assoc-exists :description            (:description source-doc))
        (util/assoc-exists :resource               (citeproc-resource source-doc))

        (util/assoc-exists :review                 (citeproc-peer-review source-doc))
        (util/assoc-exists :journal-issue          (citeproc-journal-issue source-doc))
        (util/assoc-exists :free-to-read           (citeproc-free-to-read source-doc))
        (util/assoc-exists :institution            (citeproc-institutions source-doc))
        (util/assoc-exists :status                 (citeproc-status source-doc))
        (util/assoc-exists :subtype                (:subtype source-doc))
        (util/assoc-exists :version                (util/remove-keys-by-pred (:version-info source-doc)
                                                                             {coll? empty? nil? nil?}))

        (util/assoc-exists :award                  (:award-original source-doc))
        (util/assoc-exists :award-start            (-> source-doc :award-start citeproc-date-parts))
        (util/assoc-exists :project                (citeproc-projects source-doc))
        (util/assoc-exists :proceedings-subject    (:proceedings-subject source-doc))

        (assoc :score (:_score es-doc))
        (assoc :subject []) ;; To indicate the subjects feature is gone
        (enrich-from-calculated (:calculated source-doc))
        clean)))
