(ns cayenne.elastic.index
  (:require [cayenne.elastic.convert :as convert]
            [qbits.spandex :as elastic]
            [cayenne.conf :as conf]
            [cayenne.data.relations :as relations]
            [cayenne.elastic.util :as elastic-util]
            [taoensso.timbre :as log]))

(defn bulk-index-command
  "When indexing documents we are trying to preserve existing keys holding
  relationships like updated-by, and in the future probably few more keys"
  [item]
  (when-not item
    (throw (Exception. "Cannot index a nil document")))
  (let [es-doc (convert/item->es-doc item)
        commands [{:update {:_id (:doi es-doc)}}
                  {:script {:source "if (ctx._source.containsKey('calculated')) {
         def retain_value = ctx._source['calculated'];
         ctx._source = params.new_doc;
         ctx._source['calculated'] = retain_value;
       } else {
         ctx._source = params.new_doc;
       }"
                            :params {:new_doc es-doc}}
                   :upsert es-doc}]
        relupdates (relations/find-relation-updates es-doc)]
    {:work   commands
     :updates relupdates}))

(defn bulk-index-items [index index-commands & {:keys [error-msg]
                                                :or {error-msg "Elasticsearch work index failed"}}]
  (let [tries (atom 5)]
    (elastic-util/with-retry
      {:sleep 10000
       :decay :double
       :error-hook (partial elastic-util/retry-error-hook tries error-msg)}
      error-msg
      (elastic/request
       (conf/get-service :elastic)
       {:method :post
        :url (str (elastic-util/index-url-prefix index) "_bulk")
        :body (elastic-util/raw-jsons index-commands)}))))

(defn bulk-delete-docs [index ids]
  (elastic/request
   (conf/get-service :elastic)
   {:method :post
    :url (str (elastic-util/index-url-prefix (keyword index)) "_delete_by_query")
    :body {:query {:terms {:_id ids}}}}))
