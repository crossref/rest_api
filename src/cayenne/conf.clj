(ns cayenne.conf
  (:import [java.net URI]
           [java.util UUID])
  (:require [cayenne.version]
            [clojure.java.io :as io]
            [robert.bruce :as rb]
            [taoensso.timbre :refer [info]]))

(def cores (atom {}))
(def ^:dynamic *core-name*)

(def startup-tasks (atom {}))

(def shutdown-tasks (atom {}))

(defn get-param [path & default]
  (get-in @cores (concat [*core-name* :parameters] path) default))

(defn set-param! [path value]
  (swap! cores assoc-in (concat [*core-name* :parameters] path) value))

(defn get-service [key]
  (get-in @cores [*core-name* :services key]))

(defn set-service! [key obj]
  (swap! cores assoc-in [*core-name* :services key] obj))

(defn add-startup-task [key task]
  (swap! startup-tasks assoc key task))

(defn get-resource [name]
  (-> (get-param [:res name]) io/resource))

(defn file-writer [file-name]
  (let [wrtr (io/writer file-name)]
    (add-watch (agent nil) :file-writer
               (fn [_ _ _ new]
                 (.write wrtr new)
                 (.flush wrtr)))))

(defn write-to [file-writer msg]
  (send file-writer (constantly msg)))

(defmacro with-core
  "Run operations on a particular core."
  [name & body]
  `(binding [*core-name* ~name]
     ~@body))

(defn create-core! [name]
  (swap! cores assoc name {}))

(defn create-core-from! [name copy-from-name]
  (let [params (get-in @cores [copy-from-name :parameters])]
    (swap! cores assoc-in [name :parameters] params)))

(defn start-core!
  "Create a new named core, initializes various services."
  [name & profiles]
  (doseq [p-name (concat [:base] profiles)]
    (when-let [task (get @startup-tasks p-name)]
      (info "Starting" p-name "... ")
      (task profiles)
      (info "Starting" p-name "done.")))
  (with-core name
    (set-param! [:status] :running)))

(defn stop-core! [name]
  (with-core name
    (let [stop-fn (get-service :api)]
      (stop-fn :timeout 100)) ; stop http kit allowing 100ms to close open connections
    (set-param! [:status] :stopped)))

(defn set-core! [name]
  (alter-var-root #'*core-name* (constantly name)))

(defn test-input-file [name]
  (io/file (str (get-param [:dir :test-data]) "/" name ".xml")))

(defn test-accepted-file [name test-name]
  (io/file (str (get-param [:dir :test-data]) "/" name "-" test-name ".accepted")))

(defn test-output-file [name test-name]
  (io/file (str (get-param [:dir :test-data]) "/" name "-" test-name ".out")))

(defn remote-file [url]
  (rb/try-try-again
   {:tries 10
    :error-hook #(prn "Failed to retrieve url " url " - " %)}
   #(let [content (slurp (URI. url))
          path (str (get-param [:dir :tmp]) "/remote-" (UUID/randomUUID) ".tmp")]
      (spit (io/file path) content)
      (io/file path))))
