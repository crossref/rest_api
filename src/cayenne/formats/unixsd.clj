(ns cayenne.formats.unixsd
  (:require [cayenne.formats.unixref :as unixref]
            [cayenne.data.relations :as relations]
            [cayenne.ids.prefix :as prefix]
            [cayenne.ids.container :as container]
            [cayenne.ids.member :as member-id]
            [cayenne.item-tree :as itree]
            [cayenne.util :as util]
            [cayenne.xml :as xml]
            [cayenne.ids.doi :as doi-id]
            [clojure.string :as str]
            [cayenne.dates :refer [parse-date-string]]))

(defn parse-relation [relation-loc]
  {:type :relation
   :subtype (-> (xml/xselect1 relation-loc ["claim"])
                (str/replace #"([A-Z])" "-$1")
                str/lower-case
                keyword
                relations/relation-antonym)
   :claimed-by :object
   :object (xml/xselect1 relation-loc :text)
   :object-type (xml/xselect1 relation-loc ["type"])
   :object-namespace (xml/xselect1 relation-loc ["namespace"])})

(def crm-item-parse-funcs
  {"book-id"
   #(-> % first (xml/xselect1 :> :text)
        container/book-number->uri)

   "series-id"
   #(-> % first (xml/xselect1 :> :text)
        container/series-number->uri)

   "journal-id"
   #(-> % first (xml/xselect1 :> :text)
        container/journal-number->uri)

   "citedby-count"
   #(-> % first (xml/xselect1 :> :text)
        util/parse-int-safe)

   "created"
   #(-> % first (xml/xselect1 :> :text)
        parse-date-string)

   "last-update"
   #(-> % first (xml/xselect1 :> :text)
        parse-date-string)

   "citation-id"
   #(-> % first (xml/xselect1 :> :text))

   "member-id"
   #(-> % first (xml/xselect1 :> :text)
        member-id/to-member-id-uri)

   "publisher-name"
   #(-> % first (xml/xselect1 :> :text))

   "owner-prefix"
   #(-> % first (xml/xselect1 :> :text)
        prefix/to-prefix-uri)

   "prime-doi"
   #(-> % first (xml/xselect1 :> :text) doi-id/to-doi-uri)

   "relation"
   #(->> %
         (map parse-relation)
         (filter :subtype))})

(defn parse-crm-items [grouped-crm-items]
  (reduce merge
          (map #(let [item-name (-> % first :attrs :name)
                      f (crm-item-parse-funcs item-name)]
                  (if f
                    {(keyword item-name) (f %)}
                    {}))
               grouped-crm-items)))

(defn parse-doi [oai-record]
  (-> oai-record
      (xml/xselect1 :> "query" "doi" :text)
      (doi-id/to-doi-uri)))

(defn insert-crm-data
  "Insert crm item publisher info, but do not overwrite publisher name and
   location if they are specified in the metadata body."
  [work oai-record]
  (let [{:keys [book-id series-id journal-id citedby-count created
                last-update citation-id member-id publisher-name
                owner-prefix prime-doi relation]}
        (->> "crm-item"
             (xml/xselect oai-record :>)
             (group-by (comp :name :attrs))
             vals
             (parse-crm-items))


        crm-steward-info (-> (itree/make-item :org)
                             (itree/add-property :name publisher-name)
                             (itree/add-id member-id))

        container-items (let [ids (->> [book-id series-id journal-id]
                                       (remove nil?))]
                          (map #(itree/add-id (itree/make-item) %) ids))

        crm-prefix-info (-> (itree/make-item)
                            (itree/add-id owner-prefix))

        crm-prime-doi-info (when prime-doi
                             (-> (itree/make-item)
                                 (itree/add-id prime-doi)))]

    (-> work
        (itree/add-property :citation-id citation-id)
        (itree/add-relation :prefix crm-prefix-info)
        (itree/add-relation :prime-doi crm-prime-doi-info)
        (itree/add-relations :container container-items)
        (itree/add-relation :steward crm-steward-info)
        (itree/add-relations :relation relation)
        (itree/add-property :deposited last-update)
        (itree/add-property :first-deposited created)
        (itree/add-property :cited-count citedby-count))))

(defn unixsd-record-parser
  "Parse the UNIXML and extract information from the CRM-Items in the UNIXSD wrapper.
   Associated this CRM-Items data with the Item being identified (e.g. article) not the top level element (e.g. journal)."
  [oai-record]
  (let [result (unixref/unixref-record-parser oai-record)
        work (second result)
        primary-id (first result)
        primary-id (if primary-id
                       primary-id
                      (parse-doi oai-record))]

    [primary-id
     (itree/apply-in-tree-works
      work
      primary-id
      #(insert-crm-data % oai-record))]))

(defn missing-crm-items [oai-record]
  (filter
    #(empty? (xml/xselect oai-record :> "crm-item" [:= "name" %]))
    ["owner-prefix" "last-update" "citedby-count" "publisher-name" "member-id"]))
