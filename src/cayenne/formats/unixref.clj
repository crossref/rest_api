(ns cayenne.formats.unixref
  (:require [clj-time.format :as tf]
            [clj-time.core :as t]
            [cayenne.dates :refer [parse-date parse-start-date parse-end-date]]
            [cayenne.ids.doi :as doi-id :refer [to-doi-uri extract-doi]]
            [cayenne.xml :as xml]
            [cayenne.ids.ctn :refer [normalize-ctn]]
            [clojure.string :as string]
            [cayenne.util :refer [?> safe-trim]]
            [cayenne.ids :refer [get-id-uri to-supplementary-id-uri]]
            [cayenne.ids.issn :refer [to-issn-uri]]
            [cayenne.ids.isbn :refer [to-isbn-uri]]
            [cayenne.ids.orcid :refer [to-orcid-uri]]
            [cayenne.ids.ror-id :refer [to-ror-id-uri]]
            [cayenne.ids.institution-id :refer [to-institution-id-uri]]))

;; -----------------------------------------------------------------
;; Helpers

(defn parse-attach-rel
  "Attach a relation by running a function on an xml location."
  [item relation loc kind parse-fn]
  (let [existing (get-in item [:rel relation] [])
        related (parse-fn loc)]
    (condp = kind
      :single
      (if related
        (assoc-in item [:rel relation] (conj existing related))
        item)
      :multi
      (let [non-nil-related (remove nil? related)]
        (if (not-empty non-nil-related)
          (assoc-in item [:rel relation] (concat existing non-nil-related))
          item)))))

(defn parse-attach-property
  "Attach a property by running a function on an xml location. Only attach if non-null."
  [item property loc parse-fn & args]
  (let [result (apply parse-fn loc args)]
    (if result
      (assoc item property result)
      item)))

(defn parse-text-language-item
  "Returns a function for parsing an xml element of type `elem-type` with language support"
  [elem-type]
  (fn [loc]
    (when-let [location (xml/xselect1 loc elem-type)]
      {:language (xml/xselect1 loc ["xml:lang"])
       :value location})))

(defn parse-multiple-items
  "Map `item-parse-fn` to all instances of `loc` (with or without any `tags`).
   Returns a list of results or nil if empty. Tags can be one or many instances of single tags or lists of tags.
   A list of tags will be interpreted as nested tags. The results of multiple tags are aggregated to a single result-list."
  [loc item-parse-fn & tags]
  (->> tags
       (mapcat #(if (sequential? %)
                  (apply xml/xselect loc %)
                  (xml/xselect loc %)))
       (map item-parse-fn)
       not-empty))

(defn parse-single-item
  "Map `item-parse-fn` to a single instance of `loc` (with or without any `tags`).
   Returns a single result or nil. Tags can be single tag or a list of tags.
   A list of tags will be interpreted as nested tags."
  [loc item-parse-fn tags]
  (when-let [item (apply xml/xselect1 loc tags)]
    (item-parse-fn item)))

(defn attach-rel
  "Attach related item to another item via relation."
  [item relation related-item]
  (let [existing (get item relation [])]
    (assoc-in item [:rel relation] (conj existing related-item))))

(defn attach-object-rel
  "Attach an object relationship"
  [item {id :id :as object}]
  (if (->> id
           (remove nil?)
           seq)
    (attach-rel item :object object)
    item))

(defn attach-id
  "Attach a URI id to an item.\n
   The default :id key can be overwritten using the optional `kname` alias."
  ([item id]
   (attach-id item id :id))
  ([item id kname]
   (if id 
    (let [existing (get item kname [])]
      (assoc item kname (conj existing id)))
    item)))

(defn attach-ids
  [item ids]
  (if (seq ids)
    (let [existing (get item :id [])]
      (assoc item :id (concat existing ids)))
    item))

(defn if-conj
  "Conj item to coll iff item is not nil."
  [coll item]
  (if item
    (conj coll item)
    coll))

;; -----------------------------------------------------------------
;; Component locators

(defn find-journal [record-loc]
  (xml/xselect1 record-loc :> "journal"))

(defn find-journal-metadata [journal-loc]
  (xml/xselect1 journal-loc "journal_metadata"))

(defn find-journal-issue [journal-loc]
  (xml/xselect1 journal-loc "journal_issue"))

(defn find-journal-article [journal-loc]
  (xml/xselect1 journal-loc "journal_article"))

(defn find-journal-volume [journal-loc]
  (xml/xselect1 journal-loc "journal_issue" "journal_volume"))

(defn find-posted-content [record-loc]
  (xml/xselect1 record-loc :> "posted_content"))

(defn find-conf [record-loc]
  (xml/xselect1 record-loc :> "conference"))

(defn find-peer-review [record-loc]
  (xml/xselect1 record-loc :> "peer_review"))

(defn find-event
  "Sometimes present in a proceedings. Never has a DOI."
  [conf-loc]
  (xml/xselect1 conf-loc "event_metadata"))

(defn find-conf-paper
  "Found in conferences."
  [conf-loc]
  (xml/xselect1 conf-loc "conference_paper"))

(defn find-book [record-loc]
  (xml/xselect1 record-loc :> "book"))

(defn find-dissertation [record-loc]
  (xml/xselect1 record-loc :> "dissertation"))

(defn find-report [record-loc]
  (xml/xselect1 record-loc :> "report-paper"))

(defn find-standard [record-loc]
  (xml/xselect1 record-loc :> "standard"))

(defn find-database [record-loc]
  (xml/xselect1 record-loc :> "database"))

(defn find-stand-alone-component [record-loc]
  (xml/xselect1 record-loc :> "sa_component"))

(defn find-grant [record-loc]
  (xml/xselect1 record-loc :> "grant"))

(defn find-grant-projects [grant-loc]
  (xml/xselect grant-loc "project"))

(defn find-pending-publication [record-loc]
  (xml/xselect1 record-loc :> "pending_publication"))

(defn find-version-info [record-loc]
  (xml/xselect1 record-loc :> "version_info"))

;; --------------------------------------------------------------------
;; Dates

(defn find-pub-dates
  ([work-loc kind]
   (xml/xselect work-loc "publication_date" [:= "media_type" kind]))
  ([work-loc]
   (xml/xselect work-loc "publication_date" [:has-not "media_type"])))

(defn find-approval-dates [work-loc]
  (xml/xselect work-loc "approval_date"))

(defn find-event-date [event-loc]
  (xml/xselect1 event-loc "conference_date"))

(defn find-database-dates [metadata-loc]
  (xml/xselect metadata-loc "database_date" "publication_date"))

(defn find-database-created-dates [metadata-loc]
  (xml/xselect metadata-loc "database_date" "creation_date"))

(defn find-database-updated-dates [metadata-loc]
  (xml/xselect metadata-loc "database_date" "update_date"))

;; --------------------------------------------------------------
;; Citations

(defn find-citations [work-loc]
  (xml/xselect work-loc "citation_list" "citation"))

(defn parse-citation [citation-loc]
  (-> {:key (xml/xselect1 citation-loc ["key"])
       :doi (xml/xselect1 citation-loc "doi" :text)
       :doi-asserted-by (or (xml/xselect1 citation-loc "doi" ["provider"]) "publisher")
       :issn (to-issn-uri (xml/xselect1 citation-loc "issn" :text))
       :issn-type (or (xml/xselect1 citation-loc "issn" ["media_type"]) "print")
       :journal-title (xml/xselect1 citation-loc "journal_title" :text)
       :author (xml/xselect1 citation-loc "author" :text)
       :volume (xml/xselect1 citation-loc "volume" :text)
       :issue (xml/xselect1 citation-loc "issue" :text)
       :first-page (xml/xselect1 citation-loc "first_page" :text)
       :year (xml/xselect1 citation-loc "cYear" :text)
       :isbn (to-isbn-uri (xml/xselect1 citation-loc "isbn" :text))
       :isbn-type (or (xml/xselect1 citation-loc "isbn" ["media_type"]) "print")
       :series-title (xml/xselect1 citation-loc "series_title" :text)
       :volume-title (xml/xselect1 citation-loc "volume_title" :text)
       :edition (xml/xselect1 citation-loc "edition_number" :text)
       :component (xml/xselect1 citation-loc "component_number" :text)
       :type :reference
       :subtype :work
       :cited-work-type (xml/xselect1 citation-loc ["type"])
       :article-title (xml/xselect1 citation-loc "article_title" :text)
       :standard-designator (xml/xselect1 citation-loc "std_designator" :text)
       :standards-body (xml/xselect1 citation-loc "standards_body" :text)
       :unstructured (xml/xselect1 citation-loc "unstructured_citation" :text)}
      (parse-attach-rel :object citation-loc :single #(when-let [id (to-doi-uri (xml/xselect1 % "doi" :text))]
                                                        (attach-id {} id)))))


;; ---------------------------------------------------------------------
;; Assertions

(defn find-assertions [work-loc]
  (xml/xselect work-loc :> "custom_metadata" "assertion"))

(defn parse-assertion [assertion-loc]
  {:type :assertion
   :order (xml/xselect1 assertion-loc ["order"])
   :url (xml/xselect1 assertion-loc ["href"])
   :explanation-url (xml/xselect1 assertion-loc ["explanation"])
   :value (xml/xselect1 assertion-loc :text)
   :name (xml/xselect1 assertion-loc ["name"])
   :label (xml/xselect1 assertion-loc ["label"])
   :group-label (xml/xselect1 assertion-loc ["group_label"])
   :group-name (xml/xselect1 assertion-loc ["group_name"])})

;; ---------------------------------------------------------------------
;; Resources

(defn parse-collection-item
  "Returns a resource link. :content-version can be any of tdm, vor, am."
  [intended-application coll-item-loc]
  {:type :url
   :content-version (or (xml/xselect1 coll-item-loc ["content_version"])
                        "vor")
   :intended-application intended-application
   :content-type (or (xml/xselect1 coll-item-loc ["mime_type"]) "unspecified")
   :value (xml/xselect1 coll-item-loc :text)})

(defn parse-collection
  ([with-attribute item-loc]
   (let [items (xml/xselect item-loc
                            "doi_data" :> "collection"
                            [:= "property" with-attribute] "item"
                            "resource")]
     (map (partial parse-collection-item with-attribute) items)))
  ([with-attribute with-crawler with-application item-loc]
   (let [items (mapcat #(xml/xselect item-loc
                                     "doi_data" :> "collection"
                                     [:= "property" with-attribute] "item"
                                     [:= "crawler" %]
                                     "resource")
                       with-crawler)]
     (map (partial parse-collection-item with-application) items))))

(defn parse-web-resource [resource-loc]
  (when-let [value (xml/xselect1 resource-loc "resource" :text)]
    (-> {:type :web-resource}
        (attach-id value))))

(defn parse-resource [item-loc]
  (when-let [doi-data (xml/xselect1 item-loc "doi_data")]
    (-> {:type :resolution
         :subtype :primary}
        (parse-attach-rel :object doi-data :single parse-web-resource))))

(defn parse-multiresolution-item [item-loc]
  (-> {:type :resolution
       :subtype :multi
       :label (xml/xselect1 item-loc ["label"])
       :set-by-id (xml/xselect1 item-loc ["setbyID"])}
      (parse-attach-rel :object item-loc :single parse-web-resource)))

(defn parse-multiresolution-resource [item-loc]
  (let [items (xml/xselect item-loc "doi_data" :> "collection" [:= "property" "list-based"] "item")]
    (map parse-multiresolution-item items)))

;; --------------------------------------------------------------
;; Identifiers

(defn parse-doi [item-loc]
  (when-let [doi (or (xml/xselect1 item-loc "doi_data" "doi" :text)
                     (xml/xselect1 item-loc "doi" :text))]
    {:type :id
     :subtype :doi
     :ra :crossref
     :value (to-doi-uri doi)
     :original doi}))

(defn parse-doi-uri [item-loc]
  (when-let [doi (or (xml/xselect1 item-loc "doi_data" "doi" :text)
                     (xml/xselect1 item-loc "doi" :text))]
    (to-doi-uri doi)))

(defn find-issns [item-loc]
  (xml/xselect item-loc "issn"))

(defn parse-issn [issn-loc]
  (let [issn-type (or (xml/xselect1 issn-loc ["media_type"]) "print")
        issn-value (xml/xselect1 issn-loc :text)]
    {:type :id
     :subtype :issn
     :kind (if (= issn-type "print") :print :electronic)
     :id [(to-issn-uri issn-value)]
     :original issn-value}))

(defn find-isbns [item-loc]
  (xml/xselect item-loc "isbn"))

(defn parse-isbn [isbn-loc]
  (let [isbn-type (or (xml/xselect1 isbn-loc ["media_type"]) "print")
        isbn-value (xml/xselect1 isbn-loc :text)]
    {:type :id
     :subtype :isbn
     :kind (if (= isbn-type "print") :print :electronic)
     :id [(to-isbn-uri isbn-value)]
     :original isbn-value}))

(defn parse-supplementary-id-uri [domain-id-loc]
  (let [id-val (xml/xselect1 domain-id-loc :text)]
    (to-supplementary-id-uri id-val)))

(defn find-supplementary-ids [item-loc]
  (xml/xselect item-loc "publisher_item" "identifier"))

(defn parse-ror-uri [ror-loc]
  (when-let [ror-id (xml/xselect1 ror-loc :text)]
    (to-ror-id-uri ror-id)))

(defn find-ror-id [affiliation-loc]
  (xml/xselect1 affiliation-loc "ROR"))

(defn parse-institution-id-uri [inst-id-loc]
  (to-institution-id-uri (xml/xselect1 inst-id-loc ["type"]) (xml/xselect1 inst-id-loc :text)))

(defn find-institution-ids [inst-loc]
  (xml/xselect inst-loc "institution_id"))

;; --------------------------------------------------------------
;; Institutions

(defn parse-department [department-loc]
  (when-let [department (xml/xselect1 department-loc :text)]
    {:type :org
     :subtype :department
     :name department}))

(defn parse-departments [institution-loc]
  (map parse-department (xml/xselect institution-loc "institution_department")))

(defn parse-institution [institution-loc]
  (-> {:name (xml/xselect1 institution-loc "institution_name" :text)}
      (attach-object-rel {:id (->> (find-institution-ids institution-loc)
                                    (map parse-institution-id-uri)
                                    (filter seq)
                                    seq)})
      (parse-attach-property :location institution-loc parse-multiple-items (parse-text-language-item :text) "institution_place" "institution_location")
      (parse-attach-rel :component institution-loc :multi parse-departments)
      (parse-attach-property :acronym institution-loc parse-multiple-items (parse-text-language-item :text) "institution_acronym")))

(defn parse-institutions [parent-loc]
  (map parse-institution (xml/xselect parent-loc "institution")))

(defn institutions->reference-orgs [parent-loc]
  (let [institutions (map parse-institution (xml/xselect parent-loc "institution"))
        reify-fn #(-> %
                      (assoc :type :reference)
                      (assoc :subtype :org))]
    (map reify-fn institutions)))


;; ---------------------------------------------------------------
;; Contributors

(defn parse-orcid [person-loc]
  (when-let [orcid-loc (xml/xselect1 person-loc "ORCID")]
    {:type :id
     :subtype :orcid
     :authenticated (-> (xml/xselect1 orcid-loc ["authenticated"])
                        (or "false")
                        (Boolean/parseBoolean))
     :value (to-orcid-uri (xml/xselect1 orcid-loc :text))
     :original (xml/xselect1 orcid-loc :text)}))

(defn parse-affiliation [affiliation-loc]
  (cond-> {:type :contributor
           :subtype :org
           :name (or (xml/xselect1 affiliation-loc "institution" :text)
                     (xml/xselect1 affiliation-loc :text))}

    true
    (attach-object-rel {:id (list (parse-ror-uri (find-ror-id affiliation-loc)))})

    (xml/xselect1 affiliation-loc "institution" ["country"])
    (assoc :country (xml/xselect1 affiliation-loc "institution" ["country"]))))

(defn find-affiliations [person-loc]
  (xml/xselect person-loc "affiliation"))

(defn parse-person-name [person-loc]
  (let [orcid (parse-orcid person-loc)
        person {:type :contributor
                :subtype :person
                :first-name (or (xml/xselect1 person-loc "given_name" :text)
                                (xml/xselect1 person-loc "givenName" :text))
                :last-name (or (xml/xselect1 person-loc "surname" :text)
                               (xml/xselect1 person-loc "familyName" :text))
                :sequence (or (xml/xselect1 person-loc ["sequence"]) "additional")
                :suffix (xml/xselect1 person-loc "suffix" :text)}
        parse-fn #(concat
                   (map parse-affiliation (find-affiliations %))
                   (map (fn [inst] (assoc inst :type :affiliation)) (parse-institutions (xml/xselect1 % "affiliations"))))
        alternate-names (xml/xselect person-loc "alternateName" :text)]
    (cond-> person

      (seq alternate-names)
      (assoc :alternate-name alternate-names)

      true
      (parse-attach-rel :affiliation person-loc :multi parse-fn)

      orcid
      (attach-object-rel {:id [(:value orcid)]
                      :orcid-authenticated (:authenticated orcid)}))))

(defn parse-organization [org-loc]
  {:type :contributor
   :subtype :org
   :sequence (or (xml/xselect1 org-loc ["sequence"]) "additional")
   :name (xml/xselect1 org-loc :text)})

(defn find-persons-and-orgs [item-loc kind]
  (->> (xml/xselect item-loc "contributors" "*" [:= "contributor_role" kind])
       (filter #(#{"person_name" "organization"} (xml/node->tagname %)))))

(defn find-investigators [item-loc kind]
  (xml/xselect item-loc "investigators" "person" [:= "role" kind]))


;; ---------------------------------------------------------------
;; Generic item parsing

(defn parse-item-id-uris
  [item-loc]
  (-> []
      (if-conj (parse-doi-uri item-loc))))

(defn parse-item-supplementary-id-uris
  [item-loc]  
  (-> [] 
      (concat (map parse-supplementary-id-uri (find-supplementary-ids item-loc)))
      not-empty))

(defn parse-item-publisher
  "Parse to publisher property."
  [parent-loc]
  (when-let [publisher-loc (xml/xselect1 parent-loc "publisher")]
    {:name (xml/xselect1 publisher-loc "publisher_name" :text)
     :location (parse-multiple-items publisher-loc (parse-text-language-item :text) "publisher_place")}))

(defn parse-item-citations [item-loc]
  (map parse-citation (find-citations item-loc)))

(defn parse-item-assertions [item-loc]
  (map parse-assertion (find-assertions item-loc)))

(defn parse-item-number [item-number-loc]
  {:kind (xml/xselect1 item-number-loc ["item_number_type"])
   :value (xml/xselect1 item-number-loc :text)})

(defn parse-item-pub-dates
  ([item-loc kind]
   (not-empty (map parse-date (find-pub-dates item-loc kind))))
  ([item-loc]
   (not-empty (map parse-date (find-pub-dates item-loc)))))

(defn parse-database-pub-dates [item-loc]
  (not-empty (map parse-date (find-database-dates item-loc))))

(defn parse-database-created-dates [item-loc]
  (not-empty (map parse-date (find-database-created-dates item-loc))))

(defn parse-database-updated-dates [item-loc]
  (not-empty (map parse-date (find-database-updated-dates item-loc))))

(defn parse-item-version-info [item-loc]
  (let [vloc  (-> item-loc find-version-info)
        {:keys [value language]} ((parse-text-language-item :text) (xml/xselect1 vloc "version"))]
    (when value
      {:type :version
       :version-description (parse-multiple-items vloc (parse-text-language-item :text) "description")
       :version value
       :language language})))

(defn parse-item-approval-dates [item-loc]
  (not-empty (map parse-date (find-approval-dates item-loc))))

(defn parse-item-posted-date [item-loc]
  (when-let [posted-date-loc (xml/xselect1 item-loc "posted_date")]
    (parse-date posted-date-loc)))

(defn parse-item-accepted-date [item-loc]
  (when-let [accepted-date-loc (xml/xselect1 item-loc "acceptance_date")]
    (parse-date accepted-date-loc)))

(defn parse-item-contributors [kind item-loc]
  (let [items (find-persons-and-orgs item-loc kind)
        by-sequence
        (-> (concat (map
                     #(case (xml/node->tagname %)
                        "person_name" (parse-person-name %)
                        "organization" (parse-organization %))
                     items))
            ((partial group-by :sequence)))]
    (concat
     (get by-sequence "first") (get by-sequence "additional"))))

(defn parse-funder-grant [funding-identifier-loc]
  (-> {:type :grant}
      (attach-id (xml/xselect1 funding-identifier-loc :plain))))

(defn parse-funder-grants [funder-group-loc]
  (map parse-funder-grant
       (concat
        (xml/xselect funder-group-loc "assertion" [:= "name" "funding_identifier"])
        (xml/xselect funder-group-loc "assertion" [:= "name" "award_number"]))))

(defn normalize-funder-id-val [funder-id-val]
  (when funder-id-val
    (if-let [id-only (re-find #"\A\d+\Z" (.trim funder-id-val))]
      (doi-id/with-funder-prefix id-only)
      (to-doi-uri funder-id-val))))

(defn parse-funder-ror-id 
  "Meant to be used on `fundref:fundgroup` and `grants:funding` locations.\n
   Returns nil when no ROR ids are found otherwise it returns a single ROR id.\n
   
   The :asserted-by is determined by the `provider` if one exists.\n
   It defaults to `publisher` otherwise. 
   
   ```{:id \"https://ror.org/xyz\" :id-type \"ROR\" :asserted-by \"publisher\"}```
   "
  [funder-group-loc]
  (let [ror-id (xml/xselect1 funder-group-loc :text)
        ror-id-source (or (xml/xselect1 funder-group-loc ["provider"]) "publisher")]
    (when ror-id
      {:type :org
       :id-assertion [{:id [ror-id]
                       :id-type "ROR"
                       :asserted-by ror-id-source}]})))

(defn parse-funder-name-assertion [funder-name-assertion-loc]
  (let [funder-id-loc (or (xml/xselect1 funder-name-assertion-loc
                                           [:= "name" "funder_identifier"])
                              (xml/xselect1 funder-name-assertion-loc
                                           "assertion"
                                           [:= "name" "funder_identifier"])
                              (xml/xselect1 funder-name-assertion-loc "funder-id"))
        funder-name (or
                         (xml/xselect1 funder-name-assertion-loc
                                       [:= "name" "funder_name"]
                                       :plain)
                         (xml/xselect1 funder-name-assertion-loc
                                       "assertion"
                                       [:= "name" "funder_name"]
                                       :plain)
                         (xml/xselect1 funder-name-assertion-loc "funder-name" :text))
        funder-uri (normalize-funder-id-val (xml/xselect1 funder-id-loc :plain))
        funder-id-source (or (xml/xselect1 funder-id-loc ["provider"]) "publisher")]
    (when funder-name
      (-> {:type :org 
           :name funder-name}
          (?> (and funder-uri funder-id-source) assoc :doi-asserted-by funder-id-source)
          (?> funder-uri attach-id {:id [funder-uri]
                                    :id-type "DOI"
                                    :asserted-by funder-id-source} :id-assertion)))))

(defn parse-funder-name-assertions [funder-group-loc]
  (let [funders (concat
                 (xml/xselect funder-group-loc
                              [:= "name" "funder_name"])
                 (xml/xselect funder-group-loc
                              "assertion"
                              [:= "name" "funder_name"])
                 (xml/xselect funder-group-loc "funder-name"))]
    (->> (map parse-funder-name-assertion funders) 
         (map #(parse-attach-rel % :awarded funder-group-loc :multi parse-funder-grants)))))

(defn parse-funder-ror-assertions [funder-group-loc]
  (let [funders (concat
                 (xml/xselect funder-group-loc
                               [:= "name" "ror"])
                 (xml/xselect funder-group-loc
                               "assertion"
                               [:= "name" "ror"])
                 (xml/xselect funder-group-loc "ROR"))]
    (->> (map parse-funder-ror-id funders)
         (map #(parse-attach-rel % :awarded funder-group-loc :multi parse-funder-grants)))))

(defn parse-funders [funder-group-loc]
  (concat
   (parse-funder-name-assertions funder-group-loc)
   (parse-funder-ror-assertions funder-group-loc)))

(defn parse-grants-funders [funding-loc]
  (or  (parse-funder-name-assertion funding-loc)
       (first (parse-funder-ror-assertions funding-loc))))

(defn parse-item-funders-without-fundgroup [item-loc]
  (let [funder-group-locs (concat
                           (xml/xselect item-loc "program")
                           (xml/xselect item-loc "crossmark" "custom_metadata" "program"))]
    (mapcat parse-funders funder-group-locs)))

(defn parse-item-funders-with-fundgroup [item-loc]
  (let [funder-groups-loc (concat
                           (xml/xselect item-loc
                                        "program"
                                        "assertion"
                                        [:= "name" "fundgroup"])
                           (xml/xselect item-loc
                                        "crossmark"
                                        "custom_metadata"
                                        "program"
                                        "assertion"
                                        [:= "name" "fundgroup"]))]
    (mapcat parse-funders funder-groups-loc)))

(defn parse-item-funders [item-loc] 
  (concat
   (parse-item-funders-without-fundgroup item-loc)
   (parse-item-funders-with-fundgroup item-loc)))

(defn parse-clinical-trial-number [clinical-trial-number-loc]
  (let [registry-id (xml/xselect1 clinical-trial-number-loc ["registry" :plain])
        ctn-type (xml/xselect1 clinical-trial-number-loc ["type" :plain])
        ctn-val (normalize-ctn (xml/xselect1 clinical-trial-number-loc :plain))]
    {:type :ctn :registry registry-id :ctn-type ctn-type :ctn ctn-val}))

(defn parse-item-clinical-trial-numbers
  [item-loc]
  (let [ctns (concat
              (xml/xselect item-loc
                           "crossmark"
                           "custom_metadata"
                           "program"
                           "clinical-trial-number")
              (xml/xselect item-loc
                           "program"
                           "clinical-trial-number"))
        parsed (map parse-clinical-trial-number ctns)]
    parsed))

(def yyyy-MM-dd-date-formatter (tf/formatter "yyyy-MM-dd"))

;; some publishers are depositing dates as a date time in an unknown
;; format. temporarily get around that by taking the first 10 chars -
;; what should be the yyyy-MM-dd date.

(defn parse-license-start-date [license-loc]
  (when-let [raw-date (xml/xselect1 license-loc ["start_date"])]
    (let [concat-date (apply str (take 10 raw-date))
          d (tf/parse yyyy-MM-dd-date-formatter concat-date)
          year (t/year d)
          month (t/month d)
          day (t/day d)]
      (parse-date year month day))))

(defn parse-yyyy-MM-dd-date [s]
  (when s
    (let [concat-date (apply str (take 10 s))
          d (tf/parse yyyy-MM-dd-date-formatter concat-date)
          year (t/year d)
          month (t/month d)
          day (t/day d)]
      (parse-date year month day))))

(defn parse-item-status [item-loc]
  (when-let [status (xml/xselect1 item-loc "status")]
    (let [type (xml/xselect1 status :> ["type"])
          date (xml/xselect1 status :> ["date"])]
      (-> {:type type}
          (parse-attach-property :description status parse-multiple-items (parse-text-language-item :text) "description")
          (parse-attach-property :updated date parse-yyyy-MM-dd-date)))))

(defn parse-license
  "Returns a license. :content-version can be any of tdm, vor, am or unspecified."
  [license-loc]
  (-> {:type :url
       :content-version (or (xml/xselect1 license-loc ["applies_to"]) "unspecified")
       :value (string/trim (xml/xselect1 license-loc :text))}
      (parse-attach-property :start license-loc parse-license-start-date)))

(defn parse-item-licenses [item-loc]
  (let [license-locs (xml/xselect item-loc :> "license_ref")]
    (map parse-license license-locs)))

(defn parse-archive
  [archive-loc]
  {:type :org
   :subtype :archive
   :name (xml/xselect1 archive-loc ["name"])})

(defn parse-item-archives [item-loc]
  (let [archive-locs (xml/xselect item-loc :> "archive_locations" "archive")]
    (map parse-archive archive-locs)))

(defn parse-update-policy [item-loc]
  (when-let [policy-doi (xml/xselect1 item-loc :> "crossmark" "crossmark_policy" :text)]
    {:id [(to-doi-uri policy-doi)]}))

(def update-date-formatter (tf/formatter "yyyy-MM-dd"))

(defn parse-update-date [update-loc]
  (let [update-date (tf/parse update-date-formatter
                              (xml/xselect1 update-loc ["date"]))
        day (t/day update-date)
        month (t/month update-date)
        year (t/year update-date)]
    (parse-date year month day)))

(defn parse-update [update-loc]
  (let [update-to-doi (xml/xselect1 update-loc :text)
        doi (to-doi-uri update-to-doi)]
    (-> {:type :update
         :subtype (xml/xselect1 update-loc ["type"])
         :label (xml/xselect1 update-loc ["label"])
         :value doi
         :original update-to-doi}
        (parse-attach-property :updated update-loc parse-update-date)
        (attach-object-rel {:id (list doi)}))))

(defn parse-updates [item-loc]
  (when-let [updates (xml/xselect item-loc :> "crossmark" "updates" "update")]
    (map parse-update updates)))

(defn parse-relation [relation-loc]
  (let [relation-detail-loc (or (xml/xselect1 relation-loc "inter_work_relation")
                                (xml/xselect1 relation-loc "intra_work_relation"))]
    {:type :relation
     :subtype (-> (xml/xselect1 relation-detail-loc ["relationship-type"])
                  (string/replace #"([A-Z])" "-$1")
                  string/lower-case
                  keyword)
     :claimed-by :subject
     :object-type (xml/xselect1 relation-detail-loc ["identifier-type"])
     :object-namespace (xml/xselect1 relation-detail-loc ["namespace"])
     :object (xml/xselect1 relation-detail-loc :text)}))

(defn parse-relations [item-loc]
  (when-let [relations (xml/xselect item-loc :> "related_item")]
    (map parse-relation relations)))

(defn convert-uri
  "Convert a relation item, produced by parse-relation, into a URI."
  [{subtype :subtype
    object :object
    object-type :object-type}]

  (let [try-doi (extract-doi object)]

      ; Try a number of heuristics in order. 
    (cond
            ; These known identifier types may be represented with http but can be https.
            ; Normalize to https then return them in this normalized format.
      (or
       (.startsWith object "http://hdl.handle.net/")
       (.startsWith object "http://n2t.net/"))
      (.replaceAll object "^http://" "https://")

            ; General purpose URIs are already URIs.
            ; The job of this function is to return URIs so just pass these through.
            ; e.g. some PURLs are expressed as URLs, some aren't.
            ; Don't try and prepent the PURL resolver to things that are already URLs.   
      (.startsWith object "http://")
      object

      (.startsWith object "https://")
      object

            ; Some PURLs look like DOIs, so make sure those get matched as PURLs. 
      (= object-type "purl")
      (get-id-uri :purl try-doi)

            ; Try things that look like DOIs should be turned into DOIs.
      (not (string/blank? try-doi))
      (get-id-uri :doi try-doi)

            ; Try things that look like arXiv identifiers that have the scheme, remove the scheme.
      (.startsWith object "arXiv:")
      (get-id-uri :arxiv (.replace object "arXiv:" ""))

            ; Same for ARKs.
      (.startsWith object "ark:")
      (get-id-uri :ark (.replace object "ark:" ""))

      :default
      (get-id-uri object-type object))))

(defn parse-relationships-as-rels
  "In addition to the prior relationship representation, where each relationship is a reified item, we also record them directly as rels.
  The citeproc-json renderer doesn't pay attention to these, but they are used in the item graph."
  [item-loc]
  (->> item-loc
       parse-relations
       (group-by :subtype)
       (map
        (fn [[subtype rels]]
          [subtype (map (fn [rel]
                          {:id [(convert-uri rel)]}) rels)]))
       (into {})))

(defn parse-item-issn-details [item-loc]
  (map parse-issn (find-issns item-loc)))

(defn parse-item-isbn-details [item-loc]
  (map parse-isbn (find-isbns item-loc)))

(declare parse-item)

(defn parse-component [component-loc]
  (-> (parse-item component-loc)
      (conj
       {:subtype :component
        :format (xml/xselect1 component-loc "format" :text)
        :format-mime (xml/xselect1 component-loc "format" ["mime_type"] :text)
        :size (xml/xselect1 component-loc ["component_size"] :text)
        :agency (xml/xselect1 component-loc ["reg-agency"] :text)
        :relation (xml/xselect1 component-loc ["parent_relation"] :text)
        :description (parse-multiple-items component-loc (parse-text-language-item :text) "description")})))

(defn parse-component-list [parent-loc]
  (map parse-component (xml/xselect parent-loc "component_list" "component")))

(defn parse-grant-award [grant-loc]
  (let [start-date (xml/xselect1 grant-loc "award-start-date" :text)]
    (-> {:type :award
         :number (xml/xselect1 grant-loc "award-number" :text)}
        (parse-attach-property :started start-date parse-yyyy-MM-dd-date))))

(defn parse-project-award [project-loc]
  (let [amount (xml/xselect1 project-loc "award_amount" :text)
        dates-loc (xml/xselect1 project-loc "award-dates")
        start-date (xml/xselect1 dates-loc ["start-date"])
        end-date (xml/xselect1 dates-loc ["end-date"])
        planned-start-date (xml/xselect1 dates-loc ["planned-start-date"])
        planned-end-date (xml/xselect1 dates-loc ["planned-end-date"])]
    (-> {:type :award
         :amount (try (Float/parseFloat amount) (catch Exception _ nil))
         :currency (xml/xselect1 project-loc "award_amount" ["currency"])}
        (parse-attach-property :started start-date parse-yyyy-MM-dd-date)
        (parse-attach-property :ended end-date parse-yyyy-MM-dd-date)
        (parse-attach-property :planned-started planned-start-date parse-yyyy-MM-dd-date)
        (parse-attach-property :planned-ended planned-end-date parse-yyyy-MM-dd-date))))

(defn parse-funding-award [funding-loc]
  (let [amount (xml/xselect1 funding-loc ["amount"])
        currency (xml/xselect1 funding-loc ["currency"])
        percentage (xml/xselect1 funding-loc ["funding-percentage"])]
    {:type :award
     :amount (try (Float/parseFloat amount) (catch Exception _ nil))
     :percentage (try (Integer/parseInt percentage) (catch Exception _ nil))
     :currency currency}))

(defn parse-project-investigators [kind project-loc]
  (map
   #(-> %
        parse-person-name
        (dissoc :sequence)
        (parse-attach-property :started (xml/xselect1 % ["start-date"]) parse-yyyy-MM-dd-date)
        (parse-attach-property :ended (xml/xselect1 % ["end-date"]) parse-yyyy-MM-dd-date))
   (find-investigators project-loc kind)))

(defn parse-project-fundings [project-loc]
  (map
   #(-> {:type :funding
         :subtype (xml/xselect1 % ["funding-type"])
         :scheme (xml/xselect1 % "funding-scheme" :text)}
        (parse-attach-rel :awarded % :single parse-funding-award)
        (parse-attach-rel :funder % :single parse-grants-funders))
   (xml/xselect project-loc "funding")))

(defn parse-grant-project [project-loc]
  (-> {:type :project}
      (parse-attach-property :title project-loc parse-multiple-items (parse-text-language-item :xml) "project-title")
      (parse-attach-property :description project-loc parse-multiple-items (parse-text-language-item :text) "description")
      (parse-attach-rel :awarded project-loc :single parse-project-award)
      (parse-attach-rel :lead-investigator project-loc :multi (partial parse-project-investigators "lead_investigator"))
      (parse-attach-rel :co-lead-investigator project-loc :multi (partial parse-project-investigators "co-lead_investigator"))
      (parse-attach-rel :investigator project-loc :multi (partial parse-project-investigators "investigator"))
      (parse-attach-rel :funding project-loc :multi parse-project-fundings)))

(defn parse-item
  "Pulls out metadata that is somewhat standard across types: contributors,
   resource urls, some dates, titles, ids, citations, components, crossmark assertions
   and programs."
  [item-loc]
  (let [parse-similarity-collection (partial parse-collection
                                             "crawler-based"
                                             ["iParadigms" "similarity-check"]
                                             "similarity-checking")
        relationships (parse-relationships-as-rels item-loc)]

    (-> {:type :work :rel relationships}
        (attach-ids (parse-item-id-uris item-loc))
        (parse-attach-rel :update-policy item-loc :single parse-update-policy)
        (parse-attach-rel :updates item-loc :multi parse-updates)
        (parse-attach-rel :component item-loc :multi parse-component-list)
        (parse-attach-rel :institution item-loc :multi institutions->reference-orgs)
        (parse-attach-rel :funder item-loc :multi parse-item-funders)
        (parse-attach-rel :clinical-trial-number item-loc :multi parse-item-clinical-trial-numbers)
        (parse-attach-rel :author item-loc :multi (partial parse-item-contributors "author"))
        (parse-attach-rel :editor item-loc :multi (partial parse-item-contributors "editor"))
        (parse-attach-rel :translator item-loc :multi (partial parse-item-contributors "translator"))
        (parse-attach-rel :chair item-loc :multi (partial parse-item-contributors "chair"))
        (parse-attach-rel :resource-resolution item-loc :single parse-resource)
        (parse-attach-rel :resource-resolution item-loc :multi parse-multiresolution-resource)
        (parse-attach-rel :resource-fulltext item-loc :multi (partial parse-collection "text-mining"))
        (parse-attach-rel :resource-fulltext item-loc :multi (partial parse-collection "unspecified"))
        (parse-attach-rel :resource-fulltext item-loc :multi (partial parse-collection "syndication"))
        (parse-attach-rel :resource-fulltext item-loc :multi parse-similarity-collection)
        (parse-attach-rel :license item-loc :multi parse-item-licenses)
        (parse-attach-rel :archived-with item-loc :multi parse-item-archives)
        (parse-attach-rel :cites item-loc :multi parse-item-citations)
        (parse-attach-rel :assertion item-loc :multi parse-item-assertions)
        (parse-attach-rel :issn item-loc :multi parse-item-issn-details)
        (parse-attach-rel :isbn item-loc :multi parse-item-isbn-details)

        (parse-attach-property :status item-loc parse-item-status)
        (parse-attach-property :supplementary-id item-loc parse-item-supplementary-id-uris)
        (parse-attach-property :title-short item-loc parse-multiple-items (parse-text-language-item :xml) "abbrev_title")
        (parse-attach-property :title-group item-loc parse-multiple-items (parse-text-language-item :xml) "group_title")
        (parse-attach-property :title-long item-loc parse-multiple-items (parse-text-language-item :xml) "full_title" "proceedings_title" ["titles" "title"])
        (parse-attach-property :title-secondary item-loc parse-multiple-items (parse-text-language-item :xml) ["titles" "subtitle"])
        (parse-attach-property :title-original item-loc parse-multiple-items (parse-text-language-item :xml) ["titles" "original_language_title"])

        (parse-attach-property :version-info item-loc parse-item-version-info)
        (parse-attach-property :approved item-loc parse-item-approval-dates)
        (parse-attach-property :published-print item-loc  parse-item-pub-dates "print")
        (parse-attach-property :published-online item-loc parse-item-pub-dates "online")
        (parse-attach-property :published-other item-loc parse-item-pub-dates "other")
        (parse-attach-property :published item-loc parse-item-pub-dates)
        (parse-attach-property :posted item-loc parse-item-posted-date)
        (parse-attach-property :accepted item-loc parse-item-accepted-date)

        (parse-attach-property :number item-loc parse-multiple-items parse-item-number ["publisher_item" "item_number"])
        (parse-attach-property :abstract item-loc parse-multiple-items (parse-text-language-item :xml) "abstract")
        (parse-attach-property :crossmark-domains item-loc parse-multiple-items #(string/trim (xml/xselect1 % :text)) [:> "crossmark" "crossmark_domains" "crossmark_domain"])
        (parse-attach-property :crossmark-domain-exclusive item-loc parse-single-item #(when % (= (string/lower-case %) "true"))  [:> "crossmark" "crossmark_domain_exclusive" :text]))))

;; crawler should be 'crawler-based' - but we may not want to include those anyway

;; -----------------------------------------------------------------
;; Specific item parsing

(defn parse-grant [grant-loc]
  (when grant-loc
    (let [project-parse-fn #(map parse-grant-project (find-grant-projects %))]
      (-> (parse-item grant-loc)
          (parse-attach-rel :awarded grant-loc :single parse-grant-award)
          (parse-attach-rel :project grant-loc :multi project-parse-fn)
          (conj {:subtype :grant})))))

(defn parse-posted-content [item-loc]
  (when item-loc
    (conj (parse-item item-loc)
          {:subtype :posted-content
           :content-type (xml/xselect1 item-loc ["type"])})))

(defn parse-journal-article [article-loc]
  (when article-loc
    (-> (parse-item article-loc)
        (parse-attach-property :free-to-read-start (xml/xselect1 article-loc "doi_data" "free_to_read" ["start_date"]) parse-yyyy-MM-dd-date)
        (parse-attach-property :free-to-read-end (xml/xselect1 article-loc "doi_data" "free_to_read" ["end_date"]) parse-yyyy-MM-dd-date)
        (conj {:subtype :journal-article
               :first-page (xml/xselect1 article-loc "pages" "first_page" :text)
               :last-page (xml/xselect1 article-loc "pages" "last_page" :text)
               :other-pages (xml/xselect1 article-loc "pages" "other_pages" :text)}))))

(defn parse-journal-issue [issue-loc]
  (when issue-loc
    (-> (parse-item issue-loc)
        (conj {:subtype :journal-issue
               :numbering (xml/xselect1 issue-loc "special_numbering" :text)
               :published-print (first (parse-item-pub-dates issue-loc "print"))
               :published-online (first (parse-item-pub-dates issue-loc "online"))
               :issue (xml/xselect1 issue-loc "issue" :text)}))))

(defn parse-journal-volume [volume-loc]
  (when volume-loc
    (-> (parse-item volume-loc)
        (conj {:subtype :journal-volume
               :volume (xml/xselect1 volume-loc "volume" :text)}))))

(defn parse-journal [journal-loc]
  (when journal-loc
    (let [issue (-> journal-loc (find-journal-issue) (parse-journal-issue))
          volume (-> journal-loc (find-journal-volume) (parse-journal-volume))
          article (-> journal-loc (find-journal-article) (parse-journal-article))
          meta-loc (find-journal-metadata journal-loc)
          journal (-> meta-loc
                      (parse-item)
                      (conj {:subtype :journal
                             :language (xml/xselect1 meta-loc ["language"])}))]
      (cond
        (nil? issue)   (->> article
                            (attach-rel journal :component))
        (nil? volume)  (->> article
                            (attach-rel issue :component)
                            (attach-rel journal :component))
        :else          (->> article
                            (attach-rel volume :component)
                            (attach-rel issue :component)
                            (attach-rel journal :component))))))

(defn parse-conf-paper [conf-paper-loc]
  (when conf-paper-loc
    (-> (parse-item conf-paper-loc)
        (conj
         {:subtype :proceedings-article
          :first-page (xml/xselect1 conf-paper-loc "pages" "first_page" :text)
          :last-page (xml/xselect1 conf-paper-loc "pages" "last_page" :text)
          :other-pages (xml/xselect1 conf-paper-loc "pages" "other_pages" :text)}))))

(defn parse-review [peer-review-loc]
  (let [running-number (xml/xselect1 peer-review-loc "running_number" :text)
        competing-interest-statement (xml/xselect1 peer-review-loc "competing_interest_statement" :text)
        revision-round (xml/xselect1 peer-review-loc ["revision-round"])
        stage (xml/xselect1 peer-review-loc ["stage"])
        recommendation (xml/xselect1 peer-review-loc ["recommendation"])
        r-type (xml/xselect1 peer-review-loc ["type"])
        language (xml/xselect1 peer-review-loc ["language"])]
    {:review {:running-number (safe-trim running-number)
              :revision-round (safe-trim revision-round)
              :stage (safe-trim stage)
              :competing-interest-statement (safe-trim competing-interest-statement)
              :recommendation (safe-trim recommendation)
              :review-type (safe-trim r-type)
              :language (safe-trim language)}}))

(defn parse-peer-review [peer-review-loc]
  (when peer-review-loc
    (-> (parse-item peer-review-loc)
        (parse-attach-rel :author peer-review-loc :multi (partial parse-item-contributors "reviewer"))
        (parse-attach-property :published (xml/xselect1 peer-review-loc "review_date") parse-date)
        (conj (parse-review peer-review-loc))
        (conj {:subtype :peer-review}))))

(defn parse-event [event-loc]
  (when event-loc
    (-> (parse-item event-loc)
        (parse-attach-property :start event-loc (comp parse-start-date find-event-date))
        (parse-attach-property :end event-loc (comp parse-end-date find-event-date))
        (conj
         {:type :event
          :subtype :conference
          :name (xml/xselect1 event-loc "conference_name" :text)
          :theme (xml/xselect1 event-loc "conference_theme" :text)
          :location (parse-multiple-items event-loc (parse-text-language-item :text) "conference_location")
          :sponsor (xml/xselect event-loc "conference_sponsor" :text)
          :acronym (parse-multiple-items event-loc (parse-text-language-item :text) "conference_acronym")
          :number (xml/xselect1 event-loc "conference_number" :text)}))))

(defn parse-conf [conf-loc]
  (when conf-loc
    (let [series-loc (xml/xselect1 conf-loc :> "series_metadata")
          proceedings-loc (or (xml/xselect1 conf-loc :> "proceedings_metadata")
                              (xml/xselect1 conf-loc :> "proceedings_series_metadata"))
          proceedings (-> (parse-item proceedings-loc)
                          (parse-attach-rel :about conf-loc :single (comp parse-event find-event))
                          (parse-attach-rel :component conf-loc :single (comp parse-conf-paper find-conf-paper))
                          (conj
                           {:subtype :proceedings
                            :coden (xml/xselect1 conf-loc :> "coden" :text)
                            :subject (xml/xselect1 conf-loc :> "proceedings_subject" :text)
                            :volume (or (xml/xselect1 conf-loc "proceedings_series_metadata" "volume" :text)
                                        (xml/xselect1 conf-loc "proceedings_metadata" "volume" :text))
                            :publisher (parse-item-publisher proceedings-loc)}))]
      (if series-loc
        (-> (parse-item series-loc)
            (assoc :subtype :proceedings-series)
            (attach-rel :component proceedings))
        proceedings))))

(defn parse-content-item-type [content-item-loc]
  (let [type (xml/xselect1 content-item-loc ["component_type"])]
    (condp = type
      "chapter" :chapter
      "section" :section
      "part" :part
      "track" :track
      "reference_entry" :reference-entry
      "other" :other
      :other)))

(defn parse-content-item [content-item-loc]
  (when content-item-loc
    (-> (parse-item content-item-loc)
        (conj
         {:subtype :component
          :language (xml/xselect1 content-item-loc ["language"])
          :nest-level (xml/xselect1 content-item-loc ["level_sequence_number"])
          :component-number (xml/xselect1 content-item-loc "component_number" :text)
          :first-page (xml/xselect1 content-item-loc "pages" "first_page" :text)
          :last-page (xml/xselect1 content-item-loc "pages" "last_page" :text)
          :other-pages (xml/xselect1 content-item-loc "pages" "other_pages" :text)}))))

(defn parse-book-content-item [content-item-loc]
  (when content-item-loc
    (-> (parse-content-item content-item-loc)
        (assoc :subtype :book-component)
        (assoc :book-component-type (parse-content-item-type content-item-loc)))))

(defn parse-report-content-item [content-item-loc]
  (when content-item-loc
    (-> (parse-content-item content-item-loc)
        (assoc :subtype :report-component))))

(defn parse-content-items [parent-loc]
  (map parse-content-item (xml/xselect parent-loc "content_item")))

(defn parse-report-content-items [parent-loc]
  (map parse-report-content-item (xml/xselect parent-loc "content_item")))

(defn parse-single-book* [book-meta-loc content-item-loc book-type]
  (-> (parse-item book-meta-loc)
      (parse-attach-rel :component content-item-loc :single parse-book-content-item)
      (conj
       {:subtype :book
        :book-type book-type
        :language (xml/xselect1 book-meta-loc ["language"])
        :edition-number (xml/xselect1 book-meta-loc "edition_number" :text)
        :volume (xml/xselect1 book-meta-loc "volume" :text)
        :publisher (parse-item-publisher book-meta-loc)})))

(defn parse-single-book [book-meta-loc content-item-loc book-type]
  (if-let [series-meta-loc (xml/xselect1 book-meta-loc "series_metadata")]
    (-> (parse-item series-meta-loc)
        (parse-attach-rel :component book-meta-loc
                          :single #(parse-single-book* % content-item-loc book-type))
        (conj {:subtype :book-series}))
    (parse-single-book* book-meta-loc content-item-loc book-type)))

(defn parse-book-set [book-set-meta-loc content-item-loc book-type]
  (-> (parse-item (xml/xselect1 book-set-meta-loc "set_metadata"))
      (parse-attach-rel
       :component book-set-meta-loc
       :single #(parse-single-book* % content-item-loc book-type))
      (conj
       {:subtype :book-set
        :part-number (xml/xselect1 book-set-meta-loc "set_metadata" "part_number" :text)
        :publisher (parse-item-publisher book-set-meta-loc)})))

(defn parse-book-series [book-series-meta-loc content-item-loc book-type]
  (-> (parse-item (xml/xselect1 book-series-meta-loc "series_metadata"))
      (parse-attach-rel
       :component book-series-meta-loc
       :single #(parse-single-book* % content-item-loc book-type))
      (conj
       {:subtype :book-series
        :coden (xml/xselect1 book-series-meta-loc "series_metadata" "coden" :text)
        :series-number (xml/xselect1 book-series-meta-loc "series_metadata" "series_number" :text)
        :publisher (parse-item-publisher book-series-meta-loc)})))

(defn parse-book-type [book-loc]
  (let [type (xml/xselect1 book-loc ["book_type"])]
    (condp = type
      "edited_book" :edited-book
      "monograph" :monograph
      "reference" :reference
      "other" :other
      :book)))

(defn parse-book [book-loc]
  (let [book-meta-loc (xml/xselect1 book-loc "book_metadata")
        book-series-meta-loc (xml/xselect1 book-loc "book_series_metadata")
        book-set-meta-loc (xml/xselect1 book-loc "book_set_metadata")
        content-item-loc (xml/xselect1 book-loc "content_item")
        book-type (parse-book-type book-loc)]
    (cond
      book-meta-loc (parse-single-book book-meta-loc content-item-loc book-type)
      book-series-meta-loc (parse-book-series book-series-meta-loc content-item-loc book-type)
      book-set-meta-loc (parse-book-set book-set-meta-loc content-item-loc book-type))))


(defn parse-dataset-type [dataset-loc]
  (let [type (xml/xselect1 dataset-loc ["dataset_type"])]
    (condp = type
      "record" :record
      "collection" :collection
      "crossmark_policy" :crossmark-policy
      "other" :other
      :record)))

(defn parse-dataset [dataset-loc]
  (-> (parse-item dataset-loc)
      (parse-attach-property :published dataset-loc parse-database-pub-dates)
      (parse-attach-property :content-created dataset-loc parse-database-created-dates)
      (parse-attach-property :content-updated dataset-loc parse-database-updated-dates)
      (conj
       {:subtype :dataset
        :kind (parse-dataset-type dataset-loc)
        :format (xml/xselect1 dataset-loc "format" :text)
        :format-mime (xml/xselect1 dataset-loc "format" ["mime_type"] :text)
        :description (parse-multiple-items dataset-loc (parse-text-language-item :text) "description")})))

(defn parse-datasets [database-loc]
  (map parse-dataset (xml/xselect database-loc "dataset")))

(defn parse-database [database-loc]
  (when database-loc
    (let [metadata-loc (xml/xselect1 database-loc "database_metadata")]
      (-> (parse-item metadata-loc)
          (parse-attach-rel :component database-loc :multi parse-datasets)
          (parse-attach-property :published metadata-loc parse-database-pub-dates)
          (parse-attach-property :content-created metadata-loc parse-database-created-dates)
          (parse-attach-property :content-updated metadata-loc parse-database-updated-dates)
          (conj
           {:subtype :database
            :language (xml/xselect1 metadata-loc ["language"] :text)
            :description (parse-multiple-items metadata-loc (parse-text-language-item :text) "description")
            :publisher (parse-item-publisher metadata-loc)})))))

(defn parse-report-contract-number [report-loc]
  (xml/xselect1 report-loc "contract_number" :text))

(defn parse-single-report [report-loc report-meta-loc]
  (-> (parse-item report-meta-loc)
      (parse-attach-rel :component report-loc :multi parse-report-content-items)
      (conj
       {:subtype :report
        :contract-number (parse-report-contract-number report-meta-loc)
        :publisher (parse-item-publisher report-meta-loc)})))

(defn parse-report-series [report-loc report-meta-loc series-meta-loc]
  (-> (parse-item series-meta-loc)
      (conj
       {:subtype :report-series
        :coden (xml/xselect1 series-meta-loc "coden" :text)
        :series-number (xml/xselect1 series-meta-loc "series_number" :text)
        :publisher (parse-item-publisher series-meta-loc)})
      (parse-attach-rel :component report-meta-loc :single (partial parse-single-report report-loc))))

(defn parse-report [report-loc]
  (let [report-meta-loc (or (xml/xselect1 report-loc "report-paper_metadata")
                            (xml/xselect1 report-loc "report-paper_series_metadata"))
        series-meta-loc (or (xml/xselect1 report-loc "report-paper_metadata" "series_metadata")
                            (xml/xselect1 report-loc "report-paper_series_metadata" "series_metadata"))]
    (cond
      series-meta-loc
      (parse-report-series report-loc report-meta-loc series-meta-loc)
      report-meta-loc
      (parse-single-report report-loc report-meta-loc))))

(defn parse-standards-body [standards-body-loc]
  (when standards-body-loc
    {:type :org
     :subtype :standards-body
     :name (xml/xselect1 standards-body-loc "standards_body_name" :text)
     :acronym (parse-multiple-items standards-body-loc  (parse-text-language-item :text) "standards_body_acronym")}))

(defn parse-single-standard [standard-loc]
  (let [standard-metadata-loc (xml/xselect1 standard-loc "standard_metadata")
        standards-body-loc (xml/xselect1 standard-metadata-loc "standards_body")]
    (-> (parse-item standard-metadata-loc)
        (parse-attach-rel :component standard-loc :multi parse-content-items)
        (parse-attach-rel :standards-body standards-body-loc :single parse-standards-body)
        (conj
         {:subtype :standard
          :volume (xml/xselect1 standard-metadata-loc "volume" :text)
          :edition-number (xml/xselect1 standard-metadata-loc "edition_number" :text)
          :publisher (parse-item-publisher standard-metadata-loc)}))))

(defn parse-standard-series [standard-loc series-meta-loc]
  (-> (parse-item series-meta-loc)
      (parse-attach-rel :component standard-loc :multi parse-content-items)
      (conj
       {:subtype :standard-series
        :coden (xml/xselect1 series-meta-loc "coden" :text)
        :series-number (xml/xselect1 series-meta-loc "series_number" :text)})))

(defn parse-single-standard-with-series [standard-loc series-meta-loc]
  (-> (parse-standard-series standard-loc series-meta-loc)
      (parse-attach-rel :component standard-loc :single parse-single-standard)))

(defn parse-standard [standard-loc]
  (let [standard-meta-loc (xml/xselect1 standard-loc "standard_metadata")
        standard-series-meta-loc (xml/xselect1 standard-loc "standard_series_metadata")
        series-meta-loc (xml/xselect1 standard-loc "standard_metadata" "series_metadata")]
    (cond
      series-meta-loc
      (parse-single-standard-with-series standard-loc series-meta-loc)
      standard-meta-loc
      (parse-single-standard standard-loc)
      standard-series-meta-loc
      (parse-standard-series standard-loc standard-series-meta-loc))))

(defn parse-dissertation [dissertation-loc]
  (when dissertation-loc
    (let [person-loc (xml/xselect dissertation-loc "person_name")]
      (-> (parse-item dissertation-loc)
          (parse-attach-rel :author person-loc :single parse-person-name)
          (parse-attach-rel :component dissertation-loc :multi parse-content-items)
          (parse-attach-property :degree dissertation-loc parse-multiple-items (parse-text-language-item :text) "degree")
          (conj
           {:subtype :dissertation
            :language (xml/xselect1 dissertation-loc ["language"] :text)})))))

(defn parse-stand-alone-component [sa-component-loc]
  (when sa-component-loc
    (let [id (xml/xselect1 sa-component-loc ["parent_doi"] :text)
          parent-doi (to-doi-uri id)]
      (-> {:type :work}
          (attach-id parent-doi)
          (parse-attach-rel :component sa-component-loc :multi parse-component-list)))))

(defn parse-pending-publication [pending-publication-loc]
  (when pending-publication-loc
    (-> (parse-item pending-publication-loc)
        (conj
         {:subtype :pending-publication}))))

;; ---------------------------------------------------------------

(defn parse-primary-id [record]
  (-> record
      (xml/xselect1 "header" "identifier" :text)
      (to-doi-uri)))

(def oai-deposit-date-formatter (tf/formatter "yyyy-MM-dd"))
(def openurl-deposit-date-formatter (tf/formatter "yyyy-MM-dd HH:mm:ss"))

(defn parse-oai-deposit-date [loc]
  (when-let [date-text (xml/xselect1 loc "header" "datestamp" :text)]
    (tf/parse oai-deposit-date-formatter date-text)))

(defn parse-openurl-deposit-date [loc]
  (when-let [date-text (xml/xselect1 loc ["timestamp"])]
    (tf/parse openurl-deposit-date-formatter date-text)))

(defn parse-deposit-date [record]
  (when-let [d (or (parse-oai-deposit-date record)
                   (parse-openurl-deposit-date record))]
    (parse-date (t/year d) (t/month d) (t/day d))))

(defn unixref-record-parser
  "Returns a tree of the items present in an OAI record. Each item has 
   :contributor, :cites and :component keys that may list further items.

   Items can be any of:

   work
     journal
     journal-issue
     journal-volume
     journal-article
     proceedings
     proceedings-article
     report
     report-series
     standard
     standard-series
     dataset
     edited-book
     monograph
     reference-book
     book
     book-series
     book-set
     chapter
     section
     part
     track
     reference-entry
     dissertation
     other
   event
     conference
   citation
   peer-review
   person
   org
   date
   title
     short
     long
     secondary
   grant
   license

   Relations include:

   published-online
   published-print
   deposited
   first-deposited
   start
   end
   citation
   author
   chair
   translator
   editor
   contributor
   about
   title
   resource-fulltext
   resource-resolution
   affiliation
   publisher
   component
   number
   grant
   funder
   archived-with

   Each item may have a list of :id structures, a list of :title structures,
   a list of :date structures, any number of flat :key value pairs, and finally, 
   relationship structures :rel, which contains a key per relation type. Some
   common relationship types include :author, :cites and :component.
   Anything mentioned as a value in :rel will be an item itself.

   The result of this function is a list, [primary-id, item-tree]."
  [oai-record]
  (let [work (or
              (parse-stand-alone-component (find-stand-alone-component oai-record))
              (parse-dissertation (find-dissertation oai-record))
              (parse-standard (find-standard oai-record))
              (parse-report (find-report oai-record))
              (parse-database (find-database oai-record))
              (parse-journal (find-journal oai-record))
              (parse-posted-content (find-posted-content oai-record))
              (parse-book (find-book oai-record))
              (parse-conf (find-conf oai-record))
              (parse-peer-review (find-peer-review oai-record))
              (parse-grant (find-grant oai-record))
              (parse-pending-publication (find-pending-publication oai-record)))]
    (if work
      [(parse-primary-id oai-record) 
      (parse-attach-property work :deposited oai-record parse-deposit-date)]
      [(parse-primary-id oai-record) work])))
