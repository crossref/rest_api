<?xml version="1.0" encoding="UTF-8"?>
<crossref_result xmlns="http://www.crossref.org/qrschema/3.0" version="3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/qrschema/3.0 http://www.crossref.org/schemas/crossref_query_output3.0.xsd">
  <query_result>
    <head>
      <doi_batch_id>none</doi_batch_id>
    </head>
    <body>
      <query status="resolved">
        <doi type="grant">10.35802/215573</doi>
        <crm-item name="publisher-name" type="string">Wellcome</crm-item>
        <crm-item name="prefix-name" type="string">Wellcome</crm-item>
        <crm-item name="member-id" type="number">13928</crm-item>
        <crm-item name="citation-id" type="number">112083922</crm-item>
        <crm-item name="book-id" type="number">3408981</crm-item>
        <crm-item name="deposit-timestamp" type="number">20201217181734806</crm-item>
        <crm-item name="owner-prefix" type="string">10.35802</crm-item>
        <crm-item name="last-update" type="date">2020-12-17T19:04:32Z</crm-item>
        <crm-item name="created" type="date">2019-12-09T11:31:17Z</crm-item>
        <crm-item name="citedby-count" type="number">0</crm-item>
        <doi_record>
          <crossref xmlns="http://www.crossref.org/xschema/1.1" xsi:schemaLocation="http://www.crossref.org/xschema/1.1 http://doi.crossref.org/schemas/unixref1.1.xsd">
            <grant xmlns="http://www.crossref.org/grant_id/0.0.1">
              <project>
                <project-title xml:lang="en">Integrative imaging of brain structure and function in populations and individuals</project-title>
                <project-title>Another title</project-title>
                <investigators>
                  <person role="investigator" start-date="2017-05-29" end-date="2017-08-29">
                    <givenName>Mark</givenName>
                    <familyName>Jenkinson</familyName>
                    <alternateName>Mark Jenkinson</alternateName>
                    <alternateName>M. Jenkinson</alternateName>
                    <affiliation>
                      <institution country="GB">University of Oxford</institution>
                      <ROR>https://ror.org/052gg0110</ROR>
                    </affiliation>
                    <affiliation>
                      <institution>Some Other University</institution>
                    </affiliation>
                    <ORCID>https://orcid.org/0000-0001-6043-0166</ORCID>
                  </person>
                  <person role="investigator">
                    <givenName>Christian</givenName>
                    <familyName>Beckmann</familyName>
                    <affiliation>
                      <institution country="NL">Radboud Universiteit Nijmegen</institution>
                    </affiliation>
                  </person>
                  <person role="lead_investigator">
                    <givenName>Stephen</givenName>
                    <familyName>Smith</familyName>
                    <affiliation>
                      <institution country="GB">University of Oxford</institution>
                    </affiliation>
                    <ORCID>https://orcid.org/0000-0001-8166-069X</ORCID>
                  </person>
                  <person role="co-lead_investigator">
                    <givenName>Steven</givenName>
                    <familyName>Schmit</familyName>
                  </person>
                </investigators>
                <description xml:lang="en">Neuroimaging enables the mapping of many aspects of the brain’s anatomy, connections, and function. New landmark studies including UK Biobank and the Human Connectome Projects are taking neuroimaging to the scale of populations. Such studies deploy multiple imaging modalities with the aim of learning more about the brain, and identifying imaging markers relevant to neurological disease. However, we cannot currently take full advantage of the richness of these new resources, including: major advances in the quality of data; complementarity of multi-modal imaging; large subject numbers; and linked information about health outcomes, genetics and risk factors. Our vision is to extend the reach of imaging neuroscience. This requires new research across multiple domains: from integrated cross-modal analysis, to more detailed and biologically-interpretable markers, to machine learning. We aim to enable neuroimaging to achieve its full potential, from the modelling of variation in populations to the characterisation of individual subjects. We will deliver powerful modelling approaches and research platforms, continuing our long track record of disseminating software for use by basic and clinical neuroscientists. Further, we will leverage our leadership in big data projects to demonstrate how these approaches and computational tools can advance our understanding of the brain and its diseases.</description>
                <description xml:lang="en">Neuroscientists and doctors use neuroimaging to study the brain without cutting into it.</description>
                <award_amount currency="GBP">4106203</award_amount>
                <funding amount="4106202" currency="GBP" funding-type="grant" funding-percentage="99">
                  <funder-name>Wellcome Trust</funder-name>
                  <funder-id>https://doi.org/10.13039/100004440</funder-id>
                  <funding-scheme>Cognitive Neuroscience and Mental Health</funding-scheme>
                </funding>
                <funding amount="1" currency="GBP" funding-type="facilities" funding-percentage="1">
                  <funder-name>Another Funder</funder-name>
                  <funder-id>https://doi.org/10.13039/1000</funder-id>
                </funding>
                <award-dates start-date="2019-06-01" end-date="2024-05-31" planned-start-date="2019-05-01" planned-end-date="2024-04-30" />
              </project>
              <project>
                <project-title xml:lang="en">Second project</project-title>
                <investigators>
                  <person role="investigator">
                    <givenName>Steve</givenName>
                    <familyName>Curry</familyName>
                    <affiliation>
                      <institution country="GB">University of Oxford</institution>
                    </affiliation>
                  </person>
                </investigators>
                <award_amount currency="GBP">9</award_amount>
                <funding amount="9" currency="GBP" funding-type="grant" funding-percentage="100">
                  <funder-name>Wellcome Trust</funder-name>
                  <funder-id>https://doi.org/10.13039/100004440</funder-id>
                </funding>
              </project>
              <award-number>215573</award-number>
              <award-start-date>2019-11-25</award-start-date>
              <program xmlns="http://www.crossref.org/relations.xsd">
                <related_item>
                  <intra_work_relation identifier-type="doi" relationship-type="finances">10.5555/5555</intra_work_relation>
                </related_item>
              </program>
              <doi_data>
                <doi>10.35802/215573</doi>
                <resource>https://europepmc.org/grantfinder/grantdetails?query=pi:"Jenkinson+M"+gid:"215573"+ga:"Wellcome Trust"</resource>
              </doi_data>
            </grant>
          </crossref>
        </doi_record>
      </query>
    </body>
  </query_result>
</crossref_result>
