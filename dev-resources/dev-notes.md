# Local development and debugging using Calva and VS Code

## Elasticearch

```shell
docker run -d --name elasticsearch -p 127.0.0.1:9200:9200 -p 127.0.0.1:9300:9300 -e "discovery.type=single-node" elasticsearch:6.8.13
```

Calva is a very good solution for developing Clojure using VS Code.
The combination is good enough but probably not the best Clojure IDE ever.
Still it is easier than Emacs + Cider for newcomers and it is free, in contrast to Idea + cursive.

The following Calva config allows us to create a configuration for loading the necessary env vars in order to run a REPL locally without a container. Upon launching Calva we should be able to select the following `rest-api` configuration (and then ideally use the `:dev` profile). 

To use this configuration:
- go to the Calva plugin settings within VS Code
- find `Output Destinations -> Edit in settings.json`
- paste the bellow configuration at the top level of the settings object

```json
"calva.replConnectSequences": [

    {
      "name": "rest-api",
      "projectType": "Leiningen",
      "jackInEnv": {
        "API_PORT": 3000,
        "AWS_ACCESS_KEY": 1,
        "AWS_ENDPOINT": "http://localhost:4566",
        "AWS_REGION": "us-east-1",
        "AWS_SECRET_KEY": 1,
        "ELASTICSEARCH_NO_AUTH": 1,
        "ELASTICSEARCH_REFRESH_INTERVAL": "2s",
        "ELASTICSEARCH_URL": "http://localhost:9200",
        "INDEXING_TIMEOUT": 10,
        "KEYCLOAK_URL": "http://keycloak:8888/auth",
        "KEYCLOAK_CLIENT_SECRET": "**********",
        "METADATA_BUCKET": "md-bucket",
        "METADATA_LOCAL_STORAGE": 1,
        "NREPL_DOCKER": 1,
        "NREPL_PORT": 7880,
        "SNAPSHOT_BUCKET": "sn-bucket",
        "SNAPSHOT_DOWNLOAD": 1,
        "SNAPSHOT_LOCAL_STORAGE": 1,
        "SQS_QUEUE_URL": "http://localhost:4566/000000000000/mdqueue"
      }
    }
  ]
  ```

## Launching services after connecting to the REPL 
    
At the REPL call `(begin)`:

```clojure
(begin)
```
	
To start a test version of the API type:

```clojure
(user/start)
```

Then, to load the corpus of documents complete with coverage checks (the same function that's used in the integration tests):

```clojure
(user/index-feed)
```

Or to index a single file:

```clojure
(user/index-work-files ["dev-resources/parser-regression/funder.xml"])
```

Then visit the API endpoint, e.g. <http://localhost:3000/v1/works>

# Generating parser regression .edn files

While developing we often need to add or update elements in the item-tree, this is done through `cayenne.formats.unixref`.
Most of the time this means that we will need to modify the resulting `.edn` files in `dev-resources/parser-regression`.
The following code allows us to re-generate any missing files. Ideally we would delete the affected `.edn` files and run `(generate-result-files)` to generate them for us. 

Therefore:

1. Modify the item-tree
2. Delete any affected .edn files
3. Regenerate using the below code
4. Copy all new .edn files to `dev-resources/convert-regression`

```clojure
; Re-generate parser regression .edn files 
(load "/cayenne/formats/parser_regression_test")
(in-ns 'cayenne.formats.parser-regression-test)
(generate-result-files)
```

# Generating all test files

After a bit of refactoring to add unit tests for the conversion between es-doc and citeproc-json we 
moved the produced test files under their own directories, so now es- doc is under `dev-resources/convert-regression/es-json`
and citeproc-json is under `dev-resources/convert-regression/citeproc-json`.

A copy of all of the edn files exists on the parent level
`dev-resources/convert-regression` so that we can maintain a list of edn files for conversion separately from those meant for ingestion.

As of lately though that doesn't seem to be  all that necessary and in the future we may keep just one copy of all edn files and convert all of them to es-doc and citeproc-json.

```clojure
;; REST-API REGENERATE TEST FILES
;; ==============================

; $ rm dev-resources/parser-regression/*.edn

(load "/cayenne/formats/parser_regression_test")
(in-ns 'cayenne.formats.parser-regression-test)
(generate-result-files)

; $ cp dev-resources/parser-regression/*.edn dev-resources/convert-regression/
; $ rm dev-resources/convert-regression/es-json/*.json

(load "/cayenne/elastic/convert_regression_test")
(in-ns 'cayenne.elastic.convert-regression-test)
(generate-result-files)

; $ rm dev-resources/convert-regression/citeproc-json/*.json

(load "/cayenne/api/v1/output_regression_test")
(in-ns 'cayenne.api.v1.output-regression-test)
(generate-result-files)
```

# Adding new xml elements

Every new xml element has to be appropriately modeled in `cayenne.formats.unixref`.
If the element is meant to be included into the es-doc format then it needs to be modeled as an Elasticsearch mapping in `cayenne.elastic.mappings`.
Some elements, such as a single values for example, may be straight forward to model but others may need to be modeled as nested documents.

For example:

```clojure
(def funder-id-properties
  {:id {:type "keyword"}
   :id-type {:type "keyword"}
   :asserted-by {:type "keyword"}})

(def work-funder-properties
  {:name            {:type "keyword" :copy_to [:funder-name :funder-name-text]}
   :doi             {:type "keyword" :copy_to :funder-doi}
   :doi-asserted-by {:type "keyword"}
   :award           {:type "text"} ; deprecated; do not use
   :award-original  {:type "keyword"}
   :award-keyword   {:type "keyword"}
   :id     {:type "nested" :properties funder-id-properties}})
```

Here `id` is an array of objects with their own data structure.
We need to use the `nested` type and define a map describing the nested object.

## copy_to

In order to optimise search we can use the `copy_to` mapping to copy the (usually nested) value of an attribute to the parent node. This will allow us to avoid nested search queries.

**Important**: Elasticsearch does not (yet at least) provide support for conditionally using `copy_to`.

For example, in the above mapping we have a list of funder ids and we would like to only copy the ROR ids, if any, to `:institution-ror-id`.
In such a situation we cannot use copy to as it would only be able to copy all of the ids within `:id`.

In this case we can implemented this functionality in `cayenne.formats.convert`.

For example:

```clojure
(defn assoc-exists-funder-ror-id 
  "Assoc a funder ROR id to the top level :institution-ror-id if a funder has one."
  [item] 
  (let [ror-ids (->>
                 (:funder item)
                 (mapcat #(:id %))
                 (map #(when (= "ROR" (:id-type %)) (:id %)))
                 (remove nil?))]
    (util/assoc-exists item :institution-ror-id ror-ids)))
```
> Another solution would be to create a new top level attribute in the es-doc to store these ROR ids (something like `:funder-ror-ids [...]`) and use `copy_to` to copy these to `:institution-ror-id`.

# Creating/updating filters

API filters are defined in `cayenne.api.v1.filter`.

In order to introduce or update an API filter we will have to construct or modify existing Elasticsearch queries.
These queries are not constructed by hand. Rather, we use a set of functions including but not limited to `[binary-values, greater-than-zero, range-nested-query, match-nested-query, existence, bool, equality, nested-terms, nested]`.

These functions model Elasticsearch clauses and can be mixed and matched to a certain extend in order to construct a search query.

## Modeling queries

When trying to create a new filter first try to realise the actual ES query you are trying to construct. It will probably be difficult to get a query right without knowing the ES search syntax.

In order to develop for the REST API we run ES locally, therefore we can run queries against http://localhost:9200/index_name/_search (or against whichever ES port we have exposed). Once you have populated the cluster with test data you can test your queries till you get them right and then try and replicate them using the relevant filter functions.

# Debugging

Just a few quick notes on debugging for now:

- use `tap>` to debug with portal, see code snippet bellow
  - this is especially effective when for some reason Calva's `#break` does not work or you cannot use it (when using `clojure.core.async/go` for example)
- use `lein difftest` instead of just `lein test` to make test failures more readable
- run only specific tests while being able to debug using `clojure.test/test-vars` 

## `tap>`

Check Portal's repository for more details https://github.com/djblue/portal

```clojure
; open portal in the web browser
(def p (p/open))

; open's within VS Code if you have installed the Portal plugin
(def p (p/open {:launcher :vs-code}))

; Add portal as a tap> target
(add-tap #'p/submit)

; Start tapping out values
(tap> :hello)

; Clear all values
(p/clear)

(tap> :world)
; Tap out more values

; bring selected value back into repl
(prn @p)

; Remove portal from tap> targetset
(remove-tap #'p/submit)

; Close the inspector when done
(p/close)

; View docs locally via Portal - jvm / node only
(p/docs)
```

## `clojure.test/test-vars`

An example of running specific tests while connected to the REPL. This way we can debug tests using breakpoints etc.

```clojure
(clojure.test/test-vars [#'cayenne.works.filter-test/filtering-works])
```

