<?xml version="1.0" encoding="UTF-8"?>
<crossref_result xmlns="http://www.crossref.org/qrschema/3.0" version="3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/qrschema/3.0 http://www.crossref.org/schemas/crossref_query_output3.0.xsd">
  <query_result>
    <head>
      <doi_batch_id>none</doi_batch_id>
    </head>
    <body>
      <query status="resolved">
        <doi type="book_content">10.1007/978-981-13-2802-2_4</doi>
        <crm-item name="publisher-name" type="string">Springer Science and Business Media LLC</crm-item>
        <crm-item name="prefix-name" type="string">Springer-Verlag</crm-item>
        <crm-item name="member-id" type="number">297</crm-item>
        <crm-item name="citation-id" type="number">102507401</crm-item>
        <crm-item name="book-id" type="number">3191714</crm-item>
        <crm-item name="deposit-timestamp" type="number">20200514190604684</crm-item>
        <crm-item name="owner-prefix" type="string">10.1007</crm-item>
        <crm-item name="last-update" type="date">2020-05-14T17:42:04Z</crm-item>
        <crm-item name="created" type="date">2018-11-03T10:22:57Z</crm-item>
        <crm-item name="citedby-count" type="number">0</crm-item>
        <doi_record>
          <crossref xmlns="http://www.crossref.org/xschema/1.1" xsi:schemaLocation="http://www.crossref.org/xschema/1.1 http://doi.crossref.org/schemas/unixref1.1.xsd">
            <book book_type="other">
              <book_metadata language="en">
                <contributors>
                  <person_name contributor_role="editor" sequence="first">
                    <given_name>Kristian</given_name>
                    <surname>Bjørkdahl</surname>
                  </person_name>
                  <person_name contributor_role="editor" sequence="additional">
                    <given_name>Benedicte</given_name>
                    <surname>Carlsen</surname>
                  </person_name>
                </contributors>
                <titles>
                  <title>Pandemics, Publics, and Politics</title>
                  <subtitle>Staging Responses to Public Health Crises</subtitle>
                </titles>
                <publication_date media_type="print">
                  <year>2019</year>
                </publication_date>
                <isbn media_type="print">978-981-13-2801-5</isbn>
                <isbn media_type="electronic">978-981-13-2802-2</isbn>
                <publisher>
                  <publisher_name>Springer Singapore</publisher_name>
                  <publisher_place>Singapore</publisher_place>
                </publisher>
                <ai:program xmlns:ai="http://www.crossref.org/AccessIndicators.xsd" name="AccessIndicators">
                  <ai:license_ref applies_to="tdm">http://www.springer.com/tdm</ai:license_ref>
                  <ai:license_ref applies_to="vor">http://www.springer.com/tdm</ai:license_ref>
                </ai:program>
                <doi_data>
                  <doi>10.1007/978-981-13-2802-2</doi>
                  <resource>http://link.springer.com/10.1007/978-981-13-2802-2</resource>
                  <collection property="crawler-based">
                    <item crawler="iParadigms">
                      <resource>http://link.springer.com/content/pdf/10.1007/978-981-13-2802-2</resource>
                    </item>
                  </collection>
                  <collection property="text-mining">
                    <item>
                      <resource mime_type="application/pdf">http://link.springer.com/content/pdf/10.1007/978-981-13-2802-2.pdf</resource>
                    </item>
                  </collection>
                </doi_data>
              </book_metadata>
              <content_item component_type="chapter" level_sequence_number="1" publication_type="full_text">
                <contributors>
                  <person_name contributor_role="author" sequence="first">
                    <given_name>Kristian</given_name>
                    <surname>Bjørkdahl</surname>
                  </person_name>
                  <person_name contributor_role="author" sequence="additional">
                    <given_name>Benedicte</given_name>
                    <surname>Carlsen</surname>
                  </person_name>
                </contributors>
                <titles>
                  <title>Enacting Pandemics: How Health Authorities Use the Press—And Vice Versa</title>
                </titles>
                <component_number>Chapter 4</component_number>
                <publication_date media_type="print">
                  <year>2019</year>
                </publication_date>
                <publication_date media_type="online">
                  <month>11</month>
                  <day>04</day>
                  <year>2018</year>
                </publication_date>
                <pages>
                  <first_page>43</first_page>
                  <last_page>58</last_page>
                </pages>
                <crossmark>
                  <crossmark_version>1</crossmark_version>
                  <crossmark_policy>10.1007/springer_crossmark_policy</crossmark_policy>
                  <crossmark_domains>
                    <crossmark_domain>
                      <domain>link.springer.com</domain>
                    </crossmark_domain>
                  </crossmark_domains>
                  <crossmark_domain_exclusive>false</crossmark_domain_exclusive>
                  <custom_metadata>
                    <assertion group_label="Chapter History" group_name="ChapterHistory" label="First Online" name="first_online" order="1">4 November 2018</assertion>
                    <assertion label="Free to read" name="free">This content has been made available to all.</assertion>
                    <ai:program xmlns:ai="http://www.crossref.org/AccessIndicators.xsd" name="AccessIndicators">
                      <ai:free_to_read start_date="2020-05-14" />
                    </ai:program>
                  </custom_metadata>
                </crossmark>
                <doi_data>
                  <doi>10.1007/978-981-13-2802-2_4</doi>
                  <resource>http://link.springer.com/10.1007/978-981-13-2802-2_4</resource>
                  <collection property="crawler-based">
                    <item crawler="iParadigms">
                      <resource>http://link.springer.com/content/pdf/10.1007/978-981-13-2802-2_4</resource>
                    </item>
                  </collection>
                </doi_data>
                <citation_list>
                  <citation key="4_CR1">
                    <journal_title>Science Communication</journal_title>
                    <author>Kristian Bjørkdahl</author>
                    <volume>39</volume>
                    <issue>3</issue>
                    <first_page>358</first_page>
                    <cYear>2017</cYear>
                    <doi>10.1177/1075547017709792</doi>
                    <unstructured_citation>Bjørkdahl, Kristian, and Benedicte Carlsen. 2017. Fear of the Fear of the Flu: Assumptions About Media Effects in the 2009 Pandemic. Science Communication 39 (3): 358–381.</unstructured_citation>
                  </citation>
                  <citation key="4_CR2">
                    <author>Kristian Bjørkdahl</author>
                    <first_page>261</first_page>
                    <cYear>2018</cYear>
                    <volume_title>Rhetorical Audience Studies and Reception of Rhetoric: Exploring Audiences Empirically</volume_title>
                    <doi>10.1007/978-3-319-61618-6_10</doi>
                    <unstructured_citation>Bjørkdahl, Kristian, and Benedicte Carlsen. 2018. Pandemic Rhetoric and Public Memory: What People (Don’t) Remember from the 2009 Swine Flu. In Rhetorical Audience Studies and Reception of Rhetoric: Exploring Audiences Empirically, ed. Jens Kjeldsen, 261–284. London: Palgrave Macmillan.</unstructured_citation>
                  </citation>
                  <citation key="4_CR3">
                    <journal_title>BMC Health Services Research</journal_title>
                    <author>Benedicte Carlsen</author>
                    <volume>16</volume>
                    <first_page>203</first_page>
                    <cYear>2016</cYear>
                    <doi>10.1186/s12913-016-1466-7</doi>
                    <unstructured_citation>Carlsen, Benedicte, and Claire Glenton. 2016. The Swine Flu Vaccine, Public Attitudes, and Researcher Interpretations: A Systematic Review of Qualitative Research. BMC Health Services Research 16: 203.</unstructured_citation>
                  </citation>
                  <citation key="4_CR4">
                    <unstructured_citation>CDC (Centers for Disease Control and Prevention). 2010. The 2009 H1N1 Pandemic: Summary Highlights, April 2009–April 2010. 
https://www.cdc.gov/h1n1flu/cdcresponse.htm

.</unstructured_citation>
                  </citation>
                  <citation key="4_CR5">
                    <journal_title>Public Interest</journal_title>
                    <author>Anthony Downs</author>
                    <volume>28</volume>
                    <first_page>38</first_page>
                    <cYear>1972</cYear>
                    <unstructured_citation>Downs, Anthony. 1972. Up and Down with Ecology: The Issue-Attention Cycle. Public Interest 28: 38–50.</unstructured_citation>
                  </citation>
                  <citation key="4_CR6">
                    <journal_title>Journal of Peace Research</journal_title>
                    <author>Johan Galtung</author>
                    <volume>2</volume>
                    <issue>1</issue>
                    <first_page>64</first_page>
                    <cYear>1965</cYear>
                    <doi>10.1177/002234336500200104</doi>
                    <unstructured_citation>Galtung, Johan, and Mari Holmboe Ruge. 1965. The Structure of Foreign News. The Presentation of the Congo, Cuba, and Cyprus Crises in Four Norwegian Newspapers. Journal of Peace Research 2 (1): 64–91.</unstructured_citation>
                  </citation>
                  <citation key="4_CR7">
                    <author>Erving Goffman</author>
                    <cYear>1959</cYear>
                    <volume_title>The Presentation of Self in Everyday Life</volume_title>
                    <unstructured_citation>Goffman, Erving. 1959. The Presentation of Self in Everyday Life. New York: Anchor Books.</unstructured_citation>
                  </citation>
                  <citation key="4_CR8">
                    <author>Stuart Hall</author>
                    <first_page>226</first_page>
                    <cYear>1973</cYear>
                    <volume_title>The Manufacture of News: Deviance, Social Problems and the Mass Media</volume_title>
                    <unstructured_citation>Hall, Stuart. 1973. The Determinations of News Photographs. In The Manufacture of News: Deviance, Social Problems and the Mass Media, ed. Stanley Cohen and Jock Young, 226–243. London: Constable.</unstructured_citation>
                  </citation>
                  <citation key="4_CR9">
                    <author>Stephen Hilgartner</author>
                    <cYear>2000</cYear>
                    <volume_title>Science on Stage: Expert Advice as Public Drama</volume_title>
                    <unstructured_citation>Hilgartner, Stephen. 2000. Science on Stage: Expert Advice as Public Drama. Stanford: Stanford University Press.</unstructured_citation>
                  </citation>
                  <citation key="4_CR10">
                    <journal_title>The Public Opinion Quarterly</journal_title>
                    <author>Maxwell McCombs</author>
                    <volume>36</volume>
                    <issue>2</issue>
                    <first_page>176</first_page>
                    <cYear>1972</cYear>
                    <doi>10.1086/267990</doi>
                    <unstructured_citation>McCombs, Maxwell, and Donald Shaw. 1972. The Agenda-Setting Function of Mass Media. The Public Opinion Quarterly 36 (2): 176–187.</unstructured_citation>
                  </citation>
                  <citation key="4_CR11">
                    <author>Deirdre O’Neill</author>
                    <first_page>161</first_page>
                    <cYear>2009</cYear>
                    <volume_title>The Handbook of Journalism Studies</volume_title>
                    <unstructured_citation>O’Neill, Deirdre, and Tony Harcup. 2009. News Values and Selectivity. In The Handbook of Journalism Studies, ed. Karin Wahl-Jorgensen and Thomas Hanitzsch, 161–174. New York: Routledge.</unstructured_citation>
                  </citation>
                  <citation key="4_CR12">
                    <author>Jonathan Quick</author>
                    <cYear>2018</cYear>
                    <volume_title>The End of Epidemics: The Looming Threat to Humanity and How to Stop It</volume_title>
                    <unstructured_citation>Quick, Jonathan. 2018. The End of Epidemics: The Looming Threat to Humanity and How to Stop It. New York: St. Martin’s Press.</unstructured_citation>
                  </citation>
                  <citation key="4_CR13">
                    <journal_title>European Journal of Communication</journal_title>
                    <author>Peter Vasterman</author>
                    <volume>20</volume>
                    <issue>4</issue>
                    <first_page>508</first_page>
                    <cYear>2005</cYear>
                    <doi>10.1177/0267323105058254</doi>
                    <unstructured_citation>Vasterman, Peter. 2005. Media-Hype: Self-Reinforcing News Waves, Journalistic Standards and the Construction of Social Problems. European Journal of Communication 20 (4): 508–530.</unstructured_citation>
                  </citation>
                  <citation key="4_CR14">
                    <author>Peter Vasterman</author>
                    <first_page>17</first_page>
                    <cYear>2018</cYear>
                    <volume_title>From Media Hype to Twitter Storm: News Explosions and Their Impact on Issues, Crises, and Public Opinion</volume_title>
                    <doi>10.2307/j.ctt21215m0.5</doi>
                    <unstructured_citation>Vasterman, Peter. 2018. Introduction. In From Media Hype to Twitter Storm: News Explosions and Their Impact on Issues, Crises, and Public Opinion, ed. Peter Vasterman, 17–38. Amsterdam: Amsterdam University Press.</unstructured_citation>
                  </citation>
                  <citation key="4_CR15">
                    <journal_title>European Journal of Communication</journal_title>
                    <author>Charlotte Wien</author>
                    <volume>24</volume>
                    <issue>2</issue>
                    <first_page>183</first_page>
                    <cYear>2009</cYear>
                    <doi>10.1177/0267323108101831</doi>
                    <unstructured_citation>Wien, Charlotte, and Christian Elmelund-Præstekær. 2009. An Anatomy of Media Hypes: Developing a Model for the Dynamics and Structure of Intense Media Coverage of Single Issues. European Journal of Communication 24 (2): 183–201.</unstructured_citation>
                  </citation>
                </citation_list>
              </content_item>
            </book>
          </crossref>
        </doi_record>
      </query>
    </body>
  </query_result>
</crossref_result>
