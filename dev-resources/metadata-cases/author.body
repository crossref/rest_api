<?xml version="1.0" encoding="UTF-8"?>
<crossref_result xmlns="http://www.crossref.org/qrschema/3.0" version="3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.crossref.org/qrschema/3.0 http://www.crossref.org/schemas/crossref_query_output3.0.xsd">
  <query_result>
    <head>
      <doi_batch_id>none</doi_batch_id>
    </head>
    <body>
      <query status="resolved">
        <doi type="journal_article">10.1136/bmjopen-2019-030788</doi>
        <crm-item name="publisher-name" type="string">BMJ</crm-item>
        <crm-item name="prefix-name" type="string">BMJ</crm-item>
        <crm-item name="member-id" type="number">239</crm-item>
        <crm-item name="citation-id" type="number">110461323</crm-item>
        <crm-item name="journal-id" type="number">126725</crm-item>
        <crm-item name="deposit-timestamp" type="number">2019100804251171000</crm-item>
        <crm-item name="owner-prefix" type="string">10.1136</crm-item>
        <crm-item name="last-update" type="date">2019-10-08T11:25:49Z</crm-item>
        <crm-item name="created" type="date">2019-10-08T11:25:43Z</crm-item>
        <crm-item name="citedby-count" type="number">0</crm-item>
        <doi_record>
          <crossref xmlns="http://www.crossref.org/xschema/1.1" xsi:schemaLocation="http://www.crossref.org/xschema/1.1 http://doi.crossref.org/schemas/unixref1.1.xsd">
            <journal>
              <journal_metadata language="en">
                <full_title>BMJ Open</full_title>
                <abbrev_title>BMJ Open</abbrev_title>
                <issn media_type="print">2044-6055</issn>
                <issn media_type="electronic">2044-6055</issn>
              </journal_metadata>
              <journal_issue>
                <publication_date media_type="online">
                  <month>10</month>
                  <day>07</day>
                  <year>2019</year>
                </publication_date>
                <publication_date media_type="print">
                  <month>10</month>
                  <year>2019</year>
                </publication_date>
                <journal_volume>
                  <volume>9</volume>
                </journal_volume>
                <issue>10</issue>
              </journal_issue>
              <journal_article publication_type="full_text">
                <titles>
                  <title>Web-based support for self-management strategies versus usual care for people with COPD in primary healthcare: a protocol for a randomised, 12-month, parallel-group pragmatic trial</title>
                </titles>
                <contributors>
                  <person_name contributor_role="author" sequence="first">
                    <given_name>Tobias</given_name>
                    <surname>Stenlund</surname>
                    <ORCID>http://orcid.org/0000-0003-0569-9490</ORCID>
                  </person_name>
                  <person_name contributor_role="author" sequence="additional">
                    <given_name>André</given_name>
                    <surname>Nyberg</surname>
                  </person_name>
                  <person_name contributor_role="author" sequence="additional">
                    <given_name>Sara</given_name>
                    <surname>Lundell</surname>
                  </person_name>
                  <person_name contributor_role="author" sequence="additional">
                    <given_name>Karin</given_name>
                    <surname>Wadell</surname>
                  </person_name>
                </contributors>
                <jats:abstract xmlns:jats="http://www.ncbi.nlm.nih.gov/JATS1">
                  <jats:sec>
                    <jats:title>Introduction</jats:title>
                    <jats:p>The use of adequate self-management strategies for people with chronic obstructive pulmonary disease (COPD) may increase the level of physical activity (PA), improve health-related quality of life (HRQoL) and reduce healthcare use. Whether web-based support in addition to prompts (email and SMS) could be used to promote self-management strategies to facilitate behaviour change in people with COPD is not clear. This clinical trial aims to generate evidence on the effect of a web-based solution, the COPD Web, in a cohort of people with COPD in a primary healthcare context.</jats:p>
                  </jats:sec>
                  <jats:sec>
                    <jats:title>Methods and analysis</jats:title>
                    <jats:p>The overall design is a pragmatic randomised controlled trial with preassessments and postassessments (3 and 12 months) and an implementation and user experience evaluation. People with a diagnosis of COPD, treated in primary healthcare will be eligible for the study. A total of 144 participants will be enrolled by healthcare professionals at included primary healthcare units and, after fulfilled baseline assessments, randomised to either control or intervention group. All participants will receive usual care, a pedometer and a leaflet about the importance of PA. Participants in the intervention will, in addition, get access to the COPD Web, an interactive self-managed website that aims to support people with COPD in self-management strategies. They will also continuously get support from prompts with a focus on behaviour change.</jats:p>
                    <jats:p>The effect on participants’ PA, dyspnoea, COPD-related symptoms, HRQoL and health economics will be assessed using accelerometer and questionnaires. To identify enablers and barriers for the use of web-based support to change behaviour, semistructured interviews will be conducted in a subgroup of participants at the 3 months follow-up.</jats:p>
                  </jats:sec>
                  <jats:sec>
                    <jats:title>Ethics and dissemination</jats:title>
                    <jats:p>Ethical approval has been received from the Regional Ethical Review Board in Umeå, Sweden. Dnr 2018-274-31. Findings will be presented at conferences, submitted for publication in peer-reviewed journals and presented to the involved healthcare professionals, participants and patient organisations.</jats:p>
                  </jats:sec>
                  <jats:sec>
                    <jats:title>Trial registration number</jats:title>
                    <jats:p>
                      <jats:ext-link xmlns:xlink="http://www.w3.org/1999/xlink" ext-link-type="clintrialgov" xlink:href="NCT03746873">NCT03746873</jats:ext-link>
                    </jats:p>
                  </jats:sec>
                </jats:abstract>
                <publication_date media_type="online">
                  <month>10</month>
                  <day>07</day>
                  <year>2019</year>
                </publication_date>
                <publication_date media_type="print">
                  <month>10</month>
                  <year>2019</year>
                </publication_date>
                <pages>
                  <first_page>e030788</first_page>
                </pages>
                <publisher_item>
                  <item_number item_number_type="atom">/bmjopen/9/10/e030788.atom</item_number>
                  <identifier id_type="doi">10.1136/bmjopen-2019-030788</identifier>
                </publisher_item>
                <crossmark>
                  <crossmark_version>1.0</crossmark_version>
                  <crossmark_policy>10.1136/crossmarkpolicy</crossmark_policy>
                  <crossmark_domains>
                    <crossmark_domain>
                      <domain>bmj.com</domain>
                    </crossmark_domain>
                  </crossmark_domains>
                  <crossmark_domain_exclusive>true</crossmark_domain_exclusive>
                  <custom_metadata>
                    <fr:program xmlns:fr="http://www.crossref.org/fundref.xsd" name="fundref">
                      <fr:assertion name="fundgroup">
                        <fr:assertion name="funder_name">Strategic Research Area - Care Science</fr:assertion>
                        <fr:assertion name="award_number">No grant number available</fr:assertion>
                      </fr:assertion>
                      <fr:assertion name="fundgroup">
                        <fr:assertion name="funder_name">
                          <fr:assertion name="funder_identifier">http://dx.doi.org/10.13039/501100004359</fr:assertion>
                          Vetenskapsrådet
                        </fr:assertion>
                        <fr:assertion name="award_number">521-2013-3503</fr:assertion>
                      </fr:assertion>
                    </fr:program>
                    <ct:program xmlns:ct="http://www.crossref.org/clinicaltrials.xsd">
                      <ct:clinical-trial-number registry="10.18810/clinical-trials-gov">NCT03746873</ct:clinical-trial-number>
                      <ct:clinical-trial-number registry="10.18810/clinical-trials-gov">NCT03746873</ct:clinical-trial-number>
                    </ct:program>
                  </custom_metadata>
                </crossmark>
                <doi_data>
                  <doi>10.1136/bmjopen-2019-030788</doi>
                  <timestamp>2019100804251171000</timestamp>
                  <resource>http://bmjopen.bmj.com/lookup/doi/10.1136/bmjopen-2019-030788</resource>
                  <collection property="crawler-based">
                    <item crawler="iParadigms">
                      <resource>https://syndication.highwire.org/content/doi/10.1136/bmjopen-2019-030788</resource>
                    </item>
                  </collection>
                </doi_data>
              </journal_article>
            </journal>
          </crossref>
        </doi_record>
      </query>
    </body>
  </query_result>
</crossref_result>