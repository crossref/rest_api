# cayenne

Cayenne serves the Crossref REST API. Implements useful metadata transforms, ID handling, a resource API, OAI metadata
download and ingest / indexing.

## Quick Start

Cayenne runs in Docker, both in development and production. In development the environment is mounted inside the container so any file changes are immediately reflected in the container.

Make sure you have Docker and Docker Compose installed. Check by running:

    $ docker-compose --version

### Preparing CSL Resources

Before running Cayenne, building an uberjar or building a production docker docker image, CSL resources
must be pulled into the local repository via git submodules. Manifest files must then be
created for CSL style files and locale files.

Update git submodules to bring in CSL style and locale files:

    $ git submodule update --init

Refresh `resources/styles.edn` and `resources/locales.edn`:

    $ docker-compose  -f docker-compose.yml run api lein csl

## Running in a REPL

To start a repl:

    $ docker-compose -f docker-compose.yml run --service-ports api lein repl
    
Then call `(begin)`:

    > (begin)
	
To start a test version of the API type:

    > (user/start)

Then, to load the corpus of documents complete with coverage checks (the same function that's used in the integration tests):

    >  (user/index-feed)

Then visit e.g. <http://localhost:3000/v1/works>

## Testing

Tests fall into a few categories.

 - Unit tests run in isolation, and work purely by running Cayenne source code. They usually centre around single functions.
 - Component tests exercise a given chunk of service, for example, a particular API endpoint, but make no dependency on Elasticsearch.
 - Integration tests involve a dependency to test how Cayenne works with it. Currently this is Elasticsearch.

### Running tests

All tests are run using Docker Compose. In the case of integration tests, the Elastic Search instance is provided as part of the Docker Compose setup, and the test fixtures are responsible for clearing all data between tests. In theory Docker isn't required for unit and component tests, but it's better that the tests run on the target platform. 
 
To run each category:
 
     docker-compose  -f docker-compose.yml  run api lein test :unit
     docker-compose  -f docker-compose.yml  run api lein test :component
     docker-compose  -f docker-compose.yml  run api lein test :integration
     docker-compose  -f docker-compose.yml  run api lein test :manual

Or in a repl:

    (require :reload ['the-ns])
    (clojure.test/test-vars [#'the-ns/the-test])

## Run in Foreground

Run as a production service with some startup tasks:

    $ lein with-profiles prod run :create-mappings :api :update-members :update-journals

More information about available startup tasks can be found here: https://crossref.gitlab.io/knowledge_base/docs/services/rest-api-cayenne/operations/

## Run as a Daemon

Run as a daemonized production service with lein-daemon:

    $ lein with-profiles prod daemon start cayenne :create-mappings :api :update-members :update-journals

Accepts the same arguments as lein run. Also available are:

    $ lein with-profiles prod daemon stop cayenne
    $ lein with-profiles prod daemon check cayenne

When running as a daemon it is sometimes useful to start an nrepl server
to later connect a repl:

    $ lein with-profiles prod daemon start cayenne :api :nrepl

## Run within a Docker Container

Create a docker image:

    $ lein uberimage

## Running with integration project

The integration project emulates a local AWS system connecting Cayenne (indexer+api), ElasticSearch and localstack services (S3,SNS and SQS) as well as other services like manifold.

That allows to copy a UNIXSD into an s3 bucket, then getting processed by the indexer and sent to ElasticSearch and being available via the API.

In order to start the system you need to start the [`integation.sh` script](https://gitlab.com/crossref/integration/-/blob/main/integration.sh) in the integration project (You will need docker and [terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli) installed in your computer):

`$ integration.sh dev-rest-api`

When terraform has finished the following ports will be exposed
* AWS LOCALSTACK EDGE endpoint (http://localhost:4566)
* ElasticSearch endpoint (http://localhost:9200)

Then, in the rest-api project you'll find [`run_integration.sh`](https://gitlab.com/crossref/rest_api/-/blob/main/run_integration.sh). This script will start your local project and will connect it to the integration project you have just started.

```
run_integration.sh test  # To run it in test mode so you can attach to the repl and run/debug the tests manually 
run_integration.sh run # run api, indexer and scheduled tasks, nrepl on so you can attach
run_integration.sh stop  # stop the api container
```

That will expose the following ports:
* NREPL endpoint (http://localhost:7880)
* API endpoint (http://localhost:3000)

After having started all the services you need to tweak your `aws` command, so you can create a local version by adding the following to your `~/.barshrc` or equivalent:

`alias awslocal="aws --region us-east-1 --endpoint-url=http://${LOCALSTACK_HOST:-127.0.0.1}:4566"`

*IT IS IMPORTANT STICKING TO `us-east-1`, otherwise amazonica won't work*

Then you can start copying files to s3

`awslocal s3 cp crossref-unixsd-example.body s3://md-bucket/0021c58766b517f20e1ca10d4c8a1574135b4d1f/unixsd.xml --metadata '{"x-amz-meta-cr-doi": "asdfasdf"}'`

Shortly works should be available via the API http://localhost:3000/works

This is very useful as a development environment if you can attach your IDE to the NREPL endpoint (http://localhost:7880) as you can debug and inspect the live system at any point.

## Reference Visibility

References are displayed in API output. The visiblity level of those references
can be set as `open` (only open references visible), `limited` (open and limited
references visible) or `closed` (all references visible). This setting can be configured directly by
the internal config variable `[:service :api :references]` or via the ENV VAR
`REFERENCES`.

# As an External Library

Cayenne is compiled as an external library for use by the Manifold codebase. In the 'uberjar' profile, only a small selection of functionality is used here, and use of external dependencies is reduced.

To build and install locally for testing:

```
$ lein uberjar
$ mvn install:install-file -Dfile=./target/cayenne-SNAPSHOT.jar -DgroupId=org.crossref -DartifactId=cayenne -Dversion=SNAPSHOT -Dpackaging=jar
```
Note: Though the full uberjar is compiled (`target/cayenne-XXXX-standalone.jar`), the non-uberjar (`target/cayenne-XXXX.jar`) is what you want to use as a dependency.   

## Item Tree Viewer

Cayenne contains a tool for visualizing Item Trees built from UNIXSD XMLs. The easiest way to run the tool is within a Docker container:

```
docker build -t itviewer -f Dockerfile-itviewer .
docker run -v $(pwd):/usr/src/app -e FILE=<PATH TO UNIXSD> -e REMOVE_NULLS=1 -e TRIM_STRINGS=50 -e PRUNE_RELS=1 itviewer
```

This will transform the input UNIXSD XML into an Item Tree, generate its representation in DOT language and write it into `<PATH TO UNIXSD>.dot` file, and finally use Graphviz to generate an `<PATH TO UNIXSD>.jpg` file containing the graph.

In this graph nodes represent items, item properties are displayed in the nodes, and edges represent relationships.

There are three optional flags that modify the Item Tree graph before generating the DOT graph. Each flag makes the graph (and the resulting .jpg file) slightly smaller, while still visualizing the Item Tree model accurately:

- REMOVE\_NULLS=1 - do not display properties with a null value
- TRIM\_STRINGS=50 - display only up to first 50 characters of property string values
- PRUNE\_RELS=1 - when a subject item has a relationship R with multiple object items, only display one of them
